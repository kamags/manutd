<?php
/**
 * !IMPORTANT
 *
 * Helper class for type hinting. Place it wherever you want.
 * At IDE settings disable other original Yii class definitions:
 *  - yii/framework/yii.php
 */

/**
 * Yii bootstrap file.
 *
 * @author    Qiang Xue <qiang.xue@gmail.com>
 * @link      http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 * @package   system
 * @since     1.0
 */

require(dirname(__FILE__) . '/YiiBase.php');

/**
 * Yii is a helper class serving common framework functionalities.
 *
 * It encapsulates {@link YiiBase} which provides the actual implementation.
 * By writing your own Yii class, you can customize some functionalities of YiiBase.
 *
 * @author  Qiang Xue <qiang.xue@gmail.com>
 * @package system
 * @since   1.0
 *
 * @method static IDE_WebApplication app()
 */
class Yii extends YiiBase {

}

/**
 * @property WebUser              $user
 * @property YiiMail              $mail
 * @property PokeristApiComponent $pokeristApi
 * @property XsollaComponent      $xsolla
 *
 * @author lukin.a
 *
 * Class IDE_WebApplication
 */
class IDE_WebApplication extends CWebApplication {

}
