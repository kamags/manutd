
$(function() {
	var $body = $('body');


	$('.js-datepicker').datepicker({
		dateFormat: "yy-mm-dd 00:00:00"
	});

	$body.on('click', '.js-ajax-delete', function() {
		var $this = $(this);

		$.get($this.attr('href'), function(data) {
			if (parseInt(data)) {
				$this.closest('tr').fadeOut();
			} else {
				alert('Error!');
			}
		});
		return false;
	});
});