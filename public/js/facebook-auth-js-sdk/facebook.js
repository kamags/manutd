// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
	// The response object is returned with a status field that lets the
	// app know the current login status of the person.
	// Full docs on the response object can be found in the documentation
	// for FB.getLoginStatus().
	if (response.status === FB.STATUS_CONNECTED) {
		// Logged into your app and Facebook.
		FB.redirectToFbAuthUrl(response);
	} else if (response.status === FB.STATUS_NOT_AUTHORIZED) {
		// The person is logged into Facebook, but not your app.
		FB.login(function (response) {
			if (response.status == FB.STATUS_CONNECTED) {
				FB.redirectToFbAuthUrl(response);
			}
		}, {scope: 'email'});
	} else {
		// The person is not logged into Facebook, so we're not sure if
		// they are logged into this app or not.
		FB.login(function (response) {
			if (response.status == FB.STATUS_CONNECTED) {
				FB.redirectToFbAuthUrl(response);
			}
		}, {scope: 'email'});
	}
}

window.fbAsyncInit = function () {
	var $loginBtn = $('.js-facebook-auth');

	FB.init({
		appId: $loginBtn.data('appId'),
		cookie: true,  // enable cookies to allow the server to access
					   // the session
		xfbml: true,  // parse social plugins on this page
		version: 'v2.2' // use version 2.2
	});

	FB.signFbUrl = $loginBtn.data('signFbUrl');

	/**
	 * Redirect to auth action with GET params from response
	 *
	 * @param {Object} response
	 */
	FB.redirectToFbAuthUrl = function (response) {
		location.href = FB.signFbUrl + '?uid=' + response.authResponse.userID + '&token=' + response.authResponse.accessToken;
	};

	// Status constants
	FB.STATUS_CONNECTED = 'connected';
	FB.STATUS_NOT_AUTHORIZED = 'not_authorized';

	// Now that we've initialized the JavaScript SDK, we call
	// FB.getLoginStatus().  This function gets the state of the
	// person visiting this page and can return one of three states to
	// the callback you provide.  They can be:
	//
	// 1. Logged into your app ('connected')
	// 2. Logged into Facebook, but not your app ('not_authorized')
	// 3. Not logged into Facebook and can't tell if they are logged into
	//    your app or not.
	//
	// These three cases are handled in the callback function.

	$loginBtn.on('click', function () {
		FB.getLoginStatus(function (response) {
			statusChangeCallback(response);
		});
		return false;
	});
};

// Load the SDK asynchronously
(function (d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));