/**
 * Created by Work-nekki on 11/02/15.
 */
$(document).ready(function () {



	var $body = $('body');

	/**
	 * jquery notifier for any action
	 * @param {string} text
	 * @param {string} [mode]
	 * @param {object} [config]
	 * @returns {jQuery}
	 */
	$.notify = function (text, mode, config) {
		mode = mode || '';
		config = config || {};

		var settings = {
			/** container markup for notifier */
			container: null,
			/** time in msec when notifier is shown, false if no autohide */
			timeout: 4000,
			/** additional class for alert */
			addClass: ''
		};
		$.extend(settings, config);
		if (settings.container) {
			if (typeof settings.container == 'string') {
				settings.container = $(settings.container);
			}
		} else {
			settings.container = $('<div class="notifies"></div>').appendTo($body);
		}

		var $message = $('<div class="alert"></div>');

		if (mode) {
			$message.addClass('alert-' + mode);
		}

		if (settings.addClass) {
			$message.addClass(settings.addClass);
		}

		$message.html(text);
		settings.container.append($message);

		if (settings.timeout) {
			setTimeout(function () {
				$message.fadeOut(function () {
					$message.remove();
				});
			}, settings.timeout);
		} else {
			$message.append('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
		}
	};

	var isToolbarGameShow = false;


	//Обработка клика на стрелку вправо
	$(document).on('click', ".carousel-button-right", function () {
		var carusel = $(this).parents('.carousel');
		right_carusel(carusel);
		return false;
	});
	//Обработка клика на стрелку влево
	$(document).on('click', ".carousel-button-left", function () {
		var carousel = $(this).parents('.carousel');
		left_carousel(carousel);
		return false;
	});
	function left_carousel(carusel) {
		var block_width = $(carusel).find('.carousel-block').outerWidth();
		$(carusel).find(".carousel-items .carousel-block").eq(-1).clone().prependTo($(carusel).find(".carousel-items"));
		$(carusel).find(".carousel-items").css({"left": "-" + block_width + "px"});
		$(carusel).find(".carousel-items .carousel-block").eq(-1).remove();
		$(carusel).find(".carousel-items").animate({left: "0px"}, 200);

	}

	function right_carusel(carusel) {
		var block_width = $(carusel).find('.carousel-block').outerWidth();
		$(carusel).find(".carousel-items").animate({left: "-" + block_width + "px"}, 200, function () {
			$(carusel).find(".carousel-items .carousel-block").eq(0).clone().appendTo($(carusel).find(".carousel-items"));
			$(carusel).find(".carousel-items .carousel-block").eq(0).remove();
			$(carusel).find(".carousel-items").css({"left": "0px"});
		});
	}


	$(".poker").fancybox({
		maxWidth: 660,
		maxHeight: 560,
		fitToView: false,
		width: 660,
		height: 560,
		autoSize: false,
		closeClick: false,
		openEffect: 'none',
		closeEffect: 'none',
		padding: 0,
		topRatio: 0,
		scrolling: "no"
	});

	$(".fancy-casino").fancybox({
		//maxWidth: 660,
		//maxHeight: 580,
		fitToView: false,
		width: 900,
		height: 600,
		autoSize: false,
		closeClick: false,
		openEffect: 'none',
		closeEffect: 'none',
		padding: 0,
		topRatio: .3,
		scrolling: "no"
//		wrapCSS: "casino-popup"
	});

	$(".roulette").fancybox({
		maxWidth: 660,
		maxHeight: 560,
		fitToView: false,
		width: 660,
		height: 560,
		autoSize: false,
		closeClick: false,
		openEffect: 'none',
		closeEffect: 'none',
		padding: 0,
		leftRatio: .4,
		scrolling: "no"

	});

	$(".support").fancybox({
		maxWidth: 720,
		maxHeight: 630,
		fitToView: false,
		width: 660,
		height: 630,
		autoSize: false,
		closeClick: false,
		openEffect: 'none',
		closeEffect: 'none',
		padding: 0,
//		wrapCSS: "support",
		scrolling: "no"
	});


	$(".lang").fancybox({
		maxWidth: 660,
		maxHeight: 660,
		fitToView: false,
		width: 660,
		height: 240,
		autoSize: false,
		closeClick: false,
		openEffect: 'none',
		closeEffect: 'none',
		padding: 0,
		topRatio: 0,
		scrolling: "no",
		afterShow: function () {
			var lang_offset = $('.toolbar').height() - 20,
				$toolbarGames = $('.toolbar-games');

			if (!$toolbarGames.is(':hidden')) {
				lang_offset += $toolbarGames.height();
			}
			$(".fancybox-wrap").css({"margin-top": lang_offset + "px"});
		}
	});


	getPlayersOnline();
	setInterval(getPlayersOnline, 10000);


	function getPlayersOnline() {
		$.ajax('/getPlayersOnline', {
			type: 'POST',
			async: true,
			success: function (data) {
				$('#players-online').html(data);
			}
		});
	}

	$(".subscribe__btn").on('click', function () {

		var data = $(".subscribe").serialize();

		var email = $("#Subscribers_email");
		var success_block = $(".subscribe__success");
		var error_block = $(".subscribe__error");

		if (email.val() === '') {
			error_block.html('Please enter your email');
			showMessage(error_block);
			return;
		}
		else {
			var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			if (!pattern.test(email.val())) {
				error_block.html('Please enter correct email');
				showMessage(error_block);
				return;
			}
		}

		$.ajax({
			type: 'POST',
			url: '/subscribe',
			data: data,
			success: function () {
				email.val('');
				showMessage(success_block);
			},
			dataType: 'html'
		});

	});

	function showMessage(block) {
		block.css('display', 'block');
		block.fadeOut(2000);
	}

	$('#menu-games').on('click', function (event) {
		var $games = $(".toolbar-games");

		if (isToolbarGameShow) {
			$games.fadeOut(200);
		}
		else {
			$games.fadeIn(200);
		}
		isToolbarGameShow = !isToolbarGameShow;
		event.preventDefault();
	});

	$('.lang-btn').on('click', function () {
		$(this).next().toggleClass("active");
	});

	var $switchPoker = $("#switch-poker").on('click', function (event) {
		event.preventDefault();

		$switchPoker.removeClass('form-switcher_off').addClass('form-switcher_on');
		
		$switchSlots.removeClass('form-switcher_on').addClass('form-switcher_off');
		$switchRoulette.removeClass('form-switcher_on').addClass('form-switcher_off');
		
		$("#Support_app").val('poker');
	});

	var $switchRoulette = $("#switch-roulette").on('click', function (event) {
		event.preventDefault();

		$switchRoulette.removeClass('form-switcher_off').addClass('form-switcher_on');
		
		$switchSlots.removeClass('form-switcher_on').addClass('form-switcher_off');
		$switchPoker.removeClass('form-switcher_on').addClass('form-switcher_off');
		
		$("#Support_app").val('roulette');
	});
	
	var $switchSlots = $("#switch-slots").on('click', function (event) {
		event.preventDefault();
		
		$switchSlots.removeClass('form-switcher_off').addClass('form-switcher_on');

		$switchRoulette.removeClass('form-switcher_on').addClass('form-switcher_off');
		$switchPoker.removeClass('form-switcher_on').addClass('form-switcher_off');
		
		$("#Support_app").val('slots');
		
	});

	$("a").on('click', function () {
		var href = $(this).attr("href");
		if (href == "") {
			return false;
		}
	});

	// -- jQuery pre loader
	$body.jpreLoader({
		loaderVPos: "600px",
		onetimeLoad: true,
		showPercentage: false
	});
	$("#jpreLoader").append("<div id='jpreLabel'>Loading...</div>");
	// -- -- -- --

	// -- Alert close
	$body.on('click', '.alert .close', function() {
		$(this).closest('.alert').remove();
		return false;
	});
	// -- -- -- --

	$('#tabs').tabs({
		activate: function( event, ui ) {
			var panel = ui.newPanel[0];
			$(panel).addClass("ui-tabs-panel_active");
		}
	});

	function Status(options) {
		var elem = options.elem;

		var statusText = elem.find(".status__text");
		var statusField = elem.find(".status__field");
		var statusBlock = statusField.parents(".status__block");

		statusText.on("focus", function() {
			statusBlock.addClass("status__block_edit");
			statusField.focus();
			$(this).hide();
		});

		statusField.on("blur", function() {
			statusBlock.removeClass("status__block_edit");
			this.blur();
			statusText.show();
		});
	}


	var status = new Status({
		elem: $(".status")
	});


	function ProgressBar(options) {

		var elem = options.elem;
		var value = options.value;
		var rate = elem.find(".progress-bar__total");

		var progressLine = elem.find(".progress-bar__inner");
		var pixelsPerValue = progressLine.innerWidth() / 100;

		function valueToPosition(value) {
			return pixelsPerValue * value;
		}

		progressLine.css("width", valueToPosition(value));

		rate.html(value + "%");

	}

	var progressBar = new ProgressBar({
		elem: $(".progress-bar"),
		value: 73
	});


	$('#currency').highcharts({
		chart: {
//			zoomType: 'xy'
			marginTop: 120
		},
		title: {
			text: 'Chips and gold coins',
			align: "left",
			style: {
				color: "rgba(255,255,255, 1)",
				fontFamily: 'PT Sans Narrow',
				textTransform: "none",
				fontSize: "26px"
			}
		},
		subtitle: {
			text: 'Chart displays players’s successes and failures at the table',
			align: "left",
			style: {
				color: "rgba(255,255,255, 1)",
				fontFamily: 'PT Sans Narrow',
				textTransform: "none",
				fontSize: "15px"
			}
		},
		yAxis: [{ // Primary yAxis
			gridLineWidth: 1,
			gridLineDashStyle: "Dot",
			minorGridLineWidth: 0,
			tickWidth: 0,
			labels: {
				format: '{value} M',
				style: {
					color: "rgba(255,255,255, .5)",
					fontFamily: 'PT Sans Narrow',
					textTransform: "none"
				}
			},
			title: {
				text: ''
			},
			min: 0
		}, { // Secondary yAxis
			gridLineWidth: 1,
			gridLineDashStyle: "Dot",
			minorGridLineWidth: 0,
			tickWidth: 0,
			title: {
				text: ''
			},
			labels: {
				format: '{value} M',
				style: {
					color: "rgba(255,255,255, .5)",
					fontFamily: 'PT Sans Narrow',
					textTransform: "none"
				}
			},
			opposite: true,
			min: 0
		}],
		xAxis: {
			gridLineWidth: 1,
			gridLineDashStyle: "Dot",
			minorGridLineWidth: 0,
			tickWidth: 0,
			labels: {
				enabled: false
			}
		},
		tooltip: {
			shared: true
		},
		legend: {
			backgroundColor: "rgba(0,0,0,0)",
			layout: 'horizontal',
			align: 'left',
			x: 0,
			verticalAlign: 'top',
			y: 65,
			floating: true,
			itemStyle: {
				color: "rgba(255,255,255, .5)",
				fontFamily: 'PT Sans Narrow',
				textTransform: "none",
				fontWeight: "400"
			}
		},
		series: [{
			name: 'Chips',
			type: 'spline',
			yAxis: 1,
			data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6,  95.6, 54.4, 190 ],
			tooltip: {
				valueSuffix: 'M'
			}

		}, {
			name: 'Gold coins',
			type: 'spline',
			data: [19, 55, 56.4, 129.2, 14.0, 165.0, 116.6, 187.5, 22.4, 14.1, 190],
			tooltip: {
				valueSuffix: 'M'
			}
		}],
		credits: {
			enabled: false
		}
	});

	$('#games').highcharts({
		chart: {
//			zoomType: 'xy'
			marginTop: 120
		},
		title: {
			text: 'Games and wins',
			align: "left",
			style: {
				color: "rgba(255,255,255, 1)",
				fontFamily: 'PT Sans Narrow',
				textTransform: "none",
				fontSize: "26px"
			}
		},
		subtitle: {
			text: 'Chart displays win/lose rate in games played',
			align: "left",
			style: {
				color: "rgba(255,255,255, 1)",
				fontFamily: 'PT Sans Narrow',
				textTransform: "none",
				fontSize: "15px"
			}
		},
		yAxis: [{ // Primary yAxis
			gridLineWidth: 1,
			gridLineDashStyle: "Dot",
			minorGridLineWidth: 0,
			tickWidth: 0,
			labels: {
				format: '{value} M',
				style: {
					color: "rgba(255,255,255, .5)",
					fontFamily: 'PT Sans Narrow',
					textTransform: "none"
				}
			},
			title: {
				text: ''
			},
			min: 0
		}, { // Secondary yAxis
			gridLineWidth: 1,
			gridLineDashStyle: "Dot",
			minorGridLineWidth: 0,
			tickWidth: 0,
			title: {
				text: ''
			},
			labels: {
				format: '{value} M',
				style: {
					color: "rgba(255,255,255, .5)",
					fontFamily: 'PT Sans Narrow',
					textTransform: "none"
				}
			},
			opposite: true,
			min: 0
		}],
		xAxis: {
			gridLineWidth: 1,
			gridLineDashStyle: "Dot",
			minorGridLineWidth: 0,
			tickWidth: 0,
			labels: {
				enabled: false
			}
		},
		tooltip: {
			shared: true
		},
		legend: {
			backgroundColor: "rgba(0,0,0,0)",
			layout: 'horizontal',
			align: 'left',
			x: 0,
			verticalAlign: 'top',
			y: 65,
			floating: true,
			itemStyle: {
				color: "rgba(255,255,255, .5)",
				fontFamily: 'PT Sans Narrow',
				textTransform: "none",
				fontWeight: "400"
			}
		},
		series: [{
			name: 'Games',
			type: 'spline',
			yAxis: 1,
			data: [23.9, 176.5, 17.4, 85.2, 44.0, 76.0, 5.6,  95.6, 5.4, 190 ],
			tooltip: {
				valueSuffix: 'M'
			}

		}, {
			name: 'Wins',
			type: 'spline',
			data: [19, 5, 56.4, 19.2, 144.0, 165.0, 16.6, 47.5, 22.4, 163.1, 190],
			tooltip: {
				valueSuffix: 'M'
			}
		}],
		credits: {
			enabled: false
		}
	});

	$('.js-account-link').on('click', function () {
		var hash = $(this).data('tag'),
			link = $(this).attr('href');
		$(this).attr('href', link + hash);
		location.reload();
	});

	function tabContentSelection() {
		var hash = location.hash;
		$(hash).addClass("ui-tabs-panel_active");
	}

	tabContentSelection();

	$(".js-account-scroll").each(function () {
		var $form;
		$form = $(this);
		return $('html, body').animate({
			scrollTop: $form.offset().top - 70
		}, 600);
	});

	$('.help-section .title').click(function() {
		$(this).toggleClass('active');
		$(this).next('.help-section .content').toggle();
	});

});


