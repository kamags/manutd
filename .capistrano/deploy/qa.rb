server "new.#{fetch(:application)}", user: 'deploy', roles: %w{web app db}
set :deploy_to, "/media/www/new.#{fetch(:application)}"
set :branch, ENV["BRANCH"] || "fullsite"