# Stuff from .capistrano/tasks/submodule_strategy.rb
set :git_strategy, SubmoduleStrategy

# :application is required for some capistrano stuff.
# git-ssh.sh is uploaded to /tmp/#{application}, for example.
set :application, 'manutdsocialcasino.com'

# set :repo_url, 'git@gitlab.kama.gs:sites/manutdsocialcasino-com.git'
set :repo_url, 'git@gitlab.kama.gs:MU/casino-MU.git'

# set :branch, ENV["BRANCH"] || "master"

set :scm, :git
set :format, :pretty
set :log_level, :debug
set :pty, true
set :keep_releases, 50

# set :linked_files, %w{protected/config/database.php}
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system protected/runtime}}
set :linked_dirs, %w{protected/runtime public/images}

namespace :deploy do
        # after :publishing, 'yii:migrations'
        # after 'yii:migrations', :restart
end