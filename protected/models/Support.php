<?php

class Support extends CFormModel {
	public $name;
	public $email;
	public $message;
	public $antibot;
        public $app;

	public function rules() {
		return [
			['name, email, message, app', 'required'],
			['email', 'email'],
			['antibot', 'safe'],
		];
	}

	public function attributeLabels() {
		return [
			'name'       =>  Yii::t("ingame", "form-input-name"),
			'email'      =>  Yii::t("ingame", "form-input-email"),
			'message'    =>  Yii::t("ingame", "form-input-message")
		];
	}
}