<?php

class Registration extends ActiveRecord {
	public $nickname;
	public $login;
	public $password;
	public $confirmPassword;
	public $subscribe;
	public $agreeWithToU;
	public $confirmHash;
	public $created;

	/**
	 * @return string
	 */
	public function tableName() {
		return 'registration';
	}

	/**
	 * @param string $className
	 * @return $this
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return mixed|string
	 */
	public function primaryKey() {
		return 'id';
	}

	public function rules() {
		return [
			['nickname, login, password, confirmPassword, agreeWithToU', 'required'],
			['nickname', 'length', 'max' => 20],
			['subscribe, agreeWithToU', 'boolean'],
			['login', 'email'],
			['confirmPassword', 'compare', 'compareAttribute' => 'password'],
			['login', 'loginNotExist'],
			['agreeWithToU', 'in', 'range' => [1], 'message' => Yii::t('signup', 'You must agree with Terms of Use')],
			['password', 'match', 'pattern' => '/^\d+$/', 'not' => true, 'message' => Yii::t('profile', 'Password can\'t contain only digits')],
			['password', 'length', 'min' => 6],
		];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'nickname'        => Yii::t('signup', 'Nickname'),
			'login'           => Yii::t('signup', 'Email'),
			'password'        => Yii::t('signup', 'Password'),
			'confirmPassword' => Yii::t('signup', 'Confirm password'),
			'subscribe'       => Yii::t('signup', 'I subscribe to the newsletter and news'),
			'agreeWithToU'    => Yii::t('signup', 'I agree all <a href="{$link}">Terms of Use</a>', ['{$link}' => Yii::app()->createUrl('main/termsOfUse')]),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function loginNotExist($attribute, $params) {
		if (!empty($this->login)) {
			/** @var PokeristApiComponent $api */
			$api = Yii::app()->pokeristApi;

			if ($api->getPlayerApi()->checkLoginExists($this->{$attribute})) {
				$this->addError($attribute, Yii::t('signup', 'Email already exists'));
			}
		}
	}

	public function confirm() {
		/** @var PokeristApiComponent $api */
		$api = Yii::app()->pokeristApi;

		if ($playerId = $api->getAdminApi()->createPlayer($this->login, $this->nickname, $this->password)) {

			$player = new Player();
			$player->playerId = $playerId;
			$player->save();
			$this->delete();

			return true;
		}
		return false;
	}

	public function beforeSave() {
		if (!parent::beforeSave()) {
			return false;
		}

		if (!$this->confirmHash) {
			$this->confirmHash = uniqid('', true);
		}

		return true;
	}

	/**
	 * @param string $login
	 * @return Registration|null
	 */
	public function findByLogin($login) {
		return $this->findByAttributes(['login' => $login]);
	}

	/**
	 * @param string $hash
	 * @return Registration|null
	 */
	public function findByHash($hash) {
		return $this->findByAttributes(['confirmHash' => $hash]);
	}

	public function active() {
		$alias = $this->getTableAlias(false, false);
		$this->getDbCriteria()->mergeWith([
			'condition' => "{$alias}.created > DATE_SUB(NOW(), INTERVAL 1 DAY)",
		]);
		return $this;
	}
}
