<?php

/**
 * Class Player
 * @method Player findByPk()
 */
class Subscribers extends ActiveRecord {

	public $id;
	public $email;

	public function tableName() {
		return 'subscribers';
	}
	

	/**
	 * @param string $className
	 * @return Player
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return [
			['email', 'required'], 
			['email', 'length', 'max' => 256],
			['email', 'unique'],
			['email', 'email'],
			['email', 'safe'],
		];
	}
}
