<?php

class RestorePassword extends ActiveRecord {
	public $id;
	public $login;
	public $playerId;
	public $confirmHash;
	public $created;
	public $email;
	public $nickname;

	/**
	 * @return string
	 */
	public function tableName() {
		return 'restore_password';
	}

	/**
	 * @param string $className
	 * @return RestorePassword
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return mixed|string
	 */
	public function primaryKey() {
		return 'id';
	}

	public function rules() {
		return [
			['email', 'required'],
			['email', 'email'],
			['playerId, confirmHash, email', 'safe'],
		];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'login' => Yii::t('restore-password', 'Email'),
		);
	}

	public function loginExist($attribute, $params) {
		if (!empty($this->login)) {
			/** @var PokeristApiComponent $api */
			$api = Yii::app()->pokeristApi;

			if (!$api->getPlayerApi()->checkLoginExists($this->{$attribute})) {
				$this->addError($attribute, Yii::t('restore-password', "Player with this email doesn't exists"));
			}
		}
	}

	public static function generatePassword($length = 8) {
	    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $count = mb_strlen($chars);

	    for ($i = 0, $result = ''; $i < $length; $i++) {
	        $index = rand(0, $count - 1);
	        $result .= mb_substr($chars, $index, 1);
	    }

	    return $result;
	}

	public function confirm($password = null) {
		if ($password == null) {
		    $password = self::generatePassword();
		}
		/** @var PokeristApiComponent $api */
		$api = Yii::app()->pokeristApi;

		if ($player = $api->getAdminApi()->changePlayerPassword($this->playerId, $password)) {
			$_player = $api->getPlayerApi()->getPlayerById($player['playerId']);
			$player['email'] = $this->email;
			$player['nickname'] = $_player['nickname'];
			$this->delete();
			return $player;
		}
		return false;
	}

	public function beforeSave() {
		if (!parent::beforeSave()) {
			return false;
		}

		if (!$this->confirmHash) {
			$this->confirmHash = uniqid('', true);
		}

		if (!$this->playerId) {
			/** @var PokeristApiComponent $api */
			$api            = Yii::app()->pokeristApi;
			if ($playerId = $api->getPlayerApi()->getPlayerIdByLogin($this->email)) {
				$this->playerId = $playerId;
			} else {
//				throw new CHttpException(404, 'User not found');
				$this->addError('email', Yii::t('profile', 'User not found'));
				return false;
			}
		}

		$this->deleteAllByAttributes(['playerId' => $this->playerId]);

		return true;
	}

	/**
	 * @param string $hash
	 * @return Registration|null
	 */
	public function findByHash($hash) {
		return $this->findByAttributes(['confirmHash' => $hash]);
	}

	public function active() {
		$alias = $this->getTableAlias(false, false);
		$this->getDbCriteria()->mergeWith([
			'condition' => "{$alias}.created > DATE_SUB(NOW(), INTERVAL 2 HOUR)",
		]);
		return $this;
	}
}
