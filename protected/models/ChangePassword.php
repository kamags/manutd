<?php

class ChangePassword extends CFormModel {

	public $newPassword;
	public $newPassword_repeat;

	public function rules() {
		return [
			['newPassword', 'required'],
			['newPassword', 'length', 'min' => 6],
			['newPassword', 'compare', 'compareAttribute'=>'newPassword_repeat'],
			['newPassword', 'match', 'pattern' => '/^\d+$/', 'not' => true, 'message' => Yii::t('profile', "Password can\'t contain only digits")],
		];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'newPassword'        => Yii::t('profile', 'New password'),
			'newPassword_repeat' => Yii::t('profile', 'Repeat new password'),
		);
	}

}
