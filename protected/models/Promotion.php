<?php

/**
 * @method $this with(mixed $relations)
 */
class Promotion extends Press {

	public function tableName() {
		return 'promotion';
	}

	/**
	 * @param string $className
	 * @return self
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
}
