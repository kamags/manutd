<?php

/**
 * Class Player
 * @method Player findByPk()
 */
class Player extends ActiveRecord {

	const PRIVACY_EVERYONE     = 3;
	const PRIVACY_FRIENDS_ONLY = 2;
	const PRIVACY_NO_ONE       = 1;

	public $cachePrefix = 'application.player.';
	public $cacheExpire = 60;

	public $playerId;
	public $privacyFriendsList = self::PRIVACY_EVERYONE;
	public $privacyShowProfile = self::PRIVACY_EVERYONE;
	public $privacyGoldCoins = self::PRIVACY_FRIENDS_ONLY;
	public $privacyChips = self::PRIVACY_FRIENDS_ONLY;
	public $privacySavedGames = self::PRIVACY_FRIENDS_ONLY;
	public $privacyStatistics = self::PRIVACY_FRIENDS_ONLY;

	public function tableName() {
		return 'player';
	}

	/**
	 * @param string $className
	 * @return Player
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return [
			['playerId', 'required'], //privacyFriendsList, privacyGoldCoins, privacyChips, privacyShowProfile, privacyStatistics, privacySavedGames
			['privacyFriendsList, privacyGoldCoins, privacyChips, privacySavedGames, privacyStatistics, privacyShowProfile', 'in', 'range' => [self::PRIVACY_EVERYONE, self::PRIVACY_FRIENDS_ONLY, self::PRIVACY_NO_ONE]],
		];
	}

	public function attributeLabels() {
		return [
			'playerId'           => 'ID',
//			'privacyFriendsList' => 'View friends list by',
			'privacyShowProfile' => Yii::t('profile', 'Show profile'),
			'privacyFriendsList' => Yii::t('profile', 'Show friends list'),
			'privacyGoldCoins'   => Yii::t('profile', 'Show gold coins'),
			'privacyChips'       => Yii::t('profile', 'Show chips'),
			'privacySavedGames'  => 'View saved games by',
			'privacyStatistics'  => 'View statistics by',
		];
	}

	/**
	 * Checks visibility from privacy settings
	 * @param int $targetPlayerId
	 * @param Array $friendsArray
	 * @return Array
	 * @throws CDbException
	 */
	public function checkVisibilityFromSettings($targetPlayerId, $friendsArray = null) {
		$targetPlayer = $this->findByPk($targetPlayerId);
		if (!$targetPlayer) {
			$targetPlayer           = new self();
			$targetPlayer->playerId = $targetPlayerId;
			if (!$targetPlayer->save()) {
				throw new CDbException("Could not save target player with ID {$targetPlayerId} in DB");
			}
		}

		// check whether this is your friend
		if (!$friendsArray) {
			$friendsArray = Yii::app()->pokeristApi->getPlayerApi()->getPlayerInfoById($targetPlayerId)['buddies'];
		}
		$isFriend = false;
		foreach ($friendsArray as $friend) {
			if ($friend['playerId'] == $this->playerId) {
				$isFriend = true;
				break;
			}
		}

		$privacySettings = [];
		foreach ($targetPlayer->getPrivacySettings() as $setting => $value) {
			switch ($value) {
				case 1:
					$privacySettings[$setting] = $targetPlayerId == $this->playerId;
					break;
				case 2:
					$privacySettings[$setting] = $isFriend;
					break;
				default:
					$privacySettings[$setting] = true;
			}
		}
		return $privacySettings;
	}

	public function getPrivacySettings() {
		return [
			'showProfile' => $this->privacyShowProfile,
			'friendsList' => $this->privacyFriendsList,
			'goldCoins'   => $this->privacyGoldCoins,
			'chips'       => $this->privacyChips,
		];
	}

	public function getProfile() {
		if ($this->profile === null) {
			$cacheKey = $this->generateCacheKey('profile');
			if (($this->profile = Yii::app()->cache->get($cacheKey)) === false) {
				try {
					// get profile from API
					/** @var $playerApi PokeristApi_Player */
					$playerApi     = Yii::app()->pokeristApi->getPlayerApi();
					$this->profile = $playerApi->getPlayerById($this->getId());

					Yii::app()->cache->set($cacheKey, $this->profile, $this->cacheExpire);
				} catch (PokeristApi_Exception $e) {
					Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, 'application.api');
				}
			}
		}
	}

	protected function generateCacheKey($part) {
		return "{$this->cachePrefix}." . Yii::app()->user->id . ".{$part}";
	}

	public function setAvatar(FSImage $image) {
		$client = new EHttpClient(Yii::app()
								  ->createUrl('profile/setAvatar'), [
			'timeout' => 30
		]);
		$user   = Yii::app()->user;
		$client->setParameterGet([
			'login'     => $user->sessionToken,
			'hd'        => 1,
			'loginType' => 5, // authorize by session token
		]);
		$client->setParameterPost('data', bin2hex($image->toPng()));
		try {
			$response = $client->request(EHttpClient::POST);
			if ($response->getStatus() != 200) {
				Yii::log("[{$response->getStatus()}] Wrong response status", CLogger::LEVEL_ERROR, 'application.profile.avatar');
				return false;
			}
			$responseBody = $response->getBody();
			if ($responseBody != 'OK') {
				Yii::log($responseBody, CLogger::LEVEL_ERROR, 'application.profile.avatar');
				return false;
			}
		} catch (EHttpClientException $e) {
			Yii::log("[{$e->getCode()}] {$e->getMessage()}", CLogger::LEVEL_ERROR, 'application.profile.avatar');
			return false;
		}
		Yii::trace("Avatar successfully changed for player {$user->id}", 'application.profile.avatar');
		return true;
	}
}
