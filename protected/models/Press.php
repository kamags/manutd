<?php

/**
 *
 */
class Press extends ActiveRecord {


	public $id;
	public $alias;
	public $title;
	public $overview;
	public $content;
	public $images;
	public $publicationDate;
	public $keywords;
	public $description;
	

	protected function beforeSave() {
		if (!parent::beforeSave()) {
			return false;
		}

		if (is_array($this->images)) {
			$this->images = serialize($this->images);
		}

		if (!$this->alias) {
			$this->alias = self::prepareStringForUrl($this->title);
		}
		return true;
	}

	protected function afterFind() {
		if (is_string($this->images)) {
			$this->images = unserialize($this->images);
		}
		parent::afterFind();
	}

	public function tableName() {
		return 'press';
	}

	/**
	 * @param string $className
	 * @return self
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return [
			['title, overview', 'required'],
			['alias, active', 'safe'],
			['content, overview, keywords, description', 'length', 'max' => 12000],
		];
	}

	public function defaultScope() {
		return [
			'condition' => 'active = 1',
			'order'     => "t.publicationDate DESC, t.created DESC",
		];
	}
	
	public function last($limit = 10) {
		$this->dbCriteria->mergeWith([
			'limit' => (int) $limit? : 10,
		]);
		return $this;
	}
	
	public function offset($offset) {
		$this->dbCriteria->mergeWith([
			'offset' => (int) $offset,
		]);
		return $this;
	}

	public function getImage() {
		if ($this->images && is_array($this->images) && count($this->images)) {
			return current($this->images);
		}

		return null;
	}

	public function attributeLabels() {
		return [
			'images[]' => 'images',
			'keywords' => 'meta-keywords',
			'description' => 'meta-description',
		];
	}
}
