<?php

/* /views/main/index.twig */
class __TwigTemplate_ba2f2f906834745a4bf6b0bdf9acec760f48ba27ddbb236563230a7d7414a869 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "\t<div class='slide slide1'>
\t\t<div class=\"wrapper\">
\t\t\t<a class=\"legal__logo\" href=\"/\"></a>

\t\t\t<p class=\"header__text header__text_1\"></p>

\t\t\t<div class=\"header__text header__text_2\"><span>";
        // line 9
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "header-text-2"), "method");
        echo "</span></div>
\t\t\t<a class=\"btn play-now fancy-casino\" href=\"#casino\">";
        // line 10
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "play-now-btn"), "method");
        echo "</a>
\t\t</div>
\t</div>

\t<div class='slide slide2'>
\t\t<div class=\"wrapper\">
\t\t\t<a class=\"btn play-now fancy-casino\" href=\"#casino\">";
        // line 16
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "play-now-btn"), "method");
        echo "</a>

\t\t\t<div class=\"store-content\">
\t\t\t\t<h2 class=\"text_title text_line\">";
        // line 19
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-1-title"), "method");
        echo "</h2>

\t\t\t\t<p class=\"text_text\">";
        // line 21
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-1-text"), "method");
        echo "</p>

\t\t\t\t<div class=\"store\">
\t\t\t\t\t<a href=\"https://itunes.apple.com/app/id947856678\" class=\"store__item store__item_ap\"></a>
\t\t\t\t\t<a href=\"https://play.google.com/store/apps/details?id=com.kamagames.texaspokermu\"
\t\t\t\t\t   class=\"store__item store__item_gl\"></a>
\t\t\t\t\t<a href=\"http://www.amazon.com/KamaGames-Manchester-United-Social-Poker/dp/B00T9UPAAC/ref=sr_1_2?s=mobile-apps&ie=UTF8&qid=1424251869&sr=1-2&keywords=kamagames\"
\t\t\t\t\t   class=\"store__item store__item_amazon\"></a>
\t\t\t\t\t<a href=\"http://www.windowsphone.com/en-us/store/app/manchester-united-social-poker/f7a9b108-b47a-4164-bd11-5f9fc1ff361e\"
\t\t\t\t\t   class=\"store__item store__item_win\"></a>
\t\t\t\t\t<a href=\"https://apps.facebook.com/manutdsocialpoker/\" class=\"store__item store__item_fb\"></a>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"play-now-content\">
\t\t\t\t<h2 class=\"text_title text_right text_line\">";
        // line 35
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-2-title"), "method");
        echo "</h2>

\t\t\t\t<p class=\"text_text text_right\">";
        // line 37
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-2-text"), "method");
        echo "</p>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class='slide slide3'>
\t\t<div class=\"wrapper\">
\t\t\t<a class=\"btn play-now fancy-casino\" href=\"#casino\">";
        // line 44
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "play-now-btn"), "method");
        echo "</a>

\t\t\t<div class=\"features-content\">
\t\t\t\t<h2 class=\"text_title text_title_big\">";
        // line 47
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-3-title"), "method");
        echo "</h2>

\t\t\t\t<p class=\"text_title\">";
        // line 49
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-3-text"), "method");
        echo "</p>
\t\t\t\t<ul class=\"features\">
\t\t\t\t\t<li class=\"features__item\">
\t\t\t\t\t\t<img src=\"/img/features/img_1.png\" alt=\"\"/>
\t\t\t\t\t\t<p class=\"features__text\">";
        // line 53
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-3-item-1"), "method");
        echo "</p>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"features__item\">
\t\t\t\t\t\t<img src=\"/img/features/img_2.png\" alt=\"\"/>
\t\t\t\t\t\t<p class=\"features__text\">";
        // line 57
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-3-item-2"), "method");
        echo "</p>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"features__item\">
\t\t\t\t\t\t<img src=\"/img/features/img_3.png\" alt=\"\"/>
\t\t\t\t\t\t<p class=\"features__text\">";
        // line 61
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-3-item-3"), "method");
        echo "</p>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t\t<ul class=\"feature-list\">
\t\t\t\t\t<li class=\"feature-list__item text_text\">";
        // line 65
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-3-list-item-1"), "method");
        echo "</li>
\t\t\t\t\t<li class=\"feature-list__item text_text\">";
        // line 66
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-3-list-item-2"), "method");
        echo "</li>
\t\t\t\t\t<li class=\"feature-list__item text_text\">";
        // line 67
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-3-list-item-3"), "method");
        echo "</li>
\t\t\t\t\t<li class=\"feature-list__item text_text\">";
        // line 68
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-3-list-item-4"), "method");
        echo "</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class='slide slide4'>
\t\t<div class=\"wrapper\">
\t\t\t<div class=\"online\">
\t\t\t\t<p class=\"online__label text_text\">";
        // line 77
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-4-label"), "method");
        echo "</p>

\t\t\t\t<p class=\"online__total text_title text_title_big\" id=\"players-online\"></p>

\t\t\t</div>
\t\t\t<div class=\"roulette-content\">
\t\t\t\t<h2 class=\"text_title text_title_big\">";
        // line 83
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-5-title"), "method");
        echo "</h2>

\t\t\t\t<p class=\"text_title text_line\">";
        // line 85
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-5-title-2"), "method");
        echo "</p>

\t\t\t\t<p class=\"text_text\">";
        // line 87
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-5-text"), "method");
        echo "</p>
\t\t\t\t<ul class=\"feature-list feature-list_roulette\">
\t\t\t\t\t<li class=\"feature-list__item text_text\">";
        // line 89
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-4-list-item-1"), "method");
        echo "</li>
\t\t\t\t\t<li class=\"feature-list__item text_text\">";
        // line 90
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-4-list-item-2"), "method");
        echo "</li>
\t\t\t\t\t<li class=\"feature-list__item text_text\">";
        // line 91
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-4-list-item-3"), "method");
        echo "</li>
\t\t\t\t\t<li class=\"feature-list__item text_text\">";
        // line 92
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-4-list-item-4"), "method");
        echo "</li>
\t\t\t\t\t<li class=\"feature-list__item text_text\">";
        // line 93
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "section-4-list-item-5"), "method");
        echo "</li>
\t\t\t\t</ul>
\t\t\t\t<a class=\"btn btn-big play-now  fancy-casino\" href=\"#casino\">";
        // line 95
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "play-now-btn"), "method");
        echo "</a>
\t\t\t</div>
\t\t</div>
\t</div>


\t<div class='slide slide5'>
\t\t<div class=\"wrapper\">
\t\t\t<div class=\"ft-store top\">
\t\t\t\t";
        // line 104
        echo twig_include($this->env, $context, "main/facebook-container.twig");
        echo "
\t\t\t</div>

\t\t\t";
        // line 107
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t\t</div>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/main/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 107,  223 => 104,  211 => 95,  206 => 93,  202 => 92,  198 => 91,  194 => 90,  190 => 89,  185 => 87,  180 => 85,  175 => 83,  166 => 77,  154 => 68,  150 => 67,  146 => 66,  142 => 65,  135 => 61,  128 => 57,  121 => 53,  114 => 49,  109 => 47,  103 => 44,  93 => 37,  88 => 35,  71 => 21,  66 => 19,  60 => 16,  51 => 10,  47 => 9,  39 => 3,  36 => 2,  11 => 1,);
    }
}
