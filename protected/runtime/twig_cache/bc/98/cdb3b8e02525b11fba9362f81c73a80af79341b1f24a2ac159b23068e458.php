<?php

/* /views/admin/adminPressList.twig */
class __TwigTemplate_bc98cdb3b8e02525b11fba9362f81c73a80af79341b1f24a2ac159b23068e458 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/admin.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t\t<a class=\"btn btn-success btn-big\" href=\"";
        // line 6
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => ((array_key_exists("createRoute", $context)) ? (_twig_default_filter((isset($context["createRoute"]) ? $context["createRoute"] : null), "admin/pressCreate")) : ("admin/pressCreate"))), "method");
        echo "\">Add new item</a>
\t\t</div>

\t\t";
        // line 9
        if (twig_length_filter($this->env, (isset($context["models"]) ? $context["models"] : null))) {
            // line 10
            echo "\t\t\t";
            $context["model"] = twig_first($this->env, (isset($context["models"]) ? $context["models"] : null));
            // line 11
            echo "\t\t\t<table class=\"table table-hover\">
\t\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t";
            // line 15
            echo $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "getAttributeLabel", array(0 => "id"), "method");
            echo "
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t";
            // line 18
            echo $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "getAttributeLabel", array(0 => "alias"), "method");
            echo "
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t";
            // line 21
            echo $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "getAttributeLabel", array(0 => "title"), "method");
            echo "
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t";
            // line 24
            echo $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "getAttributeLabel", array(0 => "overview"), "method");
            echo "
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t";
            // line 27
            echo $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "getAttributeLabel", array(0 => "publicationDate"), "method");
            echo "
\t\t\t\t\t</td>
\t\t\t\t\t<td></td>
\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody>
\t\t\t\t";
            // line 33
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["models"]) ? $context["models"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["model"]) {
                // line 34
                echo "\t\t\t\t\t<tr class=\"";
                echo (($this->getAttribute($context["model"], "active", array())) ? ("") : ("danger"));
                echo "\">
\t\t\t\t\t\t<td>";
                // line 35
                echo $this->getAttribute($context["model"], "id", array());
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 36
                echo $this->getAttribute($context["model"], "alias", array());
                echo "</td>
\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t";
                // line 38
                echo $this->getAttribute((isset($context["Html"]) ? $context["Html"] : null), "link", array(0 => $this->getAttribute($context["model"], "title", array()), 1 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => ((array_key_exists("itemRoute", $context)) ? (_twig_default_filter((isset($context["itemRoute"]) ? $context["itemRoute"] : null), "admin/pressItem")) : ("admin/pressItem")), 1 => array("id" => $this->getAttribute($context["model"], "id", array()))), "method")), "method");
                echo "
\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td>";
                // line 40
                echo $this->getAttribute($context["model"], "overview", array());
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 41
                echo $this->getAttribute($context["model"], "publicationDate", array());
                echo "</td>
\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t<a href=\"";
                // line 43
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => ((array_key_exists("deleteRoute", $context)) ? (_twig_default_filter((isset($context["deleteRoute"]) ? $context["deleteRoute"] : null), "admin/pressDelete")) : ("admin/pressDelete")), 1 => array("id" => $this->getAttribute($context["model"], "id", array()))), "method");
                echo "\" class=\"btn btn-danger js-ajax-delete\">x</a>
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['model'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "\t\t\t\t</tbody>
\t\t\t</table>
\t\t";
        } else {
            // line 50
            echo "\t\t\t<p>Nothing to show</p>
\t\t";
        }
        // line 52
        echo "\t</div>
";
    }

    public function getTemplateName()
    {
        return "/views/admin/adminPressList.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 52,  140 => 50,  135 => 47,  125 => 43,  120 => 41,  116 => 40,  111 => 38,  106 => 36,  102 => 35,  97 => 34,  93 => 33,  84 => 27,  78 => 24,  72 => 21,  66 => 18,  60 => 15,  54 => 11,  51 => 10,  49 => 9,  43 => 6,  39 => 4,  36 => 3,  11 => 1,);
    }
}
