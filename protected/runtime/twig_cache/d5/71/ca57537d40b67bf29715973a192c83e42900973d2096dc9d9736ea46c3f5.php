<?php

/* /views/promotions/index.twig */
class __TwigTemplate_d571ca57537d40b67bf29715973a192c83e42900973d2096dc9d9736ea46c3f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "\t<div class=\"wrapper\">
\t\t<a class=\"pages__logo\" href=\"/\"></a>
\t</div>
\t<div class=\"pages pages_bg\">

\t\t<div class=\"wrapper\">
\t\t\t<div class=\"text_block promotions\">
\t\t\t\t<h1>Promotions</h1>
\t\t\t\t<span class=\"pages-line\"></span>

\t\t\t\t";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promoItems"]) ? $context["promoItems"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 14
            echo "\t\t\t\t\t<div class=\"promotions__item\">
\t\t\t\t\t\t<div class=\"promotions__img\">
\t\t\t\t\t\t\t";
            // line 16
            $context["image"] = $this->getAttribute($context["item"], "getImage", array(), "method");
            // line 17
            echo "\t\t\t\t\t\t\t";
            if ((isset($context["image"]) ? $context["image"] : null)) {
                // line 18
                echo "\t\t\t\t\t\t\t\t<img src=\"";
                echo (isset($context["image"]) ? $context["image"] : null);
                echo "\" width=\"327\" height=\"182\">
\t\t\t\t\t\t\t";
            }
            // line 20
            echo "\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"promotions-content\">
\t\t\t\t\t\t\t<h3>";
            // line 22
            echo $this->getAttribute($context["item"], "title", array());
            echo "</h3>

\t\t\t\t\t\t\t";
            // line 24
            echo $this->getAttribute($context["item"], "overview", array());
            echo "

\t\t\t\t\t\t\t<a class=\"promotions-more-link\"
\t\t\t\t\t\t\t   href=\"";
            // line 27
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "promotions/item", 1 => array("alias" => $this->getAttribute($context["item"], "alias", array()))), "method");
            echo "\">Learn
\t\t\t\t\t\t\t\tmore</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "\t\t\t\t
\t\t\t\t";
        // line 33
        echo twig_include($this->env, $context, "common/pagination.twig");
        echo "

\t\t\t\t";
        // line 36
        echo "\t\t\t\t";
        echo twig_include($this->env, $context, "main/facebook-container.twig");
        echo "
\t\t\t</div>

\t\t</div>
\t</div>


\t<div class=\"wrapper\">
\t\t";
        // line 44
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/promotions/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 44,  104 => 36,  99 => 33,  96 => 32,  85 => 27,  79 => 24,  74 => 22,  70 => 20,  64 => 18,  61 => 17,  59 => 16,  55 => 14,  51 => 13,  39 => 3,  36 => 2,  11 => 1,);
    }
}
