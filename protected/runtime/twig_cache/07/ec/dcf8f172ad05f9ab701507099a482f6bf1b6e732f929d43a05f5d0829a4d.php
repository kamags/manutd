<?php

/* /views/main/play-now.twig */
class __TwigTemplate_07ecdcf8f172ad05f9ab701507099a482f6bf1b6e732f929d43a05f5d0829a4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('content', $context, $blocks);
        // line 8
        echo "
";
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "\t<div style=\"text-align: center; padding-top: 15px;\">
\t\t<iframe scrolling=\"no\" style=\"border: 0; width: 100%; height: 755px;\" src=\"";
        // line 5
        echo (isset($context["url"]) ? $context["url"] : null);
        echo "\"></iframe>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "/views/main/play-now.twig";
    }

    public function getDebugInfo()
    {
        return array (  36 => 5,  33 => 4,  30 => 3,  25 => 8,  23 => 3,  20 => 2,);
    }
}
