<?php

/* /views/games/slots-help.twig */
class __TwigTemplate_db26f06d43e4cf207e846ade6b2d8753cd813b25da517008af09daa47ccd1bc0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
\t<meta name=\"google-site-verification\" content=\"sBfm4F9T96AQQGr6xZpTwcFyCOxFJRUIAFvTZUtWelI\" />
\t<title>Manchester United Poker - Free to Play on your mobile device, social network or online</title>
\t<meta charset=\"UTF-8\">
\t<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
";
        // line 9
        echo "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/help.css\"/>
";
        // line 12
        echo "
</head>
<body class=\"bg\">
<div class=\"wrapper\">
<a class=\"pages__logo\" href=\"/\"></a>
</div>
<div class=\"pages pages_bg\">

<div class=\"wrapper\">
<section class=\"help-section\">
<header>
\t<img src=\"/img/help/header.png\" alt=\"\"/>
</header>
<h2 class=\"title\">General Features</h2>
<div class=\"content\">
<h3>How do I play this game?</h3>
<p>After the game has finished loading, you will find yourself in the first lobby. Here you can scroll left and right
\tto select the slot machine that you want to play with. Tap on the machine you would like to play to begin the slots game.
\tThe game will automatically set to betting the maximum number of lines for you, and you only need to decide how much to bet per spin.
\tOnce you have chosen your bet amount, click the Spin button and get ready to win!</p>
<h3>How do I unlock the other slot machines?</h3>
<p>You can unlock new machines when you have reached the required levels. New machines have different number of lines, reels, graphics and bonus games.
\tGain more levels and unlock all the machines for maximum gaming pleasure!</p>
<h3>What do the following special symbols mean?</h3>
<img src=\"/img/help/special-simbols/diamond.png\" alt=\"\"/>
\t<h4>Diamonds</h4>
\t
\t<p>Every time you spin, you will gain 'diamonds'. Diamonds represent experience points that you earn while playing the game.
\t\tYou will level up when you have earned the necessary diamonds for each level. You can also earn diamonds by spinning and
\t\tgetting the diamond symbols. The amount of diamonds you gain depends on how much you are betting and your current level.
\t\tThe higher amount you bet, the faster you gain diamonds for leveling up. As you level up, the allowable max bet amount will also increase, time bonus
\t\tmoney will also be higher and new machines can be unlocked.</p>
\t<hr />
\t<img src=\"/img/help/special-simbols/Scatter.png\" alt=\"\"/>
\t<h4>Scatter</h4>
\t<p>While spinning, if you are lucky enough to see 3 Scatter symbols, based on different machines, you can get a certain amount of spins for free without having to wager any coins!</p>
\t<hr />
\t<img src=\"/img/help/special-simbols/bonus.png\" alt=\"\"/>
\t<h4>Bonus</h4>
\t<p>A Bonus game is triggered when 3 Bonus symbols appear within your reels. The bonus game does not require any additional coins to be wagered to play. There are several different types of bonus games within the game.</p>

\t\t\t<h4>Pick a box</h4>
\t\t\t<p>You are required to open various items to claim the coins within. Opening one which does not contain any coins will end the game immediately.</p>
\t\t\t<figure>
\t\t\t\t<img src=\"/img/help/special-simbols/pikcbox.png\" alt=\"\"/>
\t\t\t\t<figcaption>Aim for Goal is the pick a box bonus game within Glory Glory.</figcaption>
\t\t\t</figure>
\t\t\t\t<h4>Match Three</h4>
\t\t\t<p>You can keep picking until you matched 3 identical items, each combination gives a different amount of coins.</p>
<figure>
\t<img src=\"/img/help/special-simbols/matchtree.png\" alt=\"\"/>
\t<figcaption>Pin Up Stamps is the match three bonus game within Glamorous Pin Ups.</figcaption>
</figure>
\t\t\t<h4>Wheel of Fortune</h4>
\t\t\t<p>In \"Wheel of Fortune\" bonus game, you can spin a lucky wheel to see how much bonus coins you can earn. Your winnings will correspond to the stated amount listed next to the wheel.
\t\t\t</p>
<figure>
\t<img src=\"/img/help/special-simbols/fortune.png\" alt=\"\"/>
\t<figcaption>Round Table is the wheel of fortune bonus game within Guardian of Excalibur.</figcaption>
</figure>
\t\t\t<h4>Lucky Chance</h4>
\t\t\t<p>In \"Lucky Chance\" bonus game, there will be 4 choices glowing in orders from left to right, when you tap on the Stop button, the glow selection will decrease speed for a few second before stopping on a choice.
\t\t\t\tUser will then be rewarded with the coins associated with the selected choice.
\t\t\t</p>
<figure>
\t<img src=\"/img/help/special-simbols/luckyChance.png\" alt=\"\"/>
\t<figcaption>Toto’s Choice is the lucky chance bonus game within Emerald City.</figcaption>
</figure>
\t\t\t<h4>Race to the Finish</h4>
\t\t\t<p>In \"Race to the Finish\" bonus game, pick any of the four contestants you think will win the race. Your prize will correspond to your contestant finish order. If your contestant finishes first, you will receive the prize money for 1st place!
\t\t\t</p>
<figure>
\t<img src=\"/img/help/special-simbols/raceFinish.png\" alt=\"\"/>
\t<figcaption>Fastest Dribbler is the race to finish bonus game within Match Day.</figcaption>
</figure>

<h3>Wild</h3>
<p>Wild symbol is generally used to fill out reel combinations that produce prizes, it’s a special symbol which can replace any other reel symbol to make up combination.</p>
<h3>What is VIP?</h3>
<p>VIP represents the status of your account, the higher level you are, the more precious title you will obtain. Each title gives an extra bonuses to your Coins Purchase Packages and Daily Bonuses. There are a total of five titles:</p>
<ul class=\"vip\">
\t<li><img src=\"/img/help/vip/01.png\" alt=\"\"/><p>Rookie - Lv1 ~ 69</p></li>
\t<li><img src=\"/img/help/vip/02.png\" alt=\"\"/><p>Amateur - Lv70 ~ 169</p></li>
\t<li><img src=\"/img/help/vip/03.png\" alt=\"\"/><p>Professional - Lv170 ~ 269</p></li>
\t<li><img src=\"/img/help/vip/04.png\" alt=\"\"/><p>World Class - Lv270 ~ 369</p></li>
\t<li><img src=\"/img/help/vip/05.png\" alt=\"\"/><p>Legend - Lv370 or above</p></li>
</ul>
<h3>What is Daily Bonus?</h3>
<p>Daily bonus is available once per day (please note that you must be connected to the internet). It refreshes everyday at 00:00:00 PST. There are 3 kinds of daily bonuses available:</p>
<ul>
\t<li>Daily Bonus - At the first log in of the day, you will be given a chance to pick a box and win bonus coins. (Higher VIP status does increase the amount of bonuses.)</li>
\t<li>Loyalty Bonus - You receive bonus coins only when you log in consecutively! (Higher VIP status does increase the amount of bonuses.)</li>
\t<li>Friend Bonus - The more friends play, the more bonuses you will get. You will get 1,000 coins for each friend, you could earn up to 50,000 coins per day! (Higher VIP status does increase the amount of bonuses.)</li>
</ul>
<p>(Must be connected to the internet in order to receive all daily bonuses)</p>
<h3>How does the Leaderboard work?</h3>
<p>First of all, you have to connect to Facebook in order to view, and to participate in the leaderboard system. Leaderboard shows the top 200 players in the game, the rank is determined by the experience points the user earned, which means the higher level you are, the higher chance you can get into the top 200! The profile of the top 3 players will be surrounded by a Gold, Silver and Bronze frame respectively, and will be seen by all the players! Get Rich! Get Famous!</p>
<h3>How to sign up for newsletter?</h3>
<p>You will see a pop up dialogue asking you to submit your email address in the game. Once you have signed up, you should receive a confirmation email. Please check your regular and junk inboxes for the email. Click on the email through your phone, it will direct to you to the game. Then the coins will be added to your account. Please note that you must be connected to the internet for the whole process.</p>
</div>
</section>
<section class=\"help-section\">
<h2 class=\"title\">Facebook Connect</h2>
<div class=\"content\">
<h3>Do I need a Facebook account to play this game?</h3>
<p>You do not need a Facebook account to play.</p>
<h3>Why does the game ask me to connect to Facebook?</h3>
<p>By connecting to your Facebook account, there are plenty of benefits:</p>
<ul>
\t<li>You will be given 1000 free coins to use in the game! (Please note: The 1000 free coins are given during the very first time you connect your Facebook account only. Only the first Facebook account that is connected to the game on each device will receive the bonus.)</li>
\t<li>By connecting to Facebook, you can view the rankings and compete with other players in the leaderboard system!</li>
\t<li>Your game data will be synchronized to our server once you logged in to Facebook via this game. This enables you to keep your level profile across multiple devices or to synchronize the data after you reinstalled the game. Please be reminded that only your level and experience will be saved, the amount of coins you have, however, won't be saved, as this is done to prevent user abuses.</li>
</ul>
<h3>Will you post updates to my Facebook wall or my friends' walls?</h3>
\t<p>There will NOT be any news feeds or posts shared out on your Facebook. We will NOT post anything on your behalf, your friends will NOT be spammed! All we are doing is to communicate with you, and you only!</p>
</div>
</section>
<section class=\"help-section\">
<h2 class=\"title\">Purchasing Coins</h2>
<div class=\"content\">
<h3>I’m not getting a response after confirming my coin purchase?</h3>
<p>This may be caused by not having a stable Internet connection. Check to see if your connection is active and working properly. If this problem persists, please try to logout and login again from your iTunes/Google/Amazon account. If after trying the above steps and you are still unable to make a purchase, please contact our customer support team</p>
<h3>I don’t see my coins after paying. What should I do?</h3>
<p>Sorry to hear if you are encountering the issue. If you are not receiving the coins that you have purchased, you may do the following:</p>
<p>For refunds -</p>
<ul>
\t<li>iOS users - please contact Apple iTunes.</li>
\t<li>Google Play users - please contact Google Play Store</li>
\t<li>Amazon users - please contact Amazon</li>
</ul>
<p>For credits - Please contact us through the FAQ page.</p>
<p>To avoid the issue to happen again, while you are purchasing please make sure the following:</p>
\t<ul>
\t\t<li>your internet connection is stable</li>
\t\t<li>do not leave the payment page, close the app or exit the app</li>
\t</ul>

\t<h3>If I delete and reinstall the game, will I lose my coins and progress?</h3>
\t<p>If you had connected to Facebook via the game, your level and experience data would be saved on our server (coins data will NOT be saved). This will allow you to synchronize your data and allow you to play on any other supported device with your existing progress.</p>
\t<p>If you had never connected to Facebook you will lose all progress and coins if you delete the game. Therefore, we strongly recommend you to connect the game with your Facebook account at all times.</p>

</div>
</section>
\t<section class=\"help-section\">
\t\t<h2 class=\"title\">Supported Devices</h2>
\t\t<div class=\"content\">
\t\t\t<h3>Apple Devices</h3>
\t\t\t<p>The game runs on Apple devices which runs on iOS 6 or higher and is an:</p>
\t\t\t<ul>
\t\t\t\t<li>iPhone 4 or newer models.</li>
\t\t\t\t<li>iPad 2 or newer models.</li>
\t\t\t\t<li>iPod Touch 4th generation or newer models.</li>
\t\t\t</ul>
\t\t\t<h3>Android Devices</h3>
\t\t\t<p>The game runs on Android devices which meets the following requirements:</p>
\t\t\t<ul>
\t\t\t\t<li>Runs on Android OS version 4.0 or higher</li>
\t\t\t\t<li>A minimum resolution of 480x800 pixels or higher.</li>
\t\t\t</ul>
\t\t</div>
\t</section>
<section class=\"help-section\">
<h2 class=\"title\">Others</h2>
\t<div class=\"content\">
\t\t<h3>The game crashes when I load the app or when I'm within the app. What should I do?</h3>
\t\t<p>A game crash is usually caused by memory issues on your device. The best way to fix this is to restart your device. You can also try closing other running apps which you are not using as the will be using the memory on your device at the same time.</p>
\t\t<h3>How often is the game updated?</h3>
\t\t<p>Stay tune to updates for this game by linking to our Facebook page or Twitter accounts!</p>
\t\t<h3>Why should I update the game?</h3>
\t\t<p>We are constantly working on providing the best slots experience for you and periodically we will upload new updates to the game to fix bugs or add new machines. By running the latest version of the game, it will allow you the best gaming experience by having access to the newest machines and patches for bug fixes.</p>
\t\t<h3>Can I win real money?</h3>
\t\t<p>This game is in no way actual gambling but a simulation of a popular casino game. All the coins are used for entertainment purpose only and cannot be cashed out.</p>
\t\t<h3>The game crashes when I load the app or when I'm within the app. What should I do?</h3>
\t\t<p>A game crash is usually caused by memory issues on your device. The best way to fix this is to restart your device. You can also try closing other running apps which you are not using as the will be using the memory on your device at the same time.</p>
\t\t<h3>How often is the game updated?</h3>
\t\t<p>Stay tune to updates for this game by linking to our Facebook page or Twitter accounts!</p>
\t\t<h3>Why should I update the game?</h3>
\t\t<p>We are constantly working on providing the best slots experience for you and periodically we will upload new updates to the game to fix bugs or add new machines. By running the latest version of the game, it will allow you the best gaming experience by having access to the newest machines and patches for bug fixes.
\t\t</p>
\t\t<h3>Can I win real money?</h3>
\t\t<p>This game is in no way actual gambling but a simulation of a popular casino game. All the coins are used for entertainment purpose only and cannot be cashed out.</p>
\t</div>
</section>
<section class=\"help-section\">
\t<h2 class=\"title\">Terms of Service and Privacy Policy</h2>
\t<div class=\"content\">
\t\t<h3>Terms of Service</h3>
\t\t<p>Last updated 21st of January 2015</p>
\t\t<h4>Definitions</h4>
\t\t<p>Account: the account created when registering for Pokerist, Roulettist or any of KamaGames’ games.</p>
\t\t<p>Affiliates: refers to all KamaGames subsidiaries, parent companies, partners, joint ventures, licensees, licensors, and/or any of their agents, consultants, employees, shareholders, officers and directors and any related parties.</p>
\t\t<p>Content: refers to any data uploaded or transmitted through the Service by the user.</p>
\t\t<p>Sites: refers to the web domains at kamagames.com, pokerist.com, any other web domain hosted by KamaGames or any social media page connected with KamaGames Services hosted on Facebook, Twitter, YouTube or Instagram.</p>
\t\t<p>Services: refers to products, downloads, games, services, content and websites controlled by KamaGames and/or its Affiliates.</p>
\t\t<p>Software: refers to any games or software used or downloaded from any of the Services.</p>
\t\t<p>Virtual Items: meaning virtual “currency” and “objects”, which may include, but are not limited to, virtual “coins”, “cash”, “tokens”, “points” and “property”.</p>
\t\t<h4>Legal Agreement</h4>
\t\t<p>1.1. The following Terms of Service, which include and hereby incorporate the Privacy Policy are binding legal agreement between you, KamaGames and its Affiliates, that govern your use of the Sites and its Services. By using these Sites or accessing any of our games, you agree to accept and be bound by the terms and conditions appearing in this document, and agree to comply with any Rules of Conduct and the Privacy Policy.</p>
\t\t<p>1.2. KamaGames reserves the right, at our discretion, to change, modify, add or remove portions of these Terms of Service and its Privacy Policy at any time by posting the amended Terms on or within the Services. KamaGames may provide email notification of material changes to the Terms of Service. You will be deemed to have accepted such changes by continuing to use the Service. Except as otherwise stated, all amended terms shall automatically be effective 30 days after they are initially posted.</p>
\t\t<p>1.3. You hereby represent that you have the legal capacity to enter into this Agreement and you are not barred from receiving services offered by KamaGames under the laws of applicable jurisdiction. The Service is not intended for use by children under 13 years of age.</p>
\t\t<h4>License</h4>
\t\t<p>2.1. KamaGames hereby grants you a limited, revocable, non-exclusive, non-sublicensable, and non-transferable right and license to use the Sites and the Services offered by our Sites as well as any Software in connection with these Services, only for the purposes of accessing, viewing or playing content; posting or submitting user Content using the embedded link function; placing product orders or accessing information. Applications and services are solely for your non-commercial,
\t\t\tlimited personal use, and for no other purposes.</p>
\t\t<p>2.2. The rights granted to you under this Agreement are subject to the following conditions:</p>
\t\t<p>You may not:</p>
\t\t<ul>
\t\t\t<li>(a) sublicense, rent, lease, loan, sell or otherwise transfer the Software or the Services (or any part thereof);</li>
\t\t\t<li>(b) modify, adapt, reverse engineer or decompile the Software, or otherwise attempt to derive source code from the Software;</li>
\t\t\t<li>(c) create any derivative works in respect of the Software or the Services; or</li>
\t\t\t<li>(d) otherwise use the Software or the Services except as expressly provided in this Agreement. Title to the Software, and all rights with respect to the Software and Services not specifically granted under this Agreement, including and without limitation, all rights of reproduction, modification, distribution, display, disassembly and de-compilation and all copyrights, patents, trademarks, trade secrets and other proprietary rights and interests are reserved to KamaGames or its licensor(s).</li>
\t\t</ul>
\t\t<p>You may not, nor will you allow any third party (whether or not for your benefit</p>
\t\t<ul>
\t\t\t<li>(a) alter, delete or conceal any copyright or other notices contained on the Sites, including notices on any Content you download, transmit, display, print or reproduce from the Sites;</li>
\t\t\t<li>(b) reproduce, modify, create derivative works from, display, perform, publish, distribute, disseminate, broadcast or circulate to any third party (including, without limitation, on or via a third party website), or otherwise use, any Content without the express prior written consent of KamaGames. Any unauthorized or prohibited use of any Content may subject you to civil liability, criminal prosecution, or both.</li>
\t\t</ul>
\t\t<p>2.3. If you access one of KamaGames’ Software, you may have the opportunity to get a limited and revocable license to use virtual, in-game currency or items that can be used while playing such Software. You may be required to pay a fee to obtain Virtual Items. You understand that Virtual Items exist solely in the virtual world; have no monetary value; and cannot be used to purchase or use products or services other than within the applicable Software. You realise that Virtual Items represent a part of KamaGames Software, title and any rights to which belong to KamaGames and its Affiliates. Price and availability of Virtual Items are subject to change without notice. ANY DEALING WITH VIRTUAL ITEMS BY A USER, INSIDE OR OUTSIDE OF THE SOFTWARE, IS STRICTLY PROHIBITED!</p>
\t\t<h4>Account Registration</h4>
\t\t<p>3.1. You may be required to create an Account to access our Services. If you have questions about Account registration, please contact our Support Team at support@kamagames.com. Certain Accounts are available only to individuals 13 years of age or older (if it is not specially stated for certain portions of the Site or the Services that they contain mature contents and are not suitable for anyone younger than 18 years of age or the applicable age of majority in the jurisdiction in which you reside). If you are over 13 years of age but are still considered a minor, we ask that you review these terms with your parents or guardian to ensure they understand them and agree to them. By accepting the terms of use in the Agreement, you represent that you are 13 years of age or older. We shall not be responsible in any way for your failure to accurately confirm your age per the terms hereunder.</p>
\t\t<p>3.2. Only one person may use an Account and you may not have more than four Accounts. KamaGames reserves the right to set limits on the number of Accounts a user may have over time at its sole discretion.</p>
\t\t<p>3.3. When creating a User ID you agree to:</p>
\t\t<ul>
\t\t\t<li>(i) provide true, accurate, current and complete information as requested in the required fields; and</li>
\t\t\t<li>(ii) promptly maintain and update such User ID to keep it true, accurate, current and complete. The information you provided shall be subject to our Privacy Policy. Your Nick Name or User ID, which is considered open information for other Users, may be transferred by KamaGames to other Users to create connections and increase social relations of the Users.</li>
\t\t</ul>
\t\t<p>3.4. You are liable for all activities conducted through your Account. Your Account may be blocked if someone else uses it to engage in activity that violates the Terms of Service or is otherwise improper or illegal. You should not reveal your Account password to others. KamaGames will not ask you to reveal your password, or initiate contact with you asking for answers to your password security questions.</p>
\t\t<p>3.5. You may not be permitted to create an Account or use the Services if you have previously been removed by KamaGames or previously been banned from playing any KamaGames game.</p>
\t\t<p>3.6. Your User ID may be deactivated if you do not use it within six months after the date that it was created or for any continuous period of six months after creation. If you do not use your User ID for six or more months, it may be removed and deleted by the Website administrator at its sole discretion without preliminary notification.</p>
\t\t<p>3.7. Upon registering an Account, data (such as username and certain Virtual Items) may be transferred between the KamaGames Software.</p>
\t\t<h4>User Content</h4>
\t\t<p>4.1. We expressly reserve the right, but have no obligation, to:</p>
\t\t<ul>
\t\t\t<li>(a) monitor any communications within the Sites, forums, chats or other channels, without limitation, to ensure that appropriate standards of online conduct are being observed; and</li>
\t\t\t<li>(b) at any time remove any content that we deem objectionable or unsuitable at our sole discretion. KamaGames does not endorse, approve, or pre-screen any Content that you or other users post or communicate in connection with use of any of KamaGames’ Services. KamaGames does not assume any responsibility or liability for any Content that is generated, posted or communicated by any user in connection with use of any of KamaGames’ Services. You agree to release from liability and/or indemnify KamaGames, its Affiliates and each of their respective employees, contractors, officers, directors, shareholders, agents, representatives, vendors, and Content providers from any damages arising out of, or resulting from, any Content you post or communicate.</li>
\t\t</ul>
\t\t<p>4.2. You acknowledge and agree that your submitted Content, including your reviews and your communications with other users via online messaging, forums or bulletin boards, and any other similar types of communications and submissions in regard of use of any of KamaGames’ Services, are non-confidential, public communications, and you have no expectation of privacy concerning such communications. You acknowledge that personal information that you communicate publicly may be seen and used by others and may result in unsolicited communications. KamaGames is not liable for any information that you choose to submit or communicate to other users, or for the actions of any other users.</p>
\t\t<p>4.3. You represent and warrant that you have all necessary rights in and to any Content that you post, that such Content do not infringe any proprietary or other rights of third parties, that all such content is accurate and will not cause injury to any person or entity, and that you will not hold liable and/or indemnify KamaGames and its Affiliates and their respective employees, contractors, officers, directors, shareholders, agents, representatives, vendors, and content providers for all claims resulting from your submitted and posted Content. If any such Content incorporates the name, voice, likeness and/or image of any individual, you represent and warrant that you have the right to grant KamaGames permission to use any such name, voice, likeness and/or image of such individual appearing in the Content you post throughout the world in perpetuity. Once you post or communicate any Content in the Sites or Software, you expressly grant KamaGames the complete, worldwide, fully sublicensable and irrevocable right to quote, re-post, use, reproduce, modify, adapt, publish, translate, create derivative works from, display, distribute, transmit, and broadcast such Content, including, without limitation, the name you submit in connection with such Content, in any form, with or without attribution to you, and without any notice or compensation to you of any kind. We reserve the right to immediately remove any content that may be considered, at our sole discretion, in violation of the rights of any third party.</p>
\t\t<p>4.4. It is KamaGames’ policy to respond to notices of alleged copyright infringement that comply with the Digital Millennium Copyright Act (DMCA). If you believe that your copyrighted work has been copied, reproduced, displayed, duplicated, performed, distributed, or otherwise infringed without your authorization and is available on any Sites or in any KamaGames game in a way that may constitute copyright infringement, you may provide notice of your claim to the following e-mail address: support@kamagames.com.</p>
\t\t<h4>Rules of Conduct</h4>
\t\t<p>As a condition of your use of the Services, and without limiting any other obligations under these Terms of Service, you agree to comply with the limitations and rules of use set forth in this Section as well as any additional restrictions or rules (such as application-specific rules) set forth. Any use of the Services in violation of these Rules of Conduct is strictly prohibited, can result in the immediate revocation of your limited license granted by Section 2 and may subject you to liability for violations of law.</p>
\t\t<p>5.1. ILLEGAL CONTENT. You agree that you will not transmit, make available or otherwise promote or support, under any circumstances:</p>
\t\t<ul>
\t\t\t<li>(a) any Content that is unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, pornographic, sexual, libellous, invasive of another’s privacy, hateful, discriminatory, disparaging or otherwise objectionable or inappropriate;</li>
\t\t\t<li>(b) any abusive, offensive, or defamatory screen names and/or avatars;</li>
\t\t\t<li>(c) any Content that promotes illegal activity, such as drug use;</li>
\t\t\t<li>(d) any Content that infringes any patent, trademark, trade secret, copyright or other intellectual property, proprietary or other rights of any party;</li>
\t\t\t<li>(e) any unsolicited or unauthorized advertising, promotional Content, junk mail, spam, chain letters, pyramid schemes, or any other form of solicitation;</li>
\t\t\t<li>(f) any spyware, passive collection mechanism or any other code or Content that acts as passive or active information collection or transmission mechanism;</li>
\t\t\t<li>(g) cheats, hacks, cracks, malicious programs, viruses or any other computer code, files or programs that have the effect of or are intended to modify, impair, disrupt, destroy, interfere with or limit the functionality of the Service or any part thereof, take control of any computer software, hardware or telecommunications equipment or interrupt any user’s uninterrupted use and enjoyment of the Services;</li>
\t\t\t<li>(h) unreleased Services content (such as in-game items or equipment), areas that have been unlocked by hacking into client data files, or data not available through normal operation or game play on any part of the Services.</li>
\t\t</ul>
\t\t<p>5.2. ILLEGAL ACTIVITY. You agree that you will not, under any circumstances:</p>
\t\t<ul>
\t\t\t<li>(i) organize, effectuate or participate in any activity, group, guild that is harmful, abusive, hateful, racially, ethnically, religiously or otherwise offensive, obscene, threatening, bullying, vulgar, sexually explicit, defamatory, infringing, invasive of personal privacy or publicity rights, encourages conduct that would violate a law or in a reasonable person’s view, objectionable and/or inappropriate and defraud or mislead KamaGames or other users or otherwise engage in any suspicious activity;</li>
\t\t\t<li>(j) promote, encourage or take part in any activity involving hacking, cracking, phishing, taking advantage of exploits or cheats and/or distribution of counterfeit software and/or virtual currency/items;</li>
\t\t\t<li>(k) interfere with, disrupt or circumvent any security feature of the Services or any feature that restricts or enforces limitations on use of or access to the Services;</li>
\t\t\t<li>(l) upload files that contain a virus, worm, spyware, time bombs, corrupted data or other computer programs that may damage, interfere with or disrupt KamaGames’ Services;</li>
\t\t\t<li>(m) interfere with or disrupt the Services, servers or networks connected to the Services, or disobey any requirements, procedures, policies or regulations of networks connected to the Services;</li>
\t\t\t<li>(n) create any Account by automated means or false pretences, create more than one Account per device, or use any other user’s Account for any purpose, including to circumvent a suspension or ban;</li>
\t\t\t<li>(o) cheat or use, develop or distribute automation software programs bots, macro software programs or other cheat utility software program or applications which are designed to modify the experience to the detriment of fair play;</li>
\t\t\t<li>(p) use the Services to intentionally or unintentionally violate any applicable local, state, national or international law;</li>
\t\t\t<li>(q) abuse or exploit bugs, undocumented features, design errors or problems in the game; sublicense, rent, lease, sell, trade, gift, bequeath or otherwise transfer your Account or any Virtual Items or Virtual Currency associated with your Account to anyone without KamaGames’ written permission;</li>
\t\t\t<li>(s) use the Services to engage in any commercial activity, including without limitation any attempt to raise money for any party or any purpose or advertise, promote or attempt to trade or sell a website, pyramid scheme, multi-tiered marketing scheme or any other product or service of any kind;</li>
\t\t</ul>
\t\t<p>5.3. These Rules of Conduct are non-exhaustive, and KamaGames reserves the right to determine what conduct it considers to be in violation of the rules of use or otherwise outside the spirit of the Services and to take action — up to and including termination of a User Account and exclusion from further participation in the Services.</p>
\t\t<h4>Fees and Purchase Terms</h4>
\t\t<p>6.1. Certain areas of the Sites and the Services may charge fees to purchase a license to access and acquire certain virtual game items or participate in game activities on the Sites or other platforms. You can license Virtual Items by visiting the purchase page in one of our games, providing your billing information, confirming the particulars of your purchase and re-affirming your agreement to these Terms. You agree to pay all fees and applicable taxes incurred by you or anyone using an Account registered to you. YOU ACKNOWLEDGE THAT KAMAGAMES IS NOT REQUIRED TO PROVIDE A REFUND FOR ANY REASON, AND THAT YOU WILL NOT RECEIVE MONEY OR OTHER COMPENSATION FOR UNUSED VIRTUAL ITEMS WHEN AN ACCOUNT IS CLOSED, WHETHER SUCH CLOSURE WAS VOLUNTARY OR INVOLUNTARY.</p>
\t\t<p>6.2. KamaGames may, from time to time, modify, amend, or supplement its fee and billing methods, and post those changes in this Agreement or elsewhere on the Sites. Such modifications, amendments or supplements shall be effective immediately upon posting on the site. If any change is unacceptable to you, you may cancel your account at any time.
\t\t</p>
\t\t<h4>Deactivation/Termination of Your Registration or Use</h4>
\t\t<p>7.1. KamaGames and you each have the right to terminate or cancel any of your KamaGames account(s) at any time for any reason. You understand and agree that cancellation of your KamaGames account(s) and/or ceasing use of any and all of KamaGames Services is your sole right and remedy with respect to any dispute with KamaGames.</p>
\t\t<p>7.2. If you violate these Terms of Service, KamaGames may issue you a warning regarding the violation, or, in KamaGames’ sole discretion, immediately block or terminate any and all KamaGames Accounts and/or cancel access to the KamaGames Services. You acknowledge that KamaGames is not required to provide you with any notice or warning prior to any such cancellation under this Section.</p>
\t\t<h4>Third Party Sites</h4>
\t\t<p>8.1. Clicking a hyperlink may direct you away from the Sites or the Software. KamaGames does not endorse or control any third-party linked sites, is not responsible for their content and has no association with the owners or operators of such sites. Also, other sites may have different terms of service and different privacy policies.</p>
\t\t<p>8.2. Your correspondence or business dealings with, or participation in promotions of, advertisers found on or through the Sites or Software, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such advertiser. You agree that KamaGames shall not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of such advertisers on the Site.</p>
\t\t<h4>Communications</h4>
\t\t<p>9.1. Where the Applications or a Site utilizes the account management of a social networking web site (such as Facebook, Twitter, Game Spy or iTunes’ Game Centre) or a third-party payment provider, you are responsible for maintaining the confidentiality of your applicable web site passwords and account information; you are entirely responsible for all activities that occur under such accounts and compliance with such site’s terms of service and other policies.</p>
\t\t<p>9.2 By subscribing to our social networking outlets, such as our Twitter feed or Facebook fan pages, or setting up an Account, you understand that you may receive periodic information, by e-mail or other medium, regarding current and future Applications or changes to the Site. With respect to the third-party social networking outlets, you understand that you can use the settings of the applicable social network to stop or limit the communications we send through such social network outlets.</p>
\t\t<p>9.3. Should you subscribe to receive information via electronic communications, you may opt out of any such communications we send to you by following the unsubscribe procedure which will be located within any correspondence. By omitting to unsubscribe you will be presumed to have opted into future correspondence.</p>
\t\t<p>9.4. Most of our Applications are capable of sending you notifications, known as pop-ups and push notifications and will send you such notifications to alert you to in-Application activity as well as promotions and sales for in-Application content. You may choose to not receive or stop receiving such notifications by either playing in an off-line mode or a changing your device settings to decline or disable such notifications.</p>
\t\t<h4>Intellectual Property Rights</h4>
\t\t<p>10.1. KamaGames reserves all right, title and interest in the Sites and the Software and all associated copyrights, trademarks, and other intellectual property rights therein that are not expressly granted to you in these Terms of Service.</p>
\t\t<p>10.2. The names, logos, and other graphics, icons, and service names associated with the KamaGames Sites or Services, trademarks, registered trademarks or trade dress of KamaGames or its licensors or Affiliates in the United States and/or other countries is the property of KamaGames or its Affiliates. KamaGames’ trademarks and trade dress may not be used in connection with any product or service that is not owned or operated by or on behalf of KamaGames or its Affiliates, or in any manner that is likely to cause confusion among consumers or that disparages or discredits KamaGames, its Affiliates, or any of KamaGames’ Services. The compilation of all content of the KamaGames Sites, other websites, applications and games is the exclusive property of KamaGames or its Affiliates and is protected by United States and international copyright laws. You may not use, copy, transmit, modify, distribute, or create any derivative works from any content from the KamaGames Sites or Software unless we have expressly authorized you to do so in writing. If you fail to adhere to these Terms of Service, other content owners may take criminal or civil action against you. In the event legal action is taken against you for your acts and/or omissions with regard to any content of the KamaGames Sites or Software, you agree to indemnify and hold harmless KamaGames and its Affiliates and their respective employees, contractors, officers, directors, shareholders, agents, representatives, vendors, and content providers.</p>
\t\t<h4>Acknowledgements/Limitation on Warranty and Liability</h4>
\t\t<p>11.1. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, YOU EXPRESSLY AGREE THAT THE USE OF KAMAGAMES SERVICES, KAMAGAMES SOFTWARE, AND THE INTERNET IS AT YOUR SOLE RISK. KAMAGAMES SERVICES, KAMAGAMES SOFTWARE, KAMAGAMES PRODUCTS AND THIRD-PARTY SERVICES AND PRODUCTS ARE PROVIDED ON AN «AS IS» AND «AS AVAILABLE» BASIS FOR YOUR USE, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, UNLESS SUCH WARRANTIES ARE LEGALLY INCAPABLE OF EXCLUSION. NO WARRANTY IS GIVEN ABOUT THE QUALITY, FUNCTIONALITY, AVAILABILITY OR PERFORMANCE OF KAMAGAMES SOFTWARE OR KAMAGAMES SERVICES. KAMAGAMES DOES NOT ASSUME LIABILITY FOR INABILITY TO OBTAIN OR USE ANY CONTENT, ENTITLEMENTS, GOODS OR SERVICES. KAMAGAMES PROVIDES KAMAGAMES SERVICES ON A COMMERCIALLY REASONABLE BASIS AND DOES NOT GUARANTEE THAT YOU WILL BE ABLE TO ACCESS OR USE KAMAGAMES SERVICES AT TIMES OR LOCATIONS OF YOUR CHOOSING, OR THAT KAMAGAMES WILL HAVE ADEQUATE CAPACITY FOR KAMAGAMES SERVICES AS A WHOLE OR IN ANY SPECIFIC GEOGRAPHIC AREA.</p>
\t\t<p>11.2. YOU ACKNOWLEDGE AND AGREE THAT KAMAGAMES AND AFFILIATES ARE NOT LIABLE FOR ANY ACT OR FAILURE TO ACT BY THEM OR ANY OTHER PERSON REGARDING CONDUCT, COMMUNICATION OR CONTENT ON KAMAGAMES SERVICES OR USE OF KAMAGAMES SOFTWARE. IN NO CASE SHALL KAMAGAMES’ OR ITS LICENSORS’, LICENSEES’, AFFILIATES’, EMPLOYEES’, OFFICERS’, OR DIRECTORS’ LIABILITY TO YOU EXCEED THE AMOUNT THAT YOU PAID TO KAMAGAMES FOR KAMAGAMES SERVICES. IN NO CASE SHALL KAMAGAMES’ LIABILITY TO YOU EXCEED \$100 (ONE HUNDRED) US DOLLARS. IN NO CASE SHALL KAMAGAMES OR KAMAGAMES’ AFFILIATES BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING FROM YOUR USE OF KAMAGAMES SERVICES, KAMAGAMES SOFTWARE, THE INTERNET OR FOR ANY OTHER CLAIM RELATED IN ANY WAY TO YOUR USE OF KAMAGAMES SERVICES OR ACCOUNTS. WHILE KAMAGAMES USES COMMERCIALLY REASONABLE MEANS TO PROTECT YOUR PERSONAL INFORMATION, KAMAGAMES AND ITS LICENSORS ASSUME NO LIABILITY FOR LOSS OF DATA, DAMAGE CAUSED TO YOUR SOFTWARE OR HARDWARE, AND ANY OTHER LOSS OR DAMAGE SUFFERED BY YOU OR ANY THIRD PARTY, WHETHER DIRECT, INDIRECT, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL AND, HOWEVER, ARISING, AS A RESULT OF ACCESSING OR USING ANY KAMAGAMES SERVICE, CONTENT, KAMAGAMES SOFTWARE TO YOUR COMPUTER AND/OR DEVICE.
\t\t</p>
\t\t<p>11.3. BECAUSE SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR THE LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, IN SUCH STATES OR JURISDICTIONS, KAMAGAMES, KAMAGAMES’ LICENSORS AND KAMAGAMES’ AFFILIATES LIABILITY SHALL BE LIMITED TO THE FULL EXTENT PERMITTED BY LAW.</p>
\t\t<h4>Indemnification</h4>
\t\t<p>12.1. Upon KamaGames’ request, you agree to defend, indemnify and hold harmless KamaGames and KamaGames Affiliates, contractors, vendors, and content providers from all liabilities, claims and expenses, including attorneys’ fees, that arise from or relate to a breach of these Terms of Service for which you are responsible or in connection with your distribution of any Content on or through KamaGames’ Services. Without limiting the generality of the foregoing, you agree to indemnify and hold KamaGames harmless for any improper or illegal use of your Account, including the illegal or improper use of your Account by someone to whom you have given permission to use your Account. You agree that you will be personally responsible for your use of KamaGames Services and for all of your communication and activity on KamaGames Services, including any Content you contribute, and that you will indemnify and hold harmless KamaGames and KamaGames’ Affiliates from any liability or damages arising from your conduct on KamaGames’ Services, including any Content that you contribute.</p>
\t\t<p>12.2. KamaGames reserves the right, at their own expense, to assume the exclusive defence and control of any matter otherwise subject to indemnification by you. In that event, you shall have no further obligation to provide indemnification to KamaGames in that matter. This Section shall survive termination of this Terms of Service.</p>
\t\t<h4>Dispute Resolution</h4>
\t\t<p>If a dispute arises between you and KamaGames, our goal is to provide you with a neutral and cost effective means of resolving the dispute quickly. Accordingly, you and KamaGames agree that we will resolve any claim or controversy at law or equity that arises out of this Agreement or the Service (a «Claim») in accordance with one of the subsections below or as KamaGames and you otherwise agree in writing.
\t\t</p>
\t\t<p>13.1. Informal Negotiations/Notice of Dispute. You and KamaGames agree to first attempt to negotiate any Dispute informally for at least 30 days before initiating arbitration. Such informal negotiations commence upon receipt of written notice from one person to the other (Notice of Dispute). Notices of Dispute must:</p>
\t\t<ul>
\t\t\t<li>(a) include the full name and contact information of the complaining party;</li>
\t\t\t<li>(b) describe the nature and basis of the claim or dispute; and</li>
\t\t\t<li>(c) set forth the specific relief sought (Demand). KamaGames will send its Notice of Dispute to your billing address (if you provided it to us) or to the email address you provided to us. You will send your Notice of Dispute to: 110 Amiens street, Dublin 1, Ireland, ATTENTION: Legal Department.</li>
\t\t</ul>
\t\t<p>13.2. Arbitration Option. For any claim (excluding claims for injunctive or other equitable relief) where the total amount of the award sought is less than \$10,000, the party requesting relief may elect to resolve the dispute in a cost effective manner through binding non-appearance-based arbitration. In the event a party elects arbitration, they shall initiate such arbitration through an established alternative dispute resolution (ADR) provider mutually agreed upon by the parties. The ADR provider and the parties must comply with the following rules:</p>
\t\t<ul>
\t\t\t<li>(a) the arbitration shall be conducted by telephone, online and/or be based on written submissions, and the specific manner shall be chosen by the party initiating the arbitration;</li>
\t\t\t<li>(b) the arbitration shall not involve any personal appearance by the parties or witnesses unless otherwise mutually agreed by the parties; and</li>
\t\t\t<li>(c) any judgment on the award rendered by the arbitrator shall be final and may be entered in any court of competent jurisdiction.</li>
\t\t</ul>
\t\t<p>13.3. In case such dispute, controversies or differences, cannot be settled amicably through negotiations within a thirty 30-day period, and the Demand is in excess of \$10,000 USD, it or they shall be shall be settled by arbitration in accordance with the UNCITRAL Arbitration Rules.</p>
\t\t<p>The appointing authority shall be the Chairman of Chartered Institute of Arbitrators, Ireland.</p>
\t\t<p>The number of arbitrators shall be one.</p>
\t\t<p>The place of arbitration shall be Dublin, Ireland.</p>
\t\t<p>The arbitration shall be held, and the award rendered, in English.</p>
\t\t<p>Each Party shall bear its own expenses, but Parties shall share equally in the expenses of the arbitration tribunal.</p>
\t\t<p>The Parties agree that all arbitration proceedings conducted pursuant to this Section shall be kept strictly confidential, and all information disclosed in the course of such arbitration proceedings shall be used solely for the purpose of those proceedings. Any election to arbitrate by one party shall be final and binding on the other.</p>
\t\t<p>13.4. Restrictions. You and KamaGames agree that any arbitration shall be limited to a dispute between you and KamaGames individually. To the full extent permitted by law</p>
\t\t<ul>
\t\t\t<li>(1) no arbitration shall be joined with any other;</li>
\t\t\t<li>(2) there is no right or authority for any dispute to be arbitrated on a class action-basis or to utilize class action procedures; and</li>
\t\t\t<li>(3) there is no right or authority for any dispute to be brought in a purported representative capacity on behalf of the general public or any other persons. YOU AND KAMAGAMES AGREE THAT EACH MAY BRING CLAIMS AGAINST THE OTHER ONLY IN YOUR OR ITS INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS OR REPRESENTATIVE PROCEEDING. Further, unless both you and KamaGames agree otherwise, the arbitrator may not consolidate more than one person’s claims, and may not otherwise preside over any form of a representative or class proceeding.</li>
\t\t</ul>
\t\t<h4>Miscellaneous</h4>
\t\t<p>14.1. Entire Agreement. These Terms of Service, any supplemental policies and any documents expressly incorporated by reference herein constitutes the entire agreement between you and KamaGames hereto with respect to the subject matter hereof and supersedes all prior understandings or arrangements, oral or written, between you and us with respect to the subject matter hereof.</p>
\t\t<p>14.2. Waiver. The failure of KamaGames of require or enforce strict performance by you of any provision of these Terms of Service or failure to exercise any right shall not be construed as a waiver or relinquishment of KamaGames’ right to assert or rely upon any such provision or right in that or any other instance. The express waiver by KamaGames of any provision, condition, or requirement of these Terms of Service shall not constitute a waiver of any future obligation to comply with such provision, condition or requirement. Except as expressly and specifically set forth in this these Terms of Service, no representations, statements, consents, waivers, or other acts or omissions by KamaGames shall be deemed a modification of these Terms of Service nor be legally binding, unless documented in physical writing, hand signed by you and a duly appointed officer of KamaGames.</p>
\t\t<p>14.3. Notice. We may notify you via postings on the Sites, e-mail or any other communications means of contact you have provided to KamaGames. All notices given by you or required from you under these Terms of Service or the KamaGames Privacy Policy shall be in writing and addressed to: KamaGames, Attn: LEGAL DEPARTMENT, 110 Amiens street, Dublin 1, Ireland. Any notices that you provide without compliance with this Section on Notices shall have no legal effect.</p>
\t\t<p>14.4. Severability. The invalidity, illegality or unenforceability of any provision of this Agreement shall not affect the validity, legality or enforceability of any other provisions. This Agreement shall continue in full force and effect except for any such invalid, illegal or unenforceable provision.</p>
\t\t<p>14.5. Force Majeure. KamaGames shall not be liable for any delay or failure to perform resulting from causes outside the reasonable control of KamaGames, including, without limitation, any failure to perform hereunder due to unforeseen circumstances or cause beyond KamaGames’ control such as acts of God, war, terrorism, riots, embargoes, acts of civil or military authorities, fire, floods, accidents, network infrastructure failures, strikes, or shortages of transportation facilities, fuel, energy, labour or Content.
\t\t</p>

\t\t<h3>Privacy Policy</h3>

\t\t<p>KamaGames’ Privacy Policy has been adopted to explain how we store and use the information collected about you online on our websites, during your use of our products and/or services and on mobile platforms. KamaGames, its subsidiaries and Affiliates respect the privacy rights of Users and recognize the importance of protecting the information collected. By accepting our Privacy Policy and Terms of Service, you consent to our collection, storage, use and disclosure of your personal information as described in this Privacy Policy.</p>
\t\t<p>If you have any questions or concerns, please contact our Support Team at support@kamagames.com.</p>
\t\t<h4>Definitions</h4>
\t\t<p>Account: the account created when registering for Pokerist, Roulettist or any of KamaGames’ games.</p>
\t\t<p>Affiliates: refers to all KamaGames subsidiaries, parent companies, partners, joint ventures, licensees, licensors, and/or any of their agents, consultants, employees, shareholders, officers and directors and any related parties.</p>
\t\t<p>Content: refers to any data uploaded or transmitted through the Service by the user.</p>
\t\t<p>Sites: refers to the web domains at kamagmaes.com, pokerist.com, any other web domain hosted by KamaGames or any social media page connected with KamaGames Services hosted on Facebook, Twitter, YouTube or Instagram.</p>
\t\t<p>Services: refers to products, downloads, games, services, content and websites controlled by KamaGames and/or its Affiliates.</p>
\t\t<p>Software: refers to any games or software used or downloaded from any of the Services.</p>
\t\t<p>SNS: Third Party Social Networking Site.</p>
\t\t<p>Virtual Items: meaning virtual “currency” and “objects”, which may include, but are not limited to, virtual “coins”, “cash”, “tokens”, “points” and “property”.</p>
\t\t<h4>Introduction</h4>
\t\t<p>KamaGames and its owned or controlled subsidiaries and Affiliates may be collectively referred to herein as «KamaGames», «we», or «our», as the context requires. KamaGames is a developer and publisher of social games for the web and mobile devices. By using kamagames.com websites, KamaGames’ Services and/or Software, you signify your assent to KamaGames’ Privacy Policy and understanding that KamaGames may process your data in accordance with this Privacy Policy. IF YOU DO NOT AGREE TO THE FOREGOING, PLEASE DO NOT USE THE SITES OR APPLICATIONS. If you access our games from a third party Social Networking Site, you may be required to also read and accept the SNS Terms of Service and Privacy Policy.</p>
\t\t<h4>Information We Collect</h4>
\t\t<p>KamaGames collects both personal and non-personal User information. Personal information is information that identifies you and that may be used to contact you on-line or off-line.</p>
\t\t<p>2.1. KamaGames may collect personal information from our online Users during:</p>
\t\t<ul>
\t\t\t<li>Registration for games;</li>
\t\t\t<li>Registration for KamaGames and/or other service accounts;</li>
\t\t\t<li>User support and/or technical service requests;</li>
\t\t\t<li>Player match up and other head-to-head online competitions and/or special game-specific events;</li>
\t\t\t<li>Contest registration and prize acceptance;</li>
\t\t\t<li>Creation of a personal profile;</li>
\t\t\t<li>Access to our products and/or services on social networks or other third party services;</li>
\t\t\t<li>Otherwise through use of our software, mobile or online service where personal information is required for use and/or participation.</li>
\t\t</ul>
\t\t<p>Information collected will vary depending upon the activity and may include your name, email address, phone number, home address, birth date, mobile phone number and credit card information. You can choose not to provide (when such opportunity is provided) certain information, but then you might not be able to take advantage of many of our features.</p>
\t\t<p>2.2. Non-personal information, alone, cannot be used to identify or contact you. KamaGames collects non-personal information about your use of our online and mobile products and services both on our websites and in the course of game play and Software usage.</p>
\t\t<p>When you use KamaGames online and mobile products and Services, we may collect certain non-personal information including gender, zip code, information about your computer, hardware, software, platform, game system, media, mobile device, including device IDs, incident data, Internet Protocol (IP) address and other information.</p>
\t\t<p>When you browse certain portions of the Services, we may automatically collect and record standard user information and clickstream data about your visit (through the use of cookies and otherwise) as part of our analysis of the use of the Service. For example, when you visit or request a web page from our server, we may record the IP address of the user’s machine, the time, date and URL of the request and information about the browser being used, and other information regarding the visit, such as the pages visited on the Sites, the time spent on each page, any advertisements clicked and any search terms used (collectively «Log Data»).</p>
\t\t<p>2.3. When you use any KamaGames game at an SNS such as Facebook or Google +, you allow KamaGames to access certain information from you profile for that site. The information you allow KamaGames to access varies by game and SNS, and it is affected by the privacy settings you establish at the SNS. You can control and find our more about these settings at the SNS where you play our game. By playing a KamaGames game through an SNS, you are authorizing KamaGames to collect, store, and use in accordance with this Privacy Policy any and all information that you agreed the SNS could provide to KamaGames through the SNS Application Programming Interface. Your agreement takes place when you accept or allow (or similar terms) one of our applications on an SNS.</p>
\t\t<p>KamaGames stores information about you on servers located in the United States. Personal data collected within the European Union may be transferred to, and stored at, a destination outside the European Union. Data we collect may also be processed by staff operating outside the US and the EU who works for us or for one of our suppliers. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.
\t\t</p>
\t\t<h4>How KamaGames Uses Information Collected</h4>
\t\t<p>KamaGames uses your information only as follows:</p>
\t\t<ul>
\t\t\t<li>To provide our products and services to you and fulfil your specific requests;</li>
\t\t\t<li>To send you electronic communications and messages about our products and services, new products, features, enhancements, special offers, upgrade opportunities, contests and events of interest;</li>
\t\t\t<li>To enable user-to-user communications;</li>
\t\t\t<li>To better understand the behaviour and preferences of our Users, to troubleshoot technical problems, to deliver advertising, for the purposes of proper functioning of our products and services and improving them;</li>
\t\t\t<li>To improve our software and services by providing experiences and recommendations, language and location customization, personalized help and instructions.</li>
\t\t</ul>
\t\t<h4>How We Disclose Information Collected</h4>
\t\t<p>We disclose your information to third parties, that is parties other than KamaGames, in the following circumstances:</p>
\t\t<p>5.1. We may share your information with subsidiaries and Affiliates that we own or control, but only if those entities are either subject to this Privacy Policy or follow practices at least as protective as those described in this Privacy Policy.</p>
\t\t<p>5.2. From time to time KamaGames employs third party contractors to collect personal information on our behalf to provide email delivery, product, prize or promotional fulfilment, contest administration, credit card processing, shipping or other services on our sites. We ask some third party contractors, such as credit agencies, data analytics or market research firms, to supplement personal information that you provide to us for our own studies. When our third party agents or service providers collect and/or have access any information other than non-personal, anonymous and/or aggregated data, KamaGames requires that they use data consistently with our stated privacy policies and protect the confidentiality of personal information they collect or have access to in the course of their engagement by KamaGames. These third parties are prohibited from using your personal information for any other purpose without your specific consent.</p>
\t\t<p>5.3. The Service supports and may encourage interaction among players. Your SNS friends may see your name, profile photo and descriptions of your game activity, your game profile. Other players may also be able to send you game requests or even friend requests through the related SNS’ communication channels.</p>
\t\t<p>5.4. KamaGames may share aggregated information (information about you and other users collectively), anonymous information and certain technical information (including IP Addresses, MAC Addresses for mobile devices and mobile device IDs) to develop and deliver targeted advertising in the service and on the websites of third parties. We may also allow advertisers to collect this information within the Service and they may share it with Us.</p>
\t\t<p>5.5. We reserve the right to disclose your information when we believe release is appropriate to comply with the law, judicial proceedings, court order or other legal process; enforce or apply our Terms of Service and other agreements; or protect the rights, property, or safety of KamaGames, our employees, our users, or others. This includes exchanging information with other companies and organizations for fraud protection and credit risk reduction purposes.</p>
\t\t<p>5.6. In the event that KamaGames undergoes a business transition, such as a merger, acquisition by another company, or sale of all or a portion of its assets, we may transfer all of your information, including personal information, to the successor organization in such transition.</p>
\t\t<h4>Children</h4>
\t\t<p>KamaGames does not sell products for purchase by children and does not knowingly collect personal information from children. We sell children’s products for purchase by adults. If you are under the age of 13, you may use our Sites and Services only with the involvement of a parent or guardian.</p>
\t\t<h4>How Does KamaGames Protect Your Information?</h4>
\t\t<p>KamaGames understands the importance of keeping your information safe and secure. KamaGames will make commercially reasonable precautions to protect your personal information and ensure the security of our systems. However, there is no 100% secure method of transmission over the Internet or method of electronic storage. We cannot assume responsibility or liability for unauthorized access to our servers and systems. It is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off when finished using a shared computer. We strongly recommend that you implement measures to protect your computer from unauthorized access such as firewall protection and anti-virus software.</p>
\t\t<h4>How You Can Access and Update Your Information</h4>
\t\t<p>8.1. You can correct or update your account information at any time by logging on our sites and navigating to different account settings. Should you be unable to log in or wish to have your account(s) deactivated, you may send an e-mail to our Support Team at support@kamagames.com. You can put «Delete my Account» or «Update my information» in the subject line and include your first name, last name, e-mail address and user ID number (if applicable) in the body of the e-mail. We will be happy to review, update or remove information as appropriate. We may still retain your information in our files however, to resolve disputes, enforce our user agreement, and due to technical and legal requirements and constraints related to the security, integrity and operation of our websites.</p>
\t\t<p>8.2. If you have granted KamaGames access to your SNS account information through a KamaGames’ application, you may manage the information KamaGames receives about you from an SNS. For this purpose you will need to follow the instructions at that site for updating your information and changing your privacy settings.</p>
\t\t<p>8.3. You also may have the right (subject to certain exceptions to be given a copy of certain data that KamaGames may hold about you (and for which we may charge a small fee to process) and to have any inaccuracies in the data that KamaGames holds about you corrected and/or erased. You may exercise any such rights by emailing our Support Team at support@kamagames.com. We will need sufficient information from you to establish you identity and to verify your access request, and also to assist us in handling your request. Requests will be processed as soon as possible and not later than 40 days.</p>
\t\t<h4>Terms of Service and Notices</h4>
\t\t<p>If you choose to visit a Site, your visit and any dispute over privacy is subject to this Policy and our Terms of Service. If you have any concerns about your privacy, please contact us with a thorough description or your question or concern via the postal addresses or email address in the Section 10 of this Privacy Policy, and we will try to resolve it.</p>
\t\t<h4>Changes to Our Privacy Policy</h4>
\t\t<p>If we decide to make material changes to our Privacy Policy, we will notify you and other users by placing a notice on kamagames.com or by sending you a notice to the e-mail address we have on file for you. We may supplement this process by placing notices in our games and on other KamaGames websites. You should periodically check www.kamagames.com and this privacy policy page for updates.</p>
\t\t<h4>Contact Us</h4>
\t\t<p>If you have any questions about the privacy aspects of our products or services, or would like to make a complaint having to do with a privacy matter, please send a written request as follows:</p>
\t\t<p>KamaGames <br/>Attention: Data Privacy <br/>110 Amiens street, Dublin 1, Ireland</p>

\t</div>
</section>


<section class=\"help-section\">
\t<h2 class=\"title\">Contact Us</h2>
\t<div class=\"content\">
\t\t<p> If you could not find what you're looking for and require further assistance, feel free to contact our Customer Support team. </p>
\t</div>
</section>
</div>
</div>

<script src=\"/js/main.js\"></script>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "/views/games/slots-help.twig";
    }

    public function getDebugInfo()
    {
        return array (  31 => 12,  28 => 9,  19 => 1,);
    }
}
