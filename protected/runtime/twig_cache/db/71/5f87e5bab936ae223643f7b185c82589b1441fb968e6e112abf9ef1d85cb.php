<?php

/* /views/feedback/contact-us.twig */
class __TwigTemplate_db715f87e5bab936ae223643f7b185c82589b1441fb968e6e112abf9ef1d85cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "        <div class=\"wrapper\">
                <a class=\"pages__logo\" href=\"/\"></a>
        </div>
\t<div class=\"pages pages_bg\">
\t\t
                <div class=\"wrapper\">
\t\t\t<div class=\"text_block press\">
\t\t\t\t<h1>";
        // line 10
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "form-title"), "method");
        echo "</h1>
                                <span class=\"pages-line\"></span>
\t\t\t\t<p class=\"pages-description\">";
        // line 12
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "form-text"), "method");
        echo "</p>

                                <div class=\"feedback\">

                                        ";
        // line 16
        if ((isset($context["support_success"]) ? $context["support_success"] : null)) {
            // line 17
            echo "                                                <div class=\"feedback__message\">
                                                        <p class=\"feedback__text\">
                                                                ";
            // line 19
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "form-success"), "method");
            echo "
                                                        </p>
                                                </div>
                                        ";
        } else {
            // line 23
            echo "                                                <div class=\"feedback__form\">
                                                        ";
            // line 24
            echo twig_include($this->env, $context, "feedback/feedback.twig", array("message" => (isset($context["message"]) ? $context["message"] : null)));
            echo "
                                                </div>
                                        ";
        }
        // line 27
        echo "
                                </div>
                                ";
        // line 29
        echo twig_include($this->env, $context, "main/facebook-container.twig");
        echo "       
                                
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"wrapper\">

\t\t";
        // line 37
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/feedback/contact-us.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 37,  86 => 29,  82 => 27,  76 => 24,  73 => 23,  66 => 19,  62 => 17,  60 => 16,  53 => 12,  48 => 10,  39 => 3,  36 => 2,  11 => 1,);
    }
}
