<?php

/* /views/admin/adminPressItem.twig */
class __TwigTemplate_37d01eda66708675cd9c5d89540851a8c8ac7813883f0d6022fb6a47fad98d48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/admin.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $context["form"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginWidget", array(0 => "CActiveForm", 1 => array("htmlOptions" => array("enctype" => "multipart/form-data"), "enableAjaxValidation" => false)), "method");
        // line 5
        echo "
\t<div class=\"row\">
\t\t<div class=\"col-sm-6\">
\t\t\t";
        // line 8
        echo twig_include($this->env, $context, "common/form/text.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "alias"));
        echo "
\t\t</div>
\t\t<div class=\"col-sm-6\">
\t\t\t";
        // line 11
        echo twig_include($this->env, $context, "common/form/text.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "title"));
        echo "
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-sm-6\">
\t\t\t";
        // line 16
        echo twig_include($this->env, $context, "common/form/textarea.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "overview"));
        echo "
\t\t</div>
\t\t<div class=\"col-sm-6\">
\t\t\t";
        // line 19
        echo twig_include($this->env, $context, "common/form/textarea.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "content"));
        echo "
\t\t</div>
\t</div>
\t\t
\t<div class=\"row\">
\t\t<div class=\"col-sm-6\">
\t\t\t";
        // line 25
        echo twig_include($this->env, $context, "common/form/textarea.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "keywords"));
        echo "
\t\t</div>
\t\t<div class=\"col-sm-6\">
\t\t\t";
        // line 28
        echo twig_include($this->env, $context, "common/form/textarea.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "description"));
        echo "
\t\t</div>
\t</div>

\t<div class=\"row\">
\t\t<div class=\"col-sm-6\">
\t\t\t";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "images", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 35
            echo "\t\t\t\t<img src=\"";
            echo $context["image"];
            echo "\" alt=\"\">
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "\t\t\t";
        echo twig_include($this->env, $context, "common/form/file.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "images[]"));
        echo "
\t\t</div>
\t\t<div class=\"col-sm-6\">
\t\t\t";
        // line 40
        echo twig_include($this->env, $context, "common/form/text.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "publicationDate", "htmlOptions" => array("class" => "form-control js-datepicker")));
        echo "
\t\t</div>
\t</div>

\t<div class=\"form-group\">
\t\t";
        // line 45
        echo twig_include($this->env, $context, "common/form/checkbox.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "active", "htmlOptions" => (($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "isNewRecord", array())) ? (array("checked" => true)) : (array()))));
        echo "
\t</div>

\t<button class=\"btn btn-primary\">Save</button>
\t";
        // line 49
        $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endWidget", array(), "method");
    }

    public function getTemplateName()
    {
        return "/views/admin/adminPressItem.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 49,  119 => 45,  111 => 40,  104 => 37,  95 => 35,  91 => 34,  82 => 28,  76 => 25,  67 => 19,  61 => 16,  53 => 11,  47 => 8,  42 => 5,  39 => 4,  36 => 3,  11 => 1,);
    }
}
