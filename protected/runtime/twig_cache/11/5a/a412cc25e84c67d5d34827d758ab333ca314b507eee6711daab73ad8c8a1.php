<?php

/* common/form/checkbox.twig */
class __TwigTemplate_115aa412cc25e84c67d5d34827d758ab333ca314b507eee6711daab73ad8c8a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"checkbox\">
\t";
        // line 2
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "checkbox", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null), 2 => ((array_key_exists("htmlOptions", $context)) ? (_twig_default_filter((isset($context["htmlOptions"]) ? $context["htmlOptions"] : null), array())) : (array()))), "method");
        echo "
\t";
        // line 3
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "label", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null)), "method");
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "common/form/checkbox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }
}
