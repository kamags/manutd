<?php

/* common/sign-in-form/email.twig */
class __TwigTemplate_b6a8851cba13327af8cbab59ff7bc7798ab0c5cf40ef956118bf3751a6400c71 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
";
        // line 3
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "emailField", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null), 2 => twig_array_merge(array("placeholder" => $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "getAttributeLabel", array(0 => (isset($context["field"]) ? $context["field"] : null)), "method")), (isset($context["htmlOptions"]) ? $context["htmlOptions"] : null))), "method");
        echo "
";
        // line 5
        echo "
";
    }

    public function getTemplateName()
    {
        return "common/sign-in-form/email.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 5,  22 => 3,  19 => 2,);
    }
}
