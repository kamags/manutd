<?php

/* /views/games/poker.twig */
class __TwigTemplate_8fb132ba065cc38d554ad4c66c592ac0ad6f8ff1795e957c82e03de9e239252f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "        <div class=\"wrapper\">
                <a class=\"pages__logo\" href=\"/\"></a>
        </div>
\t<div class=\"pages pages_bg\">
\t\t
                <div class=\"wrapper\">
\t\t\t<div class=\"text_block\">
\t\t\t\t<h1>About Manchester United Social Poker</h1>
                                <span class=\"pages-line\"></span>
                                <p class=\"pages-description\">Play Texas Hold’ Em for free with millions of football fans at any time, day or night and dive in to the world of risk, bets & victories.</p>
                                <p class=\"pages-description\">You can play on your Smartphone, Tablet or Online, making it easy to enjoy the thrills no matter where you are. 
                                    Interact socially with fellow supporters of Manchester United to win chips and claim true victory!  
                                    Bluff, up the stakes like a true poker star, improve your skills, gain experience and become the best player! – or just come on in and play to have a fun time with fellow MU fans!</p>
                                <h3>Screenshots</h3>
                                <div class=\"carousel\">
                                        <a class=\"carousel-button carousel-button-left\" href=\"#\"></a>
                                        <a class=\"carousel-button carousel-button-right\" href=\"#\"></a>
                                        <div class=\"carousel-wrapper\">
                                                <div class=\"carousel-items\">
                                                        <div class=\"carousel-block\">
                                                                <img src=\"/img/screenshots/poker/screenshot_01.jpg\" alt=\"\" widht=\"280\" height=\"158\"/>
                                                        </div>
                                                        <div class=\"carousel-block\">
                                                                <img src=\"/img/screenshots/poker/screenshot_02.jpg\" alt=\"\" widht=\"280\" height=\"158\"/>
                                                        </div>
                                                        <div class=\"carousel-block\">
                                                                <img src=\"/img/screenshots/poker/screenshot_03.jpg\" alt=\"\" widht=\"280\" height=\"158\"/>
                                                        </div>
                                                        <div class=\"carousel-block\">
                                                                <img src=\"/img/screenshots/poker/screenshot_04.jpg\" alt=\"\" widht=\"280\" height=\"158\"/>
                                                        </div>
                                                        <div class=\"carousel-block\">
                                                                <img src=\"/img/screenshots/poker/screenshot_05.jpg\" alt=\"\" widht=\"280\" height=\"158\"/>
                                                        </div>
                                                </div>
                                        </div>
                                </div>

                                <div class=\"game-available\">
                                        <h3>Available</h3> 
                                        <div class=\"ft-store\">
                                                <a target=\"_blank\" class=\"ft-store__item ft-store__item_ap\" href=\"https://itunes.apple.com/app/id947856678\"></a>
                                                <a target=\"_blank\" class=\"ft-store__item ft-store__item_gl\" href=\"https://play.google.com/store/apps/details?id=com.kamagames.texaspokermu\"></a>
                                                <a target=\"_blank\" class=\"ft-store__item ft-store__item_fb\" href=\"https://apps.facebook.com/manutdsocialpoker/\"></a>
                                                <a target=\"_blank\" class=\"ft-store__item ft-store__item_win\" href=\"http://www.windowsphone.com/en-us/store/app/manchester-united-social-poker/f7a9b108-b47a-4164-bd11-5f9fc1ff361e\"></a>
                                                <a target=\"_blank\"  class=\"ft-store__item ft-store__item_amazon\" href=\"http://www.amazon.com/KamaGames-Manchester-United-Social-Poker/dp/B00T9UPAAC/ref=sr_1_2?s=mobile-apps&amp;ie=UTF8&amp;qid=1424251869&amp;sr=1-2&amp;keywords=kamagames\"></a>
                                        </div>
                                </div>
                                
                                
                                <div class=\"game-features\">
                                        <div class=\"game-features__content\">
                                               <h3>Feature</h3>

                                               <ul class=\"game-features-list\">
                                                   <li class=\"game-features-list__item\">TOURNAMENTS – Participate in Sit‘n’Go and Shootout tournaments with the opportunity to win Manchester United prizes.</li>
                                                   <li class=\"game-features-list__item\">EASY-TO-PLAY INTERFACE – A simple and attractive user interface gives you the ability to call, fold or raise the stakes with just one tap.</li>
                                                   <li class=\"game-features-list__item\">NO REGISTRATION – Use guest mode to play poker without registration.</li>
                                                   <li class=\"game-features-list__item\">GIVE GIFTS – Share Manchester United Gifts with other players.</li>
                                                   <li class=\"game-features-list__item\">PLAY WITH FRIENDS – Reward yourself with bonuses by inviting your friends to play with you through e-mail or Facebook.</li>
                                                   <li class=\"game-features-list__item\">CHAT WITH OTHER PLAYERS – Use the convenient chat messaging system to brag about the hands you’ve won. Meet new friends and other players for even more fun.</li>
                                                   <li class=\"game-features-list__item\">LEARN TO PLAY – Are you new to Poker, but always wanted to try it? We’ll help you take the first step. Use the tutorial mode, which shows you all winning combinations and the rules of the game.</li>
                                                   <li class=\"game-features-list__item\">GET REWARDS – Up the stakes, win hands, go All-In and unlock achievements.</li>
                                                   <li class=\"game-features-list__item\">FREE CHIPS – Come back to play everyday and play with free chips.</li>
                                               </ul>
                                       </div>
                                     
                                       <div class=\"game-features__play\">
                                            <a target=\"_blank\" href=\"#casino\" class=\"btn play-now fancy-casino\">";
        // line 71
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "play-now-btn"), "method");
        echo "</a>
                                       </div>
                                </div>
                                
                                <div class=\"clear\"></div>
                                
                                ";
        // line 77
        echo twig_include($this->env, $context, "main/facebook-container.twig");
        echo "
                                
\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>


\t<div class=\"wrapper\">

\t\t";
        // line 87
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/games/poker.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 87,  118 => 77,  109 => 71,  39 => 3,  36 => 2,  11 => 1,);
    }
}
