<?php

/* main/lang.twig */
class __TwigTemplate_f87254e7e4776d18ebe159fec122f22fccb1e5922edbe2e1ef76990437dabf5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"header__lang\">
\t";
        // line 2
        $context["lang_name"] = $this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array());
        // line 3
        echo "\t";
        if (($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array()) == "en")) {
            // line 4
            echo "\t\t";
            $context["lang_name"] = "ENG";
            // line 5
            echo "\t";
        } elseif (($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array()) == "ru")) {
            // line 6
            echo "\t\t";
            $context["lang_name"] = "RUS";
            // line 7
            echo "\t";
        } elseif (($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array()) == "ko")) {
            // line 8
            echo "\t\t";
            $context["lang_name"] = "KOR";
            // line 9
            echo "\t";
        } elseif (($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array()) == "th")) {
            // line 10
            echo "\t\t";
            $context["lang_name"] = "THA";
            // line 11
            echo "\t";
        } elseif (($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array()) == "jp")) {
            // line 12
            echo "\t\t";
            $context["lang_name"] = "JPN";
            // line 13
            echo "\t";
        } elseif (($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array()) == "zh_cn")) {
            // line 14
            echo "\t\t";
            $context["lang_name"] = "CHS";
            // line 15
            echo "\t";
        } elseif (($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array()) == "zh")) {
            // line 16
            echo "\t\t";
            $context["lang_name"] = "CHT";
            // line 17
            echo "\t";
        }
        // line 18
        echo "\t<a href=\"#lang\" class=\"lang header__lang_button flag flag_";
        echo $this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array());
        echo " flag_usa js-flag\">";
        echo (isset($context["lang_name"]) ? $context["lang_name"] : null);
        echo "</a>
</div>";
    }

    public function getTemplateName()
    {
        return "main/lang.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 18,  66 => 17,  63 => 16,  60 => 15,  57 => 14,  54 => 13,  51 => 12,  48 => 11,  45 => 10,  42 => 9,  39 => 8,  36 => 7,  33 => 6,  30 => 5,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
