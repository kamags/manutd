<?php

/* feedback/fields/text.twig */
class __TwigTemplate_08c8d52ee0fd10daf8b52bedfe207d2a043852f0e9ea0c66895e263acadaba29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["error"] = $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "error", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null), 2 => array("class" => "help-block")), "method");
        // line 2
        echo "
<div class=\"form__row  ";
        // line 3
        echo (( !twig_test_empty((isset($context["error"]) ? $context["error"] : null))) ? ("error") : (""));
        echo "\">
    ";
        // line 4
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "labelEx", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null), 2 => array("class" => "form__label form__label_input")), "method");
        echo "
    <div class=\"form__field\">
        ";
        // line 6
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "textField", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null), 2 => (isset($context["htmlOptions"]) ? $context["htmlOptions"] : null)), "method");
        echo "
        ";
        // line 8
        echo "    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "feedback/fields/text.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 8,  33 => 6,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
