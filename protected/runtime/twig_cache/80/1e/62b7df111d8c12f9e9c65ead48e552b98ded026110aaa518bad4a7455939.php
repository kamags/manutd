<?php

/* common/form/textarea.twig */
class __TwigTemplate_801e62b7df111d8c12f9e9c65ead48e552b98ded026110aaa518bad4a7455939 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
\t";
        // line 2
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "labelEx", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null)), "method");
        echo "
\t";
        // line 3
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "textarea", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null), 2 => twig_array_merge(array("class" => "form-control", "row" => 10), ((array_key_exists("htmlOptions", $context)) ? (_twig_default_filter((isset($context["htmlOptions"]) ? $context["htmlOptions"] : null), array())) : (array())))), "method");
        echo "
\t";
        // line 4
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "error", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null), 2 => array("class" => "error")), "method");
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "common/form/textarea.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
