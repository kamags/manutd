<?php

/* feedback/feedback.twig */
class __TwigTemplate_ec3f8f7abab0d420ea875fc3faa53096d9e2d78fa6561a0d43f83a41ed675406 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["formIsActive"] = ($this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "request", array()), "isPostRequest", array()) || $this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "user", array()), "hasFlash", array(0 => "support_success"), "method"));
        // line 2
        $context["form"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginWidget", array(0 => "CActiveForm", 1 => array("id" => "support-form", "enableAjaxValidation" => false, "htmlOptions" => array("novalidate" => true, "class" => ("form form__support " . (((isset($context["formIsActive"]) ? $context["formIsActive"] : null)) ? ("active") : ("")))))), "method");
        // line 3
        echo "<label class=\"form__label\">Choose game:</label>
<div class=\"form-switcher\">
        <a class=\"form-switcher__poker form-switcher_on\" id=\"switch-poker\" href=\"#\"></a>
\t<a class=\"form-switcher__slots form-switcher_off\" id=\"switch-slots\" href=\"#\"></a>
\t<a class=\"form-switcher__roulette form-switcher_off\" id=\"switch-roulette\" href=\"#\"></a>
        <input type=\"hidden\" id=\"Support_app\" name=\"Support[app]\" value=\"poker\">  
</div>
<div class=\"clear\"></div>

";
        // line 12
        echo twig_include($this->env, $context, "feedback/fields/text.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["message"]) ? $context["message"] : null), "field" => "name"));
        echo "
";
        // line 14
        echo twig_include($this->env, $context, "feedback/fields/email.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["message"]) ? $context["message"] : null), "field" => "email"));
        echo "
";
        // line 15
        echo twig_include($this->env, $context, "feedback/fields/textarea.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["message"]) ? $context["message"] : null), "field" => "message"));
        echo "
";
        // line 16
        echo twig_include($this->env, $context, "feedback/fields/antibot.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["message"]) ? $context["message"] : null), "field" => "antibot"));
        echo "
<div class=\"form__row\">
\t<input class=\"form__button\" type=\"submit\" value='";
        // line 18
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "form-btn"), "method");
        echo "'>
</div>
";
        // line 20
        echo twig_include($this->env, $context, "feedback/fields/error.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["message"]) ? $context["message"] : null), "header" => $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "form-error"), "method")));
        echo "
";
        // line 21
        $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endWidget", array(), "method");
    }

    public function getTemplateName()
    {
        return "feedback/feedback.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 21,  56 => 20,  51 => 18,  46 => 16,  42 => 15,  38 => 14,  34 => 12,  23 => 3,  21 => 2,  19 => 1,);
    }
}
