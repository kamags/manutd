<?php

/* main/popup-casino.twig */
class __TwigTemplate_d26af3bb75f0e20c8142c994daae035859c9e92ee40cf912ca208f62bd4a70c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"casino\" class=\"casino\">
\t<h2 class=\"casino__title\"></h2>
\t<div class=\"casino__content\">
\t\t<div class=\"casino-poker\">
\t\t\t<p class=\"casino__text\">";
        // line 5
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "casino-text-1"), "method");
        echo "</p>
\t\t\t<a class=\"button_fb\" href=\"https://apps.facebook.com/manutdsocialpoker/\" target=\"_balnk\"></a>
\t\t\t<p class=\"casino__text\">";
        // line 7
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "popup-text-2"), "method");
        echo "</p>
\t\t\t<div class=\"store_small\">
\t\t\t\t<a class=\"store__item store__item_ap\" href=\"https://itunes.apple.com/app/id947856678\" target=\"_balnk\"></a>
\t\t\t\t<a class=\"store__item store__item_gl\" href=\"https://play.google.com/store/apps/details?id=com.kamagames.texaspokermu\" target=\"_balnk\"></a>
\t\t\t\t<a class=\"store__item store__item_win\" href=\"http://www.windowsphone.com/en-us/store/app/manchester-united-social-poker/f7a9b108-b47a-4164-bd11-5f9fc1ff361e\" target=\"_balnk\"></a>
\t\t\t\t<a class=\"store__item store__item_amazon\" href=\"http://www.amazon.com/KamaGames-Manchester-United-Social-Poker/dp/B00T9UPAAC/ref=sr_1_2?s=mobile-apps&ie=UTF8&qid=1424251869&sr=1-2&keywords=kamagames\" target=\"_balnk\"></a>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"casino-roulette\">
\t\t\t<p class=\"casino__text\">";
        // line 16
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "casino-text-2"), "method");
        echo "</p>
\t\t\t<a class=\"button_fb\" href=\"https://apps.facebook.com/manutdsocialroulette/\" target=\"_balnk\"></a>
\t\t\t<p class=\"casino__text\">";
        // line 18
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "popup-text-2"), "method");
        echo "</p>
\t\t\t<div class=\"store_small\">
\t\t\t\t<a class=\"store__item store__item_ap\" href=\"https://itunes.apple.com/app/id951094676\" target=\"_balnk\"></a>
\t\t\t\t<a class=\"store__item store__item_gl\" href=\"https://play.google.com/store/apps/details?id=com.kamagames.roulettemu\" target=\"_balnk\"></a>
\t\t\t\t<a class=\"store__item store__item_win\" href=\"https://www.windowsphone.com/s?appid=B95C77C1-8286-4B56-BEAD-EE74E9FD4C42\" target=\"_balnk\"></a>
\t\t\t\t<a class=\"store__item store__item_amazon\" href=\"http://www.amazon.com/dp/B00U7YXHHM\" target=\"_balnk\"></a>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "main/popup-casino.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 18,  42 => 16,  30 => 7,  25 => 5,  19 => 1,);
    }
}
