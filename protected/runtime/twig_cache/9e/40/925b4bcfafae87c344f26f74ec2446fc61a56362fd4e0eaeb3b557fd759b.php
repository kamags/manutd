<?php

/* /views/main/tos.twig */
class __TwigTemplate_9e40925b4bcfafae87c344f26f74ec2446fc61a56362fd4e0eaeb3b557fd759b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "\t<div class=\"legal legal_bg\">
\t\t<div class=\"wrapper wrapper_inner\">
\t\t\t<a class=\"legal__logo\" href=\"/\"></a>
\t\t\t";
        // line 7
        echo "\t\t\t\t";
        // line 8
        echo "\t\t\t\t";
        // line 9
        echo "                                ";
        // line 10
        echo "                                    ";
        // line 11
        echo "                                ";
        // line 12
        echo "\t\t\t\t";
        // line 13
        echo "\t\t\t";
        // line 14
        echo "\t\t\t<h2 class=\"legal__title text_title text_line\">";
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-title"), "method");
        echo "</h2>
\t\t\t<p class=\"time\">";
        // line 15
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-data"), "method");
        echo "</p>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 17
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-defintions-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 18
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-defintions-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 19
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-defintions-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 20
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-defintions-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 21
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-defintions-4"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 22
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-defintions-5"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 23
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-defintions-6"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 24
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-defintions-7"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 27
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-legal-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 28
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-legal-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 29
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-legal-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 30
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-legal-3"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 33
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-title"), "method");
        echo "</h3>
\t\t\t\t\t<p>";
        // line 34
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-1"), "method");
        echo "</p>
\t\t\t\t\t<p>";
        // line 35
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-2"), "method");
        echo "</p>
\t\t\t\t\t<p class=\"sub-title\">";
        // line 36
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-2-title-1"), "method");
        echo "</p>
\t\t\t\t\t\t<ul class=\"sub\">
\t\t\t\t\t\t\t<li>";
        // line 38
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-2-1"), "method");
        echo "</li>
\t\t\t\t\t\t\t<li>";
        // line 39
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-2-2"), "method");
        echo "</li>
\t\t\t\t\t\t\t<li>";
        // line 40
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-2-3"), "method");
        echo "</li>
\t\t\t\t\t\t\t<li>";
        // line 41
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-2-4"), "method");
        echo "</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t<p class=\"sub-title\">";
        // line 43
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-2-title-2"), "method");
        echo "</p>
\t\t\t\t\t\t<ul class=\"sub\">
\t\t\t\t\t\t\t<li>";
        // line 45
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-2-5"), "method");
        echo "</li>
\t\t\t\t\t\t\t";
        // line 47
        echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t<p>";
        // line 48
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-license-3"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 51
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 52
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 53
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 54
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 55
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-3-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 56
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-3-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 57
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-4"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 58
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-5"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 59
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-6"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 60
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-reg-7"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 63
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-u-content-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 64
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-u-content-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 65
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-u-content-1-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 66
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-u-content-1-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 67
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-u-content-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 68
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-u-content-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 69
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-u-content-4"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 72
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 73
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-0"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 74
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 75
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-1-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 76
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-1-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 77
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-1-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 78
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-1-4"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 79
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-1-5"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 80
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-1-6"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 81
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-1-7"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 82
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-1-8"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 83
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 84
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 85
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 86
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 87
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-4"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 88
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-5"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 89
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-6"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 90
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-7"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 91
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-8"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 92
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-9"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 93
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-2-10"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 94
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rules-3"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 97
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-fees-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 98
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-fees-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 99
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-fees-2"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 102
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-deactivation-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 103
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-deactivation-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 104
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-deactivation-2"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 107
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-third-party-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 108
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-third-party-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 109
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-third-party-2"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 112
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-communications-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 113
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-communications-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 114
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-communications-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 115
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-communications-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 116
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-communications-4"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 119
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rights-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 120
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rights-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 121
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-rights-2"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 124
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-limitations-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 125
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-limitations-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 126
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-limitations-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 127
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-limitations-3"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 130
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-indemnification-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 131
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-indemnification-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 132
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-indemnification-2"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 135
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 136
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-0"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 137
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 138
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-1-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 139
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-1-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 140
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-1-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 141
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 142
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-2-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 143
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-2-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 144
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-2-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 145
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 146
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-3-1"), "method");
        echo "</p>
\t\t\t\t<ul class=\"sub-list\">
\t\t\t\t\t<li>";
        // line 148
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-sub-1"), "method");
        echo "</li>
\t\t\t\t\t<li>";
        // line 149
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-sub-2"), "method");
        echo "</li>
\t\t\t\t\t<li>";
        // line 150
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-sub-3"), "method");
        echo "</li>
\t\t\t\t</ul>
\t\t\t\t<p>";
        // line 152
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-3-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 153
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-3-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 154
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-4"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 155
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-4-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 156
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-4-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 157
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-dispute-4-3"), "method");
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"text_block\">
\t\t\t\t<h3>";
        // line 160
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-miscellaneous-title"), "method");
        echo "</h3>
\t\t\t\t<p>";
        // line 161
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-miscellaneous-1"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 162
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-miscellaneous-2"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 163
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-miscellaneous-3"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 164
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-miscellaneous-4"), "method");
        echo "</p>
\t\t\t\t<p>";
        // line 165
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "tos", 1 => "tos-miscellaneous-5"), "method");
        echo "</p>
\t\t\t</div>
\t\t</div>
\t</div>


\t<div class=\"wrapper\">

\t\t";
        // line 173
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/main/tos.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  567 => 173,  556 => 165,  552 => 164,  548 => 163,  544 => 162,  540 => 161,  536 => 160,  530 => 157,  526 => 156,  522 => 155,  518 => 154,  514 => 153,  510 => 152,  505 => 150,  501 => 149,  497 => 148,  492 => 146,  488 => 145,  484 => 144,  480 => 143,  476 => 142,  472 => 141,  468 => 140,  464 => 139,  460 => 138,  456 => 137,  452 => 136,  448 => 135,  442 => 132,  438 => 131,  434 => 130,  428 => 127,  424 => 126,  420 => 125,  416 => 124,  410 => 121,  406 => 120,  402 => 119,  396 => 116,  392 => 115,  388 => 114,  384 => 113,  380 => 112,  374 => 109,  370 => 108,  366 => 107,  360 => 104,  356 => 103,  352 => 102,  346 => 99,  342 => 98,  338 => 97,  332 => 94,  328 => 93,  324 => 92,  320 => 91,  316 => 90,  312 => 89,  308 => 88,  304 => 87,  300 => 86,  296 => 85,  292 => 84,  288 => 83,  284 => 82,  280 => 81,  276 => 80,  272 => 79,  268 => 78,  264 => 77,  260 => 76,  256 => 75,  252 => 74,  248 => 73,  244 => 72,  238 => 69,  234 => 68,  230 => 67,  226 => 66,  222 => 65,  218 => 64,  214 => 63,  208 => 60,  204 => 59,  200 => 58,  196 => 57,  192 => 56,  188 => 55,  184 => 54,  180 => 53,  176 => 52,  172 => 51,  166 => 48,  163 => 47,  159 => 45,  154 => 43,  149 => 41,  145 => 40,  141 => 39,  137 => 38,  132 => 36,  128 => 35,  124 => 34,  120 => 33,  114 => 30,  110 => 29,  106 => 28,  102 => 27,  96 => 24,  92 => 23,  88 => 22,  84 => 21,  80 => 20,  76 => 19,  72 => 18,  68 => 17,  63 => 15,  58 => 14,  56 => 13,  54 => 12,  52 => 11,  50 => 10,  48 => 9,  46 => 8,  44 => 7,  39 => 3,  36 => 2,  11 => 1,);
    }
}
