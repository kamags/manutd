<?php

/* main/facebook-container.twig */
class __TwigTemplate_93f5a938a6663f40ff9cc0b1053134a9ee5c0e1f3759fec97c02ded999e9e6e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"facebook-container\">
        <div class=\"layout-container facebook-wrapper\">
                <div class=\"fb-like-box\" data-href=\"https://www.facebook.com/MUSocialCasino\" data-width=\"960\"
                         data-height=\"230\" data-colorscheme=\"dark\" data-show-faces=\"true\" data-header=\"false\" data-stream=\"false\"
                         data-show-border=\"false\"></div>
        </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "main/facebook-container.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
