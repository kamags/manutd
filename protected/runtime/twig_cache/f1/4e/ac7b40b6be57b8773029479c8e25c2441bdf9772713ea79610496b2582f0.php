<?php

/* /views/main/error404.twig */
class __TwigTemplate_f14eac7b40b6be57b8773029479c8e25c2441bdf9772713ea79610496b2582f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "\t\t<div class=\"error-block\">
\t\t\t<div class=\"wrapper\">
\t\t\t\t<h1></h1>
\t\t\t\t<p class=\"text_text\">Oops! Sorry, page not found!</p>
\t\t\t\t<a class=\"link\" href=\"/\">Back to main page</a>
\t\t\t</div>
\t\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/main/error404.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 3,  36 => 2,  11 => 1,);
    }
}
