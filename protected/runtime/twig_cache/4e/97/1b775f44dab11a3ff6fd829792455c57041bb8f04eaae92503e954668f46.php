<?php

/* /views/games/slots.twig */
class __TwigTemplate_4e971b775f44dab11a3ff6fd829792455c57041bb8f04eaae92503e954668f46 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "        <div class=\"wrapper\">
                <a class=\"pages__logo\" href=\"/\"></a>
        </div>
\t<div class=\"pages pages_bg\">

                <div class=\"wrapper\">
\t\t\t<div class=\"text_block\">
\t\t\t\t<h1>About Manchester United Social Slots</h1>
                                <span class=\"pages-line\"></span>

                                <p class=\"pages-description\">Manchester United Social Slots, powered by KamaGames, lets you spin the reels and play a variety of different club themed slot machines. Free to download and easy to play, the game features specially designed sounds and images familiar to all Man Utd fans around the world.</p>
                                <p class=\"pages-description\">You can download the game directly to your iOS and Android smartphone or tablet device anytime, anywhere in the world and begin spinning! Connect via Facebook to play with your friends, unlock coins and track your in-game progress. </p>
                                <p class=\"pages-description\">Play Manchester United Social Slots powered by KamaGames today and jump into a world of Las Vegas casino style slot machines filled with BIG WINS, BONUS GAMES, FREE SPINS and incredible DAILY SLOTS BONUSES!  </p>
                                  <p class=\"pages-description\">Practice or success at social casino gaming does not imply future success at real money gambling. </p>


                                <h3>Screenshots</h3>
                                <div class=\"carousel\">
                                        <a class=\"carousel-button carousel-button-left\" href=\"#\"></a>
                                        <a class=\"carousel-button carousel-button-right\" href=\"#\"></a>
                                        <div class=\"carousel-wrapper\">
                                                <div class=\"carousel-items\">
                                                        <div class=\"carousel-block\">
                                                                <img src=\"/img/screenshots/slots/screenshot_01.jpg\" alt=\"\" widht=\"280\" height=\"158\"/>
                                                        </div>
                                                        <div class=\"carousel-block\">
                                                                <img src=\"/img/screenshots/slots/screenshot_02.jpg\" alt=\"\" widht=\"280\" height=\"158\"/>
                                                        </div>
                                                        <div class=\"carousel-block\">
                                                                <img src=\"/img/screenshots/slots/screenshot_03.jpg\" alt=\"\" widht=\"280\" height=\"158\"/>
                                                        </div>

                                                </div>
                                        </div>
                                </div>

                                <div class=\"game-available\">
                                        <h3>Available</h3>
                                        <div class=\"ft-store\">
\t\t\t\t\t\t<a class=\"ft-store__item ft-store__item_ap\" href=\"https://itunes.apple.com/us/app/manchester-united-social-slots/id1058789250?ls=1&mt=8\" target=\"_blank\"></a>
                                                <a class=\"ft-store__item ft-store__item_gl\" href=\"https://play.google.com/store/apps/details?id=com.kamagames.slotsmu\" target=\"_blank\"></a>
                                        </div>
                                </div>

                                <div class=\"game-features\">
                                        <div class=\"game-features__content\">
                                               <h3>Feature</h3>

                                               <ul class=\"game-features-list\">
                                                   <li class=\"game-features-list__item\">CONVENIENT INTERFACE – A simple and attractive interface which gives you the ability to bet and spin quickly.</li>
                                                   <li class=\"game-features-list__item\">NO REGISTRATION – Start playing slots straight away without registration or login.</li>
                                                   <li class=\"game-features-list__item\">PLAY WITH FRIENDS – Invite your friends to the game through Facebook and get bonuses as a reward.</li>
                                                   <li class=\"game-features-list__item\">FREE COINS – Come back to the game every day to get free coins bonuses.</li>
                                                   <li class=\"game-features-list__item\">PLAYABLE OFFLINE – Doesn’t require an online connection to play.</li>
                                                   <li class=\"game-features-list__item\">FREQUENT UPDATES – Unique new Man Utd slot machines added frequently!</li>
                                                   <li class=\"game-features-list__item\">EXCITING SLOT FEATURES – Expanding Wilds, Mega symbols, Unique reels, Free spins and Bonus Games!</li>
                                                   <li class=\"game-features-list__item\">FACEBOOK ACCOUNT - Log and register via your Facebook account on your smartphone or tablet device to keep track of your in-game progress.</li>
                                               </ul>
                                       </div>

                                       <div class=\"game-features__play\">
                                            <a target=\"_balnk\" href=\"#casino\" class=\"btn play-now fancy-casino\">";
        // line 64
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "play-now-btn"), "method");
        echo "</a>
                                       </div>
                                </div>

                                <div class=\"clear\"></div>

                                ";
        // line 70
        echo twig_include($this->env, $context, "main/facebook-container.twig");
        echo "

\t\t\t</div>

\t\t</div>
\t</div>


\t<div class=\"wrapper\">

\t\t";
        // line 80
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/games/slots.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 80,  111 => 70,  102 => 64,  39 => 3,  36 => 2,  11 => 1,);
    }
}
