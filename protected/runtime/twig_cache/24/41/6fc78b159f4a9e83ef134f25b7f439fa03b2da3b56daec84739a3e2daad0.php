<?php

/* layouts/common.twig */
class __TwigTemplate_24416fc78b159f4a9e83ef134f25b7f439fa03b2da3b56daec84739a3e2daad0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
\t<meta name=\"google-site-verification\" content=\"sBfm4F9T96AQQGr6xZpTwcFyCOxFJRUIAFvTZUtWelI\" />
\t<title>Manchester United Poker - Free to Play on your mobile device, social network or online</title>
\t<meta charset=\"UTF-8\">
\t";
        // line 7
        if ( !twig_test_empty((isset($context["metaKeywords"]) ? $context["metaKeywords"] : null))) {
            // line 8
            echo "\t<meta name=\"keywords\" content=\"";
            echo (isset($context["metaKeywords"]) ? $context["metaKeywords"] : null);
            echo "\" />
\t";
        }
        // line 10
        echo "\t";
        if ( !twig_test_empty((isset($context["metaDescription"]) ? $context["metaDescription"] : null))) {
            // line 11
            echo "\t<meta name=\"description\" content=\"";
            echo (isset($context["metaDescription"]) ? $context["metaDescription"] : null);
            echo "\" />
\t";
        }
        // line 13
        echo "\t";
        // line 14
        echo "\t<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/vendor/fancybox/jquery.fancybox.css\"/>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/main.min.css\"/>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/style.css\"/>
\t<link rel=\"stylesheet\" href=\"/vendor/preloader/jpreloader.css\"/>

</head>

";
        // line 22
        $context["ROLE_ADMIN"] = "admin";
        // line 23
        if (( !$this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "user", array()), "isGuest", array()) && ($this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "user", array()), "role", array()) != (isset($context["ROLE_ADMIN"]) ? $context["ROLE_ADMIN"] : null)))) {
            // line 24
            echo "\t";
            $context["profile"] = $this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "player", array()), "profile", array());
        }
        // line 26
        echo "
<body class=\"bg\">
<div class=\"toolbar\">
\t<div class=\"wrapper toolbar__inner\">
\t\t";
        // line 30
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["Nav"]) ? $context["Nav"] : null), "getMenu", array(0 => "top"), "method"), "setHtmlOptions", array(0 => array("class" => "toolbar-menu")), "method"), "render", array(0 => array("itemCssClass" => "toolbar-menu__item"), 1 => true), "method");
        echo "

\t\t<div class=\"toolbar-sign-in\">
\t\t\t";
        // line 33
        if (( !$this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "user", array()), "isGuest", array()) && ($this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "user", array()), "role", array()) != (isset($context["ROLE_ADMIN"]) ? $context["ROLE_ADMIN"] : null)))) {
            // line 34
            echo "\t\t\t\t";
            // line 35
            echo "\t\t\t\t<a href=\"";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/signOut"), "method");
            echo "\">Sign Out</a>
\t\t\t";
        } else {
            // line 37
            echo "\t\t\t\t<a href=\"";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/signIn"), "method");
            echo "\">Sign In</a>
\t\t\t";
        }
        // line 39
        echo "\t\t</div>
\t\t<div class=\"lang-toolbar\">
\t\t\t";
        // line 41
        $context["lang_name"] = $this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array());
        // line 42
        echo "\t\t\t";
        if (($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array()) == "en")) {
            // line 43
            echo "\t\t\t\t";
            $context["lang_name"] = "ENG";
            // line 44
            echo "\t\t\t";
        }
        // line 45
        echo "\t\t\t<a href=\"#lang\"
\t\t\t   class=\"lang lang-toolbar__btn lang-toolbar__btn_";
        // line 46
        echo $this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array());
        echo " flag flag_";
        echo $this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array());
        echo " flag_usa js-flag\">";
        echo (isset($context["lang_name"]) ? $context["lang_name"] : null);
        echo "</a>
\t\t</div>

\t\t<div id=\"lang\">
\t\t\t<div class=\"lang-switcher\">
\t\t\t\t<p>";
        // line 51
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "lang-title"), "method");
        echo "</p>
\t\t\t\t";
        // line 52
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "params", array()), "languages", array()));
        foreach ($context['_seq'] as $context["languageCode"] => $context["language"]) {
            // line 53
            echo "\t\t\t\t\t";
            if (($this->getAttribute($context["language"], "site", array(), "array") == true)) {
                // line 54
                echo "\t\t\t\t\t\t";
                $context["link"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "route", array()), 1 => twig_array_merge($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "actionParams", array()), array("lang" => $context["languageCode"]))), "method");
                // line 55
                echo "\t\t\t\t\t\t";
                $context["class"] = ((($context["languageCode"] == $this->getAttribute((isset($context["App"]) ? $context["App"] : null), "language", array()))) ? ("active") : (""));
                // line 56
                echo "\t\t\t\t\t\t<a href=\"";
                echo (isset($context["link"]) ? $context["link"] : null);
                echo "\" data-lang=\"";
                echo $context["languageCode"];
                echo "\"
\t\t\t\t\t\t   class=\"";
                // line 57
                echo (isset($context["class"]) ? $context["class"] : null);
                echo " flag flag_";
                echo $context["languageCode"];
                echo " flag_usa lang-switcher__flag\">";
                echo $this->getAttribute($context["language"], "originalTitle", array(), "array");
                echo "</a>
\t\t\t\t\t";
            }
            // line 59
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['languageCode'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "\t\t\t\t<div class=\"lang-soon\"></div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"toolbar-games\">
\t<div class=\"wrapper\">
\t\t<a class=\"toolbar-games__item toolbar-games__item_poker\"
\t\t   href=\"";
        // line 69
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/games", 1 => twig_array_merge($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "actionParams", array()), array("name" => "poker"))), "method");
        echo "\"><p>Social</p>poker</a>
\t\t\t <a class=\"toolbar-games__item toolbar-games__item_slots\"
\t\t\t\t\thref=\"";
        // line 71
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/games", 1 => twig_array_merge($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "actionParams", array()), array("name" => "slots"))), "method");
        echo "\"><p>Social</p>slots</a>
\t\t<a class=\"toolbar-games__item toolbar-games__item_roulette\"
\t\t   href=\"";
        // line 73
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/games", 1 => twig_array_merge($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "actionParams", array()), array("name" => "roulette"))), "method");
        echo "\"><p>Social</p>roulette</a>
\t</div>
</div>

";
        // line 77
        $this->displayBlock("content", $context, $blocks);
        echo "
";
        // line 78
        echo twig_include($this->env, $context, "main/casino-popup.twig");
        echo "

<script src=\"/js/";
        // line 80
        echo (((twig_constant("APPLICATION_MODE") == "production")) ? ("main.min.js") : ("main.js"));
        echo "\"></script>
<script src=\"/js/dark.js\"></script>
<script src=\"/js/facebook-auth-js-sdk/facebook.js\"></script>
";
        // line 83
        $this->displayBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-531ff68d79255e1f\"
\t\tasync=\"async\"></script>

<!-- GOOGLE ANALYTICS -->
<script>
\t(function (i, s, o, g, r, a, m) {
\t\ti['GoogleAnalyticsObject'] = r;
\t\ti[r] = i[r] || function () {
\t\t\t(i[r].q = i[r].q || []).push(arguments)
\t\t}, i[r].l = 1 * new Date();
\t\ta = s.createElement(o), m = s.getElementsByTagName(o)[0];
\t\ta.async = 1;
\t\ta.src = g;
\t\tm.parentNode.insertBefore(a, m)
\t})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
\tga('create', 'UA-32533540-14', 'auto');
\tga('send', 'pageview');
</script>
<!-- END GOOGLE ANALYTICS -->

";
        // line 105
        echo "<div id=\"fb-root\"></div>
<script>(function (d, s, id) {
\t\tvar js, fjs = d.getElementsByTagName(s)[0];
\t\tif (d.getElementById(id)) return;
\t\tjs = d.createElement(s);
\t\tjs.id = id;
\t\tjs.src = \"//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=361272397238551\";
\t\tfjs.parentNode.insertBefore(js, fjs);
\t}(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "layouts/common.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  228 => 105,  204 => 83,  198 => 80,  193 => 78,  189 => 77,  182 => 73,  177 => 71,  172 => 69,  161 => 60,  155 => 59,  146 => 57,  139 => 56,  136 => 55,  133 => 54,  130 => 53,  126 => 52,  122 => 51,  110 => 46,  107 => 45,  104 => 44,  101 => 43,  98 => 42,  96 => 41,  92 => 39,  86 => 37,  80 => 35,  78 => 34,  76 => 33,  70 => 30,  64 => 26,  60 => 24,  58 => 23,  56 => 22,  46 => 14,  44 => 13,  38 => 11,  35 => 10,  29 => 8,  27 => 7,  19 => 1,);
    }
}
