<?php

/* main/footer.twig */
class __TwigTemplate_89b88470afd175aad690c62484345c72e6f4798666fe57c134ea67367fcbaede extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"footer\" class=\"footer\">

\t<div class=\"footer__top\">
\t\t<div class=\"footer__subscribe\">
\t\t\t<form class=\"subscribe\" action=\"\">
\t\t\t\t<label class=\"subscribe__label\" for=\"subscribe\">";
        // line 6
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "footer-subscribe-title"), "method");
        echo "</label>
\t\t\t\t<input class=\"subscribe__input\" type=\"text\" name=\"Subscribers[email]\" id=\"Subscribers_email\"  placeholder=\"";
        // line 7
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "footer-subscribe-placeholder"), "method");
        echo "\">
\t\t\t\t<input class=\"subscribe__btn\" type=\"button\" value=\"";
        // line 8
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "footer-subscribe-button"), "method");
        echo "\"/>
                                <div class=\"subscribe__success\">Thank You!</div>
                                <div class=\"subscribe__error\"></div>
\t\t\t</form>
\t\t</div>
\t\t<ul class=\"footer__social\">
\t\t\t<li><a class=\"fb\" href=\"https://www.facebook.com/MUSocialCasino\" target=\"_balnk\"></a></li>
\t\t\t<li><a class=\"tw\" href=\"https://twitter.com/MUSocialCasino\" target=\"_balnk\"></a></li>
\t\t\t<li><a class=\"gl\" href=\"https://google.com/+manutdsocialcasino\" target=\"_balnk\"></a></li>
\t\t\t<li><a class=\"inst\" href=\"http://instagram.com/manutdsocialcasino/\" target=\"_balnk\"></a></li>
\t\t</ul>
\t</div>
\t<div class=\"footer__bottom\">
\t\t<p class=\"footer-legal-text\">";
        // line 21
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "footer-legal"), "method");
        echo "</p>
\t\t<p class=\"footer__copy\"><a href=\"http://kamagames.com\" class=\"footer__logo\" target=\"_balnk\"></a><span>&copy; 2015 KamaGames. ";
        // line 22
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "footer-rights"), "method");
        echo "</span></p>
\t\t<ul class=\"footer__legal\">
\t\t\t<li><a href=\"";
        // line 24
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "feedback/support"), "method");
        echo "\">";
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "footer-support"), "method");
        echo "</a></li>
\t\t\t<li><a href=\"";
        // line 25
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/tos"), "method");
        echo "\">";
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "footer-tos"), "method");
        echo "</a></li>
\t\t\t<li><a href=\"";
        // line 26
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/policy"), "method");
        echo "\">";
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "footer-policy"), "method");
        echo "</a></li>
\t\t</ul>
\t</div>
                
        <div class=\"addthis_native_toolbox\"></div>

</div>
";
    }

    public function getTemplateName()
    {
        return "main/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 26,  65 => 25,  59 => 24,  54 => 22,  50 => 21,  34 => 8,  30 => 7,  26 => 6,  19 => 1,);
    }
}
