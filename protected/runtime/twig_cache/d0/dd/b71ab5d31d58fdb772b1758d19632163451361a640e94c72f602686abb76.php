<?php

/* feedback/fields/error.twig */
class __TwigTemplate_d0ddb71ab5d31d58fdb772b1758d19632163451361a640e94c72f602686abb76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "errorSummary", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["header"]) ? $context["header"] : null), 2 => (isset($context["footer"]) ? $context["footer"] : null)), "method");
        echo "
";
    }

    public function getTemplateName()
    {
        return "feedback/fields/error.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }
}
