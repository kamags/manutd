<?php

/* /views/press/index.twig */
class __TwigTemplate_871b4a89b0f5a840f7ee6a27941ed7c9b219cfb4ba56b4f44a8d8fb9d496fd22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "\t<div class=\"wrapper\">
\t\t<a class=\"pages__logo\" href=\"/\"></a>
\t</div>
\t<div class=\"pages pages_bg\">

\t\t<div class=\"wrapper\">
\t\t\t<div class=\"text_block press\">
\t\t\t\t<h1>Press</h1>
\t\t\t\t<span class=\"pages-line\"></span>

\t\t\t\t";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pressItems"]) ? $context["pressItems"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["press"]) {
            // line 14
            echo "\t\t\t\t\t<div class=\"press__item\">
\t\t\t\t\t\t";
            // line 15
            $context["image"] = $this->getAttribute($context["press"], "getImage", array(), "method");
            // line 16
            echo "\t\t\t\t\t\t";
            if ((isset($context["image"]) ? $context["image"] : null)) {
                // line 17
                echo "\t\t\t\t\t\t\t<a href=\"";
                echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "press/item", 1 => array("alias" => $this->getAttribute($context["press"], "alias", array()))), "method");
                echo "\">
\t\t\t\t\t\t\t\t<div class=\"press__img\"><img src=\"";
                // line 18
                echo (isset($context["image"]) ? $context["image"] : null);
                echo "\" width=\"150\" height=\"94\"></div>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t";
            }
            // line 21
            echo "
\t\t\t\t\t\t<div class=\"press-content\">
\t\t\t\t\t\t\t<p class=\"date\">";
            // line 23
            echo twig_date_format_filter($this->env, $this->getAttribute($context["press"], "publicationDate", array()), "d M Y");
            echo "</p>

\t\t\t\t\t\t\t<h3><a href=\"";
            // line 25
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "press/item", 1 => array("alias" => $this->getAttribute($context["press"], "alias", array()))), "method");
            echo "\">";
            echo $this->getAttribute($context["press"], "title", array());
            echo "</a></h3>

\t\t\t\t\t\t\t";
            // line 27
            echo $this->getAttribute($context["press"], "overview", array());
            echo "

\t\t\t\t\t\t\t<div class=\"addthis_native_toolbox\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"clear\"></div>
\t\t\t\t\t</div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['press'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "\t\t\t\t
\t\t\t\t";
        // line 35
        echo twig_include($this->env, $context, "common/pagination.twig");
        echo "

\t\t\t\t";
        // line 38
        echo "\t\t\t\t";
        echo twig_include($this->env, $context, "main/facebook-container.twig");
        echo "
\t\t\t</div>

\t\t</div>
\t</div>


\t<div class=\"wrapper\">
\t\t";
        // line 46
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/press/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 46,  111 => 38,  106 => 35,  103 => 34,  90 => 27,  83 => 25,  78 => 23,  74 => 21,  68 => 18,  63 => 17,  60 => 16,  58 => 15,  55 => 14,  51 => 13,  39 => 3,  36 => 2,  11 => 1,);
    }
}
