<?php

/* /views/main/sign-up.twig */
class __TwigTemplate_87253118b7dd1feb8450d52582c5c61b9f1578ec2b23aad9358086626817c36c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(2);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        $context["flash"] = $this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "user", array()), "getFlash", array(0 => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "action", array()), "id", array())), "method");
        // line 2
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "\t<div class=\"wrapper\">
\t\t<a class=\"pages__logo\" href=\"/\"></a>
\t</div>
\t<div class=\"pages pages_bg\">

\t\t<div class=\"wrapper\">
\t\t\t";
        // line 13
        if (((isset($context["flash"]) ? $context["flash"] : null) == "success")) {
            // line 14
            echo "\t\t\t\t<div class=\"form__success\">
\t\t\t\t\t<p class=\"sign-in__text\">";
            // line 15
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signup", 1 => "To complete your registration you must follow the link we emailed you."), "method");
            echo "</p>
\t\t\t\t</div>
\t\t\t";
        } else {
            // line 18
            echo "\t\t\t\t<div class=\"text_block sign-in\">
\t\t\t\t\t";
            // line 19
            $context["form"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginWidget", array(0 => "CActiveForm", 1 => array("htmlOptions" => array("class" => "sign-in__form"), "enableAjaxValidation" => false)), "method");
            // line 20
            echo "\t\t\t\t\t<h1 class=\"sign-in__title\">";
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Create account Manchester United Social Casino"), "method");
            echo "</h1>
\t\t\t\t\t<span class=\"pages-line\"></span>

\t\t\t\t\t<p class=\"sign-in__text\">";
            // line 23
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Log in with your Facebook profile"), "method");
            echo "</p>
\t\t\t\t\t";
            // line 24
            echo twig_include($this->env, $context, "main/_social-auth.twig");
            echo "
\t\t\t\t\t<p class=\"sign-in__text\">";
            // line 25
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "or create new profile"), "method");
            echo "</p>

\t\t\t\t\t<div class=\"sign-in__block\">
\t\t\t\t\t\t";
            // line 28
            echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "textField", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => "nickname", 2 => array("class" => "sign-in__input sign-in__input_nick", "placeholder" => "Nickname")), "method");
            echo "
\t\t\t\t\t\t";
            // line 29
            echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "emailField", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => "login", 2 => array("class" => "sign-in__input sign-in__input_email", "placeholder" => "Email")), "method");
            echo "
\t\t\t\t\t\t";
            // line 30
            echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "passwordField", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => "password", 2 => array("class" => "sign-in__input sign-in__input_password", "placeholder" => "Password")), "method");
            echo "
\t\t\t\t\t\t";
            // line 31
            echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "passwordField", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => "confirmPassword", 2 => array("class" => "sign-in__input sign-in__input_password", "placeholder" => "Confirm password")), "method");
            echo "
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"sign-in__block\">
\t\t\t\t\t\t<label class=\"sign-in__label\">";
            // line 35
            echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "checkBox", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => "subscribe"), "method");
            echo "  ";
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "I subscribe to the newsletter and news"), "method");
            echo "
\t\t\t\t\t\t</label>
\t\t\t\t\t\t<label class=\"sign-in__label\">";
            // line 37
            echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "checkBox", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => "agreeWithToU"), "method");
            echo " ";
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "I agree all"), "method");
            echo " <a
\t\t\t\t\t\t\t\t\tclass=\"sign-in__link\" href=\"";
            // line 38
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/tos"), "method");
            echo "\">Terms of Service</a>
\t\t\t\t\t\t</label>
\t\t\t\t\t\t<button class=\"sign-in__btn\" type=\"submit\">";
            // line 40
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Create account"), "method");
            echo "</button>
\t\t\t\t\t</div>

\t\t\t\t\t";
            // line 43
            $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endWidget", array(), "method");
            // line 44
            echo "
\t\t\t\t\t<div class=\"sign-in__block\">
\t\t\t\t\t\t<h2 class=\"sign-in__h2\">";
            // line 46
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Already a member?"), "method");
            echo "</h2>
\t\t\t\t\t\t<a href=\"";
            // line 47
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/signIn"), "method");
            echo "\"
\t\t\t\t\t\t   class=\"sign-in__link\">";
            // line 48
            echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Sign in here"), "method");
            echo "</a>
\t\t\t\t\t</div>
\t\t\t\t\t";
            // line 50
            echo twig_include($this->env, $context, "main/facebook-container.twig");
            echo "
\t\t\t\t</div>
\t\t\t";
        }
        // line 53
        echo "\t\t</div>
\t</div>


\t<div class=\"wrapper\">
\t\t";
        // line 58
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>
";
    }

    // line 62
    public function block_scripts($context, array $blocks = array())
    {
        // line 63
        echo "\t";
        echo twig_include($this->env, $context, "common/sign-in-form/notifier.twig", array("flash" => (isset($context["flash"]) ? $context["flash"] : null)));
        echo "
";
    }

    public function getTemplateName()
    {
        return "/views/main/sign-up.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 63,  170 => 62,  163 => 58,  156 => 53,  150 => 50,  145 => 48,  141 => 47,  137 => 46,  133 => 44,  131 => 43,  125 => 40,  120 => 38,  114 => 37,  107 => 35,  100 => 31,  96 => 30,  92 => 29,  88 => 28,  82 => 25,  78 => 24,  74 => 23,  67 => 20,  65 => 19,  62 => 18,  56 => 15,  53 => 14,  51 => 13,  43 => 7,  40 => 6,  36 => 2,  34 => 4,  11 => 2,);
    }
}
