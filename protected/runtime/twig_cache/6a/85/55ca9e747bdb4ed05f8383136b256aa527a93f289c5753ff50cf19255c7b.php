<?php

/* /views/main/sign-in.twig */
class __TwigTemplate_6a8555ca9e747bdb4ed05f8383136b256aa527a93f289c5753ff50cf19255c7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(2);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        $context["flash"] = $this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "user", array()), "getFlash", array(0 => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "action", array()), "id", array())), "method");
        // line 2
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "\t<div class=\"wrapper\">
\t\t<a class=\"pages__logo\" href=\"/\"></a>
\t</div>
\t<div class=\"pages pages_bg\">

\t\t<div class=\"wrapper\">
\t\t\t<div class=\"text_block sign-in\">
\t\t\t\t<h1 class=\"sign-in__title\">";
        // line 13
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Sign in to Manchester United Social Casino"), "method");
        echo "</h1>
\t\t\t\t<span class=\"pages-line\"></span>
\t\t\t\t<p class=\"sign-in__text\">";
        // line 15
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Log in with your Facebook profile"), "method");
        echo "</p>
\t\t\t\t";
        // line 16
        echo twig_include($this->env, $context, "main/_social-auth.twig");
        echo "
\t\t\t\t<p class=\"sign-in__text\">";
        // line 17
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "or in with your email"), "method");
        echo "</p>
\t\t\t\t<div class=\"sign-in__block\">
\t\t\t\t\t";
        // line 19
        $context["form"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "beginWidget", array(0 => "CActiveForm", 1 => array("enableAjaxValidation" => false, "htmlOptions" => array("class" => "sign-in__form"))), "method");
        // line 20
        echo "\t\t\t\t\t\t";
        echo twig_include($this->env, $context, "common/sign-in-form/email.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "login", "htmlOptions" => array("class" => "sign-in__input sign-in__input_email")));
        echo "
                                                ";
        // line 21
        echo twig_include($this->env, $context, "common/sign-in-form/password.twig", array("form" => (isset($context["form"]) ? $context["form"] : null), "model" => (isset($context["model"]) ? $context["model"] : null), "field" => "password", "htmlOptions" => array("class" => "sign-in__input sign-in__input_password")));
        echo "
\t\t\t\t\t\t<button class=\"sign-in__btn\" type=\"submit\">";
        // line 22
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Sign In"), "method");
        echo "</button>
\t\t\t\t\t\t<a href=\"";
        // line 23
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/restorePassword"), "method");
        echo "\" class=\"sign-in__link\">";
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Forgot Password?"), "method");
        echo "</a>
\t\t\t\t\t";
        // line 24
        $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "endWidget", array(), "method");
        // line 25
        echo "\t\t\t\t</div>

\t\t\t\t<div class=\"sign-in__block\">
\t\t\t\t\t<h2 class=\"sign-in__h2\">";
        // line 28
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Not a member yet?"), "method");
        echo "</h2>
\t\t\t\t\t<a href=\"";
        // line 29
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/signUp"), "method");
        echo "\" class=\"sign-in__link\">";
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Register here in seconds"), "method");
        echo "</a>
\t\t\t\t</div>
\t\t\t\t";
        // line 31
        echo twig_include($this->env, $context, "main/facebook-container.twig");
        echo "
\t\t\t</div>

\t\t</div>
\t</div>


\t<div class=\"wrapper\">
\t\t";
        // line 39
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>
";
    }

    // line 43
    public function block_scripts($context, array $blocks = array())
    {
        // line 44
        echo "\t";
        echo twig_include($this->env, $context, "common/sign-in-form/notifier.twig", array("flash" => (isset($context["flash"]) ? $context["flash"] : null)));
        echo "
";
    }

    public function getTemplateName()
    {
        return "/views/main/sign-in.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 44,  127 => 43,  120 => 39,  109 => 31,  102 => 29,  98 => 28,  93 => 25,  91 => 24,  85 => 23,  81 => 22,  77 => 21,  72 => 20,  70 => 19,  65 => 17,  61 => 16,  57 => 15,  52 => 13,  43 => 6,  40 => 5,  36 => 2,  34 => 4,  11 => 2,);
    }
}
