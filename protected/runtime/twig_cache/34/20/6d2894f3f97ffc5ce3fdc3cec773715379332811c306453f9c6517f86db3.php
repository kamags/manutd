<?php

/* /views/main/letter.twig */
class __TwigTemplate_34206d2894f3f97ffc5ce3fdc3cec773715379332811c306453f9c6517f86db3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
\t<meta charset=\"UTF-8\">
\t<title>Document</title>
</head>
<body>
<div style=\"width: 650px;\">
\t<img src=\"/img/letter/body.png\" alt=\"\"/>
\t<p style=\"padding: 0 25px;\"><a href=\"http://manutdsocialcasino.com/\" style=\"color: #E01B22; text-decoration: none\" target=\"_blank\">Manchester United</a> Social Casino games are 100% free to play and bring millions of Man Utd fans from around the world together.</p>
\t<p style=\"padding: 0 25px;\">We're giving one lucky fan the chance to win a once in a lidetime VIP trip to Old Trafford.</p>
\t<p style=\"padding: 0 25px;\">How to enter the draw:</p>
\t<ul>
\t\t<li>Simply download Man Utd Social <a href=\"\" style=\"color: #E01B22;\" target=\"_blank\">Poker</a> or <a href=\"\" style=\"color: #E01B22;\">roulette</a> for one entry</li>
\t\t<li>Download both games and get three entries to the draw!</li>
\t</ul>
\t<a href=\"http://manutdsocialcasino.com/\" style=\"display: block; margin: 20px 0\" target=\"_blank\"><img style=\"display: block; margin: 0 auto;\" src=\"/img/letter/button.png\" alt=\"\"/></a>
\t<p style=\"padding: 0 25px;\">For more info visit<br /><a href=\"http://manutdsocialcasino.com\" style=\"color: #E01B22; text-decoration: none\" target=\"_blank\">www.manutdsocialcasino.com</a></p>
\t<p style=\"padding: 0 25px;\">Best of luck,<br />The MUSC Team</p>
\t<p style=\"padding: 0 25px;\">for full T&C's please <a href=\"http://manutdsocialcasino.com/en/terms-of-service/\" style=\"color: #E01B22;\" target=\"_blank\">CLICK HERE</a></p>
</div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "/views/main/letter.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
