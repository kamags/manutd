<?php

/* common/pagination.twig */
class __TwigTemplate_d3bd4571c8aca552cd6c12358d20c34c3e90ff97cf3e3c09f22a03d6cf78520f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 12
        echo "
";
        // line 13
        $context["hideLoadMoreButton"] = ((array_key_exists("hideLoadMoreButton", $context)) ? (_twig_default_filter((isset($context["hideLoadMoreButton"]) ? $context["hideLoadMoreButton"] : null),  !((isset($context["currentPage"]) ? $context["currentPage"] : null) == 1))) : ( !((isset($context["currentPage"]) ? $context["currentPage"] : null) == 1)));
        // line 14
        $context["hidePages"] = ((array_key_exists("hidePages", $context)) ? (_twig_default_filter((isset($context["hidePages"]) ? $context["hidePages"] : null), false)) : (false));
        // line 15
        $context["route"] = ((array_key_exists("route", $context)) ? (_twig_default_filter((isset($context["route"]) ? $context["route"] : null), (($this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "controller", array()), "id", array()) . "/") . $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "controller", array()), "action", array()), "id", array())))) : ((($this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "controller", array()), "id", array()) . "/") . $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "controller", array()), "action", array()), "id", array()))));
        // line 16
        echo "
";
        // line 22
        if ( !(isset($context["hidePages"]) ? $context["hidePages"] : null)) {
            // line 23
            echo "\t";
            // line 28
            echo "\t";
            // line 36
            echo "
\t";
            // line 37
            if (((isset($context["lastPage"]) ? $context["lastPage"] : null) > 1)) {
                // line 38
                echo "\t\t";
                // line 39
                echo "\t\t";
                $context["showedPages"] = ((array_key_exists("showedPages", $context)) ? (_twig_default_filter((isset($context["showedPages"]) ? $context["showedPages"] : null), 7)) : (7));
                // line 40
                echo "\t\t";
                $context["showedPagesDelta"] = intval(floor(((isset($context["showedPages"]) ? $context["showedPages"] : null) / 2)));
                // line 41
                echo "\t\t";
                $context["delta1"] = ((isset($context["page"]) ? $context["page"] : null) - (isset($context["showedPagesDelta"]) ? $context["showedPagesDelta"] : null));
                echo " ";
                // line 42
                echo "\t\t";
                $context["delta2"] = ((isset($context["lastPage"]) ? $context["lastPage"] : null) - (isset($context["showedPages"]) ? $context["showedPages"] : null));
                echo " ";
                // line 43
                echo "
\t\t";
                // line 44
                $context["startPage"] = (((((isset($context["delta1"]) ? $context["delta1"] : null) > (isset($context["delta2"]) ? $context["delta2"] : null)) && ((isset($context["delta2"]) ? $context["delta2"] : null) >= 0))) ? (((isset($context["delta2"]) ? $context["delta2"] : null) + 1)) : ((((((isset($context["delta1"]) ? $context["delta1"] : null) <= (isset($context["delta2"]) ? $context["delta2"] : null)) && ((isset($context["delta1"]) ? $context["delta1"] : null) > 0))) ? ((isset($context["delta1"]) ? $context["delta1"] : null)) : (1))));
                // line 45
                echo "\t\t";
                $context["lastShowedPage"] = (((((isset($context["startPage"]) ? $context["startPage"] : null) + (isset($context["showedPages"]) ? $context["showedPages"] : null)) <= (isset($context["lastPage"]) ? $context["lastPage"] : null))) ? ((((isset($context["startPage"]) ? $context["startPage"] : null) + (isset($context["showedPages"]) ? $context["showedPages"] : null)) - 1)) : ((isset($context["lastPage"]) ? $context["lastPage"] : null)));
                // line 46
                echo "
\t\t";
                // line 48
                echo "\t\t<ul class=\"pagination\">
\t\t\t";
                // line 50
                echo "\t\t\t";
                // line 51
                echo "
\t\t\t";
                // line 52
                if (((isset($context["currentPage"]) ? $context["currentPage"] : null) > ((isset($context["showedPagesDelta"]) ? $context["showedPagesDelta"] : null) + 1))) {
                    // line 53
                    echo "\t\t\t\t";
                    // line 54
                    echo "\t\t\t\t";
                    echo $this->getAttribute($this, "page", array(0 => (isset($context["route"]) ? $context["route"] : null), 1 => (isset($context["routeParams"]) ? $context["routeParams"] : null), 2 => 1, 3 => (isset($context["currentPage"]) ? $context["currentPage"] : null)), "method");
                    echo "
\t\t\t\t";
                    // line 56
                    echo "\t\t\t\t";
                    if ((((isset($context["currentPage"]) ? $context["currentPage"] : null) - 1) > ((isset($context["showedPagesDelta"]) ? $context["showedPagesDelta"] : null) + 1))) {
                        // line 57
                        echo "\t\t\t\t\t<li class=\"pagination__link pagination__link_separator\"><span>";
                        echo ((array_key_exists("separatorText", $context)) ? (_twig_default_filter((isset($context["separatorText"]) ? $context["separatorText"] : null), "…")) : ("…"));
                        echo "</span></li>
\t\t\t\t";
                    }
                    // line 59
                    echo "\t\t\t";
                }
                // line 60
                echo "
\t\t\t";
                // line 62
                echo "\t\t\t";
                // line 63
                echo "\t\t\t";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(range((((((isset($context["currentPage"]) ? $context["currentPage"] : null) - (isset($context["showedPagesDelta"]) ? $context["showedPagesDelta"] : null)) > 0)) ? (((isset($context["currentPage"]) ? $context["currentPage"] : null) - (isset($context["showedPagesDelta"]) ? $context["showedPagesDelta"] : null))) : (1)), (((((isset($context["currentPage"]) ? $context["currentPage"] : null) + (isset($context["showedPagesDelta"]) ? $context["showedPagesDelta"] : null)) <= (isset($context["lastPage"]) ? $context["lastPage"] : null))) ? (((isset($context["currentPage"]) ? $context["currentPage"] : null) + (isset($context["showedPagesDelta"]) ? $context["showedPagesDelta"] : null))) : ((isset($context["lastPage"]) ? $context["lastPage"] : null)))));
                foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                    // line 64
                    echo "\t\t\t\t";
                    echo $this->getAttribute($this, "page", array(0 => (isset($context["route"]) ? $context["route"] : null), 1 => (isset($context["routeParams"]) ? $context["routeParams"] : null), 2 => $context["p"], 3 => (isset($context["currentPage"]) ? $context["currentPage"] : null)), "method");
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 66
                echo "
\t\t\t";
                // line 68
                echo "\t\t\t";
                if ((((isset($context["currentPage"]) ? $context["currentPage"] : null) + (isset($context["showedPagesDelta"]) ? $context["showedPagesDelta"] : null)) < (isset($context["lastPage"]) ? $context["lastPage"] : null))) {
                    echo " ";
                    // line 69
                    echo "\t\t\t\t";
                    if (((((isset($context["currentPage"]) ? $context["currentPage"] : null) + (isset($context["showedPagesDelta"]) ? $context["showedPagesDelta"] : null)) + 1) < (isset($context["lastPage"]) ? $context["lastPage"] : null))) {
                        // line 70
                        echo "\t\t\t\t\t<li class=\"pagination__link pagination__link_separator\"><span>";
                        echo ((array_key_exists("separatorText", $context)) ? (_twig_default_filter((isset($context["separatorText"]) ? $context["separatorText"] : null), "…")) : ("…"));
                        echo "</span></li>
\t\t\t\t";
                    }
                    // line 72
                    echo "\t\t\t\t";
                    echo $this->getAttribute($this, "page", array(0 => (isset($context["route"]) ? $context["route"] : null), 1 => (isset($context["routeParams"]) ? $context["routeParams"] : null), 2 => (isset($context["lastPage"]) ? $context["lastPage"] : null), 3 => (isset($context["currentPage"]) ? $context["currentPage"] : null)), "method");
                    echo "
\t\t\t";
                }
                // line 74
                echo "
\t\t\t";
                // line 76
                echo "\t\t\t";
                // line 77
                echo "\t\t</ul>
\t";
            }
        }
    }

    // line 23
    public function getlink($__route__ = null, $__routeParams__ = null, $__page__ = null, $__text__ = null, $__class__ = null)
    {
        $context = $this->env->mergeGlobals(array(
            "route" => $__route__,
            "routeParams" => $__routeParams__,
            "page" => $__page__,
            "text" => $__text__,
            "class" => $__class__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 24
            echo "\t\t<li class=\"pagination__link ";
            echo (isset($context["class"]) ? $context["class"] : null);
            echo "\">
\t\t\t<a href=\"";
            // line 25
            echo $this->getAttribute((isset($context["App"]) ? $context["App"] : null), "createUrl", array(0 => (isset($context["route"]) ? $context["route"] : null), 1 => twig_array_merge(((array_key_exists("routeParams", $context)) ? (_twig_default_filter((isset($context["routeParams"]) ? $context["routeParams"] : null), array())) : (array())), array("page" => (isset($context["page"]) ? $context["page"] : null)))), "method");
            echo "\">";
            echo ((array_key_exists("text", $context)) ? (_twig_default_filter((isset($context["text"]) ? $context["text"] : null), (isset($context["page"]) ? $context["page"] : null))) : ((isset($context["page"]) ? $context["page"] : null)));
            echo "</a>
\t\t</li>
\t";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 28
    public function getpage($__route__ = null, $__routeParams__ = null, $__page__ = null, $__currentPage__ = null, $__text__ = null, $__class__ = null)
    {
        $context = $this->env->mergeGlobals(array(
            "route" => $__route__,
            "routeParams" => $__routeParams__,
            "page" => $__page__,
            "currentPage" => $__currentPage__,
            "text" => $__text__,
            "class" => $__class__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 29
            echo "\t\t";
            if (((isset($context["page"]) ? $context["page"] : null) != (isset($context["currentPage"]) ? $context["currentPage"] : null))) {
                // line 30
                echo "\t\t\t";
                echo $this->getAttribute($this, "link", array(0 => (isset($context["route"]) ? $context["route"] : null), 1 => (isset($context["routeParams"]) ? $context["routeParams"] : null), 2 => (isset($context["page"]) ? $context["page"] : null), 3 => (isset($context["text"]) ? $context["text"] : null), 4 => (isset($context["class"]) ? $context["class"] : null)), "method");
                echo "
\t\t";
            } elseif ( !            // line 31
(isset($context["class"]) ? $context["class"] : null)) {
                // line 32
                echo "\t\t\t";
                // line 33
                echo "\t\t\t<li class=\"pagination__link active\"><span>";
                echo ((array_key_exists("text", $context)) ? (_twig_default_filter((isset($context["text"]) ? $context["text"] : null), (isset($context["page"]) ? $context["page"] : null))) : ((isset($context["page"]) ? $context["page"] : null)));
                echo "</span></li>
\t\t";
            }
            // line 35
            echo "\t";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "common/pagination.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 35,  215 => 33,  213 => 32,  211 => 31,  206 => 30,  203 => 29,  187 => 28,  171 => 25,  166 => 24,  151 => 23,  144 => 77,  142 => 76,  139 => 74,  133 => 72,  127 => 70,  124 => 69,  120 => 68,  117 => 66,  108 => 64,  103 => 63,  101 => 62,  98 => 60,  95 => 59,  89 => 57,  86 => 56,  81 => 54,  79 => 53,  77 => 52,  74 => 51,  72 => 50,  69 => 48,  66 => 46,  63 => 45,  61 => 44,  58 => 43,  54 => 42,  50 => 41,  47 => 40,  44 => 39,  42 => 38,  40 => 37,  37 => 36,  35 => 28,  33 => 23,  31 => 22,  28 => 16,  26 => 15,  24 => 14,  22 => 13,  19 => 12,);
    }
}
