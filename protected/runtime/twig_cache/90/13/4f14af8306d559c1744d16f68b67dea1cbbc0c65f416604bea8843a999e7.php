<?php

/* /views/unity/hands.twig */
class __TwigTemplate_90134f14af8306d559c1744d16f68b67dea1cbbc0c65f416604bea8843a999e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<body>

<div align='center' width='100%'>
<div width='97%'>


<p>";
        // line 8
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_01"), "method");
        echo "</p>

<p>";
        // line 10
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_02"), "method");
        echo "</p>

<div height=\"20\"></div>

<h2 color='#00FFFF'>";
        // line 14
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_royal_flush"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 19
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_royal_flush_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='0'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='AceDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='KingDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='QueenDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='JackDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='TenDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='AceSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='KingSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='QueenSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='JackSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='TenSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 64
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_straight_flush"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 69
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_straight_flush_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='17'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='JackClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='TenClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='NineClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='EightClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SevenClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='QueenHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='JackHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='TenHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='NineHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='EightHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 114
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_four_of_kind"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 119
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_four_of_kind_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='15'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SevenDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SevenHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SevenSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SevenClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='AceClubs' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='KingDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='KingHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='KingSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='KingClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='SixDiamonds' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 164
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_full_house"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 169
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_full_house_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='17'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='TwoDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='TwoHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='TwoSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='TenDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='TenClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='AceHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='AceSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='AceClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='KingDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='KingSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 214
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_flush"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 219
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_flush_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='17'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='AceClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='QueenClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='EightClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SixClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='FourClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='JackDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='NineDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='EightDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='FiveDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='ThreeDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 264
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_straight"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 269
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_straight_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='15'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='EightDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SevenClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SixHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='FiveHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='FourSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='JackHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='TenDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='NineDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='EightSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='SevenClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 314
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_three_of_kind_hands"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 319
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_three_of_kind_hands_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='17'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='FiveDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='FiveHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='FiveSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='JackDiamonds' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='ThreeSpades' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='AceDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='AceSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='AceClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='QueenHearts' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='SevenDiamonds' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 364
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_two_pair"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 369
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_two_pair_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='17'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='KingDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='KingClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SevenDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='SevenHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='AceClubs' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='TenDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='TenHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='NineHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='NineSpades' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='EightDiamonds' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 414
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_one_pair"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 419
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_one_pair_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='15'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='ThreeDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='ThreeHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='JackSpades' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='TenSpades' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='EightDiamonds' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='QueenDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='QueenHearts' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='TenSpades' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='FourDiamonds' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='ThreeDiamonds' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 464
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_high_card"), "method");
        echo "</h2>

<table>
\t<tr>
\t\t<td valign='top' width='100%'>
\t\t\t<div>";
        // line 469
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_high_card_content"), "method");
        echo "</div>
\t\t</td>
\t\t<td width='15'></td>
\t\t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='KingDiamonds' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='TenDiamonds' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='FiveClubs' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='FourHearts' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td height=\"90\">
\t\t\t\t\t\t<img uistyle='ThreeHearts' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='QueenClubs' width='65' effect='highlight' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='EightSpades' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='SevenSpades' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='FiveHearts' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<img uistyle='TwoHearts' width='65' effect='fade' height='80'/>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</td>
\t</tr>
</table>


<h2 color='#00FFFF'>";
        // line 514
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_kicker"), "method");
        echo "</h2>

<p>";
        // line 516
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "unity", 1 => "hands_kicker_content"), "method");
        echo "</p>


</div>
</div>

</body>
</html>

";
    }

    public function getTemplateName()
    {
        return "/views/unity/hands.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  605 => 516,  600 => 514,  552 => 469,  544 => 464,  496 => 419,  488 => 414,  440 => 369,  432 => 364,  384 => 319,  376 => 314,  328 => 269,  320 => 264,  272 => 219,  264 => 214,  216 => 169,  208 => 164,  160 => 119,  152 => 114,  104 => 69,  96 => 64,  48 => 19,  40 => 14,  33 => 10,  28 => 8,  19 => 1,);
    }
}
