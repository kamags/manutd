<?php

/* common/form/file.twig */
class __TwigTemplate_8d638493eb3c9f02f31f4807567054d76425b4bf8e773a4748e46712d52d4566 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
\t";
        // line 2
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "labelEx", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null)), "method");
        echo "
\t";
        // line 3
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fileField", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null), 2 => ((array_key_exists("htmlOptions", $context)) ? (_twig_default_filter((isset($context["htmlOptions"]) ? $context["htmlOptions"] : null), array())) : (array()))), "method");
        echo "
\t";
        // line 4
        echo $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "error", array(0 => (isset($context["model"]) ? $context["model"] : null), 1 => (isset($context["field"]) ? $context["field"] : null), 2 => array("class" => "error")), "method");
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "common/form/file.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
