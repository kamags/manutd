<?php

/* /views/ingame/european.twig */
class __TwigTemplate_0520838d98ccf8d82ae98028712fcd6ad083dbc23ece70647c33c39a22ffd6b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html lang=\"en-US\" dir=\"ltr\">
<head>
\t<title>European Roulette Rules</title>
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/ingame.css\" media=\"all\"/>
\t<meta charset=\"UTF-8\">
</head>
<body>
<p>A table for European roulette contains 37 numbers, 36 of which are split into three rows in ascending order. The remaining number (\"zero\") is at the beginning of the table.</p>
<img src=\"/img/screenshot-6.png\" alt=\"\"/>
<h2>Inside Bets</h2>

<h3>Straight Up</h3>
<p>A bet on a single number. Payout ratio is 35:1.</p>

<h3>Split</h3>
<p>A bet on any two adjacent numbers (vertical or horizontal), including any combinations of the zero with a number in the first column. A chip is placed on the neighboring border of the two cells. Payout ratio is 17:1.</p>

<h3>Street</h3>
<p>A bet on any three numbers in a column. You can also make bets on a combination of the zero with any two numbers in the first column (0, 1, 2 or 0, 2, 3). A chip is placed at the bottom of the selected column. Payout ratio is 11:1.</p>

<h3>Corner or Square</h3>
<p>A bet on any four numbers, composing a square on the table. A chip is placed at the point where the lines between these numbers intersect. Payout ratio is 8:1.</p>

<h3>Line Bet or Double Street</h3>
<p>A bet on any adjoining streets (six numbers in two columns). A chip is placed in between the bottoms of the selected columns. Payout ratio is 5:1.</p>

<h2>Outside Bets</h2>

<h3>Column</h3>
<p>A bet on any of the three columns (12 numbers). A chip is placed in the \"2 to 1\" cell to the right of the selected column. Payout ratio is 2:1.</p>

<h3>Dozen</h3>
<p>A bet on any of the 12-number blocks, which are 1–12, 13–24, and 24–36. A chip is placed in the corresponding external cell (\"1st Twelve\", \"2nd Twelve\", or \"3rd Twelve\"). Payout ratio is 2:1.</p>

<h3>Low/High</h3>
<p>A bet on any of the two blocks, where \"1–18\" is \"low\" and \"19–36\" means \"high\". A chip is placed in the corresponding external cell. Payout ratio is 1:1.</p>

<h3>Even/Odd</h3>
<p>A bet on even or odd numbers. A chip is placed in the corresponding external cell (\"Even\" or \"Odd\").Payout ratio is 1:1.</p>

<h3>Color or Red/Black</h3>
<p>A bet on red or black numbers. A chip is placed in the corresponding external cell (red or black rhombus). Payout ratio is 1:1.</p>

<h2>Announced (or Call) Bets</h2>

<p>You can also make the so-called announced bets (or call bets) that cover the adjacent numbers on the roulette wheel. These bets are usually placed on the special track cell. You are encouraged to choose between the five announced bets that are described below.</p>

<h3>Neighbors</h3>
<p>A bet on a number and its four neighbors on the wheel (two to the left and two to the right). The bet is evenly divided between them (1/5 of the bet on each number). Payout ratio for each of the five adjacent numbers is 35:1.</p>

<h3>Jeu Zéro (Zero Game)</h3>
<p>A bet on the seven numbers, which are the zero and its neighbors: 12, 35, 3, 26, 32, and 15. The bet is divided into four even parts. The chips are placed on the track as follows: one chip on number 26 with payout ratio 35:1, then other three chips on pairs 12 and 15, 35 and 32, 3 and 0 with payout ratio 17:1.</p>

<h3>Voisins Du Zéro (Neighbors of Zero)</h3>
<p>A bet on the zero and the extended set of its neighbors: 22, 18, 29, 7, 28, 12, 35, 3, 26, 0, 32, 15, 19, 4, 21, 2, and 25. The bet is divided into nine even parts: five chips on pairs 4 and 7, 12 and 15, 18 and 21, 19 and 22, 35 and 32 (payout ratio is 17:1), two chips on the corner composed of numbers 25, 26, 28, 29 (payout ratio is 8:1), and the two remaining chips on the column with numbers 0, 2, 3 (payout ratio is 11:1).</p>

<h3>Orphelins (Orphans)</h3>
<p>A bet on the numbers 1, 20, 14, 31, 9, 17, 34, 6 divided into five even parts: one chip on number 1 with a payout ratio of 35:1, and then other four chips on pairs 6 and 9, 14 and 17, 17 and 20, 31 and 34 with a payout ratio of 17:1.</p>

<h3>Le Tiers Du Cylindre (Thirds of the Wheel)</h3>
<p>A bet on the numbers 27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33. Six chips are placed by pairs on the following numbers: 36 and 33, 30 and 27, 24 and 23, 16 and 13, 11 and 10, 8 and 5 with a payout ratio of 17:1.</p>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "/views/ingame/european.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
