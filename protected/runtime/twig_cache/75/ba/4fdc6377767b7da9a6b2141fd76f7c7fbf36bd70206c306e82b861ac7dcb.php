<?php

/* /views/ingame/index.twig */
class __TwigTemplate_75ba4fdc6377767b7da9a6b2141fd76f7c7fbf36bd70206c306e82b861ac7dcb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html lang=\"en-US\" dir=\"ltr\">
<head>
\t<title>Roulette Rules</title>
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/ingame.css\" media=\"all\"/>
\t<meta charset=\"UTF-8\">
</head>
<body>
<p>Before you start a game, choose the roulette table that you want to play at. You can sort the available tables out in the Lobby by way of bets and roulette rules (American, European, and French roulettes are supported).</p>
<img src=\"/img/screenshot-1.png\" alt=\"\"/>
<p>At the beginning of each round, all the players make their bets. You can place your chips into the numbered cells (\"inside bets\") or into the external cells (\"outside bets\"). Each inside or outside bet corresponds to one or more pockets of the roulette wheel. For example, a chip placed on a single number is mapped to only one pocket, while making bets on red means that you're aiming at all red pockets. By clicking any external cell you can highlight the wheel pockets that it matches.</p>
<img src=\"/img/screenshot-2.png\" alt=\"\"/>
<p>Once all the players finish placing their bets, the roulette wheel starts spinning and, after some time, the ball falls into a pocket between 0 and 36.</p>
<p>Each of your bets matching the winning pocket will bring you again according to its payout ratio. Note that the gainis decreased by your bet that is currently placed on the table. For example, if you put 5 chips on red and the payout ratio is 1:1, then you'll get 5 additional chips (+100%) in case you win.</p>
<img src=\"/img/screenshot-3.png\" alt=\"\"/>
<p>The bets and their corresponding wheel pockets vary depending on the roulette rules.</p>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "/views/ingame/index.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
