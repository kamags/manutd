<?php

/* layouts/admin.twig */
class __TwigTemplate_865ce57c79d98e6a7fe5ce225731a72845c0c19c50cbc60a48bd1090e9e6345b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
\t<title>Manchester United Poker - Free to Play on your mobile device, social network or online</title>
\t<meta charset=\"UTF-8\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t";
        // line 8
        echo "\t<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\">
\t<link rel=\"stylesheet\" href=\"/vendor/datepicker/jquery-ui-1.11.4.custom/jquery-ui.min.css\">

\t";
        // line 12
        echo "\t<style type=\"text/css\">
\t\tinput.error, textarea.error {
\t\t\tborder: 1px red solid !important;
\t\t}

\t\tlabel.error {
\t\t\tcolor: #ff0000 !important;
\t\t}
\t</style>
</head>

<body>
<div class=\"container\">
\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t";
        // line 26
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["Nav"]) ? $context["Nav"] : null), "getMenu", array(0 => "admin"), "method"), "setHtmlOptions", array(0 => array("class" => "nav nav-tabs")), "method"), "render", array(0 => array(), 1 => true), "method");
        echo "
\t</div>

\t<div style=\"margin: 20px 0;\">
\t\t";
        // line 30
        $this->displayBlock("content", $context, $blocks);
        echo "
\t</div>
</div>

<script src=\"/vendor/jquery.min.js\"></script>
<script src=\"/vendor/datepicker/jquery-ui-1.11.4.custom/jquery-ui.min.js\"></script>
<script src=\"/js/admin.js\"></script>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "layouts/admin.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 30,  48 => 26,  32 => 12,  27 => 8,  19 => 1,);
    }
}
