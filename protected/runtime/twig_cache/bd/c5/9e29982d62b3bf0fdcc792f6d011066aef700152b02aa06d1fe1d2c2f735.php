<?php

/* common/sign-in-form/notifier.twig */
class __TwigTemplate_bdc59e29982d62b3bf0fdcc792f6d011066aef700152b02aa06d1fe1d2c2f735 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["flash"] = ((array_key_exists("flash", $context)) ? (_twig_default_filter((isset($context["flash"]) ? $context["flash"] : null), $this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "user", array()), "getFlash", array(0 => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "action", array()), "id", array())), "method"))) : ($this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "user", array()), "getFlash", array(0 => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "action", array()), "id", array())), "method")));
        // line 2
        echo "
";
        // line 3
        if ((isset($context["flash"]) ? $context["flash"] : null)) {
            // line 4
            echo "\t";
            // line 5
            echo "\t";
            $context["text"] = ((twig_length_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "errors", array()))) ? ("") : (((array_key_exists("text", $context)) ? (_twig_default_filter((isset($context["text"]) ? $context["text"] : null), (twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "common", 1 => (isset($context["flash"]) ? $context["flash"] : null)), "method")) . "!"))) : ((twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "common", 1 => (isset($context["flash"]) ? $context["flash"] : null)), "method")) . "!")))));
            // line 6
            echo "\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "errors", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                // line 7
                echo "\t\t";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($context["field"]);
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 8
                    echo "\t\t\t";
                    $context["text"] = ((((isset($context["text"]) ? $context["text"] : null) . "<div>") . $context["error"]) . "</div>");
                    // line 9
                    echo "\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 10
                echo "\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "\t<script type=\"text/javascript\">
\t\t\$(function () {
\t\t\t\$.notify('";
            // line 13
            echo strtr((isset($context["text"]) ? $context["text"] : null), "'", " ");
            echo "', '', {
//\t\t\t\tcontainer: \$('.sign-in').closest('.pages_bg'),
\t\t\t\tcontainer: \$('.pages__logo').closest('.wrapper'),
\t\t\t\t";
            // line 16
            if (twig_length_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "errors", array()))) {
                // line 17
                echo "\t\t\t\ttimeout: false,
\t\t\t\t";
            }
            // line 19
            echo "\t\t\t\taddClass: 'alert__form alert__";
            echo (isset($context["flash"]) ? $context["flash"] : null);
            echo "'
\t\t\t});
\t\t});
\t</script>
";
        }
    }

    public function getTemplateName()
    {
        return "common/sign-in-form/notifier.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 19,  68 => 17,  66 => 16,  60 => 13,  56 => 11,  50 => 10,  44 => 9,  41 => 8,  36 => 7,  31 => 6,  28 => 5,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
