<?php

/* /views/main/error.twig */
class __TwigTemplate_687b70c268f8e2b747fc5503108a64de8914b49ca6ce44630491efab985293d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(2);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["title"] = "AnyTalk";
        // line 2
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"wrapper\">
\t<div class=\"error-block\">
\t\t<h1>Error ";
        // line 6
        echo $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "code", array(), "array");
        echo "</h1>
\t\t<p>";
        // line 7
        echo (($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "message", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "message", array(), "array"), "Page not found")) : ("Page not found"));
        echo "</p>
\t</div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "/views/main/error.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 7,  46 => 6,  42 => 4,  39 => 3,  35 => 2,  33 => 1,  11 => 2,);
    }
}
