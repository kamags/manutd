<?php

/* main/casino-popup.twig */
class __TwigTemplate_a74ca5c6b735208431502675780d52724f191b24c6791cd1de0ebb016155f14a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"casino\" class=\"casino\">
\t<h2 class=\"casino__title\"></h2>
\t<div class=\"casino__content\">
\t\t<div class=\"casino-poker\">
\t\t\t<p>";
        // line 5
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "casino-text-1"), "method");
        echo "</p>
\t\t\t";
        // line 7
        echo "\t\t\t<a class=\"btn btn-casino\" href=\"";
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/playNow"), "method");
        echo "\" target=\"_blank\">Play now</a>
\t\t\t<p>";
        // line 8
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "popup-text-2"), "method");
        echo "</p>
\t\t\t<div class=\"popup-store\">
\t\t\t\t<a class=\"popup-store__item popup-store__item_ap\" href=\"https://itunes.apple.com/app/id947856678\" target=\"_blank\"></a>
\t\t\t\t<a class=\"popup-store__item popup-store__item_gl\" href=\"https://play.google.com/store/apps/details?id=com.kamagames.texaspokermu\" target=\"_blank\"></a>
\t\t\t\t<a class=\"popup-store__item popup-store__item_win\" href=\"http://www.windowsphone.com/en-us/store/app/manchester-united-social-poker/f7a9b108-b47a-4164-bd11-5f9fc1ff361e\" target=\"_blank\"></a>
\t\t\t\t<a class=\"popup-store__item popup-store__item_amazon\" href=\"http://www.amazon.com/KamaGames-Manchester-United-Social-Poker/dp/B00T9UPAAC/ref=sr_1_2?s=mobile-apps&ie=UTF8&qid=1424251869&sr=1-2&keywords=kamagames\" target=\"_blank\"></a>
\t\t\t\t<a class=\"popup-store__item popup-store__item_fb\" href=\"https://apps.facebook.com/manutdsocialpoker/\" target=\"_blank\"></a>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"casino-slots\">
\t\t\t<p>Play Social Slots now</p>
\t\t\t<a class=\"btn btn-casino btn-casino__coming-soon\">Play now</a>
\t\t\t<p>";
        // line 20
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "popup-text-2"), "method");
        echo "</p>
\t\t\t<div class=\"popup-store\">
\t\t\t\t<a class=\"popup-store__item popup-store__item_ap\" href=\"https://itunes.apple.com/us/app/manchester-united-social-slots/id1058789250?ls=1&mt=8\" target=\"_blank\"></a>
\t\t\t\t<a class=\"popup-store__item popup-store__item_gl\" href=\"https://play.google.com/store/apps/details?id=com.kamagames.slotsmu\" target=\"_blank\"></a>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"casino-roulette\">
\t\t\t<p>";
        // line 27
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "casino-text-2"), "method");
        echo "</p>
\t\t\t<a class=\"btn btn-casino btn-casino__coming-soon\">Play now</a>
\t\t\t<p>";
        // line 29
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "popup-text-2"), "method");
        echo "</p>
\t\t\t<div class=\"popup-store\">
\t\t\t\t<a class=\"popup-store__item popup-store__item_ap\" href=\"https://itunes.apple.com/app/id951094676\" target=\"_blank\"></a>
\t\t\t\t<a class=\"popup-store__item popup-store__item_gl\" href=\"https://play.google.com/store/apps/details?id=com.kamagames.roulettemu\" target=\"_blank\"></a>
\t\t\t\t<a class=\"popup-store__item popup-store__item_win\" href=\"https://www.windowsphone.com/s?appid=B95C77C1-8286-4B56-BEAD-EE74E9FD4C42\" target=\"_blank\"></a>
\t\t\t\t<a class=\"popup-store__item popup-store__item_amazon\" href=\"http://www.amazon.com/dp/B00U7YXHHM\" target=\"_blank\"></a>
\t\t\t\t<a style=\"float: right\" class=\"popup-store__item popup-store__item_fb\" href=\"https://apps.facebook.com/manutdsocialroulette/\" target=\"_blank\"></a>
\t\t\t</div>
\t\t</div>
\t\t
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "main/casino-popup.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 29,  59 => 27,  49 => 20,  34 => 8,  29 => 7,  25 => 5,  19 => 1,);
    }
}
