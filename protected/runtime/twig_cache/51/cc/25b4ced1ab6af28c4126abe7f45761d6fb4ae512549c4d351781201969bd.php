<?php

/* /views/promotions/item.twig */
class __TwigTemplate_51cc25b4ced1ab6af28c4126abe7f45761d6fb4ae512549c4d351781201969bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["metaKeywords"] = $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "keywords", array());
        // line 3
        $context["metaDescription"] = $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "description", array());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "\t<div class=\"wrapper\">
\t\t<a class=\"pages__logo\" href=\"/\"></a>
\t</div>
\t<div class=\"pages pages_bg\">

\t\t<div class=\"wrapper\">
\t\t\t<a class=\"back\" href=\"";
        // line 11
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "promotions/index"), "method");
        echo "\">← Back</a>

\t\t\t<h1>Promotions</h1>
\t\t\t<span class=\"pages-line\"></span>

\t\t\t<div class=\"pages-post\">

\t\t\t\t<div class=\"pages-play\">
\t\t\t\t\t<a id=\"play-now-btn\" target=\"_blank\" href=\"#casino\" class=\"btn play-now fancy-casino\">";
        // line 19
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "ingame", 1 => "play-now-btn"), "method");
        echo "</a>
\t\t\t\t</div>


\t\t\t\t<div class=\"pages-post__img\">
\t\t\t\t\t";
        // line 24
        $context["image"] = $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "getImage", array(), "method");
        // line 25
        echo "\t\t\t\t\t";
        if ((isset($context["image"]) ? $context["image"] : null)) {
            // line 26
            echo "\t\t\t\t\t\t<img src=\"";
            echo (isset($context["image"]) ? $context["image"] : null);
            echo "\" width=\"150\" height=\"83\">
\t\t\t\t\t";
        }
        // line 28
        echo "\t\t\t\t</div>
\t\t\t\t<div class=\"pages-post__content\">

\t\t\t\t\t<p class=\"date\">";
        // line 31
        echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "publicationDate", array()), "d M Y");
        echo "</p>

\t\t\t\t\t<h3>";
        // line 33
        echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "title", array());
        echo "</h3>

\t\t\t\t\t";
        // line 35
        echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "overview", array());
        echo "
\t\t\t\t\t";
        // line 36
        echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "content", array());
        echo "

\t\t\t\t\t<div class=\"addthis_native_toolbox\"></div>

\t\t\t\t</div>

\t\t\t\t<div class=\"clear\"></div>
\t\t\t</div>
\t\t\t";
        // line 44
        echo twig_include($this->env, $context, "main/facebook-container.twig");
        echo "

\t\t</div>
\t</div>


\t<div class=\"wrapper\">

\t\t";
        // line 52
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/promotions/item.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 52,  112 => 44,  101 => 36,  97 => 35,  92 => 33,  87 => 31,  82 => 28,  76 => 26,  73 => 25,  71 => 24,  63 => 19,  52 => 11,  44 => 5,  41 => 4,  37 => 1,  35 => 3,  33 => 2,  11 => 1,);
    }
}
