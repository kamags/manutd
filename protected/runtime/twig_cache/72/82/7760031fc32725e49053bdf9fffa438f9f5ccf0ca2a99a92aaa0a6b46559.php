<?php

/* /views/main/about.twig */
class __TwigTemplate_72827760031fc32725e49053bdf9fffa438f9f5ccf0ca2a99a92aaa0a6b46559 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layouts/common.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layouts/common.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "        <div class=\"wrapper\">
                <a class=\"pages__logo\" href=\"/\"></a>
        </div>
\t<div class=\"pages pages_bg\">
\t\t
                <div class=\"wrapper\">
\t\t\t<div class=\"text_block press\">
\t\t\t\t<h1>About</h1>
                                <span class=\"pages-line\"></span>

                                <div class=\"text_block\">
                                    <p>Manchester United Social Casino, is the destination website that represents the Global Partnership between Manchester United Football Club and KamaGames®.</p>

                                        <p>Manchester United Social Casino is where Social Casino game players can learn about new games, promotions, and general news about the Partnership.</p>

                                        <p>KamaGames is a Global Developer and Publisher of Social Games for Mobile Platforms, Online, Facebook, Steam, Xbox Live & Playstation Network. 
                                            The company possesses wide experience in successful game production. The flagship game of the company - Pokerist® Texas Poker - has been featured in top positions in Grossing Charts on 
                                            the AppStore and Google Play for the past three years and currently KamaGames has achieved over 80+ Mil downloads.</p>

                                        <p>As the Official Social Casino Games Partner of Manchester United, KamaGames® will launch a portfolio of Social Casino Games that will carry the Manchester United crest, imagery and theme. 
                                            The first of these games will include multiple versions of Social Poker, Roulette & Blackjack games. KamaGames looks forward to engaging with Manchester United’s worldwide audience of 
                                            fans and followers.</p>

                                        <p>Manchester United is one of the most popular and successful sports teams in the world, playing one of the most popular spectator sports on Earth. Through our 137-year heritage we 
                                            have won 62 trophies, enabling us to develop the world’s leading sports brand and a global community of 659 million followers. Our large, passionate community provides Manchester 
                                            United with a worldwide platform to generate significant revenue from multiple sources, including sponsorship, merchandising, product licensing, new media & mobile, broadcasting and match day.</p>
                                </div>

                                
                                ";
        // line 32
        echo twig_include($this->env, $context, "main/facebook-container.twig");
        echo "
\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>


\t<div class=\"wrapper\">

\t\t";
        // line 41
        echo twig_include($this->env, $context, "main/footer.twig");
        echo "
\t</div>

";
    }

    public function getTemplateName()
    {
        return "/views/main/about.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 41,  70 => 32,  39 => 3,  36 => 2,  11 => 1,);
    }
}
