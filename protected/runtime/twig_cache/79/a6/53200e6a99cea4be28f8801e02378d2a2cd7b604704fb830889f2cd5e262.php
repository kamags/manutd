<?php

/* main/_social-auth.twig */
class __TwigTemplate_79a653200e6a99cea4be28f8801e02378d2a2cd7b604704fb830889f2cd5e262 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a href=\"#\"
   class=\"button__fb sign-in__fb-btn js-facebook-auth\"
   data-sign-fb-url=\"";
        // line 3
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "createUrl", array(0 => "main/signInWithFacebook"), "method");
        echo "\"
   data-app-id=\"";
        // line 4
        echo $this->getAttribute($this->getAttribute((isset($context["App"]) ? $context["App"] : null), "params", array()), "itemAt", array(0 => "facebookAppId"), "method");
        echo "\">";
        echo $this->getAttribute((isset($context["Yii"]) ? $context["Yii"] : null), "t", array(0 => "signin", 1 => "Connect with Facebook"), "method");
        echo "</a>";
    }

    public function getTemplateName()
    {
        return "main/_social-auth.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  23 => 3,  19 => 1,);
    }
}
