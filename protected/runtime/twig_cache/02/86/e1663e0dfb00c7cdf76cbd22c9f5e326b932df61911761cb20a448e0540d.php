<?php

/* /views/ingame/american.twig */
class __TwigTemplate_0286e1663e0dfb00c7cdf76cbd22c9f5e326b932df61911761cb20a448e0540d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html lang=\"en-US\" dir=\"ltr\">
<head>
\t<title>American Roulette Rules</title>
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/ingame.css\" media=\"all\"/>
\t<meta charset=\"UTF-8\">
</head>
<body>
<p>A table for American roulette contains 38 numbers, 36 of which are split into three rows in ascending order. The two remaining numbers (\"zero\" and \"double zero\") are at the beginning of the table.</p>
<img src=\"/img/screenshot-4.png\" alt=\"\"/>

<h2>Inside Bets</h2>

<h3>Straight Up</h3>
<p>A bet on a single number.Payout ratio is 35:1.</p>

<h3>Split</h3>
<p>A bet on any two adjacent numbers (vertical or horizontal), including any combinations of the zero with a number in the first column. A chip is placed on the neighboring border of the two cells. Payout ratio is 17:1.</p>

<h3>Street</h3>
<p>A bet on any three numbers in a column. You can also make bets on a combination of the zero with any two numbers in the first column (0, 1, 2 or 0, 2, 3). A chip is placed at the bottom of the selected column. Payout ratio is 11:1.</p>

<h3>Corner or Square</h3>
<p>A bet on any four numbers composing a square on the table. A chip is placed at the point where the lines between these numbers intersect. Payout ratio is 8:1.</p>

<h3>Basket or Top Line</h3>
<p>A bet on any combination of 00, 0, 1, 2 and 3. A chip is placed at the point where the lines between 00, 0, and 2 intersect. Payout ratio is 6:1.</p>

<h3>Line Bet or Double Street</h3>
<p>A bet on any adjoining streets (six numbers in two columns). A chip is placed in between the bottoms of the selected columns. Payout ratio is 5:1.</p>

<h2>Outside Bets</h2>
<h3>Column</h3>
<p>A bet on any of the three columns (12 numbers). A chip is placed in the \"2 to 1\" cell to the right of the selected column. Payout ratio is 2:1.</p>

<h3>Dozen</h3>
<p>A bet on any of the 12-number blocks, which are 1–12, 13–24, and 24–36. A chip is placed in the corresponding external cell (\"1st Twelve\", \"2nd Twelve\" or \"3rd Twelve\"). Payout ratio is 2:1.</p>

<h3>Low/High</h3>
<p>A bet on any of the two blocks, where \"1–18\" is \"low\" and \"19–36\" means \"high\". A chip is placed in the corresponding external cell. Payout ratio is 1:1.</p>

<h3>Even/Odd</h3>
<p>A bet on even or odd numbers. A chip is placed in the corresponding external cell (\"Even\" or \"Odd\").Payout ratio is 1:1.</p>

<h3>Color or Red/Black</h3>
<p>A bet on red or black numbers. A chip is placed in the corresponding external cell (red or black rhombus). Payout ratio is 1:1.</p>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "/views/ingame/american.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
