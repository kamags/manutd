<?php

class UrlManager extends CUrlManager {
	public $urlRuleClass = 'UrlRule';

	protected function createUrlRule($route, $pattern) {
		if (is_array($route)) {
			if (isset($route['class'])) {
				return $route;
			}
		}
		return new $this->urlRuleClass($route, $pattern);
	}
}

class UrlRule extends CUrlRule {
	public $hasLanguage = true;
	public $languageVar = 'lang';
	public $languageRule = '\w\w(?:_\w+)?';

	public function __construct($route, $pattern) {
		if (is_array($route)) {
			foreach (array('hasLanguage') as $name) {
				if (isset($route[$name])) {
					$this->$name = $route[$name];
					unset($route[$name]);
				}
			}
		}

		// add support <lang> param in route pattern (if link hasn't host info and specific it)
		if ($this->hasLanguage && strncasecmp($pattern, 'http://', 7) && strncasecmp($pattern, 'https://', 8)) {
			$pattern = "/<{$this->languageVar}:{$this->languageRule}>/" . ltrim($pattern, '/');
		}
		parent::__construct($route, $pattern);
	}

	public function createUrl($manager, $route, $params, $ampersand) {
		if (isset($this->params[$this->languageVar]) && !isset($params[$this->languageVar])) {
			$params[$this->languageVar] = $this->getDefaultLanguage();
		}
		if (($url = parent::createUrl($manager, $route, $params, $ampersand)) !== false) {
			return str_replace('%2F', '/', $url);
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getDefaultLanguage() {
		return isset($_GET[$this->languageVar]) && array_key_exists($_GET[$this->languageVar], Yii::app()->params->itemAt('languages'))? $_GET[$this->languageVar]: Yii::app()->language;
	}

	public function parseUrl($manager, $request, $pathInfo, $rawPathInfo) {
		if (isset($this->params[$this->languageVar]) && !preg_match("~^{$this->languageRule}(?:/|$)~iu", $rawPathInfo)) {
			$rawPathInfo = $this->getDefaultLanguage() . '/' . ltrim($rawPathInfo, '/');
			$pathInfo    = $manager->removeUrlSuffix($rawPathInfo, $manager->urlSuffix);
		}
		return parent::parseUrl($manager, $request, $pathInfo, $rawPathInfo);
	}
}