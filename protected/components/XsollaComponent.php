<?php

Yii::import('ext.httpclient.*');

class XsollaComponent extends CApplicationComponent {
	const CACHE_KEY_PREFIX = 'Application.XsollaComponent.';
	const CACHE_TTL        = 600; // 10 minutes
	const TYPE_CHIPS      = 'chips';
	const TYPE_GOLD_COINS = 'golds';

	public $hostname;
	public $actions = array();

	public $cacheId = 'cache';

	/**
	 * @param bool $specialPrice
	 * @return array|false
	 */
	public function getChipsRates($specialPrice = false) {
		$response = $this->request(self::TYPE_CHIPS, $specialPrice);
		if (!is_array($response)) {
			return false;
		}
		array_walk($response, [$this, 'normalizeResponseLinks']);
		return $response;
	}

	/**
	 * @param bool $specialPrice
	 * @return array|false
	 */
	public function getGoldCoinsRates($specialPrice = false) {
		$response = $this->request(self::TYPE_GOLD_COINS, $specialPrice);
		if (!is_array($response)) {
			return false;
		}
		array_walk($response, [$this, 'normalizeResponseLinks']);
		return $response;
	}

	protected function normalizeResponseLinks(&$item, $key) {
		if (!is_array($item) || !isset($item['itemURL'])) {
			return;
		}
		$item['itemURL'] = $this->hostname . $item['itemURL'];
	}

	/**
	 * @param string $action
	 * @param bool   $specialPrice
	 * @return array|false
	 */
	protected function request($action, $specialPrice = false) {
		$specialPrice = $specialPrice? 1: 0;

		/** @var CCache $cache */
//		$cache    = $this->cacheId !== false? Yii::app()->getComponent($this->cacheId): null;
//		$cacheKey = md5(self::CACHE_KEY_PREFIX . "{$action}_{$specialPrice}_{$this->getUserId()}");
//
//		if ($cache !== null && ($data = $cache->get($cacheKey)) !== false && is_array($data)) {
//			return $data;
//		}

		$url    = isset($this->actions[$action])? $this->actions[$action]: $action;
		$client = new EHttpClient(sprintf($url, $this->getUserId(), $specialPrice));

		try {
			$data = json_decode($client->request()->getBody(), true);
			if (json_last_error() !== JSON_ERROR_NONE) {
				Yii::log('Incorrect response format', CLogger::LEVEL_ERROR, 'application.payments');
				return false;
			}

//			if ($cache !== null) {
//				$cache->set($cacheKey, $data, self::CACHE_TTL);
//			}

			return $data;
		} catch (EHttpClientException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, 'application.payments');
			return false;
		}
	}

	/**
	 * @return int
	 * @throws CHttpException
	 */
	private function getUserId() {
		/** @var WebUser $user */
		$user = Yii::app()->user;
		if ($user->isGuest) {
			return false;
		}
		return $user->id;
	}

	public function getBuyFrameContent(array $paymentData) {
		/** @var CCache $cache */
//		$cache    = $this->cacheId !== false? Yii::app()->getComponent($this->cacheId): null;
//		$cacheKey = md5(self::CACHE_KEY_PREFIX . $paymentData['itemURL']);
//
//		if ($cache !== null && ($data = $cache->get($cacheKey)) !== false) {
//			return $data;
//		}

		$client = new EHttpClient($paymentData['itemURL']);
		try {
			$data = $client->request()->getBody();
//			if ($cache !== null) {
//				$cache->set($cacheKey, $data, self::CACHE_TTL);
//			}
			return $data;
		} catch (EHttpClientException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, 'application.payments');
			return false;
		}
	}
}
