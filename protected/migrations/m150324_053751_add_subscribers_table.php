<?php

class m150324_053751_add_subscribers_table extends DbMigration {
    
	public function safeUp() {
		$this->createTable('subscribers', [
			'id'          => 'int(11) unsigned NOT NULL AUTO_INCREMENT',
			'email'       => 'varchar(150) NOT NULL',
			'created'     => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
	}

	public function safeDown() {
		$this->dropTable('registration');
	}
}
