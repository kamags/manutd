<?php

class m150424_020049_init_press_and_promotions_tables extends DbMigration {

	public function safeUp() {
		$this->createTable('press', [
			'id'              => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'alias'           => 'VARCHAR(255) NOT NULL',
			'title'           => 'VARCHAR(255) NOT NULL',
			'overview'        => 'TEXT NULL DEFAULT NULL',
			'content'         => 'TEXT NULL DEFAULT NULL',
			'images'          => 'TEXT NULL DEFAULT NULL',
			'active'          => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
			'publicationDate' => 'TIMESTAMP NULL DEFAULT NULL',
			'created'         => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
		$this->createTable('promotion', [
			'id'              => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'alias'           => 'VARCHAR(255) NOT NULL',
			'title'           => 'VARCHAR(255) NOT NULL',
			'overview'        => 'TEXT NULL DEFAULT NULL',
			'content'         => 'TEXT NULL DEFAULT NULL',
			'images'          => 'TEXT NULL DEFAULT NULL',
			'active'          => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
			'publicationDate' => 'TIMESTAMP NULL DEFAULT NULL',
			'created'         => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);

		// init press
		$content = [
			[
				'alias'           => '',
				'title'           => 'KamaGames attend United Live Event in Jakarta',
				'overview'        => '<p>KamaGames’ Business Development Manager, Kevin Egan, was recently invited by Manchester United to attend the UNITED:LIVE Event in Jakarta, Indonesia that was promoted to the United Army’s 25,000 fan club members.</p>',
				'content'         => '<p>UNITED:LIVE is Manchester United’s traveling fan event that brings a day of excitement and camaraderie to fans around the world to celebrate and root for their favorite team and players!</p>

				                                    <p>Thousands of devoted Red Devils fans turned up on the day to support their team taking on Leicester City, as the Game was broadcast live to the event location in Jakarta. Manchester United
				                                        did their fans proud by clocking up 3 fantastic goals on the scoreboard.</p>

				                                    <p>KamaGames representatives met with a number of Indonesian Partners and prospects, and conducted some exciting discussions about future business deals. There was also a Kamagames’
				                                        sponsored promotion during the event, where a lucky fan won a Manchester United Jersey signed by Michael Carrick. Denis Irwin, a former United Player and an official club ambassador,
				                                        presented the delighted fan with his prized jersey.</p>

				                                    <p>For more on the event and about Manchester United’s UNITED:LIVE, Please Click Here</p>',
				'publicationDate' => '2015-02-06 15:59:15',
			],
			[
				'alias'           => '',
				'title'           => 'KamaGames become Manchester United\'s first Official Global Social Casino Games Partner',
				'overview'        => '<p>Manchester United and KamaGames have announced a global partnership and multi-year deal, in which KamaGames becomes the official Social Casino Games partner of the Club, bringing branded mobile and online games to the Club’s 659 million global followers. </p>',
				'content'         => '<p>KamaGames is a leading global developer and publisher of social games for mobile platforms, online, Steam, Xbox Live, and Playstation networks. The total audience for the KamaGames
				                                        portfolio currently reaches over 70 million users worldwide with over 1million active daily players.</p>

				                                    <p>The new partnership will see KamaGames launch a portfolio of social casino games which will carry the Manchester United branding, logo, imagery and themes.</p>

				                                    <p>Manchester United Group Managing Director, Richard Arnold comments:</p>

				                                    <p>“Manchester United is lucky enough to have a huge global following and we understand the importance of engaging and entertaining our fans. KamaGames and their innovative
				                                        products will allow us to do this in new and exciting ways. We are delighted to welcome them as our first Official Global Social Casino Games Partner and look forward to creating
				                                        a successful partnership for many years.”</p>

				                                    <p>KamaGames CEO, Danny Hammett comments:</p>

				                                    <p>“KamaGames and Manchester United share a similar worldwide audience, and with this in mind, we are confident that the partnership will be a huge success for both organisations.
				                                        We are proud and honoured to be associated with such a great team and iconic brand and we look forward to building a strong relationship with the club and its fans. “</p>',
				'publicationDate' => '2015-02-11 15:59:15',
			],
			[
				'alias'           => '',
				'title'           => 'Manchester United and KamaGames launch Social Poker',
				'overview'        => '<p>Manchester United and Official Global Social Casino Games Partner KamaGames have today unveiled the first in a series of specially designed social casino games for Manchester United fans worldwide.</p>',
				'content'         => '<p>At an event held at the Club’s famous Old Trafford stadium, Manchester United’s Group Head of Marketing, Jonathan Rigby, KamaGames CEO, Danny Hammett and first team players, Wayne Rooney,
				                                        Jonny Evans, Rafael, Tyler Blackett and Anders Lindegaard were on hand to celebrate the launch of Manchester United Social Poker, Powered By KamaGames.</p>

				                                    <p>Available to play online, on Facebook or downloaded to your mobile devices, Manchester United Social Poker, Powered By KamaGames give the club’s fans the opportunity to interact and play
				                                        social poker with fellow supporters around the world. As a social casino game, no money is won, however players can participate in tournaments with the opportunity to win Manchester United prizes,
				                                        tickets, memorabilia, trips, special events.</p>

				                                    <p>Manchester United’s Head of Marketing, Jonathan Rigby comments:</p>

				                                    <p>“Manchester United Social Poker, Powered By KamaGames is a new and exciting way for our fans to not only interact with the Club but also with each other. KamaGames is one of the
				                                        industry’s leading developers in this area and we are pleased to be teaming up with them to offer these products. This is just the first social casino game to be launched as part
				                                        of the partnership and we are looking forward to introducing fans to many more.”</p>

				                                    <p>Danny Hammett, CEO of KamaGames comments:</p>

				                                    <p>“KamaGames and Manchester United have many things in common, sharing the same dedication to excellence and providing our players with best game experiences possible. We will continue
				                                        to innovate and entertain the Manchester United Global fan base through our Social Poker Game as well with our soon coming Social Casino.”</p>

				                                    <p>Manchester United Social Poker is a social casino game and no money is exchanged or won.</p>
				',
				'publicationDate' => '2015-02-18 15:59:15',
			],
			[
				'alias'           => '',
				'title'           => 'KickTV Cover Press Launch Event for Manchester United Social Poker Powered By KamaGames',
				'overview'        => '<p>Kick TV are a global soccer media brand known for such video programming as The Mixer, "#FlipHenry," and a whole host of shows starring former US international and MLS star Jimmy Conrad, and are a football media platform with nearly one million subscribers on YouTube.</p>',
				'content'         => '<p>They teamed up with KamaGames to video coverage of the launch of the first Manchester United Social Casino game, Social Poker, which has been developed by KamaGames.</p>

				                                    <p>Kick TV are giving away a Manchester United Jersey signed by Louis Saha and Denis Irwin. Winners will be chosen on March 13th and announced on Social Media Channels Facebook and Twitter.</p>

				                                    <p>The Social Poker Game is available to play on your Mobile, Tablet or PC and can be downloaded from any of the AppStores. Click Here to Download the Game Now!</p> ',
				'publicationDate' => '2015-02-26 15:59:15',
			],
			[
				'alias'           => '',
				'title'           => 'Manchester United\'s Biggest Fans Play Poker with Legends Dwight Yorke & Andy Cole',
				'overview'        => '<p>Dwight Yorke & Andrew Cole took part in a Private Social Poker game with Two Lucky Fans today! The promotion was part of a Social Media Campaign, whereby you had to tell us Why You Were Man United’s greatest fan.</p>',
				'content'         => '<p>Manchester United, in conjunction with KamaGames, completed a successful social media promotional campaign in which Man United fans were asked to submit, using photos
				                                    or video and the hashtag "#mubiggestfan", the reason they considered themselves the club\'s biggest fan.  Submissions were collected over a 4-day period and winners were announced the evening
				                                    before the event. The event, which featured Manchester United Legends Andy Cole and Dwight Yorke, took place on 2nd March 2015 and allowed the fan-winners to play several hands of social poker with
				                                    each other and the Legends! </p>

				                                    <p>The two winners, Ivan & Bob, both submitted excellent photos of their devotion to Manchester United, and as a result, they were selected to join Cole & Yorke at
				                                    1.30pm in the Poker Room. Bob from Dublin, turned out to be the clear expert on the day, with Ivan and Andy not too far behind him! Congrats Lads ! Keep up the Good Play!</p>
				',
				'publicationDate' => '2015-03-02 15:59:15',
			],
		];

		foreach ($content as $contentItem) {
			$contentItem['alias']  = ActiveRecord::prepareStringForUrl($contentItem['title']);
			$contentItem['images'] = serialize(['/img/press/mu.jpg']);
			$this->insert('press', $contentItem);
		}

		// init promotions
		$content = [
			[
				'title'           => 'Win prizes with Manchester United Social Poker',
				'overview'        => '<p>The Card Player Poker Tour III – Beach Poker Classic $200,000 Guarantee Main Event takes place on April 30th, 2015 over four days at the Sonesta Maho Beach Resort & Casino in St. Maarten, Caribbean</p>',
				'content'         => '<p>Manchester United and KamaGames are offering supporters the chance to win great prizes with the release of its first of many social casino games.</p>
										<a href="http://manucasino.com/9fa" target="_blank">SIMPLY DOWNLOAD HERE TO ENTER THE DRAW</a>

				                                    <p>Through playing Manchester United Social Poker, fans around the world can win a VIP package that includes hospitality tickets to a match at Old Trafford and a trip to see the team train at the Aon
				                                        Training Complex, all with travel and accommodation added. Meet-and-greet experiences with Reds stars are on offer, as are opportunities to claim signed United shirts.</p>

				                                    <p>By simply clicking here and downloading the game for free, participants are immediately entered into the prize draw. The app can be played on iOS, Android, Windows and Amazon phones and can also
				                                        be operated on Facebook. No money is won or exchanged, but in-game purchases are optional.</p>

				                                    <p>For updates, follow @MUSocialCasino on Twitter and visit the Manchester United Social Casino Facebook page.</p>

				                                    <p>Terms and conditions:</p>

				                                    <p>VIP trip to Old Trafford: The winner will pick a game of their choice in the 2015/16 season that includes travel to Manchester, accommodation, hospitality tickets, a visit to the Aon Training
				                                        complex to watch the team train and a signed shirt.</p>

				                                    <p>Meet and Greet: Five winners, each accompanied by one guest, will meet members of the first team squad and receive a signed shirt.</p>

				                                    <p>Signed shirt: Ten winners will receive a shirt signed by members of the first team squad.</p>

				                                    <p>Competition closes on 22 March 2015. One prize per winner. Winners will be selected at random, and contacted through the app. You must be contactable after the competition closes.  Manchester
				                                        United Social Poker powered by KamaGames is a free-to-play social casino game, whereby no money is exchanged and won. In-game purchases are optional.</p>',
				'publicationDate' => '2015-05-12 15:59:15',
			],
			[
				'title'           => 'Congratulations to Bobby From the UK who won our Kick TV Competition:',
				'overview'        => '<p>All you have to do to participate is play real money No Limit and Fixed Limit Texas Hold’em cash games daily through the stakes below and finish on the podium to win your share of over $1,200 in daily cash prizes.</p>',
				'content'         => '<p>We ran a Global Promotion over the past few weeks in association with Kick TV, with the prize being a Manchester United Jersey signed by Denis Irwin and Louis Saha.</p>

				                                    <p>All you had to do was download Manchester United Social Poker for a chance to win!</p>

				                                    <p>Well Done Bobby! We hope you like the Jersey!</p>

				                                    <p>Stay tuned to Kamagames.com & keep an eye out for more fantastic promotions.</p>',
				'publicationDate' => '2015-05-12 15:59:15',
			],
			[
				'title'           => 'Manchester United Social Casino Leverages Social Media Accounts',
				'overview'        => '<p>So, for a limited-time only, we are offering players who take the CarbonCasino for a spin, a free poker entry coupon, just for playing Casino games!</p>',
				'content'         => '<p>Manchester United Social Casino is leveraging its newly established social media accounts to engage with followers from around the world.</p>

				                                    <p>Manchester United Social Casino, Powered by KamaGames is leveraging its newly established social media accounts to engage with followers from around the world.
				                                    The accounts were recently created on social networks such as FaceBook, Twitter, Google+ and Instagram, in coordination with the club, and have consistently engaged
				                                    with thousands of fans from around the world.</p>

				                                    <p>The social network accounts are used to connect and interact with fans and to keep them updated on game events and promotions such as the recently launched ticket
				                                    giveaway promotion running through 22March 2015. For more information about this promotion, click here.</p>

				                                    <p>For more exposure to the United Social Casino, visit www.manutdsocialcasino.com, and to connect on social visit FaceBook, Twitter, Google+ and Instagram.</p>',
				'publicationDate' => '2015-05-12 15:59:15',
			],
			[
				'title'           => 'Manchester United and KamaGames launch Social Roulette',
				'overview'        => '<p>You can compete in one or all of the following stakes below that will enter you into a daily leaderboard that will reset as each day passes.</p>',
				'content'         => '<p>Through their partnership with Manchester United Football Club, KamaGames have today unveiled Manchester United Social Roulette, the second in a series of specially designed Social Casino games for United fans
				                                        worldwide.</p>

				                                    <p>Through their partnership with Manchester United Football Club, KamaGames have today unveiled Manchester United Social Roulette, the second in a series of specially designed Social Casino
				                                        games for United fans worldwide.</p>

				                                    <p>After successfully launching Social Poker in February, the Company announced the release of a Social Roulette Game, featuring both amazing graphics and promotions, which will appeal to
				                                        Roulette fans of all experience levels.</p>

				                                    <p>Registered players can play for a chance to win Manchester United prizes such as tickets, trips & merchandise and can also invite and chat with friends at the table</p>

				                                    <p>Even more, players can choose the type of game they want to play, from classic European Roulette, Elegant French Roulette, or Risky and “like-in-the-movies” American Roulette!</p>

				                                    <p>It’s a Free-to-Play game, and is available on FaceBook, iOS, Android, Windows Mobile and Amazon Kindle devices. Download now at www.manutdsocialcasino.com</p>

				                                    <p>Danny Hammett, CEO of KamaGames comments</p>

				                                    <p>“KamaGames are delighted to introduce Social Roulette to what will become a suite of Social Casino Games. We have developed this game with the Manchester United fan
				                                        in mind, while also keeping to our highest standards of Game Production. We are confident that the game will be received well, and look forward to continuing to develop several Social Casino Games.”</p>

				                                    <p>About Manchester United</p>

				                                    <p>Manchester United is one of the most popular and successful sports teams in the world, playing one of the most popular spectator sports on Earth. Through our 137-year heritage we have won 62 trophies,
				                                        enabling us to develop the world’s leading sports brand and a global community of 659 million followers. Our large, passionate community provides Manchester United with a worldwide platform to
				                                        generate significant revenue from multiple sources, including sponsorship, merchandising, product licensing, new media & mobile, broadcasting and match day.</p>

				                                    <p>About KamaGames</p>

				                                    <p>KamaGames® is a Global Developer and Publisher of Social Games for Mobile Platforms, Online, Facebook, Steam, Xbox Live & Playstation Network. The company possesses wide experience in successful
				                                        game production. The flagship game of the company - Pokerist® Texas Poker - has been featured in top positions in Grossing Charts on the AppStore and Google Play for the past three years and
				                                        currently KamaGames has achieved over 70+ Mil downloads.</p>',
				'publicationDate' => '2015-05-12 15:59:15',
			],
			[
				'title'           => 'Manchester United and KamaGames Announce New Ticket Giveaway Promotion',
				'overview'        => '<p>You can compete in one or all of the following stakes below that will enter you into a daily leaderboard that will reset as each day passes.</p>',
				'content'         => '<p>Manchester United Social Casino, Powered by KamaGames this week announced a new ticket giveaway promotion for registered users of both Manchester United Social Poker and Manchester United Social Roulette.</p>

				                                    <p>Manchester United Social Casino, Powered by KamaGames this week announced a new ticket giveaway promotion for registered users of both Manchester United Social Poker and Manchester United Social Roulette
				                                        that places the winners at either the Manchester United vs. Aston Villa match on 4 April 2015, or the Manchester United vs. Manchester City match on 12 April 2015.</p>

				                                    <p>For more information on these two exciting promotions, visit:  http://manutdsocialcasino.com/en/apriltix/</p>

				                                    <p>About Manchester United</p>

				                                    <p>Manchester United is one of the most popular and successful sports teams in the world, playing one of the most popular spectator sports on Earth.
				                                        Through our 137-year heritage we have won 62 trophies, enabling us to develop the world’s leading sports brand and a global community of 659 million followers. Our large,
				                                        passionate community provides Manchester United with a worldwide platform to generate significant revenue from multiple sources, including sponsorship, merchandising, product
				                                        licensing, new media & mobile, broadcasting and match day.</p>

				                                    <p>About KamaGames</p>

				                                    <p>KamaGames® is a Global Developer and Publisher of Social Games for Mobile Platforms, Online, Facebook, Steam, Xbox Live & Playstation Network. The company possesses wide experience
				                                        in successful game production. The flagship game of the company - Pokerist® Texas Poker - has been featured in top positions in Grossing Charts on the AppStore and Google Play for the
				                                        past three years and currently KamaGames has achieved over 70+ Mil downloads.</p>
				',
				'publicationDate' => '2015-05-12 15:59:15',
			],
			[
				'title'           => 'Manchester United Social Casino Announces Updated Social Casino Website',
				'overview'        => '<p>You can compete in one or all of the following stakes below that will enter you into a daily leaderboard that will reset as each day passes.</p>',
				'content'         => '<p>Manchester United Social Casino, Powered by KamaGames this week announced the launch of its updated Social Casino Website.</p>

				                                    <p>Manchester United Social Casino, Powered by KamaGames this week announced the launch of its updated Social Casino Website. The new website coincided with the launch and announcement
				                                        of the new Manchester United Social Roulette game.</p>

				                                    <p>The new website at www.manutdsocialcasino.com, which will continue to be further developed, features bold colors and 3-D images depicting club-related themes and celebrating its position as
				                                        one of the leading brands globally. The simple user-interface easily allows website visitors to quickly navigate to any listed game and get playing!</p>

				                                    <p>Manchester United Social Casino plans to release additional Social Casino Games in the comping weeks and months, so stay tuned!</p>',
				'publicationDate' => '2015-05-12 15:59:15',
			],
		];

		foreach ($content as $i => $contentItem) {
			$contentItem['alias']  = ActiveRecord::prepareStringForUrl($contentItem['title']);
			$contentItem['images'] = serialize(['/img/promotions/page_0' . ++$i . '.jpg']);
			$this->insert('promotion', $contentItem);
		}
	}

	public function safeDown() {
		$this->dropTable('promotion');
		$this->dropTable('press');
	}
}
