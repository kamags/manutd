<?php

class m150505_052624_init_registration_and_restore_password_tables extends DbMigration {

	public function safeUp() {
		$this->createTable('registration', [
			'id'          => 'int(11) unsigned NOT NULL AUTO_INCREMENT',
			'nickname'    => 'varchar(50) NOT NULL',
			'login'       => 'varchar(150) NOT NULL',
			'password'    => 'varchar(50) NOT NULL',
			'subscribe'   => 'tinyint(1) unsigned NOT NULL DEFAULT 0',
			'confirmHash' => 'varchar(100) NOT NULL',
			'created'     => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);

		$this->createTable('restore_password', [
			'id'          => 'int(11) unsigned NOT NULL AUTO_INCREMENT',
			'playerId'    => 'int(11) unsigned NOT NULL',
			'email'       => 'varchar(200) NOT NULL',
			'confirmHash' => 'varchar(100) NOT NULL',
			'created'     => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);

		$this->createTable('player', [
			'playerId'           => 'int(11) unsigned NOT NULL',
			'privacyShowProfile' => 'tinyint(1) unsigned NOT NULL DEFAULT 3',
			'privacyFriendsList' => 'tinyint(1) unsigned NOT NULL DEFAULT 3',
			'privacyGoldCoins'   => 'tinyint(1) unsigned NOT NULL DEFAULT 2',
			'privacySavedGames'  => 'tinyint(1) unsigned NOT NULL DEFAULT 2',
			'privacyStatistics'  => 'tinyint(1) unsigned NOT NULL DEFAULT 2',
			'privacyChips'       => 'tinyint(1) unsigned NOT NULL DEFAULT 2',
			'created'            => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (playerId)',
		]);
	}

	public function safeDown() {
		$this->dropTable('player');
		$this->dropTable('restore_password');
		$this->dropTable('registration');
	}
}
