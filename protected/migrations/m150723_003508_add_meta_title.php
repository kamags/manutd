<?php

class m150723_003508_add_meta_title extends DbMigration {
	public function safeUp() {
		$this->addColumn('press', 'keywords', 'TEXT NULL DEFAULT NULL');
		$this->addColumn('promotion', 'keywords', 'TEXT NULL DEFAULT NULL');
		$this->addColumn('press', 'description', 'TEXT NULL DEFAULT NULL');
		$this->addColumn('promotion', 'description', 'TEXT NULL DEFAULT NULL');

	}

	public function safeDown() {
		$this->dropColumn('press', 'keywords');
		$this->dropColumn('promotion', 'keywords');
		$this->dropColumn('press', 'description');
		$this->dropColumn('promotion', 'description');
	}
}
