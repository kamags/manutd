<?php

return [
	WebUser::ROLE_GUEST         => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Guest',
		'bizRule'     => null,
		'data'        => null
	],
	WebUser::ROLE_PLAYER        => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Player',
		'children'    => ['guest'], // extend guest
		'bizRule'     => null,
		'data'        => null
	],
	WebUser::ROLE_APP_MODERATOR => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Moderator',
		'children'    => ['player'], // extend player
		'bizRule'     => null,
		'data'        => null
	],
	WebUser::ROLE_ADMIN         => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Administrator',
		'children'    => ['moderator'], // extend moderator
		'bizRule'     => null,
		'data'        => null
	],
];
