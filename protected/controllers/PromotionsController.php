<?php

class PromotionsController extends BaseController {

	public function actionIndex($page = 1) {
		/** @var Promotion[] $promoItems */
	    
	    	$perPage    = (int) Yii::app()->params->itemAt('postsPerPage')? : 10;
		$countPosts = Promotion::model()->count();
		$lastPage   = ceil($countPosts / $perPage);
		
		if ($page > $lastPage) {
			throw new CHttpException(404, Yii::t('errors', 'Page {page} doesn\'t exist', ['{page}' => $page]));
		}
		
		$promoItems = Promotion::model()
				->last($perPage)
				->offset($perPage * ($page - 1))
				->findAll([
				    'order'=>'publicationDate desc',
				    ]);
		
		if (empty($promoItems)) {
			throw new CHttpException(404, Yii::t('errors', Yii::t('errors', 'There is no press')));
		}

		$this->render('index', [
			'promoItems' => $promoItems,
			'currentPage' => $page,
			'lastPage'    => $lastPage,
			'perPage'     => $perPage,
		]);
	}

	public function actionItem($alias) {
		$promo = Promotion::model()->findByAttributes([
			'alias' => $alias,
		]);

		$this->render('item', [
			'promo' => $promo,
		]);
	}
}