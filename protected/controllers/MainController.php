<?php

class MainController extends BaseController {

	public function filters() {
		return ['accessControl'];
	}

	public function actionIndex() {
		$this->render('index');
	}

//	public function actionLetter() {
//		$this->render('letter');
//	}

	public function actionAccount() {
		$this->render('account');
	}

	public function accessRules() {
		return [
			['deny', 'actions' => ['playNow', 'buyChips'], 'users' => ['?']],
			['allow', 'users' => ['*']],
		];
	}

	public function actionPlayNow($setVersion = null) {
		$url = 'https://manutd-app.pokerist.com/index_st.php?locale={language}&amp;standalone=1&amp;token={token}';
		$url = str_replace([
			'{language}',
			'{token}'
		], [
			Yii::app()->language,
			Yii::app()->user->sessionToken
		], $url);

		if ($setVersion !== null) {
			$url .= "&amp;setVersion={$setVersion}";
		}

		$this->render('play-now', [
			'url' => $url,
		]);
	}

//	public function actionBuyChips($type, $position, $mode) {
//		/** @var XsollaComponent $xsolla */
//		$xsolla = Yii::app()->xsolla;
//
//		$data = [
//			'normal'  => [
//				'chips' => $xsolla->getChipsRates(),
//				'gold'  => $xsolla->getGoldCoinsRates(),
//			],
//			'special' => [
//				'chips' => $xsolla->getChipsRates(true),
//				'gold'  => [],
//			],
//		];
//
//		if (!isset($data[$mode][$type][$position])) {
//			throw new CHttpException(404);
//		}
//
//		// get on frontend
//		$frame = '<iframe width="100%" height="98%" style="border: 0;" src="'
//			. (Yii::app()->request->isSecureConnection?
//				str_replace('http', 'https', $data[$mode][$type][$position]['itemURL']): $data[$mode][$type][$position]['itemURL'])
//			. '"></iframe>';
//
//		$this->render('/common/simple-content', [
//			'containerCssClass' => 'xsolla',
//			'content'           => $frame,
//		]);
//	}
	
	public function actionBuyChips() {
		/** @var XsollaComponent $xsolla */
		$xsolla = Yii::app()->xsolla;

		$data = [
			'normal'  => [
				'chips' => $xsolla->getChipsRates(),
				'gold'  => $xsolla->getGoldCoinsRates(),
			],
			'special' => [
				'chips' => $xsolla->getChipsRates(true),
				'gold'  => [],
			],
		];

		$this->render('/main/buy-chips', [
			'data' => $data,
		]);
	}

	public function actionJersey() {
		$this->render('jersey');
	}

	public function actionExperience() {
		$this->render('experience');
	}
	
	public function actionTos() {
		$this->render('tos');
	}

	public function actionGames($name) {
		$this->render('/games/' . $name);
	}

	public function actionSlotsHelp() {
		$this->render('/games/slots-help');
	}

	public function actionAbout() {
		$this->render('about');
	}

	public function actionSignIn() {

		if (!Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->user->returnUrl);
		}

		$model = new PokeristSignInForm();

		// collect user input data
		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];

			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				$this->redirect(Yii::app()->user->returnUrl);
			} else {
				Yii::app()->user->setFlash($this->action->id, 'error');
			}
		}

		// display the login form
		$this->render('sign-in', ['model' => $model]);
	}

	public function actionSignUp() {
		if (!Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->user->returnUrl);
		}

		$model = new Registration();

		// collect user input data
		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];

			if (($existedModel = Registration::model()->findByLogin($model->login)) !== null) {
				$model             = $existedModel;
				$model->attributes = $_POST[get_class($model)];
				$model->created    = new CDbExpression('NOW()');
			}

			// validate user input and redirect to the previous page if valid
			if ($model->save()) {
				$hostname = str_replace('http://', '', Yii::app()->request->getHostInfo('http'));
				$this->sendEmail(
					[$model->login => $model->nickname],
					Yii::t('signup', 'Registration on :host', [':host' => $hostname]),
					[
						'model' => $model,
						'host'  => $hostname,
					],
					'signup/registration'
				);

				Yii::app()->user->setFlash($this->action->id, 'success');
			} else {
				Yii::app()->user->setFlash($this->action->id, 'error');
			}
		}

		$this->render('sign-up', ['model' => $model]);
	}

	public function actionSignUpConfirm($hash) {
		if (!$registration = Registration::model()->active()->findByHash($hash)) {
			throw new CHttpException(404, Yii::t('common', "Confirm hash doesn't exists"));
		}

		try {
			if ($registration->confirm()) {
				$hostname = str_replace('http://', '', Yii::app()->request->getHostInfo('http'));
				$this->sendEmail(
					[$registration->login => $registration->nickname],
					Yii::t('signup', 'Your registration on :host has been successfully completed', [':host' => $hostname]),
					['model' => $registration, 'host' => $hostname],
					'signup/registration-success'
				);

				// try to auto login user
				$signInForm           = new PokeristSignInForm();
				$signInForm->login    = $registration->login;
				$signInForm->password = $registration->password;

				if ($signInForm->login()) {
					$this->redirect(Yii::app()->user->returnUrl);
				}
				$this->redirect(Yii::app()->user->loginUrl);
			}
		} catch (PokeristApi_Exception $e) {
			if ($e->getCode() == 'server_error') {
				throw new CHttpException(503, Yii::t('errors', 'Account with this login already exists'));
			}
			throw $e;
		}
		Yii::app()->user->setFlash($this->action->id, 'error');

		$this->render('sign-up-confirmation', [
			'registration' => $registration,
		]);
	}

	public function actionRestorePassword() {
		if (!Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->user->returnUrl);
		}

		$model = new RestorePassword();

		// collect user input data
		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];

			// validate user input and redirect to the previous page if valid
			if ($model->save()) {
				$hostname = str_replace('http://', '', Yii::app()->request->getHostInfo('http'));

				$this->sendEmail(
					[$model->email => $model->email],
					Yii::t('restore-password', 'Changing password on :host', [':host' => $hostname]),
					[
						'model'       => $model,
						'host'        => $hostname,
						'confirmLink' => $this->createAbsoluteUrl('main/restorePasswordConfirm', ['hash' => $model->confirmHash])
					],
					'restore-password/restore-password'
				);

				Yii::app()->user->setFlash($this->action->id, 'success');
			} else {
				Yii::app()->user->setFlash($this->action->id, 'error');
			}
		}

		$this->render('restore-password', ['model' => $model]);
	}

	public function actionRestorePasswordConfirm($hash) {

		/** @var RestorePassword $restorePassword */
		if (($restorePassword = RestorePassword::model()->active()->findByHash($hash)) === null) {
			throw new CHttpException(404, Yii::t('common', "Confirm hash doesn't exists"));
		}

		$model = new ChangePassword();

		$class = get_class($model);
		if (isset($_POST[$class])) {
			$model->newPassword        = $_POST[$class]['newPassword'];
			$model->newPassword_repeat = $_POST[$class]['newPassword_repeat'];

			if (!$model->validate()) {
				Yii::app()->user->setFlash($this->action->id, 'error');
			} else {
				if (!$player = $restorePassword->confirm($_POST[$class]['newPassword'])) {
					Yii::app()->user->setFlash($this->action->id, 'error');
				};

				$signInForm           = new PokeristSignInForm();
				$signInForm->login    = $player['email'];
				$signInForm->password = $player['password'];

				if ($signInForm->login()) {
					Yii::app()->user->setFlash($this->action->id, 'success');
				} else {
					$this->redirect(CHtml::normalizeUrl(Yii::app()->user->loginUrl));
				}
			}
		}

		$this->render('restore-password-confirm', [
			'model' => $model
		]);
	}

	public function actionSignOut() {
		/** @var $user CWebUser */
		$user = Yii::app()->user;
		$user->logout();
		Yii::app()->request->redirect($this->createUrl('index'));
	}

	/**
	 * @param int    $uid
	 * @param string $token
	 * @throws Exception
	 * identify fb player within yii
	 */
	public function actionSignInWithFacebook($uid = null, $token = null) {
		if ($uid && $token) {
			$fbIdentity = new FacebookPlayerIdentity($uid, $token);
			/** @var WebUser $model */
			$model = Yii::app()->user;

			$fbIdentity->authenticate();
			if ($fbIdentity->errorCode == FacebookPlayerIdentity::ERROR_NONE) {
				if ($model->login($fbIdentity)) {
					$this->redirect(Yii::app()->user->returnUrl);
				}
			} else {
				$this->redirect($this->createUrl('signIn'));
			}
			return;
		}
//		$this->redirect()
	}

	public function actionPress() {
		$this->render('press');
	}

	public function actionApriltix() {
		$this->render('apriltix');
	}

	public function actionPolicy() {
		$this->render('policy');
	}

//	public function actionSupport() {
//		$this->render('support');
//	}

	public function actionGetPlayersOnline() {
		$bottom = 12000;
		$top = 13000;
		$increment = mt_rand(5, 20);

		$currentValue = Yii::app()->cache->get(__METHOD__);
		if ($currentValue === false) {
			$currentValue = $bottom;
		}

		$sign = Yii::app()->cache->get(__METHOD__ . '.sign');
		if ($sign === false) {
			$sign = 1;
		}

		$currentValue += $sign * $increment;

		if ($currentValue >= $top) {
			Yii::app()->cache->set(__METHOD__ . '.sign', -1);
		}

		if ($currentValue <= $bottom) {
			Yii::app()->cache->set(__METHOD__ . '.sign', 1);
		}
		Yii::app()->cache->set(__METHOD__, $currentValue);

		echo $currentValue;
	}

	public function actionPage($path) {

		try {
			$this->render($path);
		} catch (CException $e) {
			Yii::log("Failed to render page $path", CLogger::LEVEL_WARNING);

			throw new CHttpException(404);
		}
	}

	public function actionSubscribe() {

		$subscribers = new Subscribers();

		if (isset($_POST[get_class($subscribers)])) {
			$subscribers->attributes = $_POST[get_class($subscribers)];
			
			if ($subscribers->save()) {
				$mail = new YiiMailMessage("Message from " . Yii::app()->name);
				$mail->from = Yii::app()->params->itemAt('supportEmail');
				$mail->to =  $subscribers->email;
				$mail->view = 'support/subscribe';
				$mail->setBody(['subscribers' => $subscribers], 'text/plain');
				Yii::app()->mail->send($mail);
			}
		}

	}

	public function actionError() {
		$this->layout = 'error';
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				try {
					$this->render("error{$error['code']}", ['error' => $error]);
				} catch (CException $e) {
					$this->render("error", ['error' => $error]);
				}
			}
		}
	}
}
