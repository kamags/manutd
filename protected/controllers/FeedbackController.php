<?php

class FeedbackController extends BaseController {
	public function actionSupport() {


		$message = new Support();

		$support_success = false;

		if (Yii::app()->request->isPostRequest && isset($_POST[get_class($message)])) {
			$message->attributes = $_POST[get_class($message)];
                      
                        
			if ($message->validate()) {
				// send message

				$mail = new YiiMailMessage("Message from " . Yii::app()->name);
				$mail->from = [$message->email => $message->name ? : $message->email];
				 
                                $emailList = Yii::app()->params->itemAt('email');
                                
                                $mail->to =  Yii::app()->params->itemAt('supportEmail'); 
                                
                                if (array_key_exists($message->app, $emailList)) {
                                     $mail->to = $emailList[$message->app];    
                                }
                                
				$mail->view = 'support/support';
				$mail->setBody(['message' => $message], 'text/plain');
				Yii::app()->mail->send($mail);
				$support_success = true;
			}
		}

		$this->render('contact-us', [
			'message' => $message,
			'support_success' => $support_success,
		]);
                
                
                
                

//			$this->render('contact-us');
	}

}
