<?php

class PressController extends BaseController {
    

	public function actionIndex($page = 1) {
	    
		$perPage    = (int) Yii::app()->params->itemAt('postsPerPage')? : 10;
		$countPosts = Press::model()->count();
		$lastPage   = ceil($countPosts / $perPage);
		
		if ($page > $lastPage) {
			throw new CHttpException(404, Yii::t('errors', 'Page {page} doesn\'t exist', ['{page}' => $page]));
		}
		
		$pressItems = Press::model()
				 ->last($perPage)
				 ->offset($perPage * ($page - 1))
				 ->findAll();
		
		if (empty($pressItems)) {
			throw new CHttpException(404, Yii::t('errors', Yii::t('errors', 'There is no press')));
		}

		$this->render('index', [
			'pressItems' => $pressItems,
			'currentPage' => $page,
			'lastPage'    => $lastPage,
			'perPage'     => $perPage,
		]);
	}

	public function actionItem($alias) {
		
	    
	    
		$press = Press::model()->findByAttributes([
			'alias' => $alias,
		]);

		$this->render('item', [
			'press' => $press,
		]);
	}
}
