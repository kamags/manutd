<?php

class UnityController extends BaseController {
    

	public function actionunityHands() {
		$this->render('hands');	
	}
	
	public function actionunityActions() {
		$this->render('actions');	
	}
	
	public function actionunityBets() {
		$this->render('bets');	
	}
	
	public function actionunityRounds() {
		$this->render('rounds');	
	}
	
	public function actionunitySupport() {
		$this->render('support');	
	}

        public function actionunityTos() {
                $this->render('tos');
        }
	
}
