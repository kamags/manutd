<?php

abstract class BaseController extends CController {

	public $layout = null;
	private $_pageTitle;

	/**
	 * Map for convert language code from inner application format to ISO
	 * hack for params from application (ex.: /ingame/?lang=zh-chs)
	 * @var array
	 */
	protected $languageCodesMap = [

	];

	protected function beforeAction($action) {

		if (isset($_GET['lang'])) {
			// rewrite country code to ISO if needed
			if (array_key_exists($_GET['lang'], $this->languageCodesMap)) {
				$_GET['lang'] = $this->languageCodesMap[$_GET['lang']];
			}

			if (array_key_exists($_GET['lang'], Yii::app()->params->itemAt('languages'))) {
				Yii::app()->language = $_GET['lang'];
			}
		}

		// strict check localized url. redirect if not error
		if (isset($this->actionParams['lang'])) {
			$url = $this->createUrl($this->route, array_merge($this->actionParams, ['lang' => Yii::app()->language]));
			if ($url != Yii::app()->request->requestUri && !Yii::app()->errorHandler->error) {
				$this->redirect($url, true, 301);
			}
		}

		$actionId = Yii::app()->controller->action->id;

		LanguageDetector::rememberCurrentLanguage();

		Navigation::getMenu('top')
			->addItems([
				[
					'label' => 'Home',
					'url'   => ['main/index'],
				],
				[
					'label'       => 'Games',
					'linkOptions' => ['id' => 'menu-games'],
				],
				[
					'label' => 'About',
					'url'   => ['main/about'],
				],
				[
					'label' => 'Press',
					'url'   => ['press/index'],
				],
				[
					'label' => 'Promotions',
					'url'   => ['promotions/index'],
				],
				[
					'label' => 'Support',
					'url'   => ['feedback/support'],
				],
//				[
//					'label' => 'Store',
//					'url'   => ['main/buyChips'],
//				],
			]);

		Navigation::getMenu('admin')
			->addItems([
				[
					'label'  => 'Press list',
					'url'    => ['admin/pressList'],
					'active' => in_array($actionId, ['pressList', 'pressItem', 'pressCreate']),
				],
				[
					'label'  => 'Promotion list',
					'url'    => ['admin/promotionList'],
					'active' => in_array($actionId, ['promotionList', 'promotionItem', 'promotionCreate']),
				],
				[
					'label'       => 'Go to site',
					'url'         => ['main/index'],
					'itemOptions' => ['class' => 'pull-right'],
				],
			]);

		return parent::beforeAction($action);
	}

	public function renderJson(array $data) {
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($data, defined('JSON_UNESCAPED_UNICODE')? JSON_UNESCAPED_UNICODE: 0);
		Yii::app()->end();
	}

	public function getPageTitle() {
		if ($this->_pageTitle !== null) {
			return $this->_pageTitle;
		} else {
			return Yii::app()->name;
		}
	}

	public function sendEmail(array $to, $subject, $body, $view = null, $contentType = 'text/html') {
		$app = Yii::app();
		$mail = $app->mail;

		$msg = new YiiMailMessage($subject, null, $contentType, 'UTF-8');
		$msg->from = $app->params->contains('senderEmail')? $app->params['senderEmail']: 'no-reply@' . str_replace('http://', '', $app->request->getHostInfo('http'));
		$msg->to = $to;
		if ($view !== null) {
			$msg->view = $view;
		}
		$msg->setBody($body);
		return $mail->send($msg);
	}
}
