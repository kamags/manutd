<?php

class ProfileController extends BaseController {

	const ACHIEVEMENTS_COUNT = 3;

	/**
	 * Get profile info for own account or by id
	 * @param null|int $playerId
	 * @return array
	 */
	private static function getProfileInfo($playerId = null) {
		/** @var PokeristApi_Player $api */
		$api = Yii::app()->pokeristApi->getPlayerApi();
		if ($playerId) {
			return [
				'profile'     => $api->getPlayerById($playerId),
				'profileInfo' => $api->getPlayerInfoById($playerId),
			];
		} else {
			$profile = Yii::app()->player->profile;
			return [
				'profile'     => $profile,
				'profileInfo' => $api->getPlayerInfoById($profile['playerId']),
			];
		}
	}

	private static function isOwnProfile($playerId) {
		return $playerId == Yii::app()->player->profile['playerId'] || (bool) !$playerId;
	}

	/**
	 * @param null|int $playerId
	 * @param null|array $profileInfo
	 * @return array|null
	 */
	private static function getPrivacySettings($playerId = null, $profileInfo = null) {
		if (!($playerId && $player = Player::model()
				->findByPk(Yii::app()->player->profile['playerId']))
		) {
			return null;
		}
		return Player::model()
			->findByPk(Yii::app()->player->profile['playerId'])
			->checkVisibilityFromSettings($playerId, $profileInfo);
	}

	/**
	 * @return array action filters
	 */
	public function filters() {
		return ['accessControl'];
	}

	// admin actions
	public function accessRules() {
		return [
			['allow', 'roles' => ['player']],
			['allow', 'actions' => ['screenshot'], 'users' => ['*']],
			['deny', 'users' => ['*']],
		];
	}

	public function actionIndex($playerId = null) {
		$profileInfo = self::getProfileInfo($playerId);

		$this->breadcrumbs = [
			$profileInfo['profile']['nickname'],
		];

		$this->generateRightNavigation($playerId);

		// check if we have achievements, otherwise write random achievements
//		$preserveAchievements                       = $profileInfo['profileInfo']['achievements'];
//		$profileInfo['profileInfo']['achievements'] = array_filter($profileInfo['profileInfo']['achievements'], function ($item) {
//			return isset($item['current']);
//		});
//		if (($achDifference = self::ACHIEVEMENTS_COUNT - count($profileInfo['profileInfo']['achievements'])) > 0) {
//			shuffle($preserveAchievements);
//			$profileInfo['profileInfo']['achievements'] = array_merge($profileInfo['profileInfo']['achievements'], array_slice($preserveAchievements, 0, $achDifference));
//		} else {
//			shuffle($profileInfo['profileInfo']['achievements']);
//		}
//		unset($preserveAchievements);

		// filter by existing current or some progress in first stage (next)
		$profileInfo['profileInfo']['achievements'] = array_filter($profileInfo['profileInfo']['achievements'], function ($item) {
			return $item['current'] || $item['next']['completion'];
		});
		shuffle($profileInfo['profileInfo']['achievements']);

		// just shuffle properties
		shuffle($profileInfo['profileInfo']['property']);

		// calculate full percentages
		$fullCompletion = 0;
		foreach ($profileInfo['profileInfo']['achievements'] as $item) {
			$fullCompletion += $item['completion'];
		}

		// check privacy settings
//		$privacySettings = null;
//		if ($playerId) {
//			$privacySettings = Player::model()
//							   ->findByPk(Yii::app()->player->profile['playerId'])
//							   ->checkVisibilityFromSettings($playerId, $profileInfo['profileInfo']['buddies']);
//		}

		$this->render('index', [
			'fullCompletion'        => count($profileInfo['profileInfo']['achievements'])? round($fullCompletion / count($profileInfo['profileInfo']['achievements']), 2): 0,
			'profile'               => $profileInfo['profile'],
			'profileInfo'           => $profileInfo['profileInfo'],
			'isOwnProfile'          => self::isOwnProfile($playerId),
			'privacySettings'       => self::getPrivacySettings($playerId, $profileInfo['profileInfo']['buddies']),
			'playerId'              => $playerId,
			'disablePlayWithCarmen' => true,
		]);
	}

	public function actionSetStatus($status) {
		/** @var PokeristApi_Player $api */
		$api = Yii::app()->pokeristApi->getPlayerApi();
		echo (int) $api->doSetTextStatus($status, Yii::app()->user->sessionToken);
	}

	public function actionSetLocation($latitude, $longitude) {
		/** @var PokeristApi_Player $api */
		$api = Yii::app()->pokeristApi->getPlayerApi();
		echo (int) $api->doSetLocation($latitude, $longitude, Yii::app()->user->sessionToken);
	}

	public function actionAchievements($playerId = null) {
		$profileInfo = self::getProfileInfo($playerId);

		$this->breadcrumbs = [
			$profileInfo['profile']['nickname'] => ['profile/index'],
			Yii::t('profile', 'Achievements'),
		];

		$this->generateRightNavigation($playerId);

		$this->render('achievements', [
			'profile'         => $profileInfo['profile'],
			'profileInfo'     => $profileInfo['profileInfo'],
			'privacySettings' => self::getPrivacySettings($playerId, $profileInfo['profileInfo']['buddies']),
			'isOwnProfile'    => self::isOwnProfile($playerId),
		]);
	}

	public function actionProperty($playerId = null) {
		$profileInfo = self::getProfileInfo($playerId);

		$this->breadcrumbs = [
			$profileInfo['profile']['nickname'] => ['profile/index'],
			Yii::t('profile', 'Property'),
		];

		$this->generateRightNavigation($playerId);

		$groupedProps = [];
		foreach ($profileInfo['profileInfo']['property'] as $prop) {
			$groupedProps[$prop['gift']['group']['name']][] = $prop['gift'];
		}

		$this->render('property', [
			'profile'         => $profileInfo['profile'],
			'groupedProps'    => $groupedProps,
			'itemsCount'      => count($profileInfo['profileInfo']['property']),
			'privacySettings' => self::getPrivacySettings($playerId, $profileInfo['profileInfo']['buddies']),
			'isOwnProfile'    => self::isOwnProfile($playerId),
		]);
	}

	public function actionFriend($playerId = null) {
		$profileInfo = self::getProfileInfo($playerId);

		$this->breadcrumbs = [
			Yii::t('profile', 'Back to profile') => $this->createUrl('profile/index'),
			Yii::t('profile', 'Friends'),
		];
		$this->generateRightNavigation($playerId);

		$this->render('friends', [
			'profile'         => $profileInfo['profile'],
			'profileInfo'     => $profileInfo['profileInfo'],
			'privacySettings' => self::getPrivacySettings($playerId, $profileInfo['profileInfo']['buddies']),
			'isOwnProfile'    => self::isOwnProfile($playerId),
		]);
	}

	public function actionPrivacySettings() {
		$profile = Yii::app()->player->profile; //53125378

		/** @var Player $model */
		if (!$model = Player::model()
				 ->findByPk($profile['playerId'])) {
			$model = new Player();
			$model->playerId = $profile['playerId'];
			$model->save();
		};

		$this->breadcrumbs = [
			Yii::t('profile', 'Back to profile') => $this->createUrl('profile/index'),
			Yii::t('profile', 'Privacy settings'),
		];
		$this->generateRightNavigation();

		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];

			Yii::app()->user->setFlash($this->action->id, $model->save()? 'success': 'error');
		}

		$this->render('privacySettings', [
			'profile'      => $profile,
			'settings'     => $model->getPrivacySettings(),
			'model'        => $model,
			'isOwnProfile' => true,
		]);
	}

	public function actionProfileSettings() {
		$user    = Yii::app()->player;
		$profile = $user->profile;
		$model   = new ProfileSettingsForm();

		$this->breadcrumbs = [
			Yii::t('profile', 'Back to profile') => $this->createUrl('profile/index'),
			Yii::t('profile', 'Profile settings'),
		];
		$this->generateRightNavigation();

		$class = get_class($model);
		if (isset($_POST[$class])) {
			$model->attributes = $_POST[$class];
			if (isset($_POST[$class]['currentPassword'], $_POST[$class]['newPassword'])) {
				$model->currentPassword    = $_POST[$class]['currentPassword'];
				$model->newPassword        = $_POST[$class]['newPassword'];
				$model->newPassword_repeat = $_POST[$class]['newPassword_repeat'];
			}

			Yii::app()->user->setFlash($this->action->id, $model->save()? 'success': 'error');
			$this->redirect($this->createUrl(''));
		}

		$this->render('profileSettings', [
			'profile'      => $profile,
			'player'       => Player::model()->findByPk($profile['playerId']),
			'model'        => $model,
			'isOwnProfile' => true,
		]);
	}

	public function actionChangeAvatar() {
		$this->breadcrumbs = [
			Yii::t('profile', 'Back to profile') => $this->createUrl('profile/index'),
			Yii::t('profile', 'Change avatar'),
		];
		$this->generateRightNavigation();

		/** @var Player $model */
		$image = null;

                
                if (isset($_POST['default'])) {
			$avatarFileName = Yii::app()->basePath . $this->createUrl('predefinedAvatarNotStatic', ['id' => $_POST['default']]);
			if (file_exists($avatarFileName)) {
				$image = new FSImage($avatarFileName);
			}
		} elseif (isset($_FILES['avatar']) && $_FILES['avatar']['tmp_name']) {
			$image = new FSImage($_FILES['avatar']['tmp_name']);
			
		}
                

		$model = null;
		if ($image) {
			$setAvatarSuccessful = false;

			try {
				$setAvatarSuccessful = Player::model()
					->setAvatar($image);
			} catch (FSImageException $e) {
				// $model is only for errors
				$model = new CFormModel();
				$model->addError('something', Yii::t('errors', 'Error. Use only image files as your userpic'));
			}

			Yii::app()->user->setFlash($this->action->id, $setAvatarSuccessful? 'success': 'error');
		}

		$this->render('changeAvatar', [
			'profile' => Yii::app()->player->profile,
			'isOwnProfile' => true,
			'model' => $model
		]);
	}

	public function actionBuyChips($type, $position, $mode) {
		/** @var WebUser $user */
//		$user = Yii::app()->user;
//		if ($user->isGuest) {
//			Yii::app()->request->redirect(CHtml::normalizeUrl($user->loginUrl), true);
//		}

		/** @var XsollaComponent $xsolla */
		$xsolla = Yii::app()->xsolla;

		$data = [
			'normal'  => [
				'chips' => $xsolla->getChipsRates(),
				'gold'  => $xsolla->getGoldCoinsRates(),
			],
			'special' => [
				'chips' => $xsolla->getChipsRates(true),
				'gold'  => [],
			],
		];

		if (!isset($data[$mode][$type][$position])) {
			throw new CHttpException(404);
		}

		// get on backend
//		if (Yii::app()->request->isAjaxRequest) {
//			echo $xsolla->getBuyFrameContent($data[$mode][$type][$position]);
//		} else {
//			$this->render('/common/simple-content', [
//				'content'           => $xsolla->getBuyFrameContent($data[$mode][$type][$position]),
//				'containerCssClass' => 'xsolla',
//			]);
//		}

		// get on frontend
		$iframe = '<iframe width="100%" height="98%" style="border: 0;" src="'
			. (Yii::app()->request->isSecureConnection?
			str_replace('http', 'https', $data[$mode][$type][$position]['itemURL']) :$data[$mode][$type][$position]['itemURL'])
			. '"></iframe>';

		if (Yii::app()->request->isAjaxRequest) {
			echo $iframe;
		} else {
			$this->render('/common/simple-content', [
				'content'           => $iframe,
				'containerCssClass' => 'xsolla',
			]);
		}
	}

	public function actionQuestion() {
		$this->breadcrumbs = [
			Yii::t('howto', 'Support') => $this->createUrl('howto/support'),
			Yii::t('support-form', 'Ask Question'),
		];

		$profile = self::getProfileInfo()['profile'];
		// TODO define the last device
//		$device = Yii::app()->pokeristApi->getAdminApi()->getDeviceByPlayerId($profile['playerId'])['deviceType'];

		$model = new AskQuestion();
		$model->name = $profile['nickname'];
		$model->playerId = $profile['playerId'];
		$model->game = 'Pokerist';

		$emailValidator = new CEmailValidator();
		if ($emailValidator->validateValue($profile['email'])) {
			$model->email = $profile['email'];
		}

		// collect user input data
		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];
			$model->platform = $_POST[get_class($model)]['platform']?: null;
			$model->category = $_POST[get_class($model)]['category']?: null;

			if ($model->notHumanity) {
				throw new CHttpException(503);
			}

			$httpClient = new EHttpClient();
			$httpClient->setUri($this->createUrl('links/supportForm'));

			$httpClient->setParameterPost([
				'nickname'     => (string) $model->name,
				'email'        => (string) $model->email,
//				'project'      => (string) $model->game,
				'project'      => 'Pokerist',
				'partner'      => 'Pokerist.com',
				'category'     => (string) $model->category,
				'question'     => (string) $model->question,
				'account_id'   => (string) $model->playerId,
//				'app_version'  => (string) '',
				'os_version'   => (string) $model->os,
				'device_model' => (string) $model->platform,
				'message'      => (string) $model->message,
				'token'        => (string) Yii::app()->user->sessionToken,
			]);

			$fileName = null;
			foreach ($_FILES as $formName => $fileArr) {
				foreach ($fileArr['tmp_name'] as $fileName) {
					if ($fileName) {
						$httpClient->setFileUpload($fileName, $formName);
					}
				}
			}

			if ($model->validate() && $httpClient->request('POST')->getBody() == 'OK') {
				Yii::app()->user->setFlash($this->action->id, 'success');
			} else {
				Yii::app()->user->setFlash($this->action->id, 'error');
			};
		}

		$this->render('ask-question', [
			'model'  => $model,
//			'device' => $device,
		]);
	}

	public function actionScreenshot($playerId, $hash) {
		$playerId = substr(str_replace('-', '', $playerId), 0, -2);

		$profileInfo = self::getProfileInfo($playerId);

		$this->render('screenshot', [
			'profile'     => $profileInfo['profile'],
			'profileInfo' => $profileInfo['profileInfo'],
			'playerId'    => $playerId,
			'imageHash'   => $hash,
		]);
	}

	public function generateRightNavigation($playerId = null) {
		$playerIdParam = $playerId? ['playerId' => $playerId]: [];

		$actionId = $this->action->id;

		Navigation::getMenu('right')
			->addItems([
				[
					'id'     => 'profile',
					'label'  => Yii::t('profile', 'Profile'),
					'url'    => $this->createUrl('profile/index', $playerIdParam),
					'active' => true,
					'items'  => [
						[
							'id'     => 'friends',
							'label'  => Yii::t('profile', 'Friends'),
							'url'    => $this->createUrl('profile/friend', $playerIdParam),
							'active' => $actionId == 'friend',
						],
						[
							'id'     => 'achievements',
							'label'  => Yii::t('profile', 'Achievements'),
							'url'    => $this->createUrl('profile/achievements', $playerIdParam),
							'active' => $actionId == 'achievements',
						],
						[
							'id'     => 'property',
							'label'  => Yii::t('profile', 'Property'),
							'url'    => $this->createUrl('profile/property', $playerIdParam),
							'active' => $actionId == 'property',
						],
						[
							'id'      => 'profile-settings',
							'label'   => Yii::t('profile', 'Profile settings'),
							'url'     => $this->createUrl('profile/profileSettings'),
							'active'  => $actionId == 'profileSettings',
							'visible' => $playerId === null,
						],
						[
							'id'      => 'change-avatar',
							'label'   => Yii::t('profile', 'Change avatar'),
							'url'     => $this->createUrl('profile/changeAvatar'),
							'active'  => $actionId == 'changeAvatar',
							'visible' => $playerId === null,
						],
						[
							'id'      => 'privacy-settings',
							'label'   => Yii::t('profile', 'Privacy settings'),
							'url'     => $this->createUrl('profile/privacySettings'),
							'active'  => $actionId == 'privacySettings',
							'visible' => $playerId === null,
						],
					],
				],
			]);
	}
        
        public function actionfileAvatar() {
            if (isset($_POST['filename'])) {
                $this->render('fileAvatar', [
                            'filename' => $_POST['filename']
                    ]);
            }
            
        }
}
