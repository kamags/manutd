<?php

class AdminController extends BaseController {

	public function filters() {
		return ['accessControl'];
	}

	public function accessRules() {
		return [
			['allow', 'users' => ['@']],
			['deny', 'users' => ['*']],
		];
	}

	public $promotionClass = 'Promotion';

	private function _postList($className = 'Press') {
		/** @var Press[]|Promotion[] $models */
		$models = $className::model()->all()->findAll();

		$this->render("adminPressList", [
			'createRoute' => 'admin/' . lcfirst($className) . 'Create',
			'itemRoute'   => 'admin/' . lcfirst($className) . 'Item',
			'deleteRoute' => 'admin/' . lcfirst($className) . 'Delete',
			'models'      => $models,
		]);
	}

	private function _postCreate($className = 'Press') {
		/** @var Press|Promotion $model */
		$model = new $className();

		if (Yii::app()->request->isPostRequest) {
			if (($images = self::saveImages($className)) !== false) {
				$model->images = serialize($images);
			}

			$post              = Yii::app()->request->getPost($className);
			$model->attributes = $post;

			$model->publicationDate = $post['publicationDate']?: date('Y-m-d H:i:s');

			if ($model->save()) {
				$this->redirect(['admin/' . lcfirst($className) . 'Item', 'id' => $model->id]);
			}
		}

		$this->render("adminPressItem", [
			'model' => $model,
		]);
	}

	private function _postItem($id, $className = 'Press') {
		/** @var Press|Promotion $model */
		$model = $className::model()->all()->findByPk($id);

		if (!$model) {
			throw new CHttpException(404, 'Press item not found');
		}

		if (Yii::app()->request->isPostRequest) {
			if (($images = self::saveImages($className)) !== false) {
				$model->images = serialize($images);
			}

			$post              = Yii::app()->request->getPost($className);
			$model->attributes = $post;
			$model->save();

			if (is_string($model->images)) {
				$model->images = unserialize($model->images);
			}
		}

		$this->render("adminPressItem", [
			'model' => $model,
		]);
	}

	private function _postDelete($id, $className = 'Press') {
		/** @var Press|Promotion $model */
		$model = $className::model()->all()->findByPk($id);

		if (!$model) {
			throw new CHttpException(404, 'Press item not found');
		}

		echo (int) $model->delete();
	}

	/**
	 * Saves images to file system and returns array with web paths to them
	 *
	 * @author Lukin A.
	 *
	 * @param string $className Class name for use in $_FILES array
	 * @param string $fieldName Table column name where kept images serialized info
	 * @return array|bool
	 * @throws CException
	 * @throws FSImageException
	 */
	public static function saveImages($className, $fieldName = 'images') {
		if (!array_key_exists($className, $_FILES) || !current($_FILES[$className]['name'][$fieldName])) {
			return false;
		}

		$files      = $_FILES[$className];
		$imageLinks = [];
		foreach ($files['tmp_name'][$fieldName] as $i => $tmpName) {
			$ext         = pathinfo($files['name'][$fieldName][$i], PATHINFO_EXTENSION);
			$md5FileName = md5_file($tmpName) . '.' . $ext;
			$path = Yii::getPathOfAlias('webroot.img.' . strtolower($className));
			if (!file_exists($path)) {
				@mkdir($path, 0777, true);
			}

			$fileName = implode(DIRECTORY_SEPARATOR, [
				$path,
				$md5FileName
			]);
			$image    = new FSImage($fileName);
			$image->setContent(file_get_contents($tmpName), false);
			$image->resize(327);
			if ($image->save()) {
				$imageLinks[] = implode(DIRECTORY_SEPARATOR, [
					'',
					'img',
					strtolower($className),
					$md5FileName,
				]);
			}
		}

		return count($imageLinks)? $imageLinks: false;
	}

	public function actionPressList() {
		$this->_postList();
	}

	public function actionPressCreate() {
		$this->_postCreate();
	}

	public function actionPressItem($id) {
		$this->_postItem($id);
	}

	public function actionPressDelete($id) {
		$this->_postDelete($id);
	}

	public function actionPromotionList() {
		$this->_postList($this->promotionClass);
	}

	public function actionPromotionCreate() {
		$this->_postCreate($this->promotionClass);
	}

	public function actionPromotionItem($id) {
		$this->_postItem($id, $this->promotionClass);
	}

	public function actionPromotionDelete($id) {
		$this->_postDelete($id, $this->promotionClass);
	}
}
