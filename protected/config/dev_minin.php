<?php

Yii::import('system.collections.CMap');

$config = CMap::mergeArray(require('_main.php'), [
		'components' => [
			'db'  => [
				'connectionString' => 'mysql:host=127.0.0.1;dbname=manutdsocialcasino',
				'emulatePrepare'   => true,
				'username'         => 'minin',
				'password'         => 'd57ghfghfgh9U',
				'charset'          => 'utf8',
			],
			'log' => [
				'class'  => 'CLogRouter',
				'routes' => [
					[
						'class'  => 'CFileLogRoute',
						'levels' => 'error, warning, info, trace',
					],
					[
						'class'  => 'CWebLogRoute',
						'levels' => 'error, warning, info, trace',
					],
				],
			],
		],
		'params'     => [
			'facebookAppId' => '439508346146037', // fav casino qa
			//'supportEmail' => ['sergey@larionov.biz' => 'Sergey Larionov'],
		],
	]
);

return $config;
