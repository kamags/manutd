<?php

return [
	'en'    => ['site' => true, 'title' => 'English', 'originalTitle' => 'English'],
	'ru'    => ['site' => true, 'title' => 'Russian', 'originalTitle' => 'Русский'],
	'fr'    => ['site' => true, 'title' => 'French',  'originalTitle' => 'Français'],
	'it'    => ['site' => true, 'title' => 'Italian',  'originalTitle' => 'Italiano'],
	'de'    => ['site' => true, 'title' => 'German',  'originalTitle' => 'Deutsch'],
	'es'    => ['site' => true, 'title' => 'Spanish',  'originalTitle' => 'Español'],
	'jp'    => ['site' => true, 'title' => 'Japanese', 'originalTitle' => '日本語'],
	'th'    => ['site' => true, 'title' => 'Thailand', 'originalTitle' => 'ภาษาไทย'],
	'zh_cn' => ['site' => true, 'title' => 'Simplified Chinese', 'originalTitle' => '简体中文'],
	'zh' => ['site' => true, 'title' => 'Tradition Chinese', 'originalTitle' => '繁體中文'],
	'ko' => ['site' => true, 'title' => 'Korean', 'originalTitle' => '한국어'],
];
