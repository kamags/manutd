<?php

return [
	'basePath'       => __DIR__ . DIRECTORY_SEPARATOR . '..',
	'name'           => 'Manchester United Poker',
	'language'       => 'en',
	'sourceLanguage' => 'en',

	// init actions
	'preload'        => ['log'],
	//'onBeginRequest' => ['LanguageDetector', 'detect'],

	// autoloading model and component classes
	'import'         => [
		'application.controllers.BaseController',
		'application.models.*',
		'application.components.*',
		'ext.mail.*',
		'ext.httpclient.*',
		'ext.navigation.*',
		'ext.pokerist-auth.*',
		'ext.phpQuery.*',
		'ext.fs-tools.*',
	],

	'language'       => '',
	'sourceLanguage' => '',

	'onBeginRequest' => ['LanguageDetector', 'detect'],

	// application components
	'components'     => [
		'urlManager'   => [
			'class'            => 'UrlManager',
			'urlFormat'        => 'path',
			'caseSensitive'    => true,
			'matchValue'       => true,
			'showScriptName'   => false,
			'urlSuffix'        => '/',
			'useStrictParsing' => true,
			'rules'            => require('_routes.php'),
		],
		'db'           => [
			'connectionString' => 'mysql:host=127.0.0.1;dbname=mu_casino',
			'emulatePrepare'   => true,
			'username'         => 'mu_casino',
			'password'         => 'EQ1aVJTraxXBaElyQVaORYPsn',
			'charset'          => 'utf8',
		],
		'errorHandler' => [
			'errorAction' => 'main/error',
		],
//            'session'=> [
//                'timeout' => 300
//             ],
		'log'          => [
			'class'  => 'CLogRouter',
			'routes' => [
				['class' => 'CFileLogRoute', 'levels' => 'error, warning'],
			],
		],
		'viewRenderer' => [
			'class'         => 'ext.ETwigViewRenderer',
			'twigPathAlias' => 'ext.twig-renderer.lib.Twig',
			'fileExtension' => '.twig',
			'paths'         => ['__main__' => 'application.views'],
			'functions'     => [
				'dump' => 'var_dump',
				'die'  => 'die',
			],
			'globals'       => [
				'Yii'  => 'Yii',
				'Nav'  => 'Navigation',
				'Html' => 'CHtml',
			],
		],
		'mail'         => [
			'class' => 'ext.mail.YiiMail',
		],
		'pokeristApi'  => [
			'class'           => 'ext.pokerist-api-client.PokeristApiComponent',
			'playerApiConfig' => [
				'server'   => 'http://api.pokerist.kama.gs/',
				'username' => 'pclub_api',
				'password' => 'Queur0wo',
			],
			'adminApiConfig'  => [
				'server'   => 'http://api.pokerist.kama.gs/',
				'username' => 'pclub_admin_api',
				'password' => 'beCh7poo',
			],
		],
		'cache'        => array(
			'class' => 'CFileCache',
		),
		'authManager'  => [
			'class' => 'ext.pokerist-auth.AuthManager',
		],
		'user'         => [
			'class'          => 'application.components.WebUser',
			'loginUrl'       => ['main/signIn'],
			'allowAutoLogin' => true,
			'identityCookie' => [
				'path'   => '/',
				'domain' => '.' . (array_key_exists('HTTP_HOST', $_SERVER)? $_SERVER['HTTP_HOST']: null),
			],
			'authTimeout'    => 7776000, // 3 month in seconds
		],
		'player'       => [
			'class' => 'ext.pokerist-auth.PlayerComponent',
		],
		'xsolla' => [
			'class'    => 'application.components.XsollaComponent',
			'hostname' => 'http://base.pokerist.com',
			'actions'  => [
				'chips' => 'http://base.pokerist.com/poker/automation/buy_chips_actions.php?userID=%s&type=json&clientType=site&special_prices=%d',
				'golds' => 'http://base.pokerist.com/poker/automation/buy_gold_actions.php?userID=%s&type=json&clientType=site&special_prices=%d'
			],
		],
	],

	'params'         => [
		'languages'     => require('_languages.php'),
		'facebookAppId' => '1498937533701366',
		'postsPerPage'	=> 10,
		'email'         => [
			'poker'    => [
				'support@manutdpoker.com' => 'ManUtdSocialCasino Support',
			],
			'roulette' => [
				'support@manutdroulette.com' => 'ManUtdSocialCasino Support',
			],
			'slots' => [
				'support@manutdsocialslots.zendesk.com' => 'ManUtdSocialCasino Support',
			]
		],
		'supportEmail'  => ['support@manutdpoker.com' => 'ManUtdSocialCasino Support'],
	],
];
