<?php

return [
	'/'                                                                          => 'main/index',
	//'letter'                                                                     => 'main/letter',
	'/terms-of-service/'                                                         => 'main/tos',
	'/apriltix.html/'                                                            => 'main/apriltix',
	'/account/'                                                                 => 'main/account',
	'/privacy-policy/'                                                           => 'main/policy',
	'/games/<name:[\w\d.]+>'                                                     => 'main/games',
	'/about/'                                                                    => 'main/about',
	'/play-now/'                                                                 => 'main/playNow',
	'/jersey/'                                                                   => 'main/jersey',
	'/experience/'                                                               => 'main/experience',
    
    	# ingame roulette
   	'/ingame/<path:american|european|french|privacy-policy>/'                   => ['ingame/page', 'hasLanguage' => false],
        '/ingame/terms-of-use/'                                                     => ['ingame/unityTos', 'hasLanguage' => false],
	'/ingame/'                                                                  => ['ingame/index', 'hasLanguage' => false],
    
        # unity poker
        '/unity/hands/'                                                             => ['unity/unityHands', 'hasLanguage' => false],
	'/unity/actions/'                                                           => ['unity/unityActions', 'hasLanguage' => false],
	'/unity/rounds/'                                                            => ['unity/unityRounds', 'hasLanguage' => false],
	'/unity/bets/'                                                              => ['unity/unityBets', 'hasLanguage' => false],
	'/unity/support/'                                                           => ['unity/unitySupport', 'hasLanguage' => false],
        '/unity/terms-of-use/'                                                      => ['unity/unityTos', 'hasLanguage' => false],

	//games
	'/games/slots/help'                                                          => 'main/slotsHelp',

	'/contact-us/'                                                               => 'feedback/support',
	'/getPlayersOnline'                                                          => 'main/getPlayersOnline',
	'/subscribe'                                                                 => ['main/subscribe', 'hasLanguage' => false],

	// sign in and sign up actions
	'/sign-in/facebook/'                                                         => 'main/signInWithFacebook',
	'/sign-in/'                                                                  => 'main/signIn',
	'/sign-out/'                                                                 => 'main/signOut',
	'/sign-up/'                                                                  => 'main/signUp',
	'/sign-up/<hash:[\w\d.]+>/confirm/'                                          => 'main/signUpConfirm',
	'/restore-password/'                                                         => 'main/restorePassword',
	'/restore-password/<hash:[\w\d.]+>/confirm/'                                 => 'main/restorePasswordConfirm',

	# Press
	'/press/'                                                                    => 'press/index',
	'/press/<alias:[\w-_]+>'                                                     => 'press/item',

	# Promotions
	'/promotions/'                                                               => 'promotions/index',
	'/promotions/<alias:[\w-_]+>'                                                => 'promotions/item',

	// admin actions
	'/admin/'                                                                    => 'admin/pressList',
	'/admin/press/new'                                                           => 'admin/pressCreate',
	'/admin/press/<id:\d+>'                                                      => 'admin/pressItem',
	'/admin/press/delete/<id:\d+>'                                               => 'admin/pressDelete',
	'/admin/promotion-list'                                                      => 'admin/promotionList',
	'/admin/promotion/new'                                                       => 'admin/promotionCreate',
	'/admin/promotion/<id:\d+>'                                                  => 'admin/promotionItem',
	'/admin/promotion/delete/<id:\d+>'                                           => 'admin/promotionDelete',

	'/buy-chips/'                                                                => 'main/buyChips',
//	'/buy-chips/<mode:normal|special>/<type:chips|gold>_<position:\d+>/' => 'main/buyChips',

	# profile
	'/profile/<playerId:\d+>/'                                                   => 'profile/index',
	'/profile/'                                                                  => 'profile/index',
	'/profile/buy-chips/<mode:normal|special>/<type:chips|gold>_<position:\d+>/' => 'profile/buyChips',

	'/<path:[a-zA-Z0-9_-]+(?:\/[a-zA-Z0-9_-]+)*>'                                => 'main/page',
    


];
