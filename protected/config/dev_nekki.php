<?php

Yii::import('system.collections.CMap');

$config = CMap::mergeArray(require('_main.php'), [
	'components' => [
                'db'        => [
				'connectionString' => 'mysql:host=127.0.0.1;dbname=mu_casino',
				'emulatePrepare'   => true,
				'username'         => 'nekki',
				'password'         => '123kama',
				'charset'          => 'utf8',
			],
		'log' => [
			'class' => 'CLogRouter',
			'routes' => [
				[
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning, info, trace',
				],
				[
					'class' => 'CWebLogRoute',
					'levels' => 'error, warning, info, trace',
				],
			],
		],
	],
	'params' => [
		//'supportEmail' => ['sergey@larionov.biz' => 'Sergey Larionov'],
	],
]
);

return $config;
