<?php

return [
    
    # Common questions 
    'cq_text' => 'Preguntas comunes',
            
    'cq_question1' => '¿Qué variedad de póker puedo jugar en esta aplicación?',
    'cq_answer1' => 'En Manchester United Poker puedes jugar a los tipos más populares de póker Texas Hold’em. Puedes jugar en mesas con fichas o unirte a torneos.',
    
    'cq_question2' => '¿En qué plataformas se encuentra disponible Manchester United Poker?',
    'cq_answer2' => 'La aplicación Manchester United Poker está disponible para iOS, Android, Windows Phone y Facebook.',
    
    'cq_question3' => '¿Cómo se juega a Texas Hold’em?',
    'cq_answer3' => 'Para saber más acerca de las reglas y la interfaz del juego, por favor dirígete a “Ayuda” en el menú principal. Te contaremos todo lo que necesites acerca de manos del póker, apuestas y acciones posibles. Nota: esto solo te proporcionará conocimientos básicos del póker, pero no bastará para ganar. Para tener posibilidades de éxito tendrás que conocer más acerca de las estrategias del juego y el comportamiento de los jugadores, así como trucos para mejorar tus habilidades. ¡No puedes ni figurarte lo apasionante que puede ser una partida contra un adversario experimentado!',
    
    'cq_question4' => '¿Qué es el modo multipantalla?',
    'cq_answer4' => 'Es un modo que muestra cuatro pantallas de juego simultáneamente. Con el modo multipantalla puedes jugar en el modo normal, tomar parte en un torneo y conversar en el menú de mensajes personales al mismo tiempo. Este modo está disponible para todos los jugadores de nivel 15+. El menú principal, el menú de chat, el vestíbulo y los torneos están disponibles automáticamente en el modo multipantalla. Puedes desactivarlo en el menú de parámetros.',
    
    'cq_question5' => '¿Cómo se usa el modo multipantalla?',
    'cq_answer5' => 'Puedes cambiar al modo multipantalla presionando sobre el icono situado en la esquina superior derecha de la pantalla. Este icono muestra tu zona de juego actual. Para ir a un menú en concreto, solo hace falta presionar el sector correspondiente. En los dispositivos móviles es posible cambiar la pantalla simplemente presionando con el dedo.',
    
    'cq_question6' => '¿Dónde se encuentra el historial del juego?',
    'cq_answer6' => 'Puedes encontrar un historial del juego en la pestaña “Registro de eventos” en el menú de mensajes personales. Todos tus logros y acciones se muestran en orden cronológico, empezando por la más reciente.',
    
    'cq_question7' => '¿Qué es un “Vestíbulo”?',
    'cq_answer7' => 'El vestíbulo es un modo de juego en el que puedes ver las partidas activas e información acerca de éstas (velocidad del juego, tamaño de las ciegas, cantidad de asientos, etc.) y elegir en que mesa deseas unirte. Los jugadores más avanzados estudian con cuidado la información que ofrece el vestíbulo, eligen el banco medio y observan el comportamiento de los jugadores, y se benefician de ello en cada partida. La elección de una mesa en el vestíbulo es una de las más importantes a fin de aumentar tus beneficios.',
    
    'cq_question8' => '¿Cómo puedo elegir una mesa desde el vestíbulo?',
    'cq_answer8' => 'Presiona sobre “Vestíbulo” en el menú principal. En este modo puedes elegir la mesa que quieres observar (imagina que estás en un casino, y miras la partida como un espectador más). Elige la cantidad de asientos en la mesa, la velocidad del juego y configura la vista de las mesas llenas. La cantidad de las ciegas en este caso, es determinada por la cantidad de fichas en tu cuenta (Fondo). Algunos jugadores pueden elegir qué mesa les conviene según sus ciegas. Muévete por las mesas, observa las partidas en curso y el comportamiento de los jugadores. Cuando hayas encontrado la mesa que más te convenza, toma asiento en uno de los huecos disponibles y haz tus apuestas.',  
    
    'cq_question9' => '¿Es posible jugar en varias mesas a la vez?',
    'cq_answer9' => 'Solo puedes jugar en una mesa.',
    
    'cq_question10' => '¿Cómo puedo subir la apuesta?',
    'cq_answer10' => 'Para subir la apuesta con una cantidad arbitraria, presiona en el botón “Subir” y MANTÉNLO. Mueve el indicador de la barra que aparecerá hasta la cantidad que te interese y suéltalo. En ocasiones se te pedirá que confirmes tu selección con un toque adicional.',
    
    'cq_question11' => '¿Cómo puedo participar en los torneos?',
    'cq_answer11' => 'Elige “Torneos” en el menú principal. Tendrás que aceptar los términos del juego antes de empezar. Solo puedes participar en un torneo cada vez.',
    
    'cq_question12' => '¿Por qué me “salgo” de la mesa y no puedo unirme a ella?',
    'cq_answer12' => 'La causa más común puede ser una mala conexión a Internet. Asegúrate de tener una conexión estable cuando juegues.',
    
    'cq_question13' => '¿Qué puedo hacer si creo que otro jugador ha ganado injustamente? ',
    'cq_answer13' => 'Una buena mano no siempre significa una mano ganadora. Lo más probable es que el jugador con peores cartas que las tuyas no ganase, aunque sí que hizo una apuesta mayor a la tuya, mientras que tú fuiste con el resto, todo el dinero que te quedaba. También puede ser que vuestras combinaciones fuesen iguales, pero la otra carta de su mano (kicker) fuese más alta. Aprende más acerca de las reglas del póker y lo verás todo mucho más claro.',
    
    'cq_question14' => '¿Qué significa esa estrella dorada al lado del nombre de un jugador?',
    'cq_answer14' => 'La estrella indica que ese jugador está en una lista de amigos.',
    
    'cq_question15' => '¿Cómo puedo capturar una pantalla y compartirla con mis amigos?',
    'cq_answer15' => 'Para guardar y compartir un momento único del juego, presiona sobre el icono con forma de cámara y elige una de las siguientes opciones: guardar en tu dispositivo, enviar por correo electrónico o publicarla en tu muro de Facebook. Si no ves la opción del correo electrónico, comprueba que tengas una dirección configurada en tu dispositivo.',
    
    
    # Creating a game profile
    'cgp_text' => 'Creación de un perfil',
            
    'cgp_question1' => '¿Qué es una cuenta de invitado?',
    'cgp_answer1' => 'No necesitas registrarte para jugar. Puedes hacerlo como un invitado, pero en ese caso tu cuenta quedará vinculada al dispositivo. Si quieres seguir jugando y que tu logros y puntuaciones se guarden, solo podrás seguir usando ese dispositivo. No podrás recuperar tu cuenta de invitado si pierdes o se te rompe tu dispositivo.',
    
    'cgp_question2' => '¿Por qué tengo que registrarme?',
    'cgp_answer2' => 'Si te registras, puedes crear un perfil de juego. Todos tus logros, fichas, monedas y propiedades se guardarán en este perfil. También podrás añadir amigos y usar este perfil desde cualquier dispositivo iOS o Android, así como en las redes sociales.',
    
    'cgp_question3' => '¿Cómo puedo registrarme en el juego?',
    'cgp_answer3' => 'Usa el formulario de registro que hay en el menú del juego. Tendrás que proporcionar tu cuenta de correo electrónico, una contraseña y un nombre de usuario y si lo deseas, podrás subir una imagen para tu usuario. Te recomendamos que uses una dirección de correo electrónico real pues así podrás recuperar tu contraseña. Si dispones de un perfil en una red social puedes usarlo como perfil del juego.',
    
    'cgp_question4' => 'Tengo un perfil en Manchester United Roulette, ¿puedo usarlo para jugar en Manchester United Poker?',
    'cgp_answer4' => 'Si puedes. Manchester United Poker y Manchester United Roulette tienen un sistema integrado de cuentas. Si ya dispones de una cuenta en Manchester United Roulette con un correo electrónico, no podrás usar esa dirección para crear una nueva cuenta. Lo que debes hacer es iniciar sesión en Manchester United Poker con el mismo usuario y contraseña.',
    
    'cgp_question5' => '¿Cómo se cual es mi ID?',
    'cgp_answer5' => 'Tu ID está formada por 9 o 10 números y puedes encontrarla en tu perfil dentro de la aplicación o en la página web. Recuerda indicar tu ID siempre que contactes al soporte técnico.',
    
    'cgp_question6' => '¿Puedo cambiar mi usuario?',
    'cgp_answer6' => 'No es posible cambiar tu usuario pero puedes registrar una cuenta nueva y elegir así otro nombre de usuario.',
    
    'cgp_question7' => '¿Cómo puedo recuperar mi contraseña?',
    'cgp_answer7' => 'Para recuperar tu contraseña presiona en el enlace “¿Has olvidado tu contraseña?” y proporciona los datos solicitados en el formulario. Te recomendamos que uses una dirección de correo electrónico real. Si proporcionaste una cuenta falsa o no operativa al crear el perfil, usa nuestro formulario de contacto para recibir instrucciones de nuestro equipo de soporte técnico.',
    
    'cgp_question8' => '¿Puedo borrar mi cuenta?',
    'cgp_answer8' => 'No se pueden borrar las cuentas, pero si lo deseas puedes crear una nueva. Puedes usar hasta 20 cuentas diferentes en un mismo dispositivo (incluyendo la cuenta de invitado).',
    
    'cgp_question9' => '¿Cómo puedo definir los parámetros de mi perfil de juego?',
    'cgp_answer9' => 'Para acceder al perfil del juego, presiona la caja con tu imagen de usuario, el nombre o el balance en el menú principal.',
    
    'cgp_question10' => '¿Cómo puedo cambiar mi nombre de usuario?',
    'cgp_answer10' => 'Para cambiar tu nombre de usuario presiona sobre tu nombre actual en los parámetros de tu perfil (o la imagen de un lápiz a su lado) e introduce el nuevo nombre.',
    
    'cgp_question11' => '¿Cómo puedo subir una foto o cambiar la que tengo en mi usuario?',
    'cgp_answer11' => 'Presiona sobre tu imagen actual en los parámetros de tu perfil. Elige “Nuevo” en el menú emergente y usa una de las imágenes disponibles o elige una foto de la galería de tu dispositivo. Si te registraste usando tu cuenta de Facebook, el juego usará tu avatar de Facebook.',
    
    'cgp_question12' => '¿Cómo puedo cambiar mi estado?',
    'cgp_answer12' => 'Entra en “Perfil” en el menú principal y presiona en la línea que hay al lado de tu avatar e introduce un nuevo estado. Deja este campo vacío si no quieres que aparezca nada.',
    
    'cgp_question13' => '¿Cómo puedo indicar mi ubicación?',
    'cgp_answer13' => 'La "Ubicación" indica la región en la que te encuentras. Para actualizar tu ubicación, simplemente presiona en el icono correspondiente del perfil del juego. Para determinar tu ubicación en el perfil de la página web, presiona en “Determinar” situado al lado de “Ubicación” en tu línea de ID, a la derecha de tu imagen de perfil.',
    
    'cgp_question14' => '¿Por qué mi ubicación no está determinada?',
    'cgp_answer14' => 'Asegúrate de que los parámetros de la ubicación están activados en tu dispositivo, y que nuestra aplicación tiene acceso a ellos. Si no hay una ubicación determinada, prueba a reinstalar la aplicación y dale acceso a los detalles de ubicación.',
    
    'cgp_question15' => '¿Cómo puedo comprar una propiedad?', 
    'cgp_answer15' => 'Puedes comprar una propiedad con fichas o monedas. La propiedad no cambia nada en el avance general del juego, pero indica tu estado. Para comprar una propiedad, activa tu cuenta, presiona sobre “Propiedad” y usa el menú de compra. Puedes regalarles una propiedad a tus amigos o a cualquier otro usuario. Para vender una propiedad usa la opción correspondiente en el menú de tu perfil de juego. Ten en cuenta de que solo recuperarás un 10% de su precio original. No recibirás nada por una propiedad que te haya regalado otro jugador. Los objetos simplemente desaparecerán de tu perfil.',  
    
    'cgp_question16' => '¿Cómo puedo comprar regalos?',
    'cgp_answer16' => 'Cuando estés sentado en una mesa, presiona sobre el icono con forma de copa, al lado de tu imagen de usuario, y elige un regalo. Este se mostrará al lado del avatar del jugador. Una propiedad se mostrará en el lugar designado en el perfil del usuario. Los regalos son temporales, a diferencia de una propiedad que es permanente. Los regalos seguirán activos mientras permanezcas en la mesa, o varios días, si lo has comprado con oro.',
    
    'cgp_question17' => '¿Cómo puedo hacer regalos a otros jugadores?',
    'cgp_answer17' => 'Presiona sobre el icono con forma de copa o de casa, situados al lado de la imagen del usuario. Elige el regalo y confirma la compra. Para hacer un regalo a todos los jugadores de la mesa elige “comprar para todos” en el menú de compra.',
    
    'cgp_question18' => '¿Qué puedo hacer si no me gusta el regalo que me han hecho?',
    'cgp_answer18' => 'Si no te gusta el regalo que has comprado o que te han hecho, puedes cambiarlo por uno nuevo. Presiona sobre el regalo actual (que ha ocupado el lugar del icono con forma de copa), y elige uno nuevo que se mostrará inmediatamente junto a tu avatar. Puedes sustituir de la misma forma un regalo de otro jugador. Además, los regalos desaparecen cuando cierras la aplicación.',
    
    'cgp_question19' => '¿Por qué no puedo comprar alcohol o tabaco?',
    'cgp_answer19' => 'De acuerdo con las reglas de Facebook, los usuarios no pueden recibir alcohol ni tabaco cuando juegan en esta red social, o han sido autorizados con una cuenta de Facebook.',
    
    'cgp_question20' => '¿Para qué necesito los logros?',
    'cgp_answer20' => 'Los logros son un indicador de tu avance en el juego. Para ver la lista de logros dirígete al perfil y presiona sobre “Logros”. Presiona sobre el que quieras ver en detalle. También puedes verlos en el perfil de nuestra página web.',
    
    'cgp_question21' => '¿Cómo puedo salir de la sesión?',
    'cgp_answer21' => 'En las aplicaciones móviles, algunos iconos están ocultos en un panel de opciones que se despliega presionando sobre los indicadores situados en la esquina inferior derecha de la pantalla del menú principal. Con un movimiento de dedo es posible moverse entre las opciones de Torneos, Invitaciones, Tienda, Tutorial y Salir de la sesión.',
      
    
    #Game settings
    'gs_text' => 'Parámetros del juego',
            
    'gs_question1' => '¿Cómo puedo acceder a los parámetros del juego?',
    'gs_answer1' => 'Para acceder a los parámetros del juego hay que presionar sobre el icono con forma de engranaje, situado en la esquina superior derecha de la pantalla del menú principal.',
    
    'gs_question2' => '¿Cómo puedo cambiar el idioma del juego?',
    'gs_answer2' => 'El juego elige el idioma a partir del que está configurado en el dispositivo. Sin embargo, es posible cambiarlo a inglés, que se encuentra disponible en todas las versiones del juego. Para hacer esto, hay que activar la opción “Localización”, situada en los parámetros del juego.',
    
    'gs_question3' => '¿Cómo puedo desactivar el sonido?',
    'gs_answer3' => 'Para activar/desactivar o regular el volumen del sonido del juego, puedes usar la barra desplazadora que hay en el menú de parámetros.',
    
    'gs_question4' => '¿Cómo puedo desactivar las vibraciones?',
    'gs_answer4' => 'Las vibraciones te recuerdan que te ha llegado el turno de hacer una apuesta. Es posible desactivar esta opción en el menú de parámetros (esta función solo está disponible en smartphones).',
    
    'gs_question5' => '¿Cómo puedo bloquear el chat?',
    'gs_answer5' => 'En la mesa no solo puedes jugar, sino también conversar con otros jugadores. Los mensajes de los participantes aparecen en forma de burbujas de texto, sobre sus imágenes de usuario. En el caso de que te molesten, es posible desactivarlas en un menú especial que aparece al presionar el icono que hay al lado del botón de “chat”. Elige aquellos jugadores cuyos mensajes no deseas ver. Asimismo puedes bloquear los mensajes de todos los jugadores. Para ello, activa el bloqueo de chat en el menú de parámetros.',
    
    'gs_question6' => '¿Cómo puedo desactivar los estados?',
    'gs_answer6' => 'Es posible desactivar los estados de los jugadores desde el menú de parámetros.',
    
    'gs_question7' => '¿Cómo puedo activar la iluminación de cartas?',
    'gs_answer7' => 'Los parámetros del juego te permiten iluminar las cartas para mostrar una combinación ganadora entre las cartas de tu mano y de la mesa. Esta opción simplifica mucho el juego, pero hace que decrezca el interés y el suspense.',
    
    'gs_question8' => '¿Cómo puedo desactivar las notificaciones push?',
    'gs_answer8' => 'Puedes desactivar las notificaciones push en los parámetros del dispositivo. Para esto ve a Parámetros/Notificaciones push/Manchester United Poker.',
    
    'gs_question9' => '¿Cómo puedo desactivar los regalos?',
    'gs_answer9' => 'Es posible desactivar los regalos para que no aparezcan en la pantalla. Para esto, haz uso de la barra desplazadora situada en el menú de parámetros. Si desactivas esta opción no podrás dar ni recibir regalos.',
    
    'gs_question10' => '¿Cómo puedo desactivar el reconocimiento de voz?',
    'gs_answer10' => 'ntra en los parámetros del juego y presiona sobre la pestaña “Voz”. Puedes activar/desactivar el reconocimiento de voz, así como elegir el idioma del reconocimiento de voz de los mensajes.',
    
     # Financial issues   
    'fi_text' => 'Asuntos económicos',
            
    'fi_question1' => '¿Juego con dinero real o fichas virtuales?',
    'fi_answer1' => 'Usamos una moneda virtual., es decir, tu cuenta permanece inalterada Sin embargo es posible usar dinero real en nuestra Tienda para comprar fichas y monedas.',
        
    'fi_question2' => '¿Cómo puedo conseguir fichas gratis?',
    'fi_answer2' => 'Puedes conseguir fichas gracias si:<br> - participas en la lotería diaria;<br> - invitas a jugar a tus amigos;<br> - síguenos en Facebook.<br>Además, tus amigos pueden regalarte hasta 100 000 fichas cada día.',
        
    'fi_question3' => '¿Qué sucede si he perdido todas las fichas y mi balance está a cero?',
    'fi_answer3' => 'Las cinco primeras veces, recibirás en tu cuenta 500 fichas gratis para que puedas continuar jugando.',
        
    'fi_question4' => '¿Cómo puedo comprar fichas?',
    'fi_answer4' => '- Presiona sobre “Tienda” en el menú principal y compra la cantidad de fichas que desees a través de iTunes o Google Play Store. El pago se realizará con la tarjeta de crédito vinculada a tu cuenta. Igualmente, puedes usar tarjetas de regalo de iTunes.<br>- Si estás usando la cuenta de una red social para jugar, puedes comprar fichas con la moneda virtual de esa red social en particular (por ejemplo, los créditos de Facebook).',
        
    'fi_question5' => '¿Cómo puedo comprar monedas? ¿Para qué me hacen falta?',
    'fi_answer5' => 'Necesitas monedas para comprar regalos y propiedades. Puedes comprar monedas y fichas en la Tienda. A veces puedes incluso recibirlas como regalo de parte de los desarrolladores del juego.',
        
    'fi_question6' => '¿Puedo cancelar una compra?',
    'fi_answer6' => 'Sí, pero para ello debes solicitarlo al sistema de pago que has usado para realizar dicha compra. No trabajamos directamente con esos sistemas por lo que no podemos cancelar las compras.',
        
    'fi_question7' => '¿De qué forma cambia la cantidad de fichas y monedas adquiridas el nivel de bonificación?',
    'fi_answer7' => 'El nivel de bonificación te permite recibir un aumento bastante significativo cuando compras fichas o monedas las tres primeras veces. Por defecto recibes el primer nivel y consigues un 100% de fichas gratis. Tras la primera compra, subes al segundo nivel y recibes un 200% de fichas gratis en la siguiente compra que hagas. Finalmente, al alcanzar el tercer nivel, te llevas un 300% gratis de monedas o fichas con cualquier compra que realices.',
        
    'fi_question8' => '¿Cómo puedo comprar fichas adicionales para seguir jugando?',
    'fi_answer8' => '¿Estás en una mesa y te has quedado sin fichas? Presiona sobre tu avatar y después sobre el icono con forma de ficha en la ventana del perfil. Introduce la cantidad de fichas que necesites y vuelve al juego. También puedes “Levantarte” y volver a “Sentarte en la mesa, y se abrirá una ventana donde puedes elegir cuantas fichas deseas.',
        
    'fi_question9' => '¿Puedo intercambiar fichas por monedas y viceversa?',
    'fi_answer9' => 'No es posible intercambiar monedas por fichas. Tampoco fichas por moneas. Solo puedes conseguir monedas comprándolas en la Tienda. Para ello, presiona sobre “Tienda” en el menú principal.',
        
    'fi_question10' => '¿Qué puedo hacer para recuperar fichas “desaparecidas”?',
    'fi_answer10' => 'Si entras en el juego y notas que te faltan fichas y que has perdido nivel, primero comprueba que has entrado con la cuenta correcta, pues puede ser que estés usando la cuenta de invitado por error, u otra cuenta distinta. Si ya estás seguro de que no es eso, por favor ponte en contacto con nuestro soporte técnico, estaremos encantados de ayudarte.',
        
     'fi_question11' => '¿Cómo puedo transferir fichas de una cuenta a otra?', 
     'fi_answer11' => 'Está prohibido transferir fichas de una cuenta a otra.',

    
    # Friends 
    'f_text' => 'Amigos',
            
    'f_question1' => '¿Cómo uso la pestaña de Amigos en el menú principal?',
    'f_answer1' => 'Puedes ver tu lista de amigos en el menú principal, debajo de tu perfil. El avatar superior es el perfil de un amigo que has seleccionado (en el centro de la pantalla puedes ver la información de ese amigo y unos botones de acción). Más abajo puedes ver una lista de dos filas con el resto de tus amigos. Para ver la lista completa, haz scroll en dicha lista. En la parte inferior de la pantalla puedes ver los avatares de otros jugadores, elegidos aleatoriamente. ',
        
    'f_question2' => '¿Cómo puedo convertir en “Amigos” a otros Jugadores?',
    'f_answer2' => ' - Entra en la sección "Amigos" en el menú principal y ve a la pestaña “Buscar”. Ahí podrás buscar usuarios por su nombre, su dirección de correo electrónico, o su ID, y después presionar en “Añadir como amigo”.<br> - Puedes enviar una solicitud de amistad a alguien que esté jugando en tu misma mesa. Para esto, abre su perfil y presiona sobre “Añadir como amigo”.',
        
    'f_question3' => '¿Cómo puedo regalarle fichas a un Amigo?',
    'f_answer3' => 'Entra en la sección “Amigos” en el menú principal, elige a un amigo y presiona sobre “Enviar fichas”. Puedes regalar hasta 100 000 fichas cada día.',
        
    'f_question4' => '¿Cómo puedo comprar fichas para un Amigo?',
    'f_answer4' => 'Entra en el perfil de un amigo, presiona sobre “Enviar fichas” y arrastra el cursor de la barra hasta su valor máximo. Aparecerá un botón “Comprar y enviar fichas”. Tras presionarlo, serás redirigido automáticamente a la sección “Tienda”. Las fichas que compres se enviarán automáticamente a tu amigo.',
        
    'f_question5' => '¿Cuántas fichas puedo comprar para mis amigos?',
    'f_answer5' => 'No hay límite en la cantidad de fichas que compras para tus amigos, a diferencia de las fichas de regalo.',
        
    'f_question6' => '¿Cómo puedo hacerle un regalo a un Amigo o enviarle mi propiedad?',
    'f_answer6' => 'Puedes comprar propiedades y regalos para otros jugadores. Presiona sobre el icono con forma de copa o de casa junto al avatar de tu amigo. También puedes elegir “Comprar para todos” y regalarles algo a todos los jugadores de la mesa. Recuerda que los regalos son temporales y las propiedades son permanentes. Algunos regalos pueden durar varios días, si han sido comprados con monedas.',
        
    'f_question7' => '¿Cómo puedo unirme a una mesa en la que está jugando un Amigo?',
    'f_answer7' => 'Entra en la sección “Amigos” en el menú principal y elige el perfil de un amigo. Si el usuario está jugando, presiona sobre “Unirse a esta mesa”. Si quieres invitar a un amigo a la mesa en la que estás jugando, presiona sobre sitio vacío y elige al amigo de la lista. Tu amigo recibirá una invitación para jugar.',
        
    'f_question8' => '¿Cómo puedo borrar usuarios de mi lista de “Amigos”?',
    'f_answer8' => 'Para borrar un usuario de tu lista de amigos elige “Amigos” en el menú principal del juego. Elige al amigo que deseas borrar y presiona sobre “Borrar de la lista de amigos”.',
        
    'f_question9' => '¿Cómo puedo conseguir fichas y otras bonificaciones por invitar a mis amigos?',
    'f_answer9' => 'Presiona sobre “Amigos” en el menú principal y elige “Invitar”. Después envía a tus amigos una solicitud para unirse a Manchester United Poker por SMS, correo electrónico o Facebook. Si tus amigos siguen el vínculo adjunto en el mensaje conseguirás la bonificación. Si juegas en Facebook, presiona sobre “Fichas gratis” y conseguirás fichas por cada jugador al que invites.',
    
    
    # Communication with other players
    'cwop_text' => 'Comunicación con otros jugadores',
            
    'cwop_question1' => '¿Qué son los mensajes personales?',
    'cwop_answer1' => 'Los mensajes personales son mensajes que envías a un jugador concreto. De ahí que solo ella o él pueda leerlo. En el historial de mensajes personales encontrarás no solo los mensajes que has enviado recientemente, sino también aquellos archivados, con su fecha y hora.',
            
    'cwop_question2' => '¿Cómo entro al menú de mensajes personales?',
    'cwop_answer2' => 'Para entrar al menú de mensajes personales presiona sobre el icono de “Mensajes” con forma de sobre en el menú principal. También puedes elegir a un amigo con el que quieras comenzar una conversación y presiona sobre el icono con forma de sobre en su perfil.',
            
    'cwop_question3' => '¿Cómo funciona el menú de mensajes personales?',
    'cwop_answer3' => 'A la izquierda verás la lista de los amigos con los que ya has mantenido una conversación. Para añadir a un nuevo usuario a esta lista presiona sobre “Añadir” (el icono con forma de lápiz con un símbolo de “+”, situado en la esquina inferior izquierda de la pantalla). También puedes entrar en el perfil de tu amigo y realizar algunas acciones, como enviarle fichas o unirte a un juego.',
            
    'cwop_question4' => '¿Cómo puedo enviar un mensaje?',
    'cwop_answer4' => 'Elige al amigo al que quieres mandarle el mensaje y presiona sobre el campo especial designado para escribir el contenido del mensaje. Introduce el texto y presiona sobre “Enviar”.',
            
    'cwop_question5' => '¿Puedo enviarle un mensaje a un amigo que está desconectado?',
    'cwop_answer5' => 'Puedes enviarles mensajes a los amigos que están desconectados. Los mensajes que tú hayas recibido mientras estabas desconectado, aparecerán resaltados como números sobre el icono de “mensajes”.',
            
    'cwop_question6' => '¿Cómo borro los mensajes personales?',
    'cwop_answer6' => 'Presiona y mantén sobre el mensaje que deseas borrar y después elige “Borrar” en el menú que aparecerá.',
            
    'cwop_question7' => '¿Cómo puedo escribir algo en el chat?',
    'cwop_answer7' => 'Presiona sobre el icono de “Mensajes” situado en la esquina inferior derecha de la pantalla, para abrir la ventana de chat mientras estás jugando en una mesa. Introduce el mensaje presiona sobre “Enter” y después sobre “Hecho”.',
            
    'cwop_question8' => '¿Cómo puedo usar los mensajes de voz?',
    'cwop_answer8' => 'Puedes comunicarte con otros jugadores con mensajes de voz, además de con mensajes de texto. Presiona sobre el icono con forma de micrófono situado en la esquina inferior derecha de la pantalla y pronuncia el mensaje que quieres compartir con tus adversarios. El sistema reconocerá el contenido y lo mostrará como texto en las pantallas de los otros jugadores. Esta función tiene un periodo de uso limitado, pero puede alargarse con monedas en nuestra Tienda.',
  
];
