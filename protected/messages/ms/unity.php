<?php

return [
    'actions' => 'Apabila tiba giliran pemain untuk bertindak, mereka boleh melakukan satu daripada tindakan berikut:',
    'actions_call' => 'IKUT',
    'actions_call_content' => "Jika sudah ada taruhan, ikut bermakna pemain bersetuju untuk taruhan yang sepadan. Dalam erti kata lain, jika pemain A bertaruh 10 dan pemain B ikut, pemain B perlu memadankan taruhan 10 dan meletakkan 10 ke dalam pot.",
    'actions_check' => 'CEK',
    'actions_check_content' => 'Jika tiada taruhan dalam pusingan semasa, pemain boleh cek. Cek juga boleh dinyatakan sebagai tidak berbuat apa-apa. Anda kekal dalam permainan, tetapi memilih untuk tidak menaikkan taruhan.',
    'actions_raise' => 'NAIKKAN',
    'actions_raise_content' => 'Kenaikan adalah taruhan yang dibuat atas taruhan yang lain. Jika taruhan dibuat pada pusingan semasa, pemain boleh menaikkannya. Untuk melakukannya, pemain perlu membuat taruhan sekurang-kurangnya dua kali ganda saiz taruhan terdahulu pemain. Untuk terus bersaing untuk pot, semua pemain seterusnya perlu mengikut atau menaikkan semula; jika tidak mereka akan tutup.',
    'actions_fold' => 'TUTUP',
    'actions_fold_content'=> 'Tutup bermakna tidak mahu bersaing untuk pot semasa. Jika pemain tutup, kad pemain tidak menyertai permainan dan tidak boleh memenangi pusingan semasa.',
    
    'bets_blinds' => 'TIRAI',
    'bets_blinds_content' => 'Sebelum kad dibahagikan, pemain yang berada di kiri Butang perlu membuat taruhan dipanggil tirai kecil. Pemain seterusnya yang duduk di kiri tirai kecil juga perlu membuat taruhan dipanggil tirai besar. Jumlah tirai besar adalah dua kali ganda jumlah tirai kecil.',
    'bets_ante' => 'TARUHAN',
    'bets_ante_content' => 'Jika pemain menyertai meja pada mana-mana kedudukan selain daripada tirai besar, pemain perlu meletakkan tirai besar bagi daun pertama mereka (taruhan). Pemain hanya perlu meletakkan taruhan sekali sahaja. Selepas itu, mereka hanya perlu meletakkan tirai besar dan kecil apabila Butang Pembahagi bergerak ke posisi.',
    'bets_minimum_raise' => 'KENAIKKAN MINIMUM',
    'bets_minimum_raise_content' => 'Pemain tidak boleh menaikkan amaun yang kurang daripada amaun tirai besar.',
    'bets_all_in' => 'SEMUA',
    'bets_all_in_content' => "Jika anda tidak cukup cip untuk taruhan, anda boleh lakukan 'semua sekali'. Ini bermakna anda membuat taruhan semua yang ada pada anda. Jika anda meletakkan semua sekali, anda tidak dibenarkan memenangi wang lebih daripada apa yang anda dapat bertaruh. Jika pot meningkat selepas anda bertaruh semua sekali dan menang, pot akan dibahagikan antara anda dengan pemain dengan daun yang kedua terbaik yang dapat meliputi kenaikkan.",
    'bets_split_pot' => 'POT TERBAHAGI',
    'bets_split_pot_content' => 'Jika dua atau lebih pemain seri, pot dibahagikan secara sama rata antara mereka.',
    
    'hands_01' => 'Poker adalah permainan kad paling popular di dunia. Matlamat permainan adalah untuk mengalahkan bak, yang dibentuk daripada kadar semasa empat (atau kurang) pusingan. Dua cara utama untuk memenangi pot ialah mengambil daun poker yang lebih kuat daripada pihak lawan, atau memaksa lawan meletakkan daun dan tidak mahu meneruskan permainan.',
    'hands_02' => 'Daun poker sentiasa terdiri daripada lima keping kad. Sebarang daun dalam satu kategori dalam senarai mengalahkan sebarang daun dalam sebarang kategori di bawahnya. Sebagai contoh, sebarang straight flush mengalahkan sebarang empat sejenis; sebarang sejenis mengalahkan sebarang deretan. Daun dikadarkan menurut skala berikut:',
    'hands_royal_flush' => 'ROYAL FLUSH',
    'hands_royal_flush_content' => 'Straight flush daun sat yang dikenali sebagai royal flush merupakan standard kedudukan tertinggi bagi daun poker.',
    'hands_straight_flush' => 'STRAIGHT FLUSH',
    'hands_straight_flush_content' => 'Lima kad sama jenis yang berturut-turut. Daun sat mungkin tinggi atau rendah. Straight flush daun sat tinggi dikenali sebagai royal flush, daun poker yang terbaik kedudukannya.',
    'hands_four_of_kind' => 'FOUR OF KIND',
    'hands_four_of_kind_content' => 'Kombinasi empat kad yang sama tahap. Jika dua atau lebih pemain mempunyai four of kind yang sama, kad kelima akan menentukan pemenang.',
    'hands_full_house' => 'FULL HOUSE',
    'hands_full_house_content' => 'Kombinasi tiga kad yang sama sejenis dan satu pasangan. Jika dua atau lebih pemain mempunyai full house, pemain dengan tiga sama jenis yang terbaik akan menang. Jika ia masih sama, pemain dengan pasangan terbaik akan menang.',
    'hands_flush' => 'SEJENIS',
    'hands_flush_content' => 'Semua lima keping kad yang sama jenis, tetapi tanpa turutan. Jika dua atau lebih pemain berkongsi sejenis, pemain dengan kad tertinggi (sehinggalah kad yang kelima jika perlu) dalam sejenis akan menang.',
    'hands_straight' => 'STRAIGHT',
    'hands_straight_content' => 'Lima kad berturut-turut dalam sekurang-kurangnya dua jenis kad yang berbeza. Tiada penentu dengan straight kerana semua lima kad diperlukan untuk melengkapkan daun.',
    'hands_three_of_kind_hands' => 'THREE OF KIND',
    'hands_three_of_kind_hands_content' => 'Tiga kad yang sama tahap. Jika dua atau lebih pemain mempunyai daun three of a kind yang sama, baki dua penentu akan menentukan pemenang.',
    'hands_two_pair' => 'TWO PAIR',
    'hands_two_pair_content' => 'Dua kad yang sama tahap serta dua kad daripada tahap lain. Jika dua atau lebih pemain mempunyai dua pasang, pasangan yang tertinggi akan menentukan pemenang. Jika mereka memiliki dua pasangan yang sama, kemudian penentuan kad kelima menentukan pemenang.',
    'hands_one_pair' => 'SEPASANG',
    'hands_one_pair_content' => 'Dia kad yang sama tahap. Jika para pemain mempunyai pasangan yang sama, bagi tiga kad yang tertinggi (dikenali sebagai penentu) akan menentukan pemenang.',
    'hands_high_card' => 'HIGH CARD',
    'hands_high_card_content' => 'Daun poker yang terdiri daripada mana-mana lima kad yang tidak memenuhi mana-mana keperluan daun. Jika pemain mempunyai kad yang sama tinggi, kad yang kedua tertinggi (dan seterusnya) menentukan pemenang.',
    'hands_kicker' => 'PENENTU',
    'hands_kicker_content' => 'Dalam mana-mana daun yang tidak menggunakan semua 5 kad pemain, baki kad tertinggi dipanggil penentu. Penentu digunakan untuk membandingkan kedudukan seri.',
    
    'rounds_preflop' => 'PREFLOP',
    'rounds_preflop_content_1' => "Setiap pemain menerima dua kad «poket». Taruhan bermula daripada pemain yang berada di kiri tirai besar. Oleh kerana ada taruhan paksa daripada tirai kecil dan besar, pemain lain tidak boleh cek pre flop dan kekal dalam permainan. Mereka sebaliknya perlu ikut tirai besar atau kenaikan; jika tidak, mereka boleh tutup.",
    'rounds_preflop_content_2' =>'Para pemain menganggarkan kekuatan kad mereka dan memilih tindakan yang akan diambil (ikut/naikkan/tutup). Pada akhir pusingan taruhan semua taruhan di masukkan ke dalam pot bersama; pemain akan bersaing untuk pot di pusingan seterusnya.',
    'rounds_flop' => 'FLOP',
    'rounds_flop_content' => 'Tiga kad komuniti diletakkan dengan muka ke atas di meja (flop). Semua pemain di meja boleh menggunakan kad ini untuk membuat kombinasi lima kad mereka. Bertaruh untuk flop bermula dengan pemain aktif yang pertama duduk mengikut arah jam daripada pembahagi. Para pemain menilai situasi dan mengambil tindakan. Semua taruhan ditambah ke dalam pot semasa.',
    'rounds_turn' => 'TURN',
    'rounds_turn_content' => 'Kad keempat, dipanggil turn, ditunjukkan di meja. Kini semua pemain diberi lebih banyak maklumat untuk membuat keputusan, dan satu lagi kad menjadikan lima keping daun permainan. Satu lagi pusingan taruhan yang sama seperti flop dibuat.',
    'rounds_river' => 'RIVER',
    'rounds_river_content' => 'River ialah kad komuniti kelima dan terakhir di meja. Pusingan taruhan adalah sama seperti dua jalan terdahulu (flop dan turn), kecuali tiada lagi kad komuniti yang disediakan selepas pusingan taruhan. Para pemain kini ada sehingga tujuh kad untuk membuat lima daun kad mereka, dua kad poket dan lima kad komuniti. Selepas usingan taruhan akhir, baki pemain sampai kepada Showdown.',
    'rounds_showdown' => 'SHOWDOWN',
    'rounds_showdown_content' => 'Mereka yang masih kekal dalam permainan selepas kesemua empat pusingan akan mengambil bahagian dalam showdown (mendedahkan kad) dan membandingkan kombinasi mereka. Pemain dengan daun poker lima kad tertinggi akan memang permainan dan pot. Para pemain boleh menggunakan kombinasi tujuh kad yang ada pada mereka, dan tidak perlu menggunakan kedua-dua atau salah satu kad poket mereka.',
  
];
