<?php

return [
    
    # Common questions 
    'cq_text' => 'Domande comuni ',
            
    'cq_question1' => 'A quale tipo di Poker posso giocare con l’app?',
    'cq_answer1' => 'Con la nostra mobile e social app Manchester United Poker, puoi giocare ad uno dei più popolari tipi di Poker – il classico ed eterno Texas Hold’em. Puoi o giocare a un tavolo per guadagnare chips o partecipare a dei tornei.',
    
    'cq_question2' => 'Su quali piattaforme Manchester United Poker è disponibile?',
    'cq_answer2' => 'Manchester United Poker è disponibile per le piattaforme iOS, Android, Windows Phone e Facebook.',
    
    'cq_question3' => 'Come si gioca a Manchester Poker?',
    'cq_answer3' => 'Per saperne di più sulle regole e sull’interfaccia del gioco, puoi andare all’ “Aiuto” nel menu principale. Ti spiegheremo le mani del Poker, le puntate e le mosse. Nota: queste sono solo conoscenze di base sul gioco del Poker che insegnano a giocare, non a vincere. Per riuscirci, puoi visitare il sito web nella sezione “Academy” e saperne di più sulla strategia di gioco e sul comportamento dei giocatori, oltre a migliorare le tue capacità. Non puoi nemmeno immaginare quanto incredibile possa essere un’attenta partita a Poker contro un avversario esperto!',
    
    'cq_question4' => 'Cos’è la modalità multischermo?',
    'cq_answer4' => 'Il multischermo è una modalità con Quattro schermi di gioco contemporaneamente disponibili. Con il multischermo puoi allo stesso tempo giocare in modalità standard, prendere parte a un torneo e chattare nel menu dei messaggi personali. Questa modalità è disponibile solo ai giocatori che ranno raggiunto il livello 15. Nella modalità multischermo sono automaticamente disponibili il menu principale, il menu della chat, la lobby e i tornei. Puoi spegnerli nel menu delle impostazioni.',
    
    'cq_question5' => 'Come usare la modalità multischermo?',
    'cq_answer5' => 'Per passare in modalità multischermo bisogna cliccare sull’icona nell’angolo in alto a destra; questa icona mostra anche il settore in cui sei in quel momento. Per arrivare a un particolare menu basta cliccare sul settore corrispondente. Su dispositivi mobili, puoi scorrere gli schermi con il dito.',
    
    'cq_question6' => 'Dove posso trovare la cronologia di gioco?',
    'cq_answer6' => 'Puoi trovare la cronologia nel menu dei messaggi personali nella scheda “Registro eventi”. Tutti i tuoi risultati e le mosse sono mostrati nella sequenza cronologica, a partire dagli ultimi.',
    
    'cq_question7' => 'Cos’è una Lobby?',
    'cq_answer7' => 'La Lobby è una modalità di gioco dove puoi studiare le partite in corso e visionare varie informazioni su di esse (la velocità di gioco, l’importo dei bui, il numero dei posti al tavolo, ecc.) e scegliere il tavolo a cui vorresti unirti. I giocatori esperti studiano a fondo la lobby, selezionando il conto medio e osservando il comportamento dei giocatori per beneficiarne nella partita. Scegliere un tavolo nella lobby è molto importante per scegliere la partita più giusta e aumentare il tuo profitto.',
    
    'cq_question8' => 'Come posso scegliere un tavolo nella lobby?',
    'cq_answer8' => 'Clicca su “Lobby” nel menu principale. In questa modalità puoi scegliere un tavolo e osservare il gioco (come se fossi in un casinò e stessi guardando una partita). Scegli il numero di sedie al tavolo, la velocità del gioco, imposta la visualizzazione dei tavoli al completo. Il livello di bui in questo caso è determinate dall’importo di chips sul tuo conto totale (bankroll). Alcuni giocatori possono organizzare i tavoli tramite i bui. Gira per i tavoli, osserva le partite in corso e i giocatori partecipare. Quando avrai trovato il tavolo di tuo gradimento, puoi prendere una sedie disponibile a questo tavolo e fare la tua puntata.',  
    
    'cq_question9' => 'E’ possibile prendere parte a più tavoli contemporaneamente?',
    'cq_answer9' => 'Puoi prendere parte solo a un tavolo.',
    
    'cq_question10' => 'Come posso rilanciare?',
    'cq_answer10' => 'Per rilanciare di un import arbitrario clicca il pulsante “Rilancia” e TIENILO PREMUTO. Fai avanzare il cursore fino alla puntata di cui hai bisogno e rilascialo. A volte potrebbe esserti richiesto di confermare la tua puntata con un altro click.',
    
    'cq_question11' => 'Come posso partecipare ai tornei?',
    'cq_answer11' => 'Scegli “Tornei” nel menu principale. Ti saranno mostrati i termini da accettare per cominciare a giocare. Nota che puoi prendere parte a un solo torneo per volta.',
    
    'cq_question12' => 'Perchè sono stato “buttato fuori” dal tavolo e non posso parteciparvi?',
    'cq_answer12' => 'Ciò può essere dovuto a una connessione Internet instabile. Quando giochi, assicurati che la tua connessione sia stabile.',
    
    'cq_question13' => 'Che faccio se penso che un altro giocatore ha vinto in modo scorretto?',
    'cq_answer13' => 'Una mano forte non significa sempre una vittoria. Più probabilmente un giocatore con una mano più debole non ha vinto, ma ha solo ricevuto la sua parte di puntata che era più grande della tua, se tu sei andato all-in – puntato tutti i tuoi soldi. Oppure la tua mano era uguale a quella di un giocatore con la carta più alta più grande della tua. Apprendi altro sulle regole del Poker così che la fine della partita ti sia più chiara.',
    
    'cq_question14' => 'Cosa significa una stella dorata vicino al nickname del Giocatore?',
    'cq_answer14' => 'Il simbolo di una stella vicino al nickname di un altro partecipante significa che questo utente è nella tua lista amici.',
    
    'cq_question15' => 'Come posso salvare uno screenshot e condividerlo con gli amici?',
    'cq_answer15' => 'Per salvare e condividere un momento straordinario della partita devi solo cliccare sul simbolo della fotocamera e scegliere una delle seguenti opzioni: salva sul tuo dispositivo, invia tramite e-mail oppure posta sulla bacheca di Facebook. Se l’e-mail non è nella lista, controlla se le e-mail sono disponibili sul tuo dispositivo.',
    
    
    # Creating a game profile
    'cgp_text' => 'Creare un profilo di gioco',
            
    'cgp_question1' => 'Cos’è un account ospite?',
    'cgp_answer1' => 'Non serve una registrazione per iniziare a giocare. Puoi giocare come ospite, ma in questo caso il tuo account sarà collegato a un certo dispositivo. Se vuoi continuare a giocare e salvare tutti i tuoi risultati e punteggi, dovrai usare questo dispositivo. Non potrai ripristinare il tuo account ospite, se il tuo dispositivo viene perduto o ha un guasto.',
    
    'cgp_question2' => 'Perchè devo registrarmi?',
    'cgp_answer2' => 'Dopo la registrazione sarai in grado di creare il tuo profilo di gioco. Tutti i tuoi risultati, le chips, le monete e averi saranno memorizzate in questo profilo. Potrai aggiungere amici e usare questo profilo per giocare da tutti i dispositivi iOS o Android e dai social network.',
    
    'cgp_question3' => 'Come posso registrarmi al gioco?',
    'cgp_answer3' => 'Usa la form di registrazione a disposizione nel menu del gioco. Dovrai inserire il tuo indirizzo e-mail, la password e il nickname. Se desideri, puoi anche caricare la tua immagine di profilo. Ti suggeriamo di usare una email reale, in modo da poterti aiutare a recuperare la tua password. Se hai un profilo su un social network, puoi utilizzarlo come profilo di gioco. ',
    
    'cgp_question4' => 'Ho un profilo Manchester United Roulette, posso usarlo per giocare a Manchester United Poker?',
    'cgp_answer4' => 'Sì, puoi. Manchester United Poker e Manchester United Roulette dispongono di un account integrato. Nota che se hai già un account Manchester United Roulette registrato con una particolare e-mail, non potrai creare un nuovo account con essa. In questo caso, basta autenticarti a Manchester United Poker con la stessa password.',
    
    'cgp_question5' => 'Come faccio a conoscere il mio ID?',
    'cgp_answer5' => 'Il tuo ID è un numero di 9 o 10 cifre, indicate nel tuo profilo dell’applicazione o sul sito web. Ricorda di fornire il tuo ID quando contatti il supporto tecnico.',
    
    'cgp_question6' => 'Posso cambiare la mia login?',
    'cgp_answer6' => 'Sfortunatamente non puoi cambiare la tua login. Ma puoi registrare un nuovo account e scegliere un altro qualsiasi username.',
    
    'cgp_question7' => 'Come posso recuperare la mia password?',
    'cgp_answer7' => 'Per recuperare la tua password clicca sul link “Password dimenticata?” e inserisci i tuoi dati nella form che appare. Ti suggeriamo di usare una email reale. Nel caso tu abbia usato un indirizzo e-mail inesistente o l’indirizzo che hai indicato non è più attivo, ti preghiamo di usare la nostra contact form per ricevere ulteriori istruzioni dal nostro supporto tecnico.',
    
    'cgp_question8' => 'Posso cancellare il mio account?',
    'cgp_answer8' => 'Un account non può essere cancellato. Se desideri, puoi creare un nuovo account. Nota che su un dispositivo è possibile usare fino a 20 account (incluso l’account ospite).',
    
    'cgp_question9' => 'Come posso entrare nelle impostazioni del mio profilo di gioco?',
    'cgp_answer9' => 'Per entrare nel profilo, clicca il riquadro con la tua immagine di profilo, nome e quadro di gioco nel menu principale.',
    
    'cgp_question10' => 'Come posso cambiare il mio nickname?',
    'cgp_answer10' => 'Per cambiare il tuo nickname clicca sul tuo nickname nelle impostazioni di profilo (o l’icona della matita vicino ad esso) e inseriscine uno nuovo, poi clicca su “Fatto”.',
    
    'cgp_question11' => 'Come posso caricare o cambiare la mia immagine di profilo?',
    'cgp_answer11' => 'Clicca sulla tua immagine di profilo corrente nelle impostazioni del profilo. Seleziona “Nuovo” nel menu a tendina, poi scatta una foto o scegli una foto dalla gallery del tuo dispositivo o un’altra foto esistente. Se ti sei autenticato per la prima volta usando il tuo account Facebook, verrà utilizzato il tuo avatar di Facebook.',
    
    'cgp_question12' => 'Come posso cambiare il mio stato?',
    'cgp_answer12' => 'Entra in “Profilo” nel menu principale, poi clicca sulla linea sotto il tuo avatar e inserisci un nuovo stato. Lascia questo campo vuoto, se vuoi che non venga mostrato nulla.',
    
    'cgp_question13' => 'Come posso determinare la mia posizione?',
    'cgp_answer13' => '"Posizione" indica la regione dove ti trovi in questo momento. Per aggiornare la tua posizione clicca sull’icona corrispondente nel profilo di gioco. Per determinare la tua posizione sul sito web clicca su “Determina” vicino a “Posizione” nella riga del tuo ID a destra dell’immagine di profilo.',
    
    'cgp_question14' => 'Perchè non è possibile determinare la mia posizione?',
    'cgp_answer14' => 'Ti preghiamo di assicurarti che le tue impostazioni di localizzazione sul tuo dispositivo siano attivate e che la nostra applicazione abbia accesso ad esse. Se la posizione non viene determinata con le impostazioni specificate, allora prova a reinstallare l’applicazione e a consentire il rilevamento di posizione al primo utilizzo.',
    
     'cgp_question15' => 'Come posso acquistare delle proprietà?', 
     'cgp_answer15' => 'Puoi acquistare delle proprietà con chips o monete. La proprietà non modifica nulla nel meccanismo di gioco, ma può indicare il tuo stato. Per comprare una proprietà, entra nel tuo account, clicca “Proprietà” e usa il menu di acquisto. Puoi regalare proprietà ai tuoi amici o a qualsiasi utente tu voglia. Per vendere proprietà, utilizza l’opzione corrispondente nel menu del profilo. Considera che riceverai indietro solo il 10% del prezzo. Quando vendi una proprietà che ti è stata regalata da altri giocatori, non riceverai nulla. Gli oggetti saranno solo rimossi dal tuo profilo.',  
    
    'cgp_question16' => 'Come posso acquistare regali?',
    'cgp_answer16' => 'Stando seduto al tavolo, clicca sull’icona della lente vicino alla tua immagine di profilo, poi scegli un regalo. Il regalo sarà mostrato vicino all’avatar del giocatore. La proprietà sarà mostrata con il click corrispondente nel profilo utente. Nota che i regali sono temporanei, a differenza della proprietà che è permanente. I regali durano per tutta la partita al tavolo, o alcuni giorni, se acquistati con le monete.',
    
    'cgp_question17' => 'Come faccio regali ad altri giocatori?',
    'cgp_answer17' => 'Clicca sull’icona della lente o della casa vicino all’immagine di profilo del giocatore. Scegli il regalo e conferma l’acquisto. Per fare un regalo a tutti i giocatori al tavolo scegli “acquista per tutti” nel menu di vendita.',
    
    'cgp_question18' => 'Cosa faccio se non mi piace il regalo mandatomi?',
    'cgp_answer18' => 'Nel caso tu non gradisca il regalo che hai acquistato o che ti è stato fatto, puoi cambiarlo con uno nuovo. Clicca sul regalo corrente (il posto dove tu di solito vedi l’icona della lente), poi scegline uno nuovo. Dopo sarà mostrato vicino al tuo avatar. Allo stesso modo puoi cambiare un regalo di un altro giocatore. Inoltre, i regali scompaiono quando esci dall’app.',
    
    'cgp_question19' => 'Perché non posso acquistare alcool e tabacco come regali? ',
    'cgp_answer19' => 'Le regole di Facebook vietano agli utenti di acquistare o ricevere alcool e tabacco mentre si gioca tramite questo social netwoek o quando si è autorizzati tramite l\'account di Facebook.',
    
    'cgp_question20' => 'Come posso guardare i risultati?',
    'cgp_answer20' => 'I risultati sono un indicatore dei tuoi progressi di gioco. Per scorrere la lista dei risultati, vai nel profilo e clicca su “Risultati”. Tappa sul risultato corrispondente per sapere come lo hai raggiunto. Puoi anche scorrere i tuoi risultati sul profilo del sito web.',
    
    'cgp_question21' => 'Come posso uscire?',
    'cgp_answer21' => 'Nelle mobile apps alcune icone sono nascoste nel pannello scorrevole – mostrato con gli indicatori della pagina nell’angolo in alto a destra del menu principale. Muovendo il dito, puoi entrare nelle sezioni Tornei, Invita, Negozio, Tutorial e Uscita dal profilo.',
      
    
    #Game settings
    'gs_text' => 'Impostazioni di gioco',
            
    'gs_question1' => 'Come entro nelle impostazioni?',
    'gs_answer1' => 'Per entrare nelle impostazioni devi cliccare sull’icona dell’ingranaggio nell’angolo in alto a destra del menu principale.',
    
    'gs_question2' => 'Come posso cambiare la lingua?',
    'gs_answer2' => 'La lingua di gioco automatic è la stessa configurata nel tuo dispositivo. In ogni caso, puoi selezionare l’Inglese, disponibile in ogni versione del gioco. Per far questo abilita l’opzione “Localizzazione” nelle impostazioni.',
    
    'gs_question3' => 'Come posso disattivare l’audio?',
    'gs_answer3' => 'Per attivare/disattivare i suoni di gioco o regolare il volume, utilizza il cursore corrispondente nel menu impostazioni.',
    
    'gs_question4' => 'Come posso disattivare la vibrazione?',
    'gs_answer4' => 'La vibrazione ti ricorda di puntare. Puoi disattivarla nel menu impostazioni (funzione disponibile per i soli smartphone).',
    
    'gs_question5' => 'Come posso bloccare la chat?',
    'gs_answer5' => 'Ad un tavolo non puoi solo giocare, ma anche avviare una chat. I messaggi dei giocatori appaiono in alcuni popup al di sopra delle loro immagini di profilo. Se ciò ti dà fastidio, puoi bloccare la chat in un menu special cliccando l’icona di lock vicino al pulsante “chat”. Scegli i giocatori dei quali in particolare non vuoi vedere i messaggi. Puoi anche bloccare le chat di tutti i giocatori. Per fare questo attiva il blocco della chat nelle impostazioni.',
    
    'gs_question6' => 'Come posso disabilitare gli stati?',
    'gs_answer6' => 'Se ti danno fastidio gli stati dei giocatori puoi disabilitarli nelle impostazioni.',
    
    'gs_question7' => 'Come posso abilitare l’illuminazione delle carte?',
    'gs_answer7' => 'Nelle impostazioni puoi abilitare l’illuminazione delle carte che formano una combinazione vincente nella tua mano o sul tavolo. Questo semplifica il gioco, ma diminuisce la suspense e l’interesse. ',
    
    'gs_question8' => 'Come posso disabilitare le notifiche automatiche?',
    'gs_answer8' => 'Puoi disabilitare le notifiche automatiche nelle impostazioni del dispositivo. Per fare questo vai in Impostazioni/Notifiche automatiche/Manchester United Poker.',
    
    'gs_question9' => 'Come posso disabilitare la visione dei regali?',
    'gs_answer9' => 'Puoi disabilitare la visione dei regali al tavolo, se ti dà fastidio. Per fare questo, utilizza il corrispondente cursore nel menu impostazioni. Nota che in questo caso non potrai fare e ricevere regali.',
    
    'gs_question10' => 'Come disabilito il riconoscimento dei messaggi vocali?',
    'gs_answer10' => 'Entra nelle impostazioni, poi scegli il tab “Voce”. Potrai abilitare/disabilitare il riconoscimento dei messaggi vocali e scegliere una lingua per i messaggi da riconoscere.',
    
     # Financial issues   
    'fi_text' => 'Termini economici ',
            
    'fi_question1' => 'Gioco con denaro reale o chips virtuali?',
    'fi_answer1' => 'Noi utilizziamo denaro virtuale, il tuo account rimane intatto. Ma puoi utilizzare denaro reale per acquistare chips e monete nel nostro Store.',
        
    'fi_question2' => 'Come ricevere chips gratuite?',
    'fi_answer2' => 'Riceverai chips gratuite se:<br> - partecipi alla lotteria giornaliera;<br> - invite i tuoi amici a giocare;<br> - seguici su Facebook.<br>Inoltre, i tuoi amici possono regalarti fino a 100.000 chips al giorno.',
        
    'fi_question3' => 'Che succede se ho giocato tutte le mie chips e ho un conto pari a zero?',
    'fi_answer3' => 'Per le prime cinque volte ti verranno ricaricate 500 chips gratuitamente e potrai continuare a giocare.',
        
    'fi_question4' => 'Come acquisto chips?',
    'fi_answer4' => '- Clicca su “Negozio” nel menu principale e acquista l’importo desiderato in chips tramite iTunes o Google Play Store. Il pagamento sarà effettuato con la tua carta di credito collegata all’account. Puoi utilizzare anche una carta regalo iTunes.<br>- Se giochi utilizzando il tuo account di un social network, puoi acquistare chips con la moneta virtuale di un particolare social network (come i crediti Facebook).',
        
    'fi_question5' => 'Come posso acquistare monete? A cosa servono?',
    'fi_answer5' => 'Le monete ti servono per acquistare regali esclusivi e proprietà. Le monete possono essere acquistate nelle store come le chips. A volte potrai riceverle come regalo dagli sviluppatori del gioco.',
        
    'fi_question6' => 'Posso cancellare un acquisto?',
    'fi_answer6' => 'Sì, puoi. Devi rivolgerti al sistema di pagamento che hai utilizzato per fare questo acquisto. Non lavoriamo direttamente con i sistemi di pagamento, perciò non possiamo cancellare i tuoi acquisti.',
        
    'fi_question7' => 'Come fa un livello bonus a modificare l’importo delle chips acquistate e delle monete?',
    'fi_answer7' => 'Il livello Bonus ti permette di ricevere un rilevante aumento quando acquisti chips o monete per le prime tre volte. Di default, ricevi il primo livello e guadagni il 100% di chips bonus gratuitamente. Dopo il primo acquisto passerai al secondo livello bonus e riceverai il 200% di chips bonus gratuitamente al prossimo acquisto di chips o monete. Infine, raggiungendo il terzo livello bonus riceverai il 300% di chips o monete acquistate.',
        
    'fi_question8' => 'Come posso acquistare altre chips per giocare?',
    'fi_answer8' => 'Stai seduto ad un tavolo ma non hai alcuna chip? Clicca sul tuo avatar e poi clicca l’icona della chip nella finestra del profilo. Inserisci l’importo di chips di cui necessiti e torna a giocare. Puoi anche semplicemente cliccare su “Alzati” o “Non giocare” e poi “Sedere” nuovamente al tavolo. In questo caso ti sarà richiesto di portare con te l’importo di chips necessario. ',
        
    'fi_question9' => 'Posso scambiare le chips con le monete e le monete con le chips?',
    'fi_answer9' => 'Le monete non possono essere scambiate con le chips. Anche le chips non possono essere scambiate con le monete, puoi acquistare le monete solo nello Store. Per acquistare, clicca su “Negozio” nel menu principale.',
        
    'fi_question10' => 'Come posso recuperare delle chips “scomparse”?',
    'fi_answer10' => 'Se entri nel gioco e noti che hai meno chips e un livello inferiore, per prima cosa, controlla se ti sei autenticato con lo stesso account. Potresti esserti autenticato come guest o usando un altro account. Se sei sicuro che non sia uno dei problem sopra riportati, contatta il nostro supporto tecnico, saremo felici di aiutarti.',
        
     'fi_question11' => 'Come posso trasferire chips ad un altro account ?', 
     'fi_answer11' => 'E’ vietato trasferire i chips da un proprio account ad un altro.',

    
    # Friends 
    'f_text' => 'Amici',
            
    'f_question1' => 'Come utilizzo il tab Amici nel menu principale?',
    'f_answer1' => 'Nel menu principale puoi trovare la lista dei tuoi amici sotto l’anteprima del tuo profilo. L’avatar più in alto è un profilo di un amico che hai scelto (al centro dello schermo sono mostrate le informazioni dell’amico e i pulsanti delle varie opzioni). Più in basso, puoi vedere la lista su due righe del resto delle anteprime degli avatar dei tuoi amici. Per vedere l’intera lista degli amici, scorri la lista con le anteprime degli avatar. In basso sullo schermo, sono mostrati gli avatar di giocatori scelti a caso.',
        
    'f_question2' => 'Come posso aggiungere altri giocatori agli “Amici”?',
    'f_answer2' => ' - Entrando nella sezione “Amici” nel menu principale e andando al tab “Ricerca”. Potrai cercare gli utenti tramite i loro nickname, i loro indirizzi e-mail e i loro ID e cliccare su “Aggiungi agli amici”.<br> - Giocando ad un tavolo, potrai inviare richieste di amicizia agli utenti che partecipano allo stesso tavolo. Per fare questo basta aprire il loro profile e cliccare su “Aggiungi agli amici”.',
        
    'f_question3' => 'Come posso regalare chips a un Amico?',
    'f_answer3' => 'Entra nella sezione “Amici” nel menu principale, scegli un amico e clicca su “Invia chips”. Puoi regalare fino a 100.000 chips al giorno.',
        
    'f_question4' => 'Come posso acquistare chips per un Amico?',
    'f_answer4' => 'Entra nel profilo di un amico, clicca su “Invia chips” e fai avanzare la barra scorrevole fino a un valore massimo, poi vedrai un pulsante “Acquista e invia chips”. Dopo aver cliccato su di esso, sarai automaticamente reindirizzato alla sezione “Negozio”. Le chips che comprerai saranno inviate al tuo amico.',
        
    'f_question5' => 'Quante chips posso comprare per i miei amici?',
    'f_answer5' => 'Quando regali chips ai tuoi amici, l’importo è limitato. Ma puoi acquistare invece un import illimitato di chips per i tuoi amici.',
        
    'f_question6' => 'Come faccio un regalo a un Amico o come gli mando una mia proprietà?',
    'f_answer6' => 'Puoi acquistare proprietà e regali per altri giocatori. Clicca sull’icona della “lente” o della “casa” vicino all’avatar del tuo amico. Puoi anche scegliere “Acquista per tutti” e fare un regalo a tutti i giocatori che prendono parte al tavolo. Ti preghiamo di notare che i regali sono temporanei a differenza della proprietà che è permanente. Alcuni regali possono durare alcuni giorni, questi sono i regali acquistati con monete.',
        
    'f_question7' => 'Come posso prendere parte al tavolo in cui sta giocando un Amico?',
    'f_answer7' => 'Entra nella sezione “Amici” nel menu principale e seleziona il profilo di un amico. Se l’utente sta giocando, clicca su “Partecipa a questo tavolo”. Se vuoi invitare un amico al tavolo dove stai giocando al momento, clicca su una sedia vuota e scegli l’amico dalla lista. Il tuo amico riceverà un invito a giocare.',
        
    'f_question8' => 'Come posso cancellare utenti dagli “Amici”?',
    'f_answer8' => 'Per cancellare un utente dalla lista amici, seleziona “Amici” nel menu principale. Scegli un amico che vuoi cancellare, clicca “Cancella dalla lista amici”.',
        
    'f_question9' => 'Come posso ricevere chips o altri bonus invitando amici?',
    'f_answer9' => 'Seleziona “Amici” nel menu principale e scegli “Invita”. Poi invia ai tuoi amici una richiesta per raggiungerti su Manchester United Poker tramite SMS, e-mail o Facebook. Se i tuoi amici cliccano sul link allegato, riceverai il bonus. Se giochi su Facebook clicca “Chips gratuite” e riceverai chips per ogni giocatore che inviterai.',
    
    
    # Communication with other players
    'cwop_text' => 'Comunicazione con altri giocatori',
            
    'cwop_question1' => 'Cosa sono i messaggi personali?',
    'cwop_answer1' => 'I messaggi personali sono messaggi che invii a un certo giocatore. Perciò, solo lui/lei può leggerli. Nella cronologia dei messaggi personali sono mostrati non solo i messaggi inviati durante una conversazione del momento, ma anche l’archivio dei messaggi con indicazioni di data e ora.',
            
    'cwop_question2' => 'Come faccio ad entrare nel menu dei messaggi personali?',
    'cwop_answer2' => 'Per entrare nel menu dei messaggi personali, clicca sull’icona della bustina “Messaggi” nel menu principale. Puoi anche scegliere un amico con cui vorresti iniziare una conversazione e cliccare sul pulsante della bustina nel suo profilo.',
            
    'cwop_question3' => 'Come funzionano i messaggi personali?',
    'cwop_answer3' => 'Sulla sinistra viene mostrata la lista degli amici con cui hai già avuto una conversazione. Per aggiungere un nuovo amico a questa lista clicca “Aggiungi” (icona con la matita e il più localizzata nell’angolo in basso a sinistra dello schermo. Puoi anche andare sul profilo del tuo amico ed effettuare alcune operazioni, come per esempio, inviare chips o prender parte a un tavolo.',
            
    'cwop_question4' => 'Come invio un messaggio?',
    'cwop_answer4' => 'Seleziona un amico a cui vorresti inviare un messaggio, clicca su un campo speciale per scrivere del testo. Digita il tuo messaggio e clicca su “Invia”.',
            
    'cwop_question5' => 'Posso inviare un messaggio a un amico che è offline?',
    'cwop_answer5' => 'Puoi anche inviare un messaggio a un amico che è offline. I messaggi che tu hai ricevuto mentre eri offline sono mostrati come numeri sull’icona “messaggi”.',
            
    'cwop_question6' => 'Come cancello messaggi personali?',
    'cwop_answer6' => 'Clicca e tieni premuto sul messaggio che vorresti cancellare, poi seleziona “Cancella” nel menu che apparirà.',
            
    'cwop_question7' => 'Come scrivo qualcosa in una chat?',
    'cwop_answer7' => 'Mentre partecipi a un tavolo, apri la finestra della chat cliccando sull’icona “Messaggi” nell’angolo in basso sullo schermo. Digit ail tuo messaggio e clicca “Invio”, poi “Fatto”.',
            
    'cwop_question8' => 'Come utilizzo i messaggi vocali?',
    'cwop_answer8' => 'Puoi comunicare con gli altri giocatori non solo tramite messaggi di testo, ma anche con messaggi vocali. Clicca sull’icona del microfono nell’angolo in basso a destra dello schermo e pronuncia un messaggio che vorresti condividere con i tuoi avversari. Il sistema riconoscerà il tuo discorso e lo convertirà in testo sugli schermi degli altri giocatori. Il periodo di tempo in cui puoi utilizzare questa funzionalità è limitato, può essere acquistato tramite monete nel nostro Store.',
  
];
