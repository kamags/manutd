<?php

return [
    'actions' => 'Ad ogni turno, il giocatore o la giocatrice può compiere una delle seguenti azioni:',
    'actions_call' => 'VEDERE',
    'actions_call_content' => "Se c’è già stata una puntata, vedere significa che un giocatore accetta di corrispondere quella puntata. Ad esempio, se il giocatore A punta 10, e il giocatore B vede, il giocatore B dovrà corrispondere la puntata di 10, mettendo anch’egli 10 nel piatto.",
    'actions_check' => 'CHECK',
    'actions_check_content' => 'Se non ci sono state puntate nel turno corrente, un giocatore può fare check. Fare check significa passare. Si resta nella mano, ma si sceglie di non alzare la posta in gioco attraverso una puntata.',
    'actions_raise' => 'RILANCIARE',
    'actions_raise_content' => 'Un rilancio è una puntata effettuata per rilanciare un’altra. Se era stata fatta una puntata nel turno corrente, un giocatore può rilanciarla. Per farlo, dovrebbe fare una puntata grande almeno il doppio della puntata del giocatore precedente. Per continuare a concorrere per il piatto, tutti i giocatori successivi devono vedere o rilanciare di nuovo; altrimenti essi dovranno passare.',
    'actions_fold' => 'PASSARE',
    'actions_fold_content'=> 'Passare significa rinunciare a concorrere per il piatto corrente. Se un giocatore passa, le sue carte non partecipano più alla mano e non possono essere vincenti in questo turno.',
    
    'bets_blinds' => 'BUI',
    'bets_blinds_content' => 'Prima che le carte siano distribuite, il giocatore seduto alla sinistra del Bottone è costretto a fare una puntata chiamata il piccolo buio. Il giocatore successivo seduto alla sinistra del piccolo buio è costretto anch’egli a fare una puntata chiamata il grande buio. L’importo del grande buio è il doppio dell’importo del piccolo buio.',
    'bets_ante' => 'ANTE',
    'bets_ante_content' => 'Se un giocatore partecipa ad un tavolo in una qualsiasi posizione a parte il grande buio, questi sarà costretto per la prima mano a puntare un importo pari al grande buio (ante). I giocatori punteranno un ante una volta sola. Dopo, essi dovranno solamente puntare il grande e il piccolo buio mentre si sposta il Dealer.',
    'bets_minimum_raise' => 'RILANCIO MINIMO',
    'bets_minimum_raise_content' => 'Un giocatore non può rilanciare di un importo minore di quello del grande buio.',
    'bets_all_in' => 'ALL IN',
    'bets_all_in_content' => "Se non hai abbastanza chips per puntare, puoi chiamare un ‘all-in’. Significa che punti tutto ciò che ti è rimasto. Se chiami un all-in, non ti è permesso di vincere più denaro di quanto tu ne hai potuto puntare. Se il piatto aumenta dopo che tu hai chiamato l’all-in e vinci, il piatto sarà diviso tra te e il giocatore successivo con la mano migliore, che ha potuto coprire i rilanci.",
    'bets_split_pot' => 'PIATTO DIVISO',
    'bets_split_pot_content' => 'Se due o più giocatori pareggiano, il piatto viene diviso equamente.',
    
    'hands_01' => 'Il Poker è il gioco di carte più famoso al mondo. Lo scopo del gioco è di sbancare il tavolo, con la classifica di quattro (o meno) round. I due modi principali di vincere un piatto sono collezionare una mano più forte di quella degli avversari o piegarli e far sì che rifiutino di continuare a giocare.',
    'hands_02' => 'La mano nel Poker consiste sempre in cinque carte. Ogni mano in una categoria sulla lista ne batte una di una categoria al di sotto. Per esempio, ogni Scala a colore batte ogni Poker; ogni Colore batte ogni Scala. Le mani sono classificate secondo la scala seguente:',
    'hands_royal_flush' => 'SCALA REALE',
    'hands_royal_flush_content' => 'Una scala a colore formata dalle cinque carte più alte di un seme conosciuta come scala reale è la più alta combinazione di una mano del poker.',
    'hands_straight_flush' => 'SCALA A COLORE',
    'hands_straight_flush_content' => 'Cinque carte consecutive dello stesso seme. Gli assi possono aprire o chiudere la scala. Se l’asso è la carta più alta della scala a colore, questa è chiamata scala reale, la migliore mano possibile nel poker.',
    'hands_four_of_kind' => 'POKER',
    'hands_four_of_kind_content' => 'Combinazione di quattro carte dello stesso valore. Se due o più giocatori hanno lo stesso poker, allora la quinta carta più alta, il kicker, determina il vincitore.',
    'hands_full_house' => 'FULL',
    'hands_full_house_content' => 'La combinazione di tre carte dello stesso valore e una coppia. Se due o più giocatori hanno un full allora il giocatore con il miglior tris vince. Se questi sono dello stesso valore allora il giocatore con la coppia migliore vince.',
    'hands_flush' => 'COLORE',
    'hands_flush_content' => 'Cinque carte dello stesso seme, senza una sequenza. Se due o più giocatori hanno un colore allora il giocatore con la carta più alta (fino alla quinta carta se necessario) nel colore vince.',
    'hands_straight' => 'SCALA',
    'hands_straight_content' => 'Cinque carte di valore consecutive appartenenti ad almeno due semi differenti. Non ci sono kickers con le scale perchè tutte e cinque le carte sono necessarie per formare la mano.',
    'hands_three_of_kind_hands' => 'TRIS',
    'hands_three_of_kind_hands_content' => 'Tre carte dello stesso valore. Se due o più giocatori condividono lo stesso tris, I due kickers rimanenti determinano il vincitore.',
    'hands_two_pair' => 'DOPPIA COPPIA',
    'hands_two_pair_content' => 'Due carte dello stesso valore più due carte di un altro valore. Se due o più giocatori hanno una doppia coppia, allora la coppia più alta determina il vincitore. Se hanno la stessa doppia coppia allora la quinta carta kicker determina il vincitore.',
    'hands_one_pair' => 'COPPIA',
    'hands_one_pair_content' => 'Due carte dello stesso valore. Se i giocatori hanno la stessa coppia, allora la più alta delle tre carte restanti (chiamate kickers) determina il vincitore.',
    'hands_high_card' => 'CARTA ALTA',
    'hands_high_card_content' => 'La mano del poker formata da cinque carte qualsiasi che non danno luogo a nessuna combinazione. Se i giocatori hanno la stessa carta più alta, allora la seconda carta più alta (e così via) determina il vincitore.',
    'hands_kicker' => 'KICKER',
    'hands_kicker_content' => 'In ogni mano in cui non vengono usate tutte e 5 le carte del giocatore, la carta più alta rimasta è chiamata kicker. Il kicker è usato per confrontare i pareggi.',
    
    'rounds_preflop' => 'PREFLOP',
    'rounds_preflop_content_1' => "Ad ogni giocatore sono distribuite due carte del “mazzo”. Le puntate iniziano dal giocatore seduto dalla sinistra del grande buio. Visto che c’è una puntata obbligata dal piccolo e grande buio, gli altri giocatori non possono fare check nel pre flop e restano in gioco. Piuttosto, devono vedere il grande buio o rilanciare; altrimenti, possono passare.",
    'rounds_preflop_content_2' =>'I giocatori valutano la forza delle proprie carte e decidono quale azione compiere (vedere/rilanciare/passare). Alla fine del giro di puntate, tutte le puntate sono messe nel piatto comune; i giocatori gareggeranno per vincerlo nei prossimi turni.',
    'rounds_flop' => 'FLOP',
    'rounds_flop_content' => 'Tre carte in comune sono poste scoperte sul tavolo (the flop). Tutti i giocatori seduti al tavolo possono utilizzare queste carte per costruire le proprie combinazioni di cinque carte. Le puntate durante il flop cominciano con il primo giocatore ancora in gioco seduto in senso orario dal mazziere. I giocatori valutano la situazione e fanno la loro mossa. Tutte le puntate sono aggiunte al piatto corrente.',
    'rounds_turn' => 'TURN',
    'rounds_turn_content' => 'Viene rivelata sul tavolo la quarta carta, chiamata il turn. Ora i giocatori hanno più informazioni per prendere le loro decisioni, e una carta in più per comporre la loro mano. Avviene un ulteriore giro di puntate simile al flop.',
    'rounds_river' => 'RIVER',
    'rounds_river_content' => 'Il river è la quinta e ultima carta in comune sul tavolo. Il giro di puntate è simile ai due precedenti (flop e turn), eccetto che non ci saranno ulteriori carte in comune da scoprire dopo il giro di puntate. I giocatori ora hanno fino a sette carte disponibili per comporre la loro mano, due carte dal mazzo e cinque carte in comune. Dopo il giro finale di puntate, i giocatori rimanenti arrivano allo Showdown.',
    'rounds_showdown' => 'SHOWDOWN (POKER)',
    'rounds_showdown_content' => 'Coloro che sono rimasti in gioco dopo tutti e quattro i round prendono parte allo showdown (scoperta delle carte) e confrontano le loro combinazioni. Il giocatore con la mano più alta vince la mano e il piatto. I giocatori possono utilizzare qualsiasi combinazione delle sette carte loro disponibili, e non devono usare per forza l’una o l’altra delle loro carte dal mazzo.',
  
];
