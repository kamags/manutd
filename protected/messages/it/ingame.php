<?php

return [
	"header-text-2"                => "Gioca gratuitamente sul tuo dispositivo mobile, tramite social network o online",

	"play-now-btn"                 => "Gioca ora",

	"section-1-title"              => "Manchester United Social Poker – <br />Powered by KamaGames<br />Dai il calcio d’inizio alla tua passione per il Poker!",
	"section-1-text"               => "Manchester United Social Poker – <br />Dai il calcio d’inizio alla tua passione per il Poker!",

	"section-2-title"              => "Seleziona un tavolo<br />e preparati<br />a partite elettrizzanti!",
	"section-2-text"               => "Segna la tua tripletta personale <br />giocando a Poker con altri appassionati<br />da tutto il mondo!",

	"section-3-title"              => "Caratteristiche",
	"section-3-text"               => "Manchester United Social Poker",
	"section-3-item-1"             => "GIOCA<br />GRATUITAMENTE",
	"section-3-item-2"             => "MIGLIAIA DI GIOCATORI<br />ONLINE OGNI GIORNO",
	"section-3-item-3"             => "CHIPS GRATUITE NELLE<br />LOTTERIE GIORNALIERE",

	"section-3-list-item-1"        => "Chiacchiera con altri giocatori, acquista regali e ottieni obiettivi",
	"section-3-list-item-2"        => "Guadagna chips bonus invitando a giocare gli amici!",
	"section-3-list-item-3"        => "Usa un account su più dispositivi",
	"section-3-list-item-4"        => "Tutorial incluso",

	"section-4-label"              => "Giocatori online:",

	"section-5-title"              => "Roulette",
	"section-5-title-2"            => "Gioca a Manchester United Social<br />Roulette con altri tifosi del Manchester United",
	"section-5-text"               => "Prova il gioco leggendario che attrae<br />milioni di persone nei casinò di tutto il mondo.",

	"form-title"                   => "Supporto",
	"form-text"                    => "Scrivi la tua domanda e lascia un recapito, il nostro supporto clienti ti risponderà entro 24 ore.",
	"form-success"                 => "Il tuo messaggio è stato inviato. <br /> Un membro del nostro supporto clienti risponderà il prima possibile.",
	"form-error"                   => '<p class="form-message">Please fill the required field</p>',
	"form-btn"                     => 'Send message',

	"form-input-name"              => "Nome",
	"form-input-email"             => "E-mail",
	"form-input-message"           => "Messaggio",

	"footer-support"               => "Supporto",
	"footer-tos"                   => "Termini del servizio",
	"footer-policy"                => "Politica della privacy",
	"footer-rights"                => "Tutti i diritti riservati.",

	"footer-subscribe-title"       => "Iscriviti alla nostra newsletter:",
	"footer-subscribe-placeholder" => "Inserisci il tuo indirizzo e-mail",
	"footer-subscribe-button"      => "Iscriviti",

	"footer-legal"                 => "Questo gioco è accessibile solamente a maggiorenni. Il gioco non propone alcuna possibilità di vincere denaro o qualcos'altro di prezioso.<br />Il fatto di giocare con successo a questo gioco non implica il vostro successo in un simile gioco con denaro reale in un casinò.",

	"popup-text-1"                 => "Gioca ora",
	"popup-text-2"                 => "o scarica l’app dal tuo negozio preferito",
	"popup-text-btn"               => "Installa e gioca",

	"section-4-list-item-1"        => "Tre tipologie di roulette: americana, francese ed europea",
	"section-4-list-item-2"        => "Grafica 3D, giocabilità e fisica realistiche",
	"section-4-list-item-3"        => "La probabilità di vincere è identica a quella della vera roulette",
	"section-4-list-item-4"        => "Magnifiche funzionalità sociali: chiacchiera al tavolo della roulette o in privato",
	"section-4-list-item-5"        => "Gioca con i tifosi del Manchester United su iPhone, iPad, Android e Facebook",

	"casino-text-1"                => "Gioca a Poker ora",
	"casino-text-2"                => "Gioca alla Roulette ora",

	"lang-title"                   => "Seleziona la lingua"

];
