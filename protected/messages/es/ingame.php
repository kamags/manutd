<?php

return [
	"header-text-2"                => "Juega gratis en tu equipo, dispositivo móvil o red social",

	"play-now-btn"                 => "Jugar ya",

	"section-1-title"              => "Manchester United Social Poker – <br />Powered by KamaGames<br />¡Comienza ya tu partida de póker social!",
	"section-1-text"               => "Manchester United Social Poker – <br />¡Comienza ya tu partida de póker social!",

	"section-2-title"              => "¡Elige mesa y prepárate<br />para competiciones<br />emocionantes!",
	"section-2-text"               => "¡Consigue tu propio triplete<br />jugando al póker social<br />con aficionados de todo el mundo!",

	"section-3-title"              => "Características",
	"section-3-text"               => "Manchester United Social Poker",
	"section-3-item-1"             => "TOTALMENTE<br />GRATIS PARA JUGAR",
	"section-3-item-2"             => "MILES DE JUGADORES<br />QUE SE CONECTAN A DIARIO",
	"section-3-item-3"             => "FICHAS GRATIS<br />EN LOTERÍAS DIARIAS",

	"section-3-list-item-1"        => "Chatea con otros jugadores, compra regalos y hazte con logros",
	"section-3-list-item-2"        => "¡Gana fichas de premio al invitar a tus amigos a que jueguen!",
	"section-3-list-item-3"        => "Usa una cuenta en múltiples dispositivos",
	"section-3-list-item-4"        => "Tutorial dentro de la aplicación",

	"section-4-label"              => "Jugadores conectados:",

	"section-5-title"              => "Ruleta",
	"section-5-title-2"            => "Juega a Manchester United Social<br />Roulette con otros seguidores del United",
	"section-5-text"               => "Prueba el legendario juego que atrae a<br />millones de personas a los casinos de todo el mundo.",

	"form-title"                   => "Asistencia técnica",
	"form-text"                    => "Deja aquí tu mensaje y tus datos de contacto, y nuestro equipo de asistencia técnica te responderá en un máximo de 48 horas.",
	"form-success"                 => "Tu mensaje se ha enviado. <br /> Nuestro equipo de asistencia técnica te responderá lo antes posible.",
	"form-error"                   => '<p class="form-message">Please fill the required field</p>',
	"form-btn"                     => 'Send message',

	"form-input-name"              => "Nombre",
	"form-input-email"             => "Correo electrónico",
	"form-input-message"           => "Mensaje",

	"footer-support"               => "Asistencia técnica",
	"footer-tos"                   => "Condiciones de servicio",
	"footer-policy"                => "Política de privacidad",
	"footer-rights"                => "Todos los derechos reservados.",

	"footer-subscribe-title"       => "Suscríbete a nuestro boletín de noticias:",
	"footer-subscribe-placeholder" => "Escribe tu correo electrónic",
	"footer-subscribe-button"      => "Suscribirse",

	"footer-legal"                 => "Este juego solo está disponible para personas mayores de edad. El juego no ofrece ninguna posibilidad de ganar dinero u otro premio que tenga un valor real.<br />El éxito en este juego no implica su éxito en un juego similar en un casino real.",

	"popup-text-1"                 => "Juega ya",
	"popup-text-2"                 => "o descárgate la aplicación en tu tienda preferida",
	"popup-text-btn"               => "Instalar y jugar",

	"section-4-list-item-1"        => "Tres tipos de ruleta: americana, francesa y europea",
	"section-4-list-item-2"        => "Gráficos en 3D y viabilidad de la dinámica de juego y la física",
	"section-4-list-item-3"        => "La probabilidad de ganar es idéntica a la probabilidad de la ruleta auténtica",
	"section-4-list-item-4"        => "Estupendas funciones sociales: chatea en la mesa de la ruleta o en privado",
	"section-4-list-item-5"        => "Juega con seguidores del United en iPhone, iPad, Android y hasta Facebook",

	"casino-text-1"                => "Juega al poker ahora",
	"casino-text-2"                => "Juega a la ruleta ahora",

	"lang-title"                   => "Elegir idioma"
];
