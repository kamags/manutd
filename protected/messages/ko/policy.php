<?php

return [

"policy-title" =>  "Privacy Policy",
"policy-data" =>  "",
"policy-main-1" =>  "KamaGames’ Privacy Policy has been adopted to explain how we store and use the information collected about you online on our websites, during your use of our products and/or services and on mobile platforms. KamaGames, its subsidiaries and Affiliates respect the privacy rights of Users and recognize the importance of protecting the information collected. By accepting our Privacy Policy and Terms of Service, you consent to our collection, storage, use and disclosure of your personal information as described in this Privacy Policy.",
"policy-main-2" =>  "If you have any questions or concerns, please contact our Support Team at support@kamagames.com.",



"policy-defintions-title" =>  "Definitions",
"policy-defintions-1" =>  "Account:  the account created when registering for Pokerist, Roulettist or any of KamaGames’ games. ",
"policy-defintions-2" =>  "Affiliates:  refers to all KamaGames subsidiaries, parent companies, partners, joint ventures, licensees, licensors, and/or any of their agents, consultants, employees, shareholders, officers and directors and any related parties.",
"policy-defintions-3" =>  "Content:  refers to any data uploaded or transmitted through the Service by the user.",
"policy-defintions-4" =>  "Sites: refers to the web domains at kamagmaes.com, pokerist.com, any other web domain hosted by KamaGames or any social media page connected with KamaGames Services hosted on Facebook, Twitter, YouTube or Instagram.",
"policy-defintions-5" =>  "Services: refers to products, downloads, games, services, content and websites controlled by KamaGames and/or its Affiliates.",
"policy-defintions-6" =>  "Software: refers to any games or software used or downloaded from any of the Services.",
"policy-defintions-7" =>  "SNS: Third Party Social Networking Site.",
"policy-defintions-8" =>  "Virtual Items:  meaning virtual “currency” and “objects”, which may include, but are not limited to, virtual “coins”, “cash”, “tokens”, “points” and “property”.",



"policy-intro-title" =>  "Introduction",
"policy-intro-1" =>  "KamaGames and its owned or controlled subsidiaries and Affiliates may be collectively referred to herein as «KamaGames», «we», or «our», as the context requires. KamaGames is a developer and publisher of social games for the web and mobile devices. By using kamagames.com websites, KamaGames’ Services and/or Software, you signify your assent to KamaGames’ Privacy Policy and understanding that KamaGames may process your data in accordance with this Privacy Policy. IF YOU DO NOT AGREE TO THE FOREGOING, PLEASE DO NOT USE THE SITES OR APPLICATIONS. If you access our games from a third party Social Networking Site, you may be required to also read and accept the SNS Terms of Service and Privacy Policy.",
//"policy-intro-2" =>  "",


"policy-collect-title" =>  "Information We Collect",
"policy-collect-0" =>  "KamaGames collects both personal and non-personal User information. Personal information is information that identifies you and that may be used to contact you on-line or off-line.",
"policy-collect-1" =>  "2.1. KamaGames may collect personal information from our online Users during:",

	"policy-collect-1-1" =>  "Registration for games;",
	"policy-collect-1-2" =>  "Registration for KamaGames and/or other service accounts;",
	"policy-collect-1-3" =>  "User support and/or technical service requests;",
	"policy-collect-1-4" =>  "Player match up and other head-to-head online competitions and/or special game-specific events;",
	"policy-collect-1-5" =>  "Contest registration and prize acceptance;",
	"policy-collect-1-6" =>  "Creation of a personal profile;",
	"policy-collect-1-7" =>  "Access to our products and/or services on social networks or other third party services;",
	"policy-collect-1-8" =>  "Otherwise through use of our software, mobile or online service where personal information is required for use and/or participation.",
	"policy-collect-1-9" =>  "Information collected will vary depending upon the activity and may include your name, email address, phone number, home address, birth date, mobile phone number and credit card information. You can choose not to provide (when such opportunity is provided) certain information, but then you might not be able to take advantage of many of our features.",

"policy-collect-2" =>  "2.2. Non-personal information, alone, cannot be used to identify or contact you. KamaGames collects non-personal information about your use of our online and mobile products and services both on our websites and in the course of game play and Software usage.",
"policy-collect-2-1" =>  "When you use KamaGames online and mobile products and Services, we may collect certain non-personal information including gender, zip code, information about your computer, hardware, software, platform, game system, media, mobile device, including device IDs, incident data, Internet Protocol (IP) address and other information.",
"policy-collect-2-2" =>  "When you browse certain portions of the Services, we may automatically collect and record standard user information and clickstream data about your visit (through the use of cookies and otherwise) as part of our analysis of the use of the Service. For example, when you visit or request a web page from our server, we may record the IP address of the user’s machine, the time, date and URL of the request and information about the browser being used, and other information regarding the visit, such as the pages visited on the Sites, the time spent on each page, any advertisements clicked and any search terms used (collectively «Log Data»).",
"policy-collect-3" =>  "2.3. When you use any KamaGames game at an SNS such as Facebook or Google +, you allow KamaGames to access certain information from you profile for that site. The information you allow KamaGames to access varies by game and SNS, and it is affected by the privacy settings you establish at the SNS. You can control and find our more about these settings at the SNS where you play our game. By playing a KamaGames game through an SNS, you are authorizing KamaGames to collect, store, and use in accordance with this Privacy Policy any and all information that you agreed the SNS could provide to KamaGames through the SNS Application Programming Interface. Your agreement takes place when you accept or allow (or similar terms) one of our applications on an SNS.",
"policy-collect-3-1" =>  "Where We Store Your Information",
"policy-collect-3-2" =>  "KamaGames stores information about you on servers located in the United States. Personal data collected within the European Union may be transferred to, and stored at, a destination outside the European Union. Data we collect may also be processed by staff operating outside the US and the EU who works for us or for one of our suppliers. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.",


"policy-uses-title" =>  "How KamaGames Uses Information Collected",
"policy-uses-1" =>  "KamaGames uses your information only as follows:",

	"policy-uses-1-1" =>  "To provide our products and services to you and fulfil your specific requests;",
	"policy-uses-1-2" =>  "To send you electronic communications and messages about our products and services, new products, features, enhancements, special offers, upgrade opportunities, contests and events of interest;",
	"policy-uses-1-3" =>  "To enable user-to-user communications;",
	"policy-uses-1-4" =>  "To better understand the behaviour and preferences of our Users, to troubleshoot technical problems, to deliver advertising, for the purposes of proper functioning of our products and services and improving them;",
	"policy-uses-1-5" =>  "To improve our software and services by providing experiences and recommendations, language and location customization, personalized help and instructions.",



"policy-disclose-title" =>  "How We Disclose Information Collected",
"policy-disclose-0" =>  "We disclose your information to third parties, that is parties other than KamaGames, in the following circumstances:",
"policy-disclose-1" =>  "5.1. We may share your information with subsidiaries and Affiliates that we own or control, but only if those entities are either subject to this Privacy Policy or follow practices at least as protective as those described in this Privacy Policy.",
"policy-disclose-2" =>  "5.2. From time to time KamaGames employs third party contractors to collect personal information on our behalf to provide email delivery, product, prize or promotional fulfilment, contest administration, credit card processing, shipping or other services on our sites. We ask some third party contractors, such as credit agencies, data analytics or market research firms, to supplement personal information that you provide to us for our own studies. When our third party agents or service providers collect and/or have access any information other than non-personal, anonymous and/or aggregated data, KamaGames requires that they use data consistently with our stated privacy policies and protect the confidentiality of personal information they collect or have access to in the course of their engagement by KamaGames. These third parties are prohibited from using your personal information for any other purpose without your specific consent.",
"policy-disclose-3" =>  "5.3. The Service supports and may encourage interaction among players. Your SNS friends may see your name, profile photo and descriptions of your game activity, your game profile. Other players may also be able to send you game requests or even friend requests through the related SNS’ communication channels.",
"policy-disclose-4" =>  "5.4. KamaGames may share aggregated information (information about you and other users collectively), anonymous information and certain technical information (including IP Addresses, MAC Addresses for mobile devices and mobile device IDs) to develop and deliver targeted advertising in the service and on the websites of third parties. We may also allow advertisers to collect this information within the Service and they may share it with Us.",
"policy-disclose-5" =>  "5.5. We reserve the right to disclose your information when we believe release is appropriate to comply with the law, judicial proceedings, court order or other legal process; enforce or apply our Terms of Service and other agreements; or protect the rights, property, or safety of KamaGames, our employees, our users, or others. This includes exchanging information with other companies and organizations for fraud protection and credit risk reduction purposes.",
"policy-disclose-6" =>  "5.6. In the event that KamaGames undergoes a business transition, such as a merger, acquisition by another company, or sale of all or a portion of its assets, we may transfer all of your information, including personal information, to the successor organization in such transition.",


"policy-children-title" =>  "Children",
"policy-children-1" =>  "KamaGames does not sell products for purchase by children and does not knowingly collect personal information from children. We sell children’s products for purchase by adults. If you are under the age of 13, you may use our Sites and Services only with the involvement of a parent or guardian.",


"policy-protect-title" =>  "How Does KamaGames Protect Your Information?",
"policy-protect-1" =>  "KamaGames understands the importance of keeping your information safe and secure. KamaGames will make commercially reasonable precautions to protect your personal information and ensure the security of our systems. However, there is no 100% secure method of transmission over the Internet or method of electronic storage. We cannot assume responsibility or liability for unauthorized access to our servers and systems. It is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off when finished using a shared computer. We strongly recommend that you implement measures to protect your computer from unauthorized access such as firewall protection and anti-virus software.",


"policy-access-title" =>  "How You Can Access and Update Your Information",
"policy-access-1" =>  "8.1. You can correct or update your account information at any time by logging on our sites and navigating to different account settings. Should you be unable to log in or wish to have your account(s) deactivated, you may send an e-mail to our Support Team at support@kamagames.com. You can put «Delete my Account» or «Update my information» in the subject line and include your first name, last name, e-mail address and user ID number (if applicable) in the body of the e-mail. We will be happy to review, update or remove information as appropriate. We may still retain your information in our files however, to resolve disputes, enforce our user agreement, and due to technical and legal requirements and constraints related to the security, integrity and operation of our websites.",
"policy-access-2" =>  "8.2. If you have granted KamaGames access to your SNS account information through a KamaGames’ application, you may manage the information KamaGames receives about you from an SNS. For this purpose you will need to follow the instructions at that site for updating your information and changing your privacy settings.",
"policy-access-3" =>  "8.3. You also may have the right (subject to certain exceptions to be given a copy of certain data that KamaGames may hold about you (and for which we may charge a small fee to process) and to have any inaccuracies in the data that KamaGames holds about you corrected and/or erased. You may exercise any such rights by emailing our Support Team at support@kamagames.com. We will need sufficient information from you to establish you identity and to verify your access request, and also to assist us in handling your request. Requests will be processed as soon as possible and not later than 40 days.",


"policy-terms-title" =>  "Terms of Service and Notices",
"policy-terms-1" =>  "If you choose to visit a Site, your visit and any dispute over privacy is subject to this Policy and our Terms of Service. If you have any concerns about your privacy, please contact us with a thorough description or your question or concern via the postal addresses or email address in the Section 10 of this Privacy Policy, and we will try to resolve it.",


"policy-changes-title" =>  "Changes to Our Privacy Policy",
"policy-changes-1" =>  "If we decide to make material changes to our Privacy Policy, we will notify you and other users by placing a notice on kamagames.com or by sending you a notice to the e-mail address we have on file for you. We may supplement this process by placing notices in our games and on other KamaGames websites. You should periodically check www.kamagames.com and this privacy policy page for updates.",


"policy-contact-title" =>  "Contact Us",
"policy-contact-1" =>  "If you have any questions about the privacy aspects of our products or services, or would like to make a complaint having to do with a privacy matter, please send a written request as follows:",
"policy-contact-2" =>  "KamaGames<br />Attention: Data Privacy<br />110 Amiens street, Dublin 1, Ireland.",
"policy-contact-3" =>  "Support team: support@kamagames.com",


];
