﻿<?php

return [
	"play-now-btn"                 => "지금 플레이",

	"header-text-2"                => "Free to play on your mobile device or social network",

	"section-1-title"              => "Manchester United Social Poker – <br />Powered by KamaGames<br />지금 소셜 포커 게임을 시작하세요!",
	"section-1-text"               => "Manchester United Social Poker – <br /> 지금 소셜 포커 게임을 시작하세요!",

	"section-2-title"              => "테이블을 선택하고<br />흥미진진한 대회에 참가할<br />준비를 하세요!",
	"section-2-text"               => "전 세계 팬들과 함께 <br />소셜 포커를 하며<br />당신만의 해트 트릭을 달성하세요!",

	"section-3-title"              => "특징",
	"section-3-text"               => "Manchester United Social Poker",
	"section-3-item-1"             => "완전 무료<br />플레이",
	"section-3-item-2"             => "하루 수천 명의<br />온라인 플레이어",
	"section-3-item-3"             => "일일 복권의<br />무료 칩",

	"section-3-list-item-1"        => "다른 플레이어와 채팅하고, 선물을 사고, 성과를 획득하세요",
	"section-3-list-item-2"        => "친구들도 게임에 초대해 보너스 칩을 획득하세요!",
	"section-3-list-item-3"        => "여러 장치에서 하나의 계정을 사용하세요",
	"section-3-list-item-4"        => "인앱 튜토리얼",

	"section-4-label"              => "온라인 플레이어:",

	"section-5-title"              => "소셜 룰렛",
	"section-5-title-2"            => "United 팬들과 Manchester United<br />Social Roulette을 플레이하세요",
	"section-5-text"               => "전 세계 수백만 명의 사람들을 카지노로 불러들이는<br />유명한 게임을 플레이하세요.",

	"form-title"                   => "지원",
	"form-text"                    => "질문과 연락처를 남겨주시면 저희 지원 팀에서 24시간 이내에 답변을 드립니다.",
	"form-success"                 => "메시지가 전송되었습니다. <br /> 저희 고객 지원 팀에서 최대한 빨리 답변을 드리겠습니다.",
	"form-error"                   => '<p class="form-message">필수 항목을 입력하세요</p>',
	"form-btn"                     => '메시지 전송',

	"form-input-name"              => "이름",
	"form-input-email"             => "이메일",
	"form-input-message"           => "메시지",

	"footer-title"                 => "Manchester United 포커 플레이",
	"footer-support"               => "지원",
	"footer-tos"                   => "서비스 약관",
	"footer-policy"                => "개인정보취급방침",
	"footer-rights"                => "모든 권리 보유.",

	"footer-subscribe-title"       => "소식지 구독:",
	"footer-subscribe-placeholder" => "이메일을 입력하세요",
	"footer-subscribe-button"      => "구독",

	"popup-text-1"                 => "지금 바로 플레이하거나",
	"popup-text-2"                 => "원하는 스토어에서 앱을 다운로드하세요",
	"popup-text-btn"               => "설치 및 플레이",

	"section-4-list-item-1"        => "세 가지 룰렛: 미국식, 프랑스식, 유럽식",
	"section-4-list-item-2"        => "3D 그래픽 및 게임플레이와 물리학의 실현 가능성",
	"section-4-list-item-3"        => "진짜 룰렛과 배당률과 동일한 당첨 가능성",
	"section-4-list-item-4"        => "멋진 소셜 기능: 룰렛 테이블 또는 비공개 채팅",
	"section-4-list-item-5"        => "iPhones, iPad, Android 심지어 Facebook에서 United 팬들과 플레이",

	"popup-text-7"                 => "모바일 장치(소셜 네트워크나 온라인)에서 무료 플레이",

	"popup-text-8"                 => "지금 포커 플레이",

	"popup-text-9"                 => "지금 룰렛 플레이",

	"casino-text-1"                => "Play Social Poker now",
	"casino-text-2"                => "Play Social Roulette now",

	"footer-legal"                 => "이 게임은 법정 연령에 해당되는 사람만 이용할 수 있습니다. 이 게임은 현금이나 금품을 얻을 수 있는 가능성을 제공하지 않습니다.<br />이 게임 플레이에서의 성공이 유사한 현금 이용 카지노 게임에서의 성공을 의미하지 않습니다.",

	"lang-title"                   => "언어 선택"

];

