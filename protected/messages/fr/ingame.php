<?php

return [
	"header-text-2"                => "Jeu gratuit sur plateforme mobile, réseau social ou en ligne",

//	"play-now-btn" 		=> "Jouez dès maintenant",
	"play-now-btn"                 => "Jouez",

	"section-1-title"              => "Manchester United Social Poker – <br />Powered by KamaGames<br />Éveillez le joueur de poker qui sommeille en vous !",
	"section-1-text"               => "Manchester United Social Poker – <br />Éveillez le joueur de poker qui sommeille en vous !",

	"section-2-title"              => "Choisissez une table<br />et préparez-vous<br />à disputer des compétitions fantastiques !",
	"section-2-text"               => "Jouez au poker avec des fans<br />du monde entier<br />et réalisez votre propre « coup du chapeau » !",

	"section-3-title"              => "Caractéristiques",
	"section-3-text"               => "Manchester United Social Poker",
	"section-3-item-1"             => "TOTALEMENT<br />GRATUIT",
	"section-3-item-2"             => "DES MILLIERS DE JOUEURS<br />EN LIGNE TOUS LES JOURS",
	"section-3-item-3"             => "JETONS GRATUITS<br />AUX LOTERIES JOURNALIÈRES",

	"section-3-list-item-1"        => "Discutez avec d'autres joueurs, achetez des cadeaux, remportez des trophées",
	"section-3-list-item-2"        => "Gagnez également des jetons bonus en invitant vos amis à jouer !",
	"section-3-list-item-3"        => "Utilisez un compte sur plusieurs appareils",
	"section-3-list-item-4"        => "Didacticiel intégré",

	"section-4-label"              => "Joueurs connectés :",

	"section-5-title"              => "Roulette",
	"section-5-title-2"            => "Jouez à Manchester United Social Roulette<br />avec d'autres fans de Manchester United",
	"section-5-text"               => "Jouez au jeu légendaire qui déchaîne les passions<br />dans les casinos du monde entier",

	"form-title"                   => "Assistance",
	"form-text"                    => "Veuillez saisir votre question ainsi que vos coordonnées et un représentant de l'assistance vous répondra d'ici 24 heures.",
	"form-success"                 => "Votre message a été envoyé.<br /> Un membre de l'assistance client vous répondra dans les meilleurs délais.",
	"form-error"                   => '<p class="form-message">Please fill the required field</p>',
	"form-btn"                     => 'Send message',

	"form-input-name"              => "Nom",
	"form-input-email"             => "E-mail",
	"form-input-message"           => "Message",

	"footer-support"               => "Assistance",
	"footer-tos"                   => "Modalités du service",
	"footer-policy"                => "Politique de confidentialité",
	"footer-rights"                => "Tous droits réservés",

	"footer-subscribe-title"       => "S'inscrire à nos Newsletters :",
	"footer-subscribe-placeholder" => "Veuillez saisir votre adresse e-mail",
	"footer-subscribe-button"      => "Abonnement",

	"footer-legal"                 => "Ce jeu est disponible uniquement aux personnes d'âge légal. Le jeu ne propose aucune possibilité de gagner de l'argent ou quoi que ce soit de valeur.<br />Le succès dans ce jeu ne peut impliquer un succès dans un jeu similaire de casino avec de l'argent réel.",

	"popup-text-1"                 => "Jouez dès maintenant",
	"popup-text-2"                 => "ou téléchargez l'application sur votre store préféré",
	"popup-text-btn"               => "Installer et jouer",

	"section-4-list-item-1"        => "Trois variantes de la roulette : américaine, française et européenne",
	"section-4-list-item-2"        => "Graphismes 3D, jouabilité captivante et lois de la physique réalistes",
	"section-4-list-item-3"        => "La probabilité de gagner est la même qu'à la vraie roulette",
	"section-4-list-item-4"        => "Fonctions sociales extraordinaires : discutez à la table de la roulette ou en privé",
	"section-4-list-item-5"        => "Jouez avec les fans de Manchester United sur iPhone iPad, Android et même Facebook",

	"casino-text-1"                => "Jouez au poker maintenant",
	"casino-text-2"                => "Jouez à la roulette maintenant",

	"lang-title"                   => "Choisir langue"
];
