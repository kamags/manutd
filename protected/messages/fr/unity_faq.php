<?php

return [
    
    # Common questions 
    'cq_text' => 'Questions fréquentes',
            
    'cq_question1' => 'À quel type de Poker puis-je jouer avec l’application ?',
    'cq_answer1' => 'Avec notre application mobile et sociale Manchester United Poker, vous pouvez jouer à un des types de Poker les plus populaires : le Texas Hold’em classique. Vous avez la possibilité de rejoindre une table pour gagner des jetons ou de vous lancer dans des tournois.',
    
    'cq_question2' => 'Sur quelles plateformes l’application Manchester United Poker est-elle disponible ?',
    'cq_answer2' => 'Manchester United Poker est disponible sur iOS, Android, Windows Phone et Facebook.',
    
    'cq_question3' => 'Comment jouer au Texas Hold’em ?',
    'cq_answer3' => 'Pour apprendre les règles et pour une description de l’interface de jeu, vous pouvez vous rendre dans la section « Aide » du menu principal. Nous vous expliquerons comment fonctionnent les mains de Poker, les mises et les actions. Note: vous n’y trouverez que les informations basiques pour jouer au Poker, pas pour gagner. Pour réussir, il vous faudra apprendre les stratégies et le comportement des joueurs, et améliorer votre jeu. Vous ne pouvez imaginer à quel point une partie réfléchie contre un adversaire expérimenté peut être prenante!',
    
    'cq_question4' => 'Qu’est-ce que le mode multi-écrans ?',
    'cq_answer4' => 'Le multi-écrans est un mode qui vous permet de gérer quatre écrans de jeu simultanément. Avec le mode multi-écrans, vous pouvez en même temps jouer dans une salle standard, participer à un tournoi et utiliser la messagerie personnelle. Ce mode est disponible pour les joueurs de niveau 15 et plus. En mode multi-écrans, le menu principal, la messagerie, le lobby et les tournois sont disponibles automatiquement. Vous pouvez les désactiver dans les paramètres du jeu.',
    
    'cq_question5' => 'Comment utiliser le mode multi-écrans ?',
    'cq_answer5' => 'Vous pouvez passer en mode multi-écrans en tapotant sur l’icône en haut à droite. Cette icône indique aussi le secteur actif. Pour vous rendre dans un menu en particulier, tapotez le secteur correspondant. Sur les appareils mobiles, vous pouvez faire glisser votre doigt pour changer d’écran.',
    
    'cq_question6' => 'Où puis trouver l’historique de jeu ?',
    'cq_answer6' => 'Vous pouvez trouver l’historique sous l’onglet Histoire de la Messagerie Personnelle. Tous vos accomplissements et toutes vos actions sont répertoriés par ordre chronologique, en commençant par les plus récents.',
    
    'cq_question7' => 'Qu’est-ce qu’un Lobby ?',
    'cq_answer7' => 'Le Lobby est un mode qui vous permet de regarder les parties en cours, de voir leurs paramètres (vitesse de la partie, taille de la blind, nombre de places, etc.) et de rejoindre celle que vous voulez. Les joueurs avancés ont pour habitude de bien étudier le lobby pour sélectionner leur partie en fonction de la banque moyenne ou pour observer le comportement des joueurs afin d’en tirer profit une fois en jeu. Choisir une table dans le lobby est très important pour se retrouver dans la partie qui vous correspond et optimiser vos profits.',
    
    'cq_question8' => 'Comment puis-je choisir une table dans le lobby ?',
    'cq_answer8' => 'Tapotez sur « lobby » dans le menu principal. Ce mode vous permet de choisir une table qui vous plait et d’en regarder la partie – comme si vous êtiez en spectateur à côté d’une table, dans un vrai casino. Choisissez le nombre de places et la vitesse de la partie, et réglez l’affichage des tables pleines. Le niveau des blinds est dans ce cas déterminé par le nombre de jetons dans votre cagnotte. Certains joueurs peuvent trier les tables en fonction des blinds. Naviguez entre les tables, regardez les parties en cours et les utilisateurs jouer. Une fois que vous aurez trouvé une table qui vous plait, vous pouvez prendre un siège libre et commencer à miser.',  
    
    'cq_question9' => 'Est-il possible de jouer sur plusieurs tables en même temps ?',
    'cq_answer9' => 'Vous ne pouvez jouer que sur une table à la fois.',
    
    'cq_question10' => 'Comment puis-je relancer la mise ?',
    'cq_answer10' => 'Pour relancer la mise d’un montant particulier, tapotez le bouton « Relancer » et maintenez. Déplacez le curseur qui apparait jusqu’à la mise voulue et relâchez. Il peut arriver qu’on vous demande de confirmer votre mise en tapotant à nouveau.',
    
    'cq_question11' => 'Comment puis-je participer à un tournoi ?',
    'cq_answer11' => 'Choisissez « tournois » dans le menu principal. Il vous faudra ensuite accepter les termes proposés pour commencer à jouer. Notez que vous ne pouvez participer qu’à un tournoi à la fois.',
    
    'cq_question12' => 'Pourquoi me suis-je fait « éjecter » de la table et pourquoi ne peux-jr plus la rejoindre ?',
    'cq_answer12' => 'Cela peut être dû à une connexion internet instable. Quand vous jouez, assurez-vous de la stabilité de votre connexion.',
    
    'cq_question13' => 'Que faire quand je pense que l’autre joueur a injustement gagné ?',
    'cq_answer13' => 'Une main forte n’assure pas pour autant la victoire. Il est très probable que le joueur avec la combinaison la plus faible n’ait pas gagné mais qu’il ait juste récupéré une partie de la mise qu’il a apportée au pot et qui excédait la vôtre, dans le cas d’un tapis. Ou vos mains étaient égales mais la carte kicker de votre adversaire était plus forte. Renseignez-vous sur les règles du poker pour que le but du jeu devienne limpide.',
    
    'cq_question14' => 'Qu’est-ce que l’étoile jaune à côté du pseudonyme d’un joueur signifie ?',
    'cq_answer14' => 'Une étoile à côté du pseudonyme d’un joueur signifie qu’il est dans votre liste d’amis.',
    
    'cq_question15' => 'Comment puis-je prendre une capture d’écran et l’envoyer à un ami ?',
    'cq_answer15' => 'Pour enregistrer et partager des moments de jeu extraordinaires, vous pouvez tapoter l’icône photo et choisir une des options suivantes : enregistrer sur votre appareil, envoyer par e-mail ou poster sur le mur Facebook. Si l’envoi par e-mail n’est pas dans la liste, vérifiez qu’il soit bien configuré sur votre appareil.',
    
    
    # Creating a game profile
    'cgp_text' => 'Créer un profil de jeu',
            
    'cgp_question1' => 'Qu’est-ce qu’un compte invité ?',
    'cgp_answer1' => 'Vous n’avez pas besoin de vous enregistrer pour jouer. Vous pouvez utiliser un compte invité qui sera lié à votre appareil. Si vous voulez continuer à jouer et garder vos accomplissements et votre score, il vous faudra continuer à utiliser cet appareil. Vous ne pourrez pas récupérer votre compte invité si votre appareil est perdu ou volé.',
    
    'cgp_question2' => 'Pourquoi dois-je m’enregistrer ?',
    'cgp_answer2' => 'Vous enregistrer vous permet de créer un profil de jeu. Vos accomplissements, vos jetons, vos pièces et vos biens seront enregistrés sur ce profil. Vous pourrez également ajouter des amis et utiliser ce profil pour jouer depuis n’importe quel appareil iOS ou Android et depuis les réseaux sociaux.',
    
    'cgp_question3' => 'Comment faire pour s’inscrire au jeu ?',
    'cgp_answer3' => 'Il vous faudra remplir le formulaire d’enregistrement situé dans le menu du jeu. Vous devrez fournir une adresse mail, un pseudonyme et un mot de passe. Vous pouvez aussi fournir une photo de profil. Nous vous suggérons d’utiliser une adresse mail valide pour pouvoir récupérer votre mot de passe si nécessaire. Si vous avez un profil sur un réseau social, il peut être utilisé comme profil de jeu.',
    
    'cgp_question4' => 'Je possède un profil Manchester United Roulette, puis-je l’utiliser pour jouer sur Manchester United Poker?',
    'cgp_answer4' => 'Oui, vous pouvez. Manchester United Poker et Manchester United Roulette ont un système de comptes liés. Veuillez noter que si vous avez utilisé une certaine adresse mail pour créer un compte Manchester United Roulette, vous ne pourrez pas l’utiliser pour créer un compte Poker. Vous avez juste à vous connecter à Manchester United Poker en utilisant le même mot de passe.',
    
    'cgp_question5' => 'Comment puis-je connaitre mon ID ?',
    'cgp_answer5' => 'Votre ID est composée de 9 à 10 chiffres et est indiqué dans votre profil dans l’application ou sur le site internet. Merci de bien préciser votre ID quand vous contactez le support.',
    
    'cgp_question6' => 'Puis-je changer mon nom d’utilisateur ?',
    'cgp_answer6' => 'Malheureusement le seul moyen de changer de nom d’utilisateur est de créer un nouveau compte.',
    
    'cgp_question7' => 'Comment puis-je récupérer mon mot de passe ?',
    'cgp_answer7' => 'Pour récupérer votre mot de passe, tapotez le lien « mot de passe oublié ? »et remplissez le formulaire. C’est la raison pour laquelle nous suggérons de fournir une adresse mail valide à la création du compte. Dans le cas où vous auriez fourni une adresse mail non valide ou que l’adresse indiquée n’est plus accessible, merci d’utiliser le formulaire de contact pour recevoir de plus amples instructions de la part de notre service technique.',
    
    'cgp_question8' => 'Puis-je supprimer mon compte ?',
    'cgp_answer8' => 'Un compte ne peut être supprimé. Si vous le désirez, vous pouvez créer un nouveau compte. Veuillez noter que vous pouvez utiliser jusqu’à 20 comptes par appareil (compte invité compris).',
    
    'cgp_question9' => 'Comment puis-je paramétrer mon profil de jeu ?',
    'cgp_answer9' => 'Pour paramétrer votre profil de jeu, tapotez l’encart du menu principal contenant votre photo de profil, votre nom et vos liquidités.',
    
    'cgp_question10' => 'Comment changer de pseudonyme ?',
    'cgp_answer10' => 'Pour changer votre pseudonyme, tapotez votre pseudonyme(ou le stylo juste à côté) dans les paramètres de votre profil, entrez-en un nouveau, puis tapotez «OK».',
    
    'cgp_question11' => 'Comment puis-je télécharger et changer ma photo de profil ?',
    'cgp_answer11' => 'Tapotez votre photo de profil actuelle dans les paramètres du profil. Sélectionnez « nouvelle » dans le menu déroulant, puis prenez une photo, choisissez en une dans la galerie de votre appareil ou choisissez une autre photo existante. Si vous vous connectez pour la première fois en utilisant votre compte Facebook, votre avatar Facebook sera utilisé.',
    
    'cgp_question12' => 'Comment puis-je changer mon statut ?',
    'cgp_answer12' => 'Allez dans l’écran « profil » depuis le menu principal puis tapotez la ligne à côté de votre avatar et entrez un nouveau statut. Laissez le champ vide si vous ne voulez pas de statut.',
    
    'cgp_question13' => 'Comment puis-je indiquer ma région ?',
    'cgp_answer13' => '« Région » renseigne sur l’endrois où vous êtes actuellement. Pour mettre à jour votre région, vous pouvez tapoter l’icône correspondante dans votre profil de jeu. Pour déterminer votre région dans le profil du site internet, tapotez « Détecter », à côté de « Région » à droite de la photo du profil.',
    
    'cgp_question14' => 'Pourquoi ma région n’est-elle pas détectée ?',
    'cgp_answer14' => 'Veuillez-vous assurer que les paramètres de localisation de votre appareil sont bien activés et que l’application y a bien accès. Si la région n’est toujours pas détectée, essayez de réinstaller l’application et d’autoriser la détection de la région au premier démarrage.',
    
    'cgp_question15' => 'Comment puis-je acheter une possession ?', 
    'cgp_answer15' => 'Vous pouvez acquérir une possession avec des jetons ou des pièces. Une possession ne change rien à la manière de jouer, mais peut indiquer votre statut en jeu. Pour acheter un bien, connectez-vous à votre compte, tapotez « Possession » et utilisez le menu d’achat. Vous pouvez donner une possession à vos amis ou aux utilisateurs que vous appréciez. Pour vendre une possession, utilisez l’option correspondante dans le menu de votre profil. Veuillez tout de même noter que vous ne récupérerez que 10% de la valeur que vous avez payée. Si vous revendez une possession donnée par un autre joueur, vous ne recevez rien, elle sera juste retirée de votre profil.',  
    
    'cgp_question16' => 'Comment puis-je acheter des cadeaux ?',
    'cgp_answer16' => 'Quand vous êtes assis à une table, tapotez le verre à côté de votre photo et choisissez un cadeau. Il sera affiché juste à côté de l’avatar du joueur. Unepossessionest affichée dans l’onglet dédié du profild’utilisateur. À noter que les cadeaux sont temporaires alors que les possessions sont permanentes. Les cadeaux ne durent que le temps de la partie, ou plusieurs jours s’ils ont été achetés avec de l\'or.',
    
    'cgp_question17' => 'Comment puis-je offrir des cadeaux aux autres joueurs ?',
    'cgp_answer17' => 'Tapotez le verre ou la maison situé à côté de la photo de profil du joueur. Choisissez le cadeau et confirmez l’achat. Pour offrir un cadeau à tous les joueurs d’une table, choisissez « Acheter pour tous » dans le menu d’achat.',
    
    'cgp_question18' => 'Que puis-je faire si je n’aime un cadeau qui m’a été envoyé ?',
    'cgp_answer18' => 'Dans le cas où vous n’aimeriez pas un cadeau que vous avez acheté ou qu’on vous a offert, vous pouvez l’échanger contre un nouveau. Tapotez le cadeau en question (situé là où se trouve en principe le verre), puis choisissez-en un nouveau. Il sera alors affiché à côté de votre avatar. Vous pouvez échanger le cadeau d’un autre joueur de la même manière. Qui plus est, les cadeaux disparaissent quand vous quittez l’application.',
    
    'cgp_question19' => 'Pourquoi ne puis-je acheter de l’alcool et du tabac en cadeau ? ',
    'cgp_answer19' => 'Conformément aux règles de Facebook, les utilisateurs n’ont pas le droit d’acheter ou de recevoir d’alcool ou de tabac quand ils jouent sur ce réseau social ou quand ils utilisent leur compte Facebook.',
    
    'cgp_question20' => 'Comment puis-je obtenir des accomplissements ?',
    'cgp_answer20' => 'Les accomplissements sont un indicateur de votre progression en jeu. Pour en voir la liste, rendez-vous sur votre profil et tapotez « Accomplissements ». Tapotez un accomplissement pour savoir comment vous l’avez obtenu. Vous pouvez aussi visionner vos accomplissements sur le profil du site internet.',
    
    'cgp_question21' => 'Comment puis-je me déconnecter ?',
    'cgp_answer21' => 'Sur les applications mobiles, certaines icones sont cachées dans un menu glissant. Vous pouvez le voir aux indicateurs situés en bas à droite du menu principal. Ce menu vous permet d’aller à Tournois, Inviter, Boutique, Tutoriel et de vous déconnecter de votre profil.',
      
    
    #Game settings
    'gs_text' => 'Paramètres du jeu',
            
    'gs_question1' => 'Comment puis-je accéder aux paramètres du jeu ?',
    'gs_answer1' => 'Pour accéder aux paramètres du jeu, tapotez l’engrenage en haut à droite du menu principal.',
    
    'gs_question2' => 'Comment puis-je changer la langue du jeu ?',
    'gs_answer2' => 'La langue du jeu est par défaut celle configurée sur votre appareil. Cependant, vous pouvez mettre le jeu en anglais sur toutes les versions. Pour ce faire, modifiez l’option « Langue » dans les paramètres du jeu.',
    
    'gs_question3' => 'Comment puis-je éteindre le son ?',
    'gs_answer3' => 'Pour éteindre/allumer le son ou pour gérer le volume, utilisez le curseur correspondant dans l’écran des paramètres du jeu.',
    
    'gs_question4' => 'Comment puis-je désactiver les vibrations ?',
    'gs_answer4' => 'Les vibrations servent à vous rappeler de miser. Vous pouvez les désactiver dans les paramètres (disponible que sur smartphones).',
    
    'gs_question5' => 'Comment puis-je désactiver la messagerie ?',
    'gs_answer5' => 'Vous pouvez non seulement jouer, mais aussi discuter à une table. Les messages des joueurs apparaissent dans une fenêtre contextuelle au-dessus de leur photo de profil. Si cela vous gêne, vous pouvez désactiver la messagerie grâce à un menu spécial qui apparait quand vous tapotez le cadenas situé au-dessus du bouton messagerie. Choisissez quels joueurs en particulier vous voulez ignorer. Vous pouvez bien sûr désactiver les messages de tous les joueurs. Pour cela, activez le blocage messagerie dans les paramètres.',
    
    'gs_question6' => 'Comment puis-je désactiver les statuts ?',
    'gs_answer6' => 'Si les statuts des joueurs vous gênent, vous pouvez les désactiver dans les paramètres du jeu.',
    
    'gs_question7' => 'Comment puis-je activer la surbrillance des cartes ?',
    'gs_answer7' => 'Dans les paramètres vous avez la possibilité d’activer la surbrillance des cartes pour mettre en valeur les combinaisons gagnantes de votre main et sur la table. Cela simplifie le jeu mais aussi affecte son intérêt et son piquant. ',
    
    'gs_question8' => 'Comment puis-je désactiver les notifications ?',
    'gs_answer8' => 'Vous pouvez désactiver les notifications dans les paramètres de votre appareil. Pour cela, rendez-vous dans Paramètres / Notifications / Manchester United Poker.',
    
    'gs_question9' => 'Comment puis-je désactiver l’affichage des cadeaux ?',
    'gs_answer9' => 'Vous pouvez désactiver l’affichage des cadeaux sur la table si cela vous gêne. Pour ce faire, utilisez le curseur correspondant dans les paramètres du jeu. Notez que dans ce cas vous ne pourrez recevoir ou donner de cadeaux.',
    
    'gs_question10' => 'Comment puis-je désactiver la reconnaissance des messages vocaux ?',
    'gs_answer10' => 'Rendez-vous dans les paramètres du jeu puis sélectionnez l’onglet « Voix ». Vous pouvez activer/désactiver la reconnaissance des messages vocaux et choisir la langue des messages à reconnaitre.',
    
     # Financial issues   
    'fi_text' => 'Questions financières',
            
    'fi_question1' => 'Est-ce que je joue avec de l’argent réel ou des jetons virtuels ?',
    'fi_answer1' => 'Nous utilisons une monnaie virtuelle, votre compte n’est donc pas affecté. Vous pouvez utiliser de l\'argent réel pour acheter des jetons et des pièces dans notre Boutique.',
        
    'fi_question2' => 'Comment obtenir des jetons grauits ?',
    'fi_answer2' => 'Vous pouvez obtenir des jetons gratuits en :<br> - participant à la loterie journalière;<br> - invitant des amis à venir jouer;<br> - suivez-nous sur Facebook.<br>Qui plus est, vos amis peuvent vous offrir jusqu’à 100 000 jetons par jour en cadeau.',
        
    'fi_question3' => 'Que se passe-t-il si j’utilise tous mes jetons et que mon compte est vide ?',
    'fi_answer3' => 'Les cinq premières fois vous serez crédité de 500 jetons gratuitement et vous pourrez continuer à jouer.',
        
    'fi_question4' => 'Comment puis-je acheter des jetons ?',
    'fi_answer4' => '- Tapotez « Boutique » dans le menu principal et achetez le montant de jetons voulu par le biais d’iTunes ou de Google Play Store. Le paiement sera effectué avec la carte bancaire liée au compte. Vous pouvez aussi utiliser les cartes iTunes.<br>- Si vous jouez sur un réseau social, vous pouvez acheter des jetons grâce à la monnaie virtuelle propre au réseau social en question (par exemple les crédits Facebook).',
        
    'fi_question5' => 'Comment puis-je acheter des pièces ? À quoi servent-elles ?',
    'fi_answer5' => 'Vous aurez besoin de pièces pour acheter des cadeaux haut de gamme et des possessions. Les pièces peuvent être achetées dans la Boutique de la même manière que les jetons. Et de temps en temps vous pouvez en recevoir comme cadeau de la part des développeurs.',
        
    'fi_question6' => 'Puis-je annuler un achat ?',
    'fi_answer6' => 'Oui, vous pouvez, mais pour cela vous devrez en faire la demande à l’organisme de paiement que vous avez utilisé pour effectuer votre achat. Nous ne travaillons pas directement avec les organismes de paiement, donc ne pouvons pas annuler vos achats nous-même.',
        
    'fi_question7' => 'Comment les niveaux de bonus influencent-ils la quantité de jetons et de pièces achetées ?',
    'fi_answer7' => 'Les niveaux de bonus vous accordent un supplément appréciable lors de vos trois premiers achats de jetons ou de pièces. Par défaut, le premier niveau vous apportera 100% de jetons en plus. Le premier achat débloque le deuxième niveau de bonus qui vous donnera 200% de jetons gratuits lors du prochain achat de jetons ou de pièces. Le troisième niveau vous accordera300% de bonus au prochain achat de jetons ou de pièces.',
        
    'fi_question8' => 'Comment puis-je acheter des jetons supplémentaires pendant une partie ?',
    'fi_answer8' => 'Vous êtes assis à une table et vous n’avez plus de jetons ? Tapotez votre avatar puis tapotez le jeton dans votre fenêtre de profil. Entrez le montant désiré et retournez en jeu. Vous pouvez également vous « lever » ou vous « retirer » puis vous « assoir » à nouveau à la table. Le jeu vous proposera alors de prendre avec vous un nouveau montant de jetons.',
        
    'fi_question9' => 'Puis-je échanger des jetons contre des pièces et des pièces contre des jetons ?',
    'fi_answer9' => 'Les pièces ne peuvent pas être changées en jetons. Les jetons ne peuvent pas non plus être changés en pièces, vous ne pouvez acheter ces dernières qu’à la Boutique. Pour ce faire, tapotez la Boutique dans le menu principal.',
        
    'fi_question10' => 'Que puis-je faire pour récupérer des jetons « disparus » ?',
    'fi_answer10' => 'Si vous entrez en jeu etque vous remarquez qu’il vous manque des jetons et que votre niveau est inférieur à d’habitude, vérifiez si vous vous êtes connecté au bon compte. Il peut arriver que vous vous connectiez sur un compte invité ou autre. Si vous êtes sûr que ça n’est pas le cas, contactez notre service technique qui se fera un plaisir de vous aider.',
        
     'fi_question11' => 'Comment puis-je transférer des jetons sur un autre compte ?', 
     'fi_answer11' => 'Transférer des jetons d’un compte à un autre est interdit.',

    
    # Friends 
    'f_text' => 'Amis',
            
    'f_question1' => 'Comment puis-je utiliser l’onglet Amis du menu principal ?',
    'f_answer1' => 'Dans le menu principal, vous pouvez voir la liste de vos amis sous l’aperçu de votre profil. L’avatar du dessus est le profil d’un ami que vous avez choisi (au centre de l’écran vous pouvez voir les informations sur votre ami et les boutons d’action). En dessous vous pouvez voir une liste de deux lignes de miniatures des avatars du reste de vos amis. Pour voir l’inventaire complet de vos amis, vous pouvez faire défiler la liste des miniatures d’avatars. Tout en bas de l’écran, vous pouvez voir des avatars choisis au hasard parmi vos amis.',
        
    'f_question2' => 'Comment puis-je ajouter d’autres joueurs à ma liste d’amis ?',
    'f_answer2' => ' - Rendez-vous dans la section « Amis » du menu principal et allez dans l’onglet « Recherche ». Vous pouvez rechercher des utilisateurs grâce à leur pseudonyme, adresse mail ou ID, et tapoter « Ajouter en ami ».<br> - Quand vous jouez à une table, vous pouvez envoyer une demande d’ami aux utilisateurs qui jouent à la même table en ouvrant leur profil et en tapotant « Ajouter en ami ».',
        
    'f_question3' => 'Comment faire don de jetons à un ami ?',
    'f_answer3' => 'Allez dans la section « Amis » du menu principal et tapotez « Envoyer des jetons ». Vous pouvez envoyer jusqu’à 100 000 jetons par jour en cadeau.',
        
    'f_question4' => 'Comment payer des jetons pour un ami ?',
    'f_answer4' => 'Allez sur le profilde votre ami, tapotez « Envoyer des jetons » et choisissez le montant voulu avec le curseur, ce qui fera apparaitre un bouton « Acheter et envoyer des jetons ». Tapotez ce bouton pour être directement redirigé vers la Boutique. Les jetons que vous achèterez seront envoyés à votre ami.',
        
    'f_question5' => 'Combien de jetons puis-je acheter à des amis ?',
    'f_answer5' => 'Si vous donnez des jetons en cadeau à des amis, le montant est limité. Mais vous pouvez acheter autant de jeton que vous le désirez pour vos amis.',
        
    'f_question6' => 'Comment puis-je faire un cadeau à un ami ou lui envoyer une possession ?',
    'f_answer6' => 'Vous pouvez acheter des possessions et des cadeaux pour d’autres joueurs. Tapotez le verre ou la maison à côté de l’avatar de votre ami. Vous pouvez aussi choisir « Acheter pour tous » et offrir des cadeaux à tous les joueurs de la table. Veuillez noter que les cadeaux sont temporaires alors que les posessions sont permanentes. Certains cadeaux peuvent durer plusieurs jours quand ils ont été achetés avec de l\'or.',
        
    'f_question7' => 'Comment puis-je rejoindre la table à laquelle joue un ami ?',
    'f_answer7' => 'Allez dans la section « Amis » du menu principal et sélectionnez le profil de votre ami. Si il est en train de jouer, tapotez « Rejoindre cette table ». Si vous voulez inviter un ami à rejoindre votre table, tapotez un siège libre et sélectionnez votre ami dans la liste. Il recevra alors une invitation pour vous rejoindre.',
        
    'f_question8' => 'Comment puis-je retirer des utilisateurs de ma liste d’amis ?',
    'f_answer8' => 'Pour retirer un utilisateur de la liste d’amis, allez dans la section « Amis » du menu principal. Choisissez celui que vous souhaitez retirer et tapotez « Retirer de la liste d’ami ».',
        
    'f_question9' => 'Comment puis-je recevoir des jetons et autres bonus en invitant des amis ?',
    'f_answer9' => 'Tapotez « Amis » dans le menu principal, choisissez « Inviter » et envoyez à vos amis une invitation à vous rejoindre sur Manchester United Poker par SMS, e-mail ou Facebook. Si vos amis suivent le lien attaché, vous gagnez le bonus. Si vous jouez sur Facebook, tapotez « Jetons gratuits » et vous gagnerez des jetons pour chaque joueur que vous invitez.',
    
    
    # Communication with other players
    'cwop_text' => 'Communication entre joueurs',
            
    'cwop_question1' => 'À quoi servent les messages personnels ?',
    'cwop_answer1' => 'Les messages personnels sont les messages que vous envoyez à un joueur en particulier. Seul ce joueur peut les lire. L’historique des messages personnels vous permet de voir les messages que vous avez envoyéspendant la conversation en cours et les archives de tous les messages avec leur date et heure d’envoi.',
            
    'cwop_question2' => 'Comment puis-je accéder à la Messagerie Personnelle ?',
    'cwop_answer2' => 'Pour vous rendre dans la Messagerie Personnelle, tapotez l’enveloppe dans le menu principal. Vous pouvez aussi vous rendre sur le profil du joueur avec qui vous souhaitez discuter et tapoter l’enveloppe.',
            
    'cwop_question3' => 'Comment la messagerie personnelle fonctionne-t-elle ?',
    'cwop_answer3' => 'Sur la gauche vous pouvez voir la liste des amis avec lesquels vous avez déjà eu une conversation. Pour ajouter un nouvel utilisateur à cette liste, tapotez « Ajouter » (lestylo « avec un plus » en bas à gauche de l’écran). Vous pouvez aussi aller sur le profil de votre ami pour effectuer certaines actions comme, par exemple, lui envoyer des jetons ou rejoindre une partie.',
            
    'cwop_question4' => 'Comment puis-je envoyer un message ?',
    'cwop_answer4' => 'Choisissez un ami à qui vous souhaiteriez envoyer un message et tapotez le champ prévu pour écrire le texte. Entrez votre message puis tapotez « Envoyer ».',
            
    'cwop_question5' => 'Puis-je envoyer un message à un ami hors ligne ?',
    'cwop_answer5' => 'Vous pouvez un message à un ami hors ligne. Vous pouvez voir si vous avez reçu des messages quand vous étiez hors ligne aux chiffres les comptabilisant sur l’icône de la messagerie.',
            
    'cwop_question6' => 'Comment puis-je supprimer un message personnel ?',
    'cwop_answer6' => 'Tapotez et maintenez le message que vous souhaitez supprimer, puis choisissez « Supprimer » dans le menu qui apparait.',
            
    'cwop_question7' => 'Comment écrire un message en jeu ?',
    'cwop_answer7' => 'Quand vous jouez à une table, tapotez l’icône de la messagerie en bas à droite de l’écran. Entrez votre message et tapotez « Valider » puis« OK ».',
            
    'cwop_question8' => 'Comment puis-je utiliser les messages vocaux ?',
    'cwop_answer8' => 'Vous pouvez non seulement communiquer avec les autres joueurs par des messages écrits, mais aussi par des messages vocaux. Tapotez l’icône micro en bas à droite de l’écran et prononcez le message que vous souhaitez envoyer aux autres joueurs. Le système va reconnaitre ce que vous avez dit et le convertir en texte qui s’affichera sur l’écran des autres. Cette fonctionnalité a un temps d’utilisation limité qui peut être acheté en pièces à la Boutique.',
  
];
