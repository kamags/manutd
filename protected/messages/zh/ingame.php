﻿<?php

return [
	"play-now-btn" 		=> "立即遊玩",

	"header-text-2"     => "Free to play on your mobile device or social network",


	"section-1-title"   => "Manchester United Social Poker – <br />Powered by KamaGames<br />馬上開始你的社群撲克遊戲！",
	"section-1-text"   => "Manchester United Social Poker – <br />Powered by KamaGames 馬上開始你的社群撲克遊戲！",

	"section-2-title"	=> "選擇一張牌桌<br />準備好來場刺激的<br />競賽！",
	"section-2-text"	=> "完成你個人的帽子戲法，<br />與來自世界各地的<br />粉絲們一同遊玩社群撲克遊戲！",

	"section-3-title"	=> "遊戲特色",
	"section-3-text"	=> "Manchester United Social Poker",
	"section-3-item-1"	=> "完全<br />免費遊玩",
	"section-3-item-2"	=> "每天都有<br />數千名的在線玩家",
	"section-3-item-3"	=> "每日樂透中<br />的免費籌碼",

	"section-3-list-item-1"	=> "與其他玩家聊天、購買禮物、賺取成就",
	"section-3-list-item-2"	=> "透過邀請朋友一同遊玩賺取獎勵籌碼！",
	"section-3-list-item-3"	=> "一個帳戶即能通用不同裝置",
	"section-3-list-item-4"	=> "應用程式內教學",


	"section-4-label"	=> "在線玩家：",


	"section-5-title"	=> "輪盤",
	"section-5-title-2"	=> "與其他曼聯粉絲們<br />一同遊玩Manchester United Social Roulette",
	"section-5-text"	=> "遊玩吸引全球數百萬玩家<br />前往賭場的傳奇遊戲。",

	"form-title"		=> "支援",
	"form-text"		=> "請將您的問題與聯絡方式在此處留下，我們的支援團隊成員會在24小時內回答您的問題。",
	"form-success"		=> "您的訊息已送出。 <br /> 我們的客戶支援團隊成員將會盡快回覆。",
	"form-error"		=> '<p class="form-message">請填寫所有必須欄位</p>',
	"form-btn"		=> '發送',

	"form-input-name" => "名稱",
	"form-input-email" => "電子郵件",
	"form-input-message" => "訊息",


	"footer-title" => "遊玩 Manchester United Poker",
	"footer-support" => "支援",
	"footer-tos" => "服務條款",
	"footer-policy" => "隱私權政策",
	"footer-rights" => "版權所有。",

	"footer-subscribe-title" => "訂閱我們的時事通訊：",
	"footer-subscribe-placeholder" => "請輸入您的電子郵件",
	"footer-subscribe-button" => "訂閱",


	"popup-text-1" => "立即遊玩",
	"popup-text-2" => "或在您最愛的商店下載應用程式",
	"popup-text-btn" => "安裝並遊玩",

	"section-4-list-item-1"	=> "三種類的輪盤：美式、法式和歐式",
	"section-4-list-item-2"	=> "遊戲物理的3D圖像與現實性",
	"section-4-list-item-3"	=> "獲勝的機率與真實輪盤相同",
	"section-4-list-item-4"	=> "優秀的社群功能：在輪盤賭桌上或私下進行聊天",
	"section-4-list-item-5"	=> "在iPhones、iPad、Android甚或是Facebook上與曼聯粉絲們一同遊玩",


	"popup-text-7" => "在您的行動裝置、社群網路或線上免費遊玩",

	"popup-text-8" => "立即遊玩撲克",

	"popup-text-9" => "立即遊玩輪盤",

	"casino-text-1" => "Play Social Poker now",
	"casino-text-2" => "Play Social Roulette now",

	"footer-legal" => "這款遊戲僅適合達到法定年齡的人士。此遊戲不提供賺錢或任何有價值物的機會。<br />在這款遊戲中獲勝並不意味著您在類似的現金賭博遊戲中也會獲勝。",

	"lang-title" => "選擇語言",

	'Sign in to Manchester United Social Casino' => "登陆曼联官方社交游戏",
	"Log in with your Facebook profile" => "此种方式在您所在的地区或国家不能使用",
	"or in with your email" => "请使用KamaGames账号登陆",
	"Not a member yet?" => "",
	"Register here in seconds" => "还没有注册？点击下方链接注册账号。"

];

