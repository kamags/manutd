﻿<?php

return [
	"play-now-btn" 		=> "เล่นตอนนี้",

	"header-text-2"     => "Free to play on your mobile device or social network",

	"section-1-title"   => "Manchester United Social Poker – <br />Powered by KamaGames<br /> เริ่มเกมโปกเกอร์ของคุณตอนนี้เลย!",
	"section-1-text"   => "Manchester United Social Poker – <br />Powered by KamaGames เริ่มเกมโปกเกอร์ของคุณตอนนี้เลย!",

	"section-2-title"	=> "เลือกโต๊ะและเตรียมตัว<br />ให้พร้อมสำหรับการแข่งขัน<br />ที่ตื่นเต้น!",
	"section-2-text"	=> "เล่นให้ชนะสามเกมรวด, <br />ในเกมโปกเกอร์ของคุณ<br />กับแฟนเกมอื่นๆ จากทั่วโลก!",

	"section-3-title"	=> "คุณสมบัติที่โดดเด่น",
	"section-3-text"	=> "Manchester United Social Poker",
	"section-3-item-1"	=> "เล่น<br />ฟรีจริงๆ",
	"section-3-item-2"	=> "แต่ละวันมีผู้ร่วมเล่น<br />จำนวนหลายพันคน",
	"section-3-item-3"	=> "รับชิปฟรีใน<br />ลอตเตอรี่รายวัน",

	"section-3-list-item-1"	=> "แชทกับผู้เล่นอื่น, ซื้อของขวัญ, รับผลสำเร็จ",
	"section-3-list-item-2"	=> "รับชิปโบนัสโดยการชวนเพื่อนของคุณมาร่วมเล่นด้วย!",
	"section-3-list-item-3"	=> "ใช้เพียงบัญชีเดียวในทุกอุปกรณ์",
	"section-3-list-item-4"	=> "บทช่วยสอนในแอพพลิเคชั่น",


	"section-4-label"	=> "ผู้เล่นที่ออนไลน์:",


	"section-5-title"	=> "รูเล็ต",
	"section-5-title-2"	=> "เล่น Manchester United Social<br />Roulette กับแฟนแมนยูทั้งหลาย",
	"section-5-text"	=> "เล่นเกมคลาสสิกที่ผู้คนหลายล้านคน<br />นิยมเล่นในคาสิโนทั่วโลก",

	"form-title"		=> "ความช่วยเหลือ",
	"form-text"		=> "กรุณาใส่คำถามของคุณและรายละเอียดการติดต่อที่นี่และทีมทีมงานฝ่ายบริการของเราจะตอบคุณภายใน 24 ชั่วโมง",
	"form-success"		=> "ข้อความของคุณถูกส่งแล้ว<br /> ทีมฝ่ายบริการลูกค้าของเราจะตอบกลับทันทีที่ทำได้",
	"form-error"		=> '<p class="form-message">กรุณากรอกข้อมูลที่ต้องระบุทั้งหมด</p>',
	"form-btn"		=> 'ส่งข้อความ',

	"form-input-name" => "ชื่อ",
	"form-input-email" => "อีเมล",
	"form-input-message" => "ข้อความ",


	"footer-title" => "เล่น Manchester United Poker",
	"footer-support" => "ความช่วยเหลือ",
	"footer-tos" => "ข้อกำหนดการบริการ",
	"footer-policy" => "นโยบายความเป็นส่วนตัว",
	"footer-rights" => "สงวนลิขสิทธิ์",

	"footer-subscribe-title" => "สมัครสมาชิกจดหมายข่าวของเรา:",
	"footer-subscribe-placeholder" => "กรุณาใส่อีเมลของคุณ",
	"footer-subscribe-button" => "สมัครสมาชิก",


	"popup-text-1" => "เล่นตอนนี้",
	"popup-text-2" => "หรือดาวน์โหลดแอพพลิเคชั่นจากร้านโปรดของคุณ",
	"popup-text-btn" => "ติดตั้งและเล่น",


	"section-4-list-item-1"	=> "รูเล็ตสามแบบ: อเมริกัน ฝรั่งเศสและยุโรป",
	"section-4-list-item-2"	=> "กราฟิก 3 มิติและความเป็นไปได้ของการเล่นเกมและกฏทางฟิสิกส์",
	"section-4-list-item-3"	=> "ความเป็นไปได้ในการชนะที่เหมือนกับความเป็นไปได้ในการเล่นรูเล็ตจริง",
	"section-4-list-item-4"	=> "คุณสมบัติที่โดดเด่นด้านสังคม: การแชทในโต๊ะรูเล็ตหรือส่วนตัว",
	"section-4-list-item-5"	=> "เล่นกับแฟนแมนยูทั้งหลายบนไอโฟน ไอเพด แอนดรอยด์และแม้กระทั่งเฟซบุค",


	"popup-text-7" => "เล่นฟรีในมือถือของคุณ สื่อสังคมหรือออนไลน์",

	"popup-text-8" => "เล่นโป๊กเกอร์ตอนนี้",

	"popup-text-9" => "เล่นรูเล็ตตอนนี้",

	"casino-text-1" => "Play Social Poker now",
	"casino-text-2" => "Play Social Roulette now",

	"footer-legal" => "เกมนี้จะใช้ได้เฉพาะกับผู้ที่บรรลุนิติภาวะเท่านั้น ตัวเกมมิได้นำเสนอความเป็นไปได้ที่จะชนะเดิมพันเป็นตัวเงินหรือสิ่งมีค่าอื่นใด<br /> การประสบความสำเร็จในการเล่นเกมนี้มิได้หมายความว่าท่านจะประสบความสำเร็จในแบบเดียวกันกับเกมคาสิโนที่เล่นด้วยเงินจริง",

	"lang-title" => "เลือกภาษา"


];
