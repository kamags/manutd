<?php

return [
	"header-text-2"                => "Free to play on your mobile device or social network",

	"play-now-btn"                 => "Play now",

	"section-1-title"              => "Manchester United Social Poker – <br />Powered by KamaGames<br />Kick off your Social Poker game now!",
	"section-1-text"               => "Play Social Poker with Man United fans and immerse yourself<br />in the world of gambling, bets and wins!",

	"section-2-title"              => "Choose a table and get ready for<br />some exciting competition!",
	"section-2-text"               => "Achieve your own hat-trick, playing Social Poker<br />with other fans from around the world!",

	"section-3-title"              => "Feature",
	"section-3-text"               => "Manchester United Social Poker",
	"section-3-item-1"             => "ABSOLUTELY<br />FREE TO PLAY",
	"section-3-item-2"             => "THOUSANDS OF<br />PLAYERS ONLINE DAILY",
	"section-3-item-3"             => "FREE CHIPS IN DAILY<br />LOTTERIES",

	"section-3-list-item-1"        => "Chat with other players, buy gifts, earn achievements",
	"section-3-list-item-2"        => "Earn bonus chips by inviting your friends to play too!",
	"section-3-list-item-3"        => "Use one account across multiple devices",
	"section-3-list-item-4"        => "In-App tutorial",

	"section-4-list-item-1"        => "Three kinds of Roulette: American, French and European",
	"section-4-list-item-2"        => "3D graphics and realistic gameplay and physics",
	"section-4-list-item-3"        => "The probability of winning is identical to the odds in real roulette",
	"section-4-list-item-4"        => "Great social features: chat at the roulette table or in private",
	"section-4-list-item-5"        => "Play with United fans on iPhones, iPad, Android and even Facebook!",

	"section-4-label"              => "Players online:",

	"section-5-title"              => "Social Roulette",
	"section-5-title-2"            => "Play Manchester United Social<br />Roulette with fellow United fans",
	"section-5-text"               => "Play the legendary game which attracts<br />millions of people to casinos around the world.",

	"form-title"                   => "Support",
	"form-text"                    => "Please leave your question and contact details here and our support team member will answer you within 24-hours.",
	"form-success"                 => "Your message has been sent. <br /> Our customer support team member will response as soon as possible.",
	"form-error"                   => '<p class="form-message">Please fill the required field</p>',
	"form-btn"                     => 'Send message',

	"form-input-name"              => "Name",
	"form-input-email"             => "Email",
	"form-input-message"           => "Message",

	"footer-support"               => "Support",
	"footer-tos"                   => "Terms of service",
	"footer-policy"                => "Privacy policy",
	"footer-rights"                => "All rights reserved.",

	"footer-subscribe-title"       => "Subscribe to our Newsletters:",
	"footer-subscribe-placeholder" => "Please enter your email",
	"footer-subscribe-button"      => "Subscribe",

	"footer-legal"                 => "This game is only available to people of legal age. The game offers no possibility of winning money or anything of value.<br />Success in playing this game does not imply your success in a similar real-money casino game.",

	"popup-text-1"                 => "Play right now",
	"popup-text-2"                 => "or download the app on your favorite store",
	"popup-text-btn"               => "Install & Play",

	"casino-text-1"                => "Play Social Poker now",
	"casino-text-2"                => "Play Social Roulette now",

	"lang-title"                   => "Choose language"

];
