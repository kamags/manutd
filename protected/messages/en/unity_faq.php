<?php

return [
    
    # Common questions 
    'cq_text' => 'Common questions',
            
    'cq_question1' => 'What kind of Poker can I play with the app?',
    'cq_answer1' => 'With our mobile and social app Manchester United Poker you can play one of the most popular types of Poker-classic unlimited Texas Hold’em. You can either play at the table for chips or join tournaments.',
    
    'cq_question2' => 'On what platforms is Manchester United Poker available?',
    'cq_answer2' => 'The Manchester United Poker app is available for iOS, Android, Windows Phone and Facebook.',
    
    'cq_question3' => 'How to play Texas Hold’em?',
    'cq_answer3' => 'To learn more about the rules and game interface please go to “Tutorial” in the main menu. We will tell you about Poker hands, bets and actions. Note: these are only basic skills of playing Poker that teach to play, not to win. To succeed you need to learn more about game strategy and players’ behavior as well as improve your gaming skills. You can’t even imagine how breathtaking a thoughtful Poker versus an advanced opponent can be!',
    
    'cq_question4' => 'What is a multiscreen mode?',
    'cq_answer4' => 'Multiscreen is a mode with four game screens available simultaneously. With multiscreen you can play in a standard mode, take part in a tournament and chat in the personal messages menu at the same time. This mode is available to all Level 15+ players. In multiscreen mode the main menu, chat menu, lobby and tournaments are available automatically. You can switch it off in the setting menu.',
    
    'cq_question5' => 'How to use multiscreen mode?',
    'cq_answer5' => 'You can switch to the multi-screen mode by tapping an icon in the upper right corner – this icon also displays the sector you are in at the moment. To go to particular menu just tap the corresponding sector. On mobile devices you can switch the screens with your finger.',
    
    'cq_question6' => 'Where can I find game history?',
    'cq_answer6' => 'The game history can be found on the “Event log” tab in the personal messages menu. All your achievements and actions are displayed in the chronological sequence, beginning from the latest ones.',
    
    'cq_question7' => 'What is a “Lobby”?',
    'cq_answer7' => 'Lobby is a game mode where you can watch currently active games and learn more about them (game speed, blinds’ size, number of seats at the table, etc.) and choose the table you’d like to join. Advanced players thoroughly study lobby, selecting the average bank and observing players’ behavior to get benefit in the game. Choosing a table in lobby is very important to choose the most appropriate game and increase your profit.',
    
    'cq_question8' => 'How can I choose a table at the lobby?',
    'cq_answer8' => 'Tap “Lobby” in the main menu. In this mode, you can choose a table you like and watch the game (imagine you are in a casino, standing behind a table and watching the game). Choose number of seats at the table, speed of the game, set the displaying of full tables. The blinds’ level in this case is determined by the amount of chips on your total bill (bankroll). Some players can sort out tables by blinds. Navigate through the tables, watch the games being played and users playing. After you have found the table you like, you can take an unavailable seat at this table and make your bet.',  
    
    'cq_question9' => 'Is it possible to play at multiple tables simultaneously?',
    'cq_answer9' => 'You can play only at one table.',
    
    'cq_question10' => 'How can I raise the bet?',
    'cq_answer10' => 'To raise the bet for an arbitrary amount tap “Raise” button and HOLD it. Pull the appeared slider up till the bet you need and leave it. Sometimes you can be asked to prove your bet with an additional tap.',
    
    'cq_question11' => 'How can I take part in tournaments?',
    'cq_answer11' => 'Choose “Tournaments” in the main menu. You’ll be offered terms to agree to start the game. Note that you can only take part in one tournament at a time.',
    
    'cq_question12' => 'Why am I being “kicked out” from the table and cannot join it?',
    'cq_answer12' => 'This may be caused by an unstable Internet connection. When playing, please make sure your connection is stable.',
    
    'cq_question13' => 'What if I think that the other player won unfairly? ',
    'cq_answer13' => 'Strong hand doesn’t mean a win yet. Most probably a player with a weaker combination didn’t win but just got his part of the bet that was bigger than yours, if you went all-in– placed all your money. Or your hands were equal with opponent’s kicker higher than yours. Learn more about Poker rules so that the game’s final becomes clearer for you.',
    
    'cq_question14' => 'What does a star next to Player\'s nickname mean?',
    'cq_answer14' => 'A star sign next to your fellow player\'s nickname means that this user is on your friend list.',
    
    'cq_question15' => 'How can I save a screen shot and share it with my friends?',
    'cq_answer15' => 'To save and share an extraordinary moment of the game please tap camera sign and choose one of the following options: save on your device, send via e-mail or post it on Facebook wall. If e-mail is not on the list check out whether e-mail is set on your device.',
    
    
    # Creating a game profile
    'cgp_text' => 'Creating a game profile',
            
    'cgp_question1' => 'What is a guest account?',
    'cgp_answer1' => 'Registration is not needed to start playing. You can play as guest, but in this case your account will be linked to a certain device. If you would like to continue playing and have all your achievements and score saved, you will have to use this device. You will not be able to restore your guest account, if your device gets lost or broken.',
    
    'cgp_question2' => 'Why do I need to register?',
    'cgp_answer2' => 'After getting registered you will be able to create your game profile. All your achievements, chips, coins and property will be stored in this profile. You will also be able to add friends and use this profile playing from all devices and social networks.',
    
    'cgp_question3' => 'How can I register in the game?',
    'cgp_answer3' => 'Please use the registration form provided in the game menu. You will have to enter your e-mail, password and nickname. If you wish, you can also upload your userpic. We suggest that you use a real email, in order we will be able to help you restore your password. If you have a social network profile, you can use it as a game profile.',
    
    'cgp_question4' => 'I have a Manchester United Roulette profile, can I use it to play Manchester United Poker?',
    'cgp_answer4' => 'Yes, you can. Manchester United Poker and Manchester United Roulette have an integrated account. Please note that if you already have Manchester United Roulette account registered with a particular e-mail, you won’t be able to create a new account with it. In this case just log in to Manchester United Poker with the same password.',
    
    'cgp_question5' => 'How do I know my ID?',
    'cgp_answer5' => 'Your ID is of 9 or 10 digit number, indicated in your profile in the application or on the website. Please remember to provide your ID when contacting technical support.',
    
    'cgp_question6' => 'Can I change my login?',
    'cgp_answer6' => 'Unfortunately, you cannot change your login. But you can register a new account and choose any other username.',
    
    'cgp_question7' => 'How can I restore my password?',
    'cgp_answer7' => 'To restore your password please tap "Forgot your password?" link and enter your data in the form provided. We suggest that you use a real email. In case you have used non-existent email address or the address you have indicated is not available anymore, please use our contact form to receive any further instructions from our technical support.',
    
    'cgp_question8' => 'Can I delete my account?',
    'cgp_answer8' => 'An account cannot be deleted. If you wish, you can create a new account. Please note that you can use up to 20 accounts on one device (including the guest account).',
    
    'cgp_question9' => 'How can I enter my game profile settings?',
    'cgp_answer9' => 'To enter game profile, please tap the block with your userpic, name and balance in the main menu.',
    
    'cgp_question10' => 'How can I change my nickname?',
    'cgp_answer10' => 'To change your nickname tap on your nickname in the profile settings (or pencil pic next to it) and enter a new one, than tap "Done".',
    
    'cgp_question11' => 'How can I upload or change my userpic?',
    'cgp_answer11' => 'Tap on your current userpic in the game profile settings. Select “New” in the drop-down menu, then take a picture or choose a photo from your device gallery or another existing photo. If you logged in for the first time using your Facebook account, your Facebook avatar will be used.',
    
    'cgp_question12' => 'How can I change my status?',
    'cgp_answer12' => 'Enter “Profile” in the main menu, then tap the line beyond your avatar and enter a new status. Leave this field empty, if you do not want anything to be displayed in the status field. To change your status in the profile, tap "Update status" next to the avatar and write new status. Then tap "Done".',
    
    'cgp_question13' => 'How can I determine my location?',
    'cgp_answer13' => '"Location" indicates the region you are currently in. To update your location please tap corresponding icon in the game profile. To determine your location on the website profile please tap “Determine” next to “Location” in your ID line on the right from the userpic.',
    
    'cgp_question14' => 'Why my location is not determined?',
    'cgp_answer14' => 'Please, make sure that the location settings on your device are activated and our application has access to it. If the location is not determined at specified settings, then try to reinstall the application and allow the determination of the location when you first start it.',
    
     'cgp_question15' => 'How can I buy property?', 
     'cgp_answer15' => 'You can buy property with chips or coins. Property does not change anything in the game process, but can indicate your game status. To buy property, please sign up to your account, tap “Property” and use purchase menu. You can give property to your friends or any user you like. To sell property please use a corresponding option in the game profile menu. Please take into account that you will get back only 10% of the price. When selling a property given to you by other players, you do not receive anything. Objects are just removed from your profile.',  
    
    'cgp_question16' => 'How can I buy gifts?',
    'cgp_answer16' => 'Sitting at the table please tap on the glass icon next to your userpic, then choose a gift. Gift will be displayed next to the player\'s avatar. Property will be displayed in the corresponding tap of the user\'s profile. Please note, that the gifts are temporary, unlike property that is permanent. The gifts last as long as you play at one table, or several days, if they were bought for gold.',
    
    'cgp_question17' => 'How can I give gifts to other players?',
    'cgp_answer17' => 'You can buy property and gifts for other players. Tap the “glass” icon next to your friend\'s avatar to send gift. To sent property you need to go to your friend\'s profile and tap on “house” icon. You can also choose “Buy for all” and give gifts to all users playing at the table.',
    
    'cgp_question18' => 'What can I do, if I do no not like the gift sent to me?',
    'cgp_answer18' => 'In case you do not like the gift you have bought or you have been given, you can substitute it with a new one. Tap on the current gift (the spot where you usually see the glass icon), then choose a new one. Then it will be displayed next to your avatar. In the exactly same way you can substitute other player’s gift. Besides, gifts disappear when you exit the app.',
    
    'cgp_question19' => 'Why can’t I buy alcohol and tobacco as gifts?',
    'cgp_answer19' => 'According to Facebook rules, users cannot buy or receive alcohol and tobacco while playing in this social network or being authorized with Facebook account.',
    
    'cgp_question20' => 'What can I get achievements for?',
    'cgp_answer20' => 'Achieves is an indicator of your gaming progress. To look through the list of achieves please go to the profile and tap “Achiev’s”. Tap the achievements to know what you received it for. You can also look through your achieves on the website profile.',
    
    'cgp_question21' => 'How can I sign out?',
    'cgp_answer21' => 'In the mobile apps some icons are hidden in the slighting panel – it is shown with page indicators in the lower right corner of the main menu. With the move of your finger you can go to Tournaments, Invite, Store, Tutorial and Signing out your profile.',
      
    
    #Game settings
    'gs_text' => 'Game settings',
            
    'gs_question1' => 'How can I enter game settings?',
    'gs_answer1' => 'To enter game settings you need to tap the gear icon in the upper right angle of the main menu.',
    
    'gs_question2' => 'How can I change game language?',
    'gs_answer2' => 'Automatically game language is the same as configured in your device. However, you can chose other language. Go to game settings, tap on “Localization” and chose any language from the list.',
    
    'gs_question3' => 'How can I turn the sound off?',
    'gs_answer3' => 'To turn in-game sound on/off or regulate volume, please use a corresponding slider in the game settings menu.',
    
    'gs_question4' => 'How can I turn the vibration off?',
    'gs_answer4' => 'Vibration reminds you of making a bet. You can turn it off in the setting menu (function available for smartphones only).',
    
    'gs_question5' => 'How can I block the chat?',
    'gs_answer5' => 'You can not only play but also chat at the table. Players’ messages appear in popups above their userpics. If it disturbs you can block the chat in a special menu tapping lock pictogram next to the “chat” button. Choose particular players whose messages you don’t want to see. You can also block chats of all players. For this enable chat block in game settings.',
    
    'gs_question6' => 'How can I disable statuses? ',
    'gs_answer6' => 'If players’ statuses disturb you can disable them in game settings.',
    
    'gs_question7' => 'How can I enable cards lightning?',
    'gs_answer7' => 'In game settings you can enable lightning of cards that set up a winning combination being in your hand and on the table. It simplifies the game but also affects its interest and suspense.',
    
    'gs_question8' => 'How can I disable push-notifications?',
    'gs_answer8' => 'You can switch off push notifications in the device settings. For this go to Settings/Push notifications/Manchester United Poker.',
    
    'gs_question9' => 'How can I disable displaying gifts?',
    'gs_answer9' => 'You can disable displaying gifts at the table, if it disturbs you. For this, use the corresponding slider in the setting menu. Note that in this case you won’t be able to give and get gifts.',
    
    'gs_question10' => 'How can I disable voice messages recognition?',
    'gs_answer10' => 'Enter game settings, then choose “Voice” tab. You can enable/disable voice messages recognition and choose a language of the messages to be recognized.',
    
     # Financial issues   
    'fi_text' => 'Financial issues',
            
    'fi_question1' => 'Do I play with real money or virtual chips?',
    'fi_answer1' => 'We use virtual currency, your account remains untouched. But you can use real money to buy chips and coins in our Store.',
        
    'fi_question2' => 'How can I get free chips?',
    'fi_answer2' => 'You can get free chips, if you:  participate in everyday lottery; invite your friends to play; follow us on Facebook. Besides, your friends can give you as gift up to 100 000 chips a day.',
        
    'fi_question3' => 'What if I played out all the chips and have a zero balance?',
    'fi_answer3' => 'For the first five times, your account will be refilled with 500 free chips and you’ll be able to continue the game.',
        
    'fi_question4' => 'How can I buy chips?',
    'fi_answer4' => 'Tap “Store” in the main menu and buy a necessary amount of chips via iTunes, Google Play Store and Windows Phone Store. The payment will be made with your credit card linked to account. You can also use iTunes gift card. If you play using your account in a social network, you can buy chips with virtual currency of a particular social network (e.g. Facebook credits).',
        
    'fi_question5' => 'How can I buy coins? What do I need them for?',
    'fi_answer5' => 'You need coins to buy exclusive gifts and property. The coins can be purchased in the Store as well as chips. Sometimes you can get them as a gift from game developers.',
        
    'fi_question6' => 'Can I cancel a purchase?',
    'fi_answer6' => 'Yes you can, but for that, you need to apply to that payment system which you use for making this purchase. We don`t work with payment systems directly, so we cannot cancel your purchases.',
        
    'fi_question7' => 'How does bonus level change amount of purchased chips and coins?',
    'fi_answer7' => 'Bonus level allows you to receive an appreciable raise when you buy chips or coins for first three times. By default, you receive the first level and get 100% bonus chips for free. After first purchase you will be switched to the second bonus level and receive 200% bonus chips for free when buying chips or coins next time. Finally, having reached the third bonus level you will receive 300% of chips or coins being purchased.',
        
    'fi_question8' => 'How can I buy additional chips for the game?',
    'fi_answer8' => 'You are sitting at a table, but do not have any chips? Tap your avatar and then tap the chip icon in the profile window. Please enter a necessary amount of chips and come back to play. You can also simply “Stand up” or “Sit out” and then “Sit” at the table again. In this case you will be offered to take with you a necessary amount of chips.',
        
    'fi_question9' => 'Can I exchange chips for coins and coins for chips?',
    'fi_answer9' => 'The coins cannot be exchanged for the chips. The chips also cannot be exchanged for the coins, you can buy the coins only in the Store. To buy them, tap “Store” in the main menu.',
        
    'fi_question10' => 'What can I do to restore “vanished” chips?',
    'fi_answer10' => 'If you enter the game and notice you have fewer chips and lower level, first, check whether you have logged in to the same account. You may have logged in as guest or using another account. If you are sure it is not related to any of the above, please contact our technical support, we will be glad to help you.',
        
     'fi_question11' => 'How can I transfer chips to another account?', 
     'fi_answer11' => 'Transferring chips from one user’s account to another is prohibited.',

    
    # Friends 
    'f_text' => 'Friends',
            
    'f_question1' => 'How do I use Friends tab in the main menu?',
    'f_answer1' => 'In the main menu you can see the list of your friends below your profile thumbnail. The upper avatar is a profile of a friend you have chosen (in the center of the screen you can see the friend info and action buttons). Lower you can see the two-row list of the rest of your friends’ avatars thumbnails. To see the whole list of the friends scroll the list with avatars thumbnails. In the very bottom of the screen you can see avatars of randomly chosen players. ',
        
    'f_question2' => 'How can I add other Players to “Friends”?',
    'f_answer2' => 'Enter "Friends" section in the main menu and go to “Search” tab. You can search for users by their nicknames, e-mail addresses or IDs and tap “Add to friends”.  Playing at the table you can send friend requests to users who play at the same table as you. For this open his profile and tap “Add to friends”.',
        
    'f_question3' => 'How can I give chips as a gift to my Friend?',
    'f_answer3' => 'Enter the “Friends” section in the main menu, choose a friend and tap “Send chips”. You can give as gift up to 100 000 chips a day.',
        
    'f_question4' => 'How can I purchase chips for my Friend?',
    'f_answer4' => 'Enter a friend’s profile , tap “Send chips” and slide the slider bar to a maximum value, then you will see a button "Buy and send chips". After tapping it you will be automatically redirected to the "Store" section. The chips you buy the will be sent to your friend.',
        
    'f_question5' => 'How many chips can I buy for my friends?',
    'f_answer5' => 'If you give chips as a gift to your friends, the amount is limited. But you can buy an unlimited amount of chips for your friends.',
        
    'f_question6' => 'How can I make a gift to my Friend or send him my property? ',
    'f_answer6' => 'You can buy property and gifts for other players. Tap the “glass” icon next to your friend\'s avatar to send gift. To sent property you need to go to your friend\'s profile and tap on “house” icon. You can also choose “Buy for all” and give gifts to all users playing at the table. Please note, that the gifts are temporary unlike property that is permanent. Some gifts can stay for several days, these are gifts bought for coins.',
        
    'f_question7' => 'How can I join the table my Friend is playing at?',
    'f_answer7' => 'Enter “Friends” section in the main menu and select a friend’s profile. If the user is playing, tap “View tables”. If you would like to invite a friend to a table you are currently playing at, tap on the empty chair and choose the friend from the list. Your friend will receive an invitation to play.',
        
    'f_question8' => 'How can I delete users from “Friends”?',
    'f_answer8' => 'To delete a user from the friends list, choose “Friends” in the main menu of the game. Choose a friend you would like to delete, tap “Delete from the friends list”.',
        
    'f_question9' => 'How can I get chips or other bonuses for inviting friends?',
    'f_answer9' => 'Tap “Friends” in the main menu and choose “Invite”. Then send your friends a request to join you in Manchester United Poker by SMS, e-mail or Facebook. If you friends follow the link attached you get the bonus. If you play on Facebook tap “Free chips” and get chips for every player you invite.',
    
    
    # Communication with other players
    'cwop_text' => 'Communication with other players',
            
    'cwop_question1' => 'What personal messages are?',
    'cwop_answer1' => 'Personal messages are messages you send to a certain player. Hence, only he/she can read them. In the personal messages history you can find not only the messages you have sent during a current conversation, but also the archive of messages with indication of dates and time.',
            
    'cwop_question2' => 'How do I enter the personal messages menu?',
    'cwop_answer2' => 'To enter personal messages menu, tap envelope icon “Messages” in the main menu. You can also choose a friend with whom you would like to start a conversation and tap envelope button in his/her profile.',
            
    'cwop_question3' => 'How does the personal messages menu work?',
    'cwop_answer3' => 'On the left you can see the list of the friends, you have already been in conversation with. To add a new user to this list tap “Add” (pencil-with-plus icon) located in the lower left angle of the screen. You can also go to your friend\'s profile and perform some actions, for example, send chips or join a game.',
            
    'cwop_question4' => 'How can I send a message?',
    'cwop_answer4' => 'Choose a friend you would like to send a message to, tap a special field to type some text. Type your message and tap “Send”.',
            
    'cwop_question5' => 'Can I send a message to a friend who is offline?',
    'cwop_answer5' => 'You can send a message to a friend who is offline too. The messages you have received being offline are displayed as numbers on the "messages" icon. You can also go to your friend\'s profile and perform some actions, for example, send chips or join a game.',
            
    'cwop_question6' => 'How do I delete personal messages?',
    'cwop_answer6' => 'Tap and hold the message you’d like to delete, than choose “Delete” in the appeared menu.',
            
    'cwop_question7' => 'How do I write something on chat?',
    'cwop_answer7' => 'Playing at the table open chat screen by tapping “Messages” icon in the right lower corner of the screen. Type your message and tap “Enter”, then “Done”.',
            
    'cwop_question8' => 'How do I use voice messages?',
    'cwop_answer8' => 'You can communicate with other players not only with text messages, but also with voice messages. Tap a microphone icon in the lower right corner of the screen and pronounce a message you would like to share with your opponents. The system recognizes your speech and converts it into text on the other players’ screens. The period you can use this function within is limited, it can be purchased with coins at our Store.',
  
];
