﻿<?php

return [

	"Sign in to Manchester United Social Casino" => "登陆曼联官方社交游戏",
	"Log in with your Facebook profile" => "此种方式在您所在的地区或国家不能使用",
	"or in with your email" => "请使用KamaGames账号登陆",
	"Not a member yet?" => "还没有注册？点击下方链接注册账号。",
	"Register here in seconds" => "",

	"Create account Manchester United Social Casino" => "建立曼联官方社交游戏账户",
	"Log in with your Facebook profile" => "此种方式在您所在的地区或国家不能使用。",
	"I subscribe to the newsletter and news" => "订阅KamaGames新闻",
	"I agree all" => "阅读并同意相关条款",
	"Already a member?" => "已经有注册？点击下方链接登入 ",
	"" => "",


];

