<?php

return [
	"play-now-btn" 		=> "立即游戏",

	"header-text-2"     => "Free to play on your mobile device or social network",

	"section-1-title"   => "Manchester United Social Poker – <br />Powered by KamaGames<br />立刻开始玩你的社交扑克游戏！",
	"section-1-text"   => "Manchester United Social Poker – <br />Powered by KamaGames。立刻开始玩你的社交扑克游戏！",

	"section-2-title"	=> "选择牌桌，准备好<br />参加一些激动人心的赛事！",
	"section-2-text"	=> "完成帽子戏法，<br />和世界各地的其他粉丝一起玩社交扑克游戏！",

	"section-3-title"	=> "特色",
	"section-3-text"	=> "Manchester United Social Poker",
	"section-3-item-1"	=> "完全免费",
	"section-3-item-2"	=> "每天有<br />数千名玩家在线",
	"section-3-item-3"	=> "每日乐透中<br />提供免费筹码",

	"section-3-list-item-1"	=> "和其他玩家交谈，购买礼物，获得成就",
	"section-3-list-item-2"	=> "邀请好友前来游戏，从而获得奖励筹码！",
	"section-3-list-item-3"	=> "在多个设备上使用一个帐户",
	"section-3-list-item-4"	=> "程序内教程",


	"section-4-label"	=> "在线玩家：",


	"section-5-title"	=> "社交轮盘赌",
	"section-5-title-2"	=> "和曼联球迷一起玩<br />Manchester United Social Roulette",
	"section-5-text"	=> "这款传奇游戏让全世界数百万人走进娱乐场<br />快来玩吧！",

	"form-title"		=> "支持",
	"form-text"		=> "请在此留下你的问题和联系方式，我们的支持团队成员将在 24 小时给你回复。",
	"form-success"		=> "信息已发送。<br />我们的客户支持团队将尽快回复。",
	"form-error"		=> '<p class="form-message">请填写所有必填字段</p>',
	"form-btn"		=> '发送信息',

	"form-input-name" => "名称",
	"form-input-email" => "电子邮件",
	"form-input-message" => "信息",


	"footer-title" => "玩 Manchester United Poker",
	"footer-support" => "支持",
	"footer-tos" => "服务条款",
	"footer-policy" => "隐私政策",
	"footer-rights" => "保留所有权利。",

	"footer-subscribe-title" => "订阅我们的新闻通讯：",
	"footer-subscribe-placeholder" => "请输入你的电子邮件",
	"footer-subscribe-button" => "订阅",


	"popup-text-1" => "立刻游戏",
	"popup-text-2" => "或在你最喜爱的应用商店下载此应用",
	"popup-text-btn" => "安装并游戏",

	"section-4-list-item-1"	=> "三种轮盘赌：美式、法式和欧式",
	"section-4-list-item-2"	=> "3D 画面以及丰富的游戏内容",
	"section-4-list-item-3"	=> "赢奖几率和真实轮盘赌相同",
	"section-4-list-item-4"	=> "超棒的社交功能：在轮盘赌赌桌上交谈，或私下交谈",
	"section-4-list-item-5"	=> "在 iPhones、iPad、Android 设备甚至 Facebook 上和曼联球迷一起游戏",


	"popup-text-7" => "在移动设备、社交网络或线上游戏免费",

	"popup-text-8" => "立刻玩扑克游戏",

	"popup-text-9" => "立刻玩轮盘赌",


	"casino-text-1" => "Play Social Poker now",
	"casino-text-2" => "Play Social Roulette now",

	"footer-legal" => "这个游戏仅限法定年龄的人使用。游戏不可能赢得金钱或任何有价值的东西。<br />成功地玩这个游戏并不意味着您在类似的真钱博彩游戏中能够成功。",

	"lang-title" => "选择语言",
];

