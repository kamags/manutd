<?php

return [
    
    # Common questions 
    'cq_text' => 'Allgemeine Fragen',
            
    'cq_question1' => 'Welche Art von Poker kann ich mit der App spielen?',
    'cq_answer1' => 'Mit unserer mobilen App Manchester United Poker können Sie eine der beliebtesten Arten von Poker spielen: Klassisches und limitloses Texas Hold’em. Spielen Sie entweder am Tisch um Chips oder nehmen Sie an einem der Tourniere Teil.',
    
    'cq_question2' => 'Für welche Plattformen ist Manchester United Poker erhältlich?',
    'cq_answer2' => 'Manchester United Poker ist erhältlich für iOS-, Android-, Windows Phone und Facebook.',
    
    'cq_question3' => 'Wie spielt man Texas Hold’em?',
    'cq_answer3' => 'Um mehr über die Spielregeln und die Spieloberfläche zu erfahren, besuchen Sie den Abschnitt „Hilfe“ im Hauptmenü. Wir werden Ihnen alles über Blätter, Einsätze und Spielzüge im Poker erklären. Achtung: Hierbei geht es lediglich um die Vermittlung von Grundlagen des Spielverlaufs in Hinblick auf das Spielen von, nicht aber das Gewinnen beim Poker. Durch die Verbesserung Ihrer Spielfähigkeiten und mehr Wissen über Spielstrategien und Spielerverhalten werden Sie zum Sieger. Sie ahnen ja nicht, wie fesselnd eine durchdachte Pokerpartie gegen einen fortgeschrittenen Gegner wirklich sein kann!',
    
    'cq_question4' => 'Was ist der Multiscreen-Modus?',
    'cq_answer4' => 'Im Multiscreen-Modus stehen dem Spieler gleichzeitig vier Spielbildschirme zur Verfügung. Mit Multiscreen können Sie gleichzeitig im Standardmodus spielen, an einem Tournier teilnehmen und über persönliche Nachrichten chatten. In diesem Modus steht ein Spielerlevel 15+ zur Verfügung. Im Multiscreen-Modus sind Hauptmenü, Chatfenster, Lobby und Tourniere automatisch verfügbar. Die Fenster können über das Einstellungsmenü deaktiviert werden.',
    
    'cq_question5' => 'Wie benutze ich den Multiscreen-Modus?',
    'cq_answer5' => 'Sie können in den Multiscreen-Modus wechseln, indem Sie auf das Icon rechts oben tippen. Das Icon zeigt den Sektor an, in dem Sie sich gerade befinden. Tippen Sie auf den entsprechenden Sektor, um zum jeweiligen Menü zu gelangen. Auf mobilen Geräten können Sie mit dem Finger zwischen Bildschirmen wechseln.',
    
    'cq_question6' => 'Wo finde ich die Spiel-History?',
    'cq_answer6' => 'Die Spiel-History finden Sie im Menü mit Ihren persönlichen Nachrichten, im Tab „Ereignisprotokoll“. Alle Ihre Errungenschaften und Aktionen werden hier beginnend mit dem jüngsten nach Alter geordnet angezeigt.',
    
    'cq_question7' => 'Was ist die „Lobby“?',
    'cq_answer7' => 'Die Lobby ist ein Spielmodus, in dem Sie laufende Spiele und die entsprechenden Informationen einsehen (Spielgeschwindigkeit, Blind-Größe, Anzahl der Plätze am Tisch etc.) und einen Tisch auswählen können, an dem Sie dazustoßen möchten. Fortgeschrittene Spieler beobachten die Lobby ganz genau und haben zu ihrem Spielvorteil stets ein Auge auf die durchschnittlichen Einätze und das Verhalten der Spieler. Die Auswahl des Tisches in der Lobby ist entscheidend für die Wahl des richtigen Spiels für den maximalen Gewinn.',
    
    'cq_question8' => 'Wie wähle ich einen Tisch aus der Lobby?',
    'cq_answer8' => 'Tippen Sie im Hauptmenü auf „Lobby“. In diesem Modus können Sie einen Tisch aussuchen, den Sie beobachten möchten, als ob sie in einem Casino an einem Tisch stehen und das Spiel verfolgen. Wählen Sie die Anzahl der Plätze am Tisch, die Spielgeschwindigkeit und geben Sie an, ob auch volle Tische angezeigt werden sollen. Das Blind-Level wird in diesem Fall anhand der Chipsmenge auf Ihrer Gesamtrechnung (Geldbörse) bestimmt. Einige Spieler können Tische nach Blinds aussortieren. Navigieren Sie sich durch die Tische und sehen Sie, wie Spiele gespielt werden und wie Spieler ihre Einsätze machen. Sobald Sie einen Tisch gefunden haben, der Ihnen zusagt, können Sie an einem der freien Plätze Platz nehmen und Ihren Einsatz machen.',  
    
    'cq_question9' => 'Kann ich an mehreren Tischen gleichzeitig spielen?',
    'cq_answer9' => 'Sie können nur an einem Tisch spielen.',
    
    'cq_question10' => 'Wie kann ich den Einsatz erhöhen?',
    'cq_answer10' => 'Um den Einsatz um einen beliebigen Betrag zu erhöhen, tippen Sie auf den Button „Erhöhen“ und halten Sie ihn gedrückt. Ziehen Sie den erscheinenden Regler nach oben, bis Sie den gewünschten Betrag erreicht haben. Dann lassen Sie los. Sie werden ggf. aufgefordert, den Einsatz durch nochmaliges Tippen zu bestätigen.',
    
    'cq_question11' => 'Wie kann ich an Tournieren teilnehmen?',
    'cq_answer11' => 'Wählen Sie im Hauptmenü „Tourniere“. Um das Spiel zu starten, stimmen Sie den angezeigten Bedingungen zu. Denken Sie daran, dass Sie jeweils nur an einem Tournier gleichzeitig teilnehmen können.',
    
    'cq_question12' => 'Warum werde ich von einem Tisch „verbannt“ und kann mich nicht mehr daran setzen?',
    'cq_answer12' => 'Der Grund könnte eine instabile Internetverbindung sein. Vergewissern Sie sich, dass während des Spiels die Verbindung stabil ist.',
    
    'cq_question13' => 'Was tun, wenn ich den Eindruck habe, dass der Gegenspieler unfair gewonnen hat? ',
    'cq_answer13' => 'Eine starke Hand ist noch keine Garantie für einen Sieg. Vermutlich hat der Spieler mit der schwächeren Kombination nicht gewonnen, sondern nur seinen Anteil vom Einsatz erhalten, der über Ihrem Gesamteinsatz im All-in liegt, wenn sie also alle Ihre Chips gesetzt haben. Oder Sie hatten die gleiche Kombination, wobei aber der gegnerische Kicker höher war als Ihrer. Erfahren Sie mehr über Pokerregeln, damit Ihnen der Spielausgang klarer wird.',
    
    'cq_question14' => 'Was bedeutet der goldene Stern neben einem Spielernamen?',
    'cq_answer14' => 'Ein Sternsymbol neben dem Benutzernamen eines Mitspielers bedeutet, dass dieser Spieler auf Ihrer Freundesliste ist.',
    
    'cq_question15' => 'Wie kann ich einen Screenshot aufnehmen, um ihn mit meinen Freunden zu teilen?',
    'cq_answer15' => 'Um bemerkenswerte Momente im Spiel festzuhalten und zu teilen, tippen Sie auf das Kamera-Symbol und wählen Sie eine der folgenden Optionen: Auf dem Gerät speichern, per E-Mail versenden oder auf Facebook posten. Wenn die E-Mail-Option nicht in der Liste erscheint, überprüfen Sie, ob auf Ihrem Gerät E-Mail konfiguriert ist.',
    
    
    # Creating a game profile
    'cgp_text' => 'Ein Spielerprofil erstellen',
            
    'cgp_question1' => 'Was ist ein Gast-Account?',
    'cgp_answer1' => 'Zum Spielen ist keine Registrierung erforderlich. Sie können auch als Gast spielen, wobei allerdings Ihr Account an ein bestimmtes Gerät gebunden ist. Wenn Sie weiterspielen und dabei Ihre Errungenschaften und Punktestände speichern möchten, müssen Sie dieses Gerät weiterbenutzen. Sollte Ihr Gerät verloren oder kaputt gehen, kann der Account nicht wiederhergestellt werden.',
    
    'cgp_question2' => 'Warum soll ich mich registrieren?',
    'cgp_answer2' => 'Nachdem Sie sich registriert haben, können Sie Ihr eigenes Spielerprofil anlegen. Alle Ihre Errungenschaften, Chips, Münzen und Eigentümer werden in diesem Profil gespeichert. Sie können auch Freunde hinzufügen und das Profil nutzen, um von allen iOS- und Android-Geräten und aus sozialen Netzwerken darauf zuzugreifen.',
    
    'cgp_question3' => 'Wie kann ich mich im Spiel anmelden?',
    'cgp_answer3' => 'Nutzen Sie das Registrierungsformular aus dem Spielmenü. Hier müssen Sie Ihre E-Mail-Adresse, ein Passwort und einen Benutzernamen angeben. Wahlweise können Sie auch ein Profilbild hochladen. Wir raten, eine echte E-Mail-Adresse anzugeben, damit wir Ihnen im Bedarfsfall dabei helfen können, Ihr Passwort wiederherzustellen. Wenn Sie ein Profil in einem sozialen Netzwerk haben, können Sie dieses auch als Spielerprofil nutzen.',
    
    'cgp_question4' => 'Ich habe ein Manchester United Roulette-Profil. Kann ich das auch für Manchester United Poker benutzen?',
    'cgp_answer4' => 'Ja. Manchester United Poker und Manchester United Roulette nutzen einen gemeinsamen Account. Bitte beachten Sie: Eine E-Mail-Adresse kann nicht für einen neuen Account genutzt werden, wenn bereits ein Manchester United Roulette-Account mit dieser E-Mail-Adresse angemeldet ist. In diesem Fall melden Sie sich einfach mit dem gleichen Passwort bei Manchester United Poker an.',
    
    'cgp_question5' => 'Wo finde ich meine ID?',
    'cgp_answer5' => 'Ihre ID ist eine 9- oder 10-stellige Nummer in Ihrem Spielerprofil oder auf der Webseite. Bitte geben Sie Ihre ID an, wenn Sie sich an den technischen Support wenden.',
    
    'cgp_question6' => 'Kann ich meinen Anmeldenamen ändern?',
    'cgp_answer6' => 'Eine Änderung des Anmeldenamens ist leider nicht möglich. Stattdessen können Sie aber einfach einen neuen Account anlagen und einen beliebigen neuen Anmeldenamen auswählen.',
    
    'cgp_question7' => 'Wie kann ich mein Passwort wiederherstellen?',
    'cgp_answer7' => 'Um Ihr Passwort wiederherzustellen, tippen Sie auf den Link „Passwort vergessen?“ und geben Sie Ihre Daten in das angezeigte Formular ein. Wir empfehlen, eine echte E-Mail-Adresse zu benutzen. Sollten Sie eine nicht existente E-Mail-Adresse angegeben haben oder die angegebene Adresse ist nicht länger verfügbar, benutzen Sie bitte unser Kontaktformular für weitere Anweisungen von unserem technischen Support.',
    
    'cgp_question8' => 'Kann ich einen Account löschen?',
    'cgp_answer8' => 'Accounts können nicht gelöscht werden. Wenn sie möchten, können Sie einen neuen Account anlegen. Bitte beachten Sie, dass auf einem Gerät höchstens 20 Accounts einschließlich Gast-Account eingerichtet werden können.',
    
    'cgp_question9' => 'Wie lege ich meine Profileinstellungen fest?',
    'cgp_answer9' => 'Um Profildaten einzutragen, tippen Sie auf den Block mit Ihrem Profilbild, Benutzernamen und Guthaben im Hauptmenü.',
    
    'cgp_question10' => 'Wie kann ich einen Benutzernamen ändern?',
    'cgp_answer10' => 'Um Ihren Benutzernamen zu ändern, tippen Sie in Ihren Profileinstellungen auf Ihren Benutzernamen (oder das Bleistift-Symbol daneben), geben Sie einen neuen Namen ein und tippen Sie auf „Fertig“.',
    
    'cgp_question11' => 'Wie kann ich ein Profilbild hochladen oder ändern?',
    'cgp_answer11' => 'Tippen Sie auf das aktuelle Profilbild in den Profileinstellungen. Wählen Sie „Neu“ aus dem Drop-Down-Menü und nehmen Sie ein Bild auf oder wählen Sie ein Bild aus der Gerätegalerie oder ein anderes bestehendes Foto aus. Wenn Sie sich beim ersten Mal über Ihren Facebook-Account anmelden, wird Ihr Facebook-Profilbild genutzt.',
    
    'cgp_question12' => 'Kann ich meinen Status ändern?',
    'cgp_answer12' => 'Öffnen Sie „Profil“ im Hauptmenü, tippen Sie auf die Zeilen bei Ihrem Profilbild und geben Sie einen neuen Status ein. Lassen Sie das Feld frei, wenn Sie nicht möchten, dass dort etwas angezeigt wird.',
    
    'cgp_question13' => 'Wie kann ich meine Location bestimmen?',
    'cgp_answer13' => '„Location“ zeigt die Region an, in der Sie sich aktuell befinden. Um Ihre Location zu aktualisieren tippen Sie auf das entsprechende Icon in Ihrem Spielerprofil. Um Ihre Location auf dem Webseiten-Profil zu bestimmen, tippen Sie auf „Bestimmen“ neben der „Location“ in Ihrer ID-Zeile rechts vom Profilbild.',
    
    'cgp_question14' => 'Warum wird meine Location nicht bestimmt?',
    'cgp_answer14' => 'Vergewissern Sie sich, dass die Ortungseinstellungen auf Ihrem Gerät aktiviert sind und unsere Anwendung darauf Zugriff hat. Sollte Ihre Location trotz korrekter Einstellungen nicht bestimmt werden, installieren sie die Anwendung erneut und aktivieren Sie die Ortungsfunktion beim ersten Start.',
    
    'cgp_question15' => 'Wie kann ich Eigentum kaufen?', 
    'cgp_answer15' => 'Eigentum können Sie für Chips oder Münzen erwerben. Ihr Eigentum ändert zwar nichts am Spielverlauf, zeigt aber Ihren Status an. Um Eigentum zu kaufen, melden Sie sich mit Ihrem Account an, tippen Sie auf „Eigentum“ und benutzen Sie das Kaufmenü. Eigentum können Sie auch an Freunde oder jeden anderen Spieler weitergeben. Um Eigentum zu verkaufen, wählen Sie die entsprechende Funktion im Profilmenü. Bitte beachten Sie, dass Sie beim Verkauf nur 10% des Kaufpreises zurückbekommen. Wenn Sie Eigentum verkaufen, das Sie von anderen Spielern erhalten haben, erhalten Sie nichts. Die Objekte werden lediglich aus Ihrem Profil entfernt.',  
    
    'cgp_question16' => 'Wie kaufe ich Geschenke?',
    'cgp_answer16' => 'Wenn Sie an einem Tisch sitzen, tippen Sie auf das Glas-Icon neben dem Profilbild und wählen Sie ein Geschenk aus. Das Geschenk wird neben dem Profilbild des Spielers angezeigt. Eigentum wird im entsprechenden Abschnitt des Spielerprofils angezeigt. Bitte beachten Sie, dass Geschenke temporär sind, während Eigentum dauerhaft ist. Die Geschenke gelten nur so lange, wie Sie an einem Tisch spielen, bzw. einige Tage, wenn sie mit Geld gekauft wurden.',
    
    'cgp_question17' => 'Wie kann ich anderen Spielern Geschenke machen?',
    'cgp_answer17' => 'Tippen Sie auf das Glas- oder Haus-Icon neben dem Profilbild. Wählen Sie ein Geschenk aus und bestätigen Sie den Kauf. Um allen Spielern am Tisch etwas zu schenken, wählen Sie die Option „Für alle kaufen“ aus dem Kaufmenü.',
    
    'cgp_question18' => 'Was kann ich tun, wenn mir ein erhaltenes Geschenk nicht gefällt?',
    'cgp_answer18' => 'Wenn Ihnen ein Geschenk, das Sie gekauft oder erhalten haben, nicht gefällt, können Sie es gegen ein neues eintauschen. Tippen Sie auf das aktuelle Geschenk (an der Stelle, wo normalerweise das Glas-Icon erscheint) und wählen Sie ein neues. Es wird dann neben Ihrem Profilbild angezeigt. Auf diese Weise können auch Spielergeschenke umgetauscht werden. Geschenke verschwinden übrigens, sobald Sie die App verlassen.',
    
    'cgp_question19' => 'Warum kann ich keinen Alkohol und keinen Tabak als Geschenk kaufen?',
    'cgp_answer19' => 'Gemäß den Facebook-Regeln können Spieler beim Spielen über das betreffende soziale Netzwerk oder in einem autorisierten Konto keinen Alkohol und keinen Tabak kaufen oder erhalten.',
    
    'cgp_question20' => 'Wofür erhalte ich Errungenschaften?',
    'cgp_answer20' => 'Errungenschaften sind ein Ausdruck Ihres Spielfortschritts. Um die Liste der Errungenschaften anzusehen, öffnen Sie das Profil und tippen Sie auf „Errungenschaften“. Tippen Sie auf eine Errungenschaft, um zu sehen, wofür Sie sie erhalten haben. Sie können Ihre Errungenschaften auch auf Ihrem Webseitenprofil einsehen.',
    
    'cgp_question21' => 'Wie kann ich mich abmelden?',
    'cgp_answer21' => 'In den mobilen Apps sind einige Icons im Gleitpanel versteckt. Das Icon wird mit den Seitenangaben unten rechts im Hauptmenü angezeigt. Mit einer Fingerbewegung kommen Sie zu den Punkten Tourniere, Einladen, Store, Tutorial und Spieler abmelden.',
      
    
    #Game settings
    'gs_text' => 'Spieleinstellungen',
            
    'gs_question1' => 'Wie kann ich die Spieleinstellungen ändern?',
    'gs_answer1' => 'Für Spieleinstellungen tippen Sie auf das Einstellungs-Icon oben rechts im Hauptmenü.',
    
    'gs_question2' => 'Wie kann ich die Spielsprache ändern?',
    'gs_answer2' => 'Die automatisch eingestellte Spielsprache entspricht der Ihres Geräts. In jeder Spielversion haben Sie alternativ die Option, das Spiel auf Englisch umzustellen. Aktivieren Sie dafür die Option „Lokalisierung“ in den Spieleinstellungen.',
    
    'gs_question3' => 'Wie schalte ich den Ton aus?',
    'gs_answer3' => 'Um Spielsounds ein- und auszuschalten oder um die Lautstärke zu regulieren, benutzen Sie den entsprechenden Schieberegler in den Spieleinstellungen.',
    
    'gs_question4' => 'Wie deaktiviere ich die Vibration?',
    'gs_answer4' => 'Vibration macht auf gesetzte Einsätze aufmerksam. Im Einstellungsmenü können Sie die Funktion deaktivieren (Funktion ist nur auf Smartphones verfügbar).',
    
    'gs_question5' => 'Wie blockiere ich die Chat-Funktion?',
    'gs_answer5' => 'Am Tisch können Sie nicht nur spielen, sondern auch chatten. Spielernachrichten erscheinen in Popup-Fenstern über den Profilbildern. Wenn Sie das stört, können Sie die Chatfunktion in einem Spezialmenü deaktivieren, indem auf das Schloss-Symbol neben dem Button „Chat“ tippen. Wählen Sie Spieler aus, deren Nachrichten Sie nicht sehen möchten, oder blockieren Sie den Chat für alle Spieler. Aktivieren Sie dafür die Chat-Sperre in den Spieleinstellungen.',
    
    'gs_question6' => 'Wie kann ich die Statusanzeige deaktivieren?',
    'gs_answer6' => 'Wenn die Statusanzeige von Spielern Sie stört, können Sie diese im Spielermenü ausschalten.',
    
    'gs_question7' => 'Wie aktiviere ich das Hervorheben von Karten?',
    'gs_answer7' => 'In den Spieleinstellungen haben Sie die Möglichkeit, das automatische Hervorheben von Gewinnerkombinationen in Ihrem Blatt und auf dem Tisch einzustellen. Dadurch wird das Spiel einfacher, wobei es sich aber auch auf das Interesse und die Spannung auswirkt.',
    
    'gs_question8' => 'Wie deaktiviere ich Push-Nachrichten?',
    'gs_answer8' => 'Push-Nachrichten können in den Spieleinstellungen deaktiviert werden. Wählen Sie dafür Einstellungen/Push-Nachrichten/Manchester United Poker.',
    
    'gs_question9' => 'Wie deaktiviere ich die Anzeige von Geschenken?',
    'gs_answer9' => 'Wenn die Anzeige von Geschenken Sie stört, können Sie sie deaktivieren. Wählen Sie dafür die entsprechende Funktion im Einstellungsmenü. Beachten Sie, dass Sie in dem Fall keine Geschenke vergeben oder erhalten können.',
    
    'gs_question10' => 'Wie deaktiviere ich die Nachrichten-Spracherkennung?',
    'gs_answer10' => 'Öffnen Sie die Spieleinstellungen und wählen Sie den Abschnitt „Sprache“. Hier können Sie die Nachrichten-Spracherkennung aktivieren/deaktivieren und die Sprache für die Spracherkennung festlegen.',
    
     # Financial issues   
    'fi_text' => 'Finanzbezogene Fragen',
            
    'fi_question1' => 'Spiele ich um echtes Geld oder um virtuelle Chips?',
    'fi_answer1' => 'Wir benutzen eine virtuelle Währung. Ihr Konto bleibt unangetastet. Sie können echtes Geld benutzen, um im Store Chips oder Münzen zu kaufen.',
        
    'fi_question2' => 'Wie bekomme ich Gratis-Chips?',
    'fi_answer2' => 'Gratis-Chips erhalten Sie, wenn Sie:<br> - an der täglichen Lotterie teilnehmen;<br> - Freunde zum Spiel einladen;<br> - folgen Sie uns auf Facebook.<br>Zudem können Freunde Ihnen am Tag bis zu 100.000 Chips schenken.',
        
    'fi_question3' => 'Was, wenn ich alle meine Chips verspielt habe und ein Guthaben von 0 habe?',
    'fi_answer3' => 'Ihr Guthaben wird bei den ersten 5 Malen kostenlos auf 500 Chips aufgefüllt, sodass Sie das Spiel fortsetzen können.',
        
    'fi_question4' => 'Wie kann ich Chips kaufen?',
    'fi_answer4' => '- Tippen Sie auf „Store“ im Hauptmenü und kaufen Sie die gewünschte Menge Chips über iTunes oder Google Play Store. Die Zahlung wird über die verknüpfte Kreditkarte abgewickelt. Sie können auch einen iTunes-Geschenkgutschein einlösen.<br>- Wenn Sie über ein soziales Netzwerk eingeloggt sind, können Sie mit der virtuellen Währung des Netzwerks Chips kaufen (z.B. Facebook-Credits).',
        
    'fi_question5' => 'Wie kann ich Münzen kaufen und wofür brauche ich sie?',
    'fi_answer5' => 'Mit Münzen können Sie anspruchsvolle Geschenke und Eigentum kaufen. Münzen gibt es genau so wie Chips im Store zu kaufen. Manchmal erhalten Sie Münzen auch als Geschenk von den Spielentwicklern.',
        
    'fi_question6' => 'Kann ich einen Kauf stornieren?',
    'fi_answer6' => 'Ja. Wenden Sie sich dafür an das Bezahlsystem, mit dem Sie den Kauf durchgeführt haben. Wir arbeiten nicht direkt mit den Bezahlsystemen zusammen und können den Kauf deswegen nicht stornieren.',
        
    'fi_question7' => 'Wie wirkt sich das Bonus-Level auf die Menge gekaufter Chips und Münzen aus?',
    'fi_answer7' => 'Das Bonus-Level garantiert Ihnen bei den ersten drei Käufen von Chips oder Münzen eine bedeutende Prämie. Standardmäßig erhalten Sie das erste Level und bekommen 100% Bonus-Chips gratis. Nach dem ersten Kauf werden Sie auf das zweite Bonus-Level hochgestuft und erhalten beim nächsten Kauf 200% Bonus-Chips gratis. Schließlich erreichen Sie das dritte Bonus-Level und erhalten beim Kauf von Chips oder Münzen 300%.',
        
    'fi_question8' => 'Wie kann ich zusätzliche Chips für das Spiel kaufen?',
    'fi_answer8' => 'Sie sitzen an einem Tisch und haben keine Chips mehr? Tippen Sie auf Ihr Profilbild und tippen Sie auf das Chips-Icon im Profilfenster. Geben Sie die gewünschte Chipsmenge an und kehren Sie zum Spiel zurück. Oder Sie wählen einfach „aufstehen“ oder „aussetzen“ und dann „setzen“, um sich wieder hinzusetzen. In dem Fall können Sie den gewünschten Betrag mitnehmen. ',
        
    'fi_question9' => 'Kann ich Chips gegen Münzen und Münzen gegen Chips tauschen?',
    'fi_answer9' => 'Münzen können nicht gegen Chips getauscht, ebenso wie Chips gegen Münzen. Münzen sind nur im Store erhältlich. Zum Kaufen, tippen Sie im Hauptmenü auf „Store“.',
        
    'fi_question10' => 'Wie kann ich „verschwundene“ Chips wiederherstellen?',
    'fi_answer10' => 'Sollten sie sich im Spiel anmelden und feststellen, dass Sie weniger Chips oder ein niedrigeres Level haben, überprüfen Sie zunächst, ob Sie beim richtigen Account angemeldet sind. Möglicherweise sind Sie als Gast oder unter einem anderen Account angemeldet. Wenn Sie das ausschließen können, wenden Sie sich an unseren technischen Support. Wir helfen Ihnen gern weiter.',
        
     'fi_question11' => 'Wie kann ich Chips auf einen anderen Account verschieben?', 
     'fi_answer11' => 'Das Verschieben von Chips zwischen den eigenen Accounts ist verboten.',

    
    # Friends 
    'f_text' => 'Freunde',
            
    'f_question1' => 'Wie benutze ich den Abschnitt Freunde im Hauptmenü?',
    'f_answer1' => 'Im Hauptmenü finden Sie unter Ihrer Profilvorschau eine Liste aller Ihrer Freunde. Das obere Profilbild ist das Profil eines ausgewählten Freundes (in der Mitte des Bildschirms finden Sie seine Spielerinformation und Aktions-Buttons). Darunter finden Sie eine zweireihige Liste mit den Vorschaubildern ihrer übrigen Freunde. Scrollen Sie durch die Vorschaubilder, um die vollständige Liste zu sehen. Ganz unten auf dem Bildschirm finden Sie Profilbilder von zufällig ausgewählten Spielern.',
        
    'f_question2' => 'Wie kann ich Spieler als „Freunde“ hinzufügen?',
    'f_answer2' => ' - Wählen Sie den Abschnitt „Freunde“ im Hauptmenü und wählen Sie „Suchen“. Suchen Sie nach Spielern anhand von Benutzernamen, E-Mail-Adressen oder IDs und tippen Sie auf „Als Freund hinzufügen“.<br> - Wenn Sie an einem Tisch spielen, können Sie Freundschaftsanfragen an Ihre Mitspieler versenden. Öffnen Sie dafür das entsprechende Profil und tippen sie auf „Als Freund hinzufügen“.',
        
    'f_question3' => 'Wie verschenke ich Chips an Freunde?',
    'f_answer3' => 'Wählen Sie den Abschnitt „Freunde“ im Hauptmenü, wählen Sie einen Freund aus und tippen Sie auf „Chips senden“. Sie können bis zu 100.000 Chips am Tag verschenken.',
        
    'f_question4' => 'Wie kaufe ich Chips für einen Freund?',
    'f_answer4' => 'Öffnen Sie das Profil eines Freundes, tippen Sie auf „Chips versenden“ und verschieben Sie den Regler bis zum Maximum. Es erscheint der Button „Chips kaufen und versenden“. Sie werden automatisch zum Abschnitt „Store“ weitergeleitet. Die gekauften Chips werden an den Freund gesendet.',
        
    'f_question5' => 'Wie viele Chips kann ich für Freunde kaufen?',
    'f_answer5' => 'Die Menge der Chips, die Sie Freunden schenken können, ist begrenzt. Für Freunde gekaufte Chips sind nicht gedeckelt.',
        
    'f_question6' => 'Wie kann ich einem Freund etwas schenken oder ihm mein Eigentum senden?',
    'f_answer6' => 'Sie haben die Möglichkeit, Geschenke und Eigentum für andere Spieler zu kaufen. Tippen Sie dafür auf das Glas- oder Haus-Icon neben dem Profilbild des Freundes. Um allen Spielern am Tisch etwas zu schenken, wählen Sie die Option „Für alle kaufen“ aus dem Kaufmenü. Bitte beachten Sie, dass Geschenke temporär sind, während Eigentum dauerhaft ist. Einige Geschenke bleiben für einige Tage bestehen, sofern die Geschenke durch Geld gekauft wurden.',
        
    'f_question7' => 'Wie setze ich mich an den Tisch, an dem ein Freund spielt?',
    'f_answer7' => 'Öffnen Sie den Abschnitt „Freunde“ im Hauptmenü und wählen Sie das Profil eines Freundes. Ist der Spieler gerade in einem Spiel, tippen Sie auf „An diesen Tisch setzen“. Wenn Sie einen Freund an Ihren aktuellen Tisch einladen möchten, tippen Sie auf einen freien Stuhl und wählen Sie einen Freund aus der Liste. Ihr Freund erhält eine Spieleinladung.',
        
    'f_question8' => 'Wie entferne ich Benutzer aus der Freundesliste?',
    'f_answer8' => 'Zum Löschen von Benutzern aus der Freundesliste, öffnen Sie den Abschnitt „Freunde“ im Hauptmenü. Wählen Sie den Freund, den Sie löschen möchten, und tippen Sie auf „Aus Freundesliste entfernen“.',
        
    'f_question9' => 'Wie erhalte ich Chips und andere Boni für das Einladen von Freunden?',
    'f_answer9' => 'Tippen Sie auf „Freunde“ im Hauptmenü und wählen Sie „Einladen“. Dann versenden Sie per SMS, E-Mail oder Facebook eine Spieleinladung zu Manchester United Poker an einen Freund. Sie erhalten einen Bonus, wenn Ihre Freunde den beigefügten Link benutzen. Wenn Sie über Facebook spielen, tippen Sie auf „Gratis-Chips“ und erhalten Sie Chips für jeden eingeladenen Spieler.',
    
    
    # Communication with other players
    'cwop_text' => 'Kommunikation mit anderen Spielern',
            
    'cwop_question1' => 'Was sind persönliche Nachrichten?',
    'cwop_answer1' => 'Persönliche Nachrichten sind Nachrichten an einen bestimmten Spieler. Nur er/sie kann sie lesen. In Ihrem persönlichen Nachrichtenverlauf finden Sie Nachrichten aus der aktuellen Konversation, sowie ältere Nachrichten mit Angabe von Datum und Uhrzeit.',
            
    'cwop_question2' => 'Wie komme ich ins Menü für persönliche Nachrichten?',
    'cwop_answer2' => 'Das Menü für persönliche Nachrichten finden Sie unter dem Umschlag-Button „Nachrichten“ im Hauptmenü. Alternativ können Sie einen Freund, mit dem Sie kommunizieren möchten, direkt auswählen und in seinem/ihrem Profil auf den Umschlag-Button tippen.',
            
    'cwop_question3' => 'Wie funktioniert das Menü für persönliche Nachrichten?',
    'cwop_answer3' => 'Links finden Sie eine Liste von Freunden, mit denen Sie bereits eine Unterhaltung hatten. Um einen neuen Nutzer zur Liste hinzuzufügen, tippen Sie auf „hinzufügen“ (Bleistift-mit-Plus-Icon unten links). Oder Sie besuchen ein Freundesprofil und führen Aktionen aus wie Chips versenden oder am Spiel teilnehmen.',
            
    'cwop_question4' => 'Wie versende ich Nachrichten?',
    'cwop_answer4' => 'Wählen Sie einen Freund, den Sie anschreiben möchten, und geben Sie Ihren Text ist das Textfeld ein. Verfassen Sie eine Nachricht und tippen Sie auf „Senden“.',
            
    'cwop_question5' => 'Kann ich auch Nachrichten verschicken, wenn ein Freund offline ist?',
    'cwop_answer5' => 'Sie können auch an Freunde, die offline sind, Nachrichten senden. Die Nachrichten, die Sie offline erhalten haben, erscheinen als Zahlen auf dem Nachrichten-Icon.',
            
    'cwop_question6' => 'Wie lösche ich persönliche Nachrichten?',
    'cwop_answer6' => 'Tippen Sie auf die Nachricht, die Sie löschen möchten, und halten Sie sie gedrückt. Im erscheinenden Menü wählen Sie „Löschen“.',
            
    'cwop_question7' => 'Wie verfasse ich eine Chatnachricht?',
    'cwop_answer7' => 'Wenn Sie an einem Tisch spielen, tippen Sie auf das Nachrichten-Icon rechts unten. Verfassen Sie eine Nachricht und tippen Sie auf „Eingabe“ und auf „Fertig“.',
            
    'cwop_question8' => 'Wie benutze ich Sprachnachrichten?',
    'cwop_answer8' => 'Neben Textnachrichten können Sie auch per Sprachnachricht mit anderen Spielern kommunizieren. Tippen Sie auf das Mikrofon-Icon rechts unten und sprechen Sie die Nachricht ein, die Sie teilen möchten. Das System wandelt das gesprochene Wort automatisch in Text um und zeigt diesen bei den anderen Spielern an. Die Verfügbarkeitsphase für diese Funktion ist begrenz und kann für Münzen im Store erworben werden.',
  
];
