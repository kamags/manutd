<?php

return [
	"header-text-2"                => "Spiele kostenlos auf deinem mobilen Gerät, im sozialen Netzwerk oder online",

	"play-now-btn"                 => "Spielen",

	"section-1-title"              => "Manchester United Social Poker – <br />Powered by KamaGames<br />Beginnen Sie gleich mit einer Runde Poker!",
	"section-1-text"               => "Manchester United Social Poker – <br /> Beginnen Sie gleich mit einer Runde Poker!",

	"section-2-title"              => "Wählen Sie einen Tisch und machen Sie<br />sich bereit für einen<br />spannenden Wettstreit!",
	"section-2-text"               => "Landen Sie Ihren eigenen Coup und <br />spielen Sie Poker mit anderen<br />Fans aus aller Welt!",

	"section-3-title"              => "Funktionsumfang",
	"section-3-text"               => "Manchester United Social Poker",
	"section-3-item-1"             => "VÖLLLIG<br />GRATIS",
	"section-3-item-2"             => "TÄGLICH TAUSENDE<br />SPIELER ONLINE",
	"section-3-item-3"             => "GRATIS-CHIPS IN<br />TÄGLICHEN LOTTERIEN",

	"section-3-list-item-1"        => "Chatten Sie mit anderen Spielern, kaufen Sie Geschenke, erreichen Sie Auszeichnungen",
	"section-3-list-item-2"        => "Verdienen Sie Bonus-Chips indem Sie Ihre Freunde zum Spiel einladen!",
	"section-3-list-item-3"        => "Verwenden Sie ein Konto auf verschiedenen Geräten",
	"section-3-list-item-4"        => "In-App-Anleitung",

	"section-4-label"              => "Spieler online:",

	"section-5-title"              => "Social Roulette",
	"section-5-title-2"            => "Spielen Sie Manchester United Social<br />Roulette anderen United-Fans",
	"section-5-text"               => "Spielen Sie das legendäre Spiel, das<br />weltweit Millionen Menschen in die Kasinos lockt.",

	"form-title"                   => "Support",
	"form-text"                    => "Bitte hinterlassen Sie uns hier Ihre Frage und Ihre Kontaktdaten. Unser Support wird sich innerhalb von 24 Stunden um Ihr Anliegen kümmern.",
	"form-success"                 => "Ihre Nachricht wurde übertragen.<br /> Unser Kundensupport wird sich so bald wie möglich um Ihr Anliegen kümmern.",
	"form-error"                   => '<p class="form-message">Please fill the required field</p>',
	"form-btn"                     => 'Send message',

	"form-input-name"              => "Name",
	"form-input-email"             => "E-Mail",
	"form-input-message"           => "Nachricht",

	"footer-support"               => "Support",
	"footer-tos"                   => "Nutzungsbedingungen",
	"footer-policy"                => "Datenschutzbestimmungen",
	"footer-rights"                => "Alle Rechte vorbehalten.",

	"footer-subscribe-title"       => "Newsletter abonnieren:",
	"footer-subscribe-placeholder" => "Bitte gib deine E-Mail ein",
	"footer-subscribe-button"      => "Abonnieren",

	"footer-legal"                 => "Dieses Spiel steht nur volljährigen Personen zur Verfügung. Das Spiel bietet keine Möglichkeit zum Gewinn von Geld oder Wertvollem.<br />Der Erfolg in diesem Spiel lässt nicht auf einen möglichen Erfolg im ähnlichen Casinospiel mit Echtgeld schließen.",

	"popup-text-1"                 => "Jetzt spielen",
	"popup-text-2"                 => "oder die App in Ihrem bevorzugten Store herunterladen",
	"popup-text-btn"               => "Installieren & Losspielen",

	"section-4-list-item-1"        => "Drei Roulette-Arten: American, French und European",
	"section-4-list-item-2"        => "3D-Grafiken sowie Spielweise und Physik",
	"section-4-list-item-3"        => "Die Wahrscheinlichkeit zu gewinnen, entspricht der des echten Roulettes",
	"section-4-list-item-4"        => "Tolle Social-Funktionen: Chatten Sie am Roulettetisch oder privat",
	"section-4-list-item-5"        => "Spielen Sie mit United-Fans über iPhone, iPad, Android oder sogar Facebook",

	"casino-text-1"                => "Spiele jetzt Poker",
	"casino-text-2"                => "Spiele jetzt Roulette",

	"lang-title"                   => "Sprache auswählen"
];
