﻿<?php

return [
	"play-now-btn"                 => "今すぐプレイ",

	"header-text-2"                => "Free to play on your mobile device or social network",

	"section-1-title"              => "Manchester United Social Poker – <br />Powered by KamaGames<br />ソーシャルポーカーゲームを今すぐキックオフ！",
	"section-1-text"               => "Manchester United Social Poker – <br /> ソーシャルポーカーゲームを今すぐキックオフ！",

	"section-2-title"              => "テーブルを選んで<br />エキサイティングなゲームに<br />参加しよう！",
	"section-2-text"               => "世界中のファンと<br />ソーシャルポーカーをプレイして<br />ハットトリックを決めよう！",

	"section-3-title"              => "特長",
	"section-3-text"               => "Manchester United Social Poker",
	"section-3-item-1"             => "完全<br />無料プレイ",
	"section-3-item-2"             => "毎日数千人の<br />プレイヤーがオンライン",
	"section-3-item-3"             => "日替わりロッタリーで<br />無料チップ",

	"section-3-list-item-1"        => "ほかのプレイヤーとのチャット、ギフト購入、実績達成",
	"section-3-list-item-2"        => "友達をゲームに招待すると、ボーナスチップを獲得できます！",
	"section-3-list-item-3"        => "複数の端末でひとつのアカウントを使えます",
	"section-3-list-item-4"        => "アプリ内チュートリアル",

	"section-4-label"              => "オンラインのプレイヤー：",

	"section-5-title"              => "ソーシャルルーレット",
	"section-5-title-2"            => "Unitedのファン仲間と<br />Manchester United Social Rouletteをプレイ",
	"section-5-text"               => "世界中、数百万の人々をカジノに引き寄せる<br />伝説のゲームをプレイ。",

	"form-title"                   => "サポート",
	"form-text"                    => "ご質問と連絡先をここにお書きください。サポートチームが24時間以内にお答えします。",
	"form-success"                 => "メッセージが送信されました。<br />カスタマーサポートチームが、できるだけ迅速にお返事します。",
	"form-error"                   => '<p class="form-message">必要なフィールドすべてに記入してください</p>',
	"form-btn"                     => '送信',

	"form-input-name"              => "氏名",
	"form-input-email"             => "メール",
	"form-input-message"           => "メッセージ",

	"footer-title"                 => "Manchester United Pokerをプレイ",
	"footer-support"               => "サポート",
	"footer-tos"                   => "利用規約",
	"footer-policy"                => "個人情報保護方針",
	"footer-rights"                => "無断複写/複製を禁じます。",

	"footer-subscribe-title"       => "ニュースレターに登録：",
	"footer-subscribe-placeholder" => "メールアドレス入力",
	"footer-subscribe-button"      => "登録",

	"popup-text-1"                 => "今すぐプレイ",
	"popup-text-2"                 => "または愛用のストアでアプリをダウンロード",
	"popup-text-btn"               => "インストールしてプレイ",

	"section-4-list-item-1"        => "3種類のルーレット：アメリカ、フランス、ヨーロッパ",
	"section-4-list-item-2"        => "3Dグラフィックスと、現実感あるゲームプレイ、物理表現",
	"section-4-list-item-3"        => "勝利の確率は本物のルーレットと同じ",
	"section-4-list-item-4"        => "充実のソーシャル機能：ルーレットテーブルのみんなと、または特定の相手とのチャット",
	"section-4-list-item-5"        => "iPhone、iPad、Android、あるいはFacebookでもUnitedのファンとプレイできる",

	"popup-text-7"                 => "携帯端末、ソーシャルネットワーク、あるいはオンラインで無料プレイ",

	"popup-text-8"                 => "今すぐポーカーをプレイ",

	"popup-text-9"                 => "今すぐルーレットをプレイ",

	"casino-text-1"                => "Play Social Poker now",
	"casino-text-2"                => "Play Social Roulette now",

	"footer-legal"                 => "このゲームは未成年の利用ができません。このゲームは成果として通貨や貨幣価値のあるものを獲得できるものではありません。<br />このゲームでの成功が現実のカジノゲームでの成功可能性を暗示するものでは決してありません。",

	"lang-title"                   => "言語選択"
];

