<?php

return [
    'actions' => '自分の番が回ってきたプレーヤーは、次のアクションのどれかを行うことができます:',
    'actions_call' => 'コール',
    'actions_call_content' => "既にベットが行われるいる場合、コールとはプレイヤーが同額のベットをするということです。言い換えれば、プレーヤーAが10のベットをしたら、プレーヤーBはコールを行い、Bはその10のベットに合わせてポットに10をベットしなければなりません。",
    'actions_check' => 'チェック',
    'actions_check_content' => '現在のラウンドで一切ベットが行われていない場合、プレーヤーはチェックを行うことができます。最も良い表現をすればチェックはパスだと言えるでしょう。ハンドを保持したままですが、賭けを通して掛け金を上げることはできません。',
    'actions_raise' => 'レイズ',
    'actions_raise_content' => 'レイズとは他の人のベットを上回るベットをするということです。現在のラウンドでベットが行われている場合、プレーヤーはレイズを行うことができます。これを行うには、プレーヤーは少なくとも直前のプレーヤーのベットの2倍の額のベットを行わなければなりません。ポットを争い続けるには、続くプレーヤー全員がコールまたはリレイズをしなければなりません。そうでなければ、続くプレーヤーはフォールドすることになります。',
    'actions_fold' => 'ﾌｫｰﾙﾄﾞ',
    'actions_fold_content'=> 'フォールドとは現在のポットを争うことを拒否するということです。プレーヤーがフォールドした場合、プレーヤーのカードはそれ以上ハンドに参加せず、現在のラウンドで勝利することはできません。',
    
    'bets_blinds' => 'ブラインド',
    'bets_blinds_content' => 'カードが配られる前に、ボタンの左側に座っているプレーヤーはスモールブラインドという賭け金を支払わなければなりません。スモールブラインドの左隣に座っているプレーヤーもまたビッグブラインドという賭け金を支払わなければなりません。ビッグブラインドの額はスモールブラインドの額の2倍です。',
    'bets_ante' => 'アンティ',
    'bets_ante_content' => 'プレーヤーがビッグブラインド以外の位置でテーブルに参加する場合、プレーヤーは最初のハンドの際にビッグブラインドを支払う必要があります（アンティ）。プレイヤーは一度だけアンティを支払います。その後はディーラーボタンが所定位置に移動するに従って、ビッグまたはスモールブラインドを支払うだけです。',
    'bets_minimum_raise' => 'ミニマムレイズ',
    'bets_minimum_raise_content' => 'プレーヤーはビッグブランドの額より少ない額でレイズすることはできません。',
    'bets_all_in' => 'ｵｰﾙｲﾝ',
    'bets_all_in_content' => "ベットをコールするためのチップが十分でない場合は、\"オールイン\"することができます。これは手持ちのチップを全て賭けるということです。オールインした場合、コールできていた以上の金額を獲得することはできません。オールインした後でポットが増えて勝った場合、ポットはあなた、およびレイズに応じることのできた次にベストハンドを持っていたプレーヤーに分配されます。",
    'bets_split_pot' => 'ポットの分配',
    'bets_split_pot_content' => '2人以上のプレーヤーが引き分けとなった場合、掛け金は均等に分配されます。',
    
    'hands_01' => 'ポーカーは世界中で最も人気のあるカードゲームです。ゲームの目的は4ラウンド（またはそれ未満）で掛け金を山分けすることです。掛け金を勝ち取る主な2つの方法は対戦相手よりも強力なポーカーハンドを集めるか、相手をギブアップさせてゲームの続行を断念させることです。',
    'hands_02' => 'ポーカーハンドは常に5枚のカードで構成されます。リスト中のあるカテゴリー内の任意のハンドは、それより下の任意のカテゴリーのどのハンドにも勝つことができます。例えば、任意のストレートフラッシュは、あらゆるフォーカードに勝つことができますし、任意のフラッシュはあらゆるストレートに勝つことができます。ハンドは次の基準に従って評価されます:',
    'hands_royal_flush' => 'ロイヤルフラッシュ',
    'hands_royal_flush_content' => 'エースハイストレートフラッシュは標準的なポーカーハンドで最もランクの高いロイヤルフラッシュとして知られています。',
    'hands_straight_flush' => 'ストレートフラッシュ',
    'hands_straight_flush_content' => '同じスートで構成される5枚の連続したカードです。エースは高くも低くもなることができます。エースハイストレートフラッシュはロイヤルフラッシュと呼ばれ、ポーカーで構成しうる最高のハンドです。',
    'hands_four_of_kind' => 'フォーカード',
    'hands_four_of_kind_content' => '同じランクのカードを4枚集めたハンドです。2人以上のプレーヤーが同じフォーカードを持っていた場合、5枚目のカードであるキッカーによって勝者が決まります。',
    'hands_full_house' => 'フルハウス',
    'hands_full_house_content' => 'スリーカードと1組のペアで構成されます。2人以上のプレーヤーがフルハウスを持っていた場合は、最も強いスリーカードを持っているプレーヤーの勝ちとなります。それらが同じ場合は最も強いペアを持つプレーヤーの勝ちとなります。',
    'hands_flush' => 'フラッシュ',
    'hands_flush_content' => '同じスートの5枚のカードの組み合わせですが、数字は連続しません。2人以上のプレーヤーがフラッシュを持っていれば、フラッシュの中で最も強いカード（必要であれば5番目のカードまで比較）を持つプレーヤーの勝ちになります。',
    'hands_straight' => 'ストレート',
    'hands_straight_content' => '最低2種類の異なるスートからなる連続した5枚のカードで構成されます。5枚のカードでハンドを作る必要があるため、ストレートではキッカーはありません。',
    'hands_three_of_kind_hands' => 'スリーカード',
    'hands_three_of_kind_hands_content' => '同じランクの3枚のカードです。もし2人以上のプレーヤーが同じスリーカードを持っていたら、残り2枚のキッカーで勝者が決まります。',
    'hands_two_pair' => 'ツーペア',
    'hands_two_pair_content' => '同じランクの2枚のカードと別のランクの2枚のカードです。2人以上のプレーヤーがツーペアを持っている場合、最も強いペアによって勝者が決まります。全員同じツーペアの場合は5枚目のカードであるキッカーによって勝者が決まります。',
    'hands_one_pair' => 'ワンペア',
    'hands_one_pair_content' => '同じランクの2枚のカードです。プレーヤーが全員同じペアを持っている場合は、残り3枚のカード（キッカー）のうち最も強いものによって勝者が決まります。',
    'hands_high_card' => 'ハイカード',
    'hands_high_card_content' => 'どのハンドも成立しない、任意の5枚のカードで構成されるポーカーハンドです。プレーヤーが全員同じハイカードを持っている場合は、2番目（およその他）に強いカードによって勝者が決まります。',
    'hands_kicker' => 'キッカー',
    'hands_kicker_content' => 'プレイヤーの5枚のカードを全て使わない任意のハンドで、最も強い残されたカードをキッカーといいます。キッカーは引き分けかどうかを判定するために使います。',
    
    'rounds_preflop' => 'プリフロップ',
    'rounds_preflop_content_1' => "各プレーヤーには2枚の\"ポケット\"カードが配られます。賭けはビッグブラインドの左隣に座っているプレーヤーから始まります。スモールブラインドとビッグブラインドからの強制ベットがあるので、他のプレーヤーはプリフロップではチェックすることはできず、手持ちのハンドを残したままとなります。言い換えれば、ビッグブラインドをコールするかレイズしなければなりません、そうでなければフォールドすることはできます。",
    'rounds_preflop_content_2' =>'プレーヤーは手持ちのカードの強さを見積もって何をするかを決めます（コール/レイズ/フォールド）。賭けのラウンドの終了時にはすべてのベットが共通ポットに追加されます。プレーヤーたちは次のラウンドでそれを争います。',
    'rounds_flop' => 'フロップ',
    'rounds_flop_content' => 'テーブルに表向きにディールされる3枚のコミュニティカードです（フロップ）。テーブルにいるプレーヤー全員がこれらのカードを使って自分の5枚のカードのコンビネーションを作ることができます。フロップでの賭けはディーラーから時計回りに座っている最初のアクティブなプレーヤーから開始します。プレーヤーは状況を把握してアクションをとります。全てのベットは現在のポットに追加されます。',
    'rounds_turn' => 'ターン',
    'rounds_turn_content' => '4番目のカードはターンと呼ばれ、テーブルに置かれます。今プレーヤーは決断するための情報をより多く手にいれ、5枚のカードのハンドを作るためのカードがもう1枚持っている状態です。あと1回のベッティングラウンドはフロップが行われるのと同様です。',
    'rounds_river' => 'リバー',
    'rounds_river_content' => 'リバーは5枚目かつ最後にテーブルに置かれるコミュニティカードです。ベッティングラウンドの後にそれ以上コミュニティカードがディールされないことを除けば、その前の2つのカード（フロップ、ターン）と同様です。プレーヤーは今5枚のカードのハンドを作ることができる最大7枚のカードを持っています。2枚のポケットカードと5枚のコミュニティカードです。最終ベッティングラウンド終了後、残ったプレーヤーはショーダウンへ進みます。',
    'rounds_showdown' => 'ショーダウン',
    'rounds_showdown_content' => '全4回のラウンド終了後に残ったプレーヤーはショーダウン（カードの公開）に参加し、手持ちのコンビネーションを比較します。最高の5枚カードのポーカーハンドを持つプレーヤーがハンドとポットを勝ち取ります。プレーヤーは利用可能な任意の7枚のカードの組み合わせを使うことができ、手持ちのポケットカードを両方あるいは片方を使う必要はありません。',
  
];
